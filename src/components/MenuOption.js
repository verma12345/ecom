import React from 'react'

import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'

const MenuOption = (props) => {
    return (
        <TouchableOpacity
            style={[styles.mainRow, props.containerStyle]}
            onPress={props.onPress}
        >

            <View style={styles.subRow} >
                <View style={styles.iconView} >
                    <Image
                        source={props.icon}
                        style={[{ width: 40, height: 40, resizeMode: 'contain', },props.iconStyle]}
                    />
                </View>
                <Text style={[msStyle.txtStyle, { fontSize: 16, flex:1, paddingHorizontal: 10 }, props.titleStyle]}>
                    {props.title}
                </Text>
            </View>

                <Image
                    source={require('../../assets/drawer/navigate.png')}
                    style={[{ width: 20, height: 20, resizeMode: 'contain',   }]}
                />

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    mainRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
        width: '100%',
    },
    subRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconView: {
        height: 40,
        width: 40,
        borderRadius: 10,
        backgroundColor: Colors.themeColor,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default MenuOption