import React from 'react'

import { ActivityIndicator, PixelRatio, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'

const IsEmptyList = (props) => {
    return (
        <View style={[msStyle.myshadow, {
            height: PixelRatio.getPixelSizeForLayoutSize(63),
            width: '100%',
            borderRadius: 5,
            marginHorizontal: 10,
            marginVertical: 20,
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',

          }]} >
            <ActivityIndicator color={Colors.themeColor} size='large' />
          </View>
    )
}

const styles = StyleSheet.create({
    submit: {
        backgroundColor: Colors.themeColor,
        borderRadius: 10,
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:15
    },
    submitText: {
        textAlign: 'center',
    },
})

export default IsEmptyList