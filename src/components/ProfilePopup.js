import React from 'react'

import { StyleSheet, View, Text, TouchableOpacity, Dimensions, TextInput, Image, Platform, ActivityIndicator, TouchableWithoutFeedback } from "react-native"
import Colors from '../common/Colors'
import Font from '../common/Font'
import { msStyle } from '../common/MyStyle'
import MyButton from './MyButton'
const ProfilePopup = (props) => {
    return (
        <TouchableOpacity
            // onPress={props.onCancel}
            style={[styles.container]}
        >
            <TouchableOpacity style={{
                backgroundColor: 'white', margin: 20,
                padding: 20,
                marginBottom:  100 ,
                borderRadius: 10
            }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                    <Text style={[msStyle.txtStyle]} >
                        {props.title}
                    </Text>

                    <TouchableOpacity onPress={props.onCancel}

                        style={{ padding: 5 }} >
                        <Image source={require('../../assets/cross.png')}
                            style={{ width: 15, height: 15, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity>

                </View>

                {props.image ?
                    <Image source={{ uri: props.image }}
                        style={{ width: '100%', height: 200, resizeMode: 'contain' }}
                    />
                    : <TextInput
                        onChangeText={props.onChangeText}
                        placeholder={props.placeholder}
                        value={props.value}
                        keyboardType={props.keyboardType}
                        maxLength={props.maxLength}

                        editable={props.editable == false ? false : true}
                        style={[{
                            textAlignVertical: 'top',
                            borderWidth: 1, fontSize: 18, marginVertical: 10, borderRadius: 10, borderColor: "gray",
                            fontFamily: Font.SourceSansPro, fontWeight: "700", color: 'black'
                        }, props.inputStyle]}
                    />}

                {props.document ?
                    <Text style={[msStyle.txtStyle]} >
                        {props.document}
                    </Text>
                    : null
                }

                {
                    props.loading ?
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                            <ActivityIndicator
                                size={'small'}
                                color={Colors.themeColor}
                            />
                            <Text style={[msStyle.txtStyle], { marginLeft: 10 }}  >{props.loading_percent == '100 %' ? 'Saving...' : props.loading_percent}</Text>
                        </View>
                        : null
                }

                {/* <View style={{flexDirection:'row',justifyContent:'center',marginTop: 30,}} > */}
                <MyButton
                    title="SAVE"
                    titleStyle={{ color: "white", fontSize: 18 }}
                    containerStyle={{ paddingVertical: 10, marginTop: 20 }}
                    onPress={props.onPress}
                />
                {/* <MyButton
                        title="CANCEL"
                        titleStyle={{ color: "white", fontSize: 18 }}
                        containerStyle={{ paddingVertical: 10,  marginHorizontal: 10,flex:1 }}
                        onPress={props.onCancel}
                    /> */}
                {/* </View> */}
            </TouchableOpacity>
        </TouchableOpacity>
    )
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themeColor,
        borderRadius: 10,
        paddingVertical: 20,
        width, height,
        position: 'absolute',
        backgroundColor: '#0009',
        justifyContent: 'center',
        elevation: 5,
        zIndex: 5
    },
    submitText: {
        textAlign: 'center',
    },
})

export default ProfilePopup