import React from 'react'

import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'

const SocialMediaOption = (props) => {
    return (
        <TouchableOpacity
            style={[styles.mainRow, props.containerStyle]}
            onPress={props.onPress}
        >
            <TouchableOpacity
                style={{ height: 40, width: 40, justifyContent: "center", alignItems: 'center' }}
            >
                <Image
                    source={props.icon}
                    style={{ width: 30, height: 30, resizeMode: 'contain' }}
                />
            </TouchableOpacity>

            <Text style={[msStyle.txtStyle, { fontSize: 12, }, props.titleStyle]}>
                {props.title}
            </Text>

            {props.value ?
                <TouchableOpacity
                    onPress={props.onPress}
                    style={{ height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}
                >
                    <Image
                        source={require('../../assets/edit.png')}
                        style={{ width: 20, height: 20, tintColor: Colors.grayColor }}
                    />
                </TouchableOpacity>
                : <View style={{ height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }} />

            }

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({

    mainRow: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconView: {
        height: 60,
        width: 70,
        borderRadius: 15,
        backgroundColor: Colors.themeColor,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export const SocialMediaOptionMemo = React.memo(SocialMediaOption)