
import React, { useState } from 'react';
import {
  StyleSheet,
  StatusBar,
  SafeAreaView,
  View,
  Dimensions,
  Keyboard,
} from 'react-native';
import QuillEditor, { QuillToolbar } from 'react-native-cn-quill';
import Colors from '../common/Colors';
import MyHeader from './MyHeader';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setIsProfileDescription, setIsQuillEditorVideo1, setProfileDescription } from '../redux_store/actions/indexActionsJaswant';
import { hitUploadVideoOnASW } from '../redux_store/actions/indexActionsApi';

class QuillEditorComponent1More extends React.PureComponent {

  constructor(props) {
    super(props);
    this._editor = React.createRef();
    this._editor1 = null
    this.state = {
      disabled: false,
    
    };
    Keyboard.dismiss()

  }

  onGoBack = () => {
    this.props.navigation.goBack()
  }


  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, }}
      >
        <View style={{ flex: 1, backgroundColor: 'white' }} >

          <MyHeader
            title={this.props.title}
            icon={require('../../assets/back.png')}
            tintColor="white"
            onPress={() => this.onGoBack()}
            containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
            titleStyle={{ color: Colors.whiteText }}
          />

          <QuillEditor
            //  container={CustomContainer} // not required just to show how to pass cusom container

            style={[styles.input, styles.editor]}
            ref={this._editor}
            onSelectionChange={Keyboard.dismiss()}
            onTextChange={Keyboard.dismiss()}

            onFocus={() => Keyboard.dismiss()}
            onPress={() => Keyboard.dismiss()}
            onHtmlChange={({ html }) => this.setState({ html: html })}
            quill={{
              // not required just for to show how to pass this props
              placeholder: 'Type here ...',
              modules: {
                toolbar: false, // this is default value
              },
              theme: 'snow', // this is default value
            }}
            import3rdParties="cdn" // default value is 'local'
            initialHtml={this.props.profileDescription}
          />
        
        </View>

      </SafeAreaView>

    );
  }

}



const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
 
});



const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {

    profileDescription: jaswant.profileDescription,
    isProfileDescription: jaswant.isProfileDescription,
    accessAndSecretsKey: jaswant.accessAndSecretsKey,
    loginResponse: common.loginResponse,
    isQuillEditorVideo: jaswant.isQuillEditorVideo,


  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({


  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(QuillEditorComponent1More)