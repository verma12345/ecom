import React, { Component } from 'react'

import { Dimensions, Image, ImageBackground, PixelRatio, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'



class ProfilePickerComponent extends Component {
    render() {
        return (
            <View
                style={[styles.cintainer, this.props.containerStyle]}
            >

                <Text style={[styles.txt, this.props.titleStyle]}>
                    {this.props.title}
                </Text>



                <View style={{ padding: 10, backgroundColor: "white", elevation: 5, borderRadius: 100 }} >
                    {this.props.url != null ?
                        <ImageBackground
                            source={{ uri: this.props.url }}
                            borderRadius={100}
                            style={{ height: 150, width: 150, resizeMode: 'contain' }}
                        /> :
                        <View style={{ height: 130, width: 130, borderRadius: 360, backgroundColor: Colors.placeHolderColor }} />
                    }


                    <TouchableOpacity
                        onPress={() => this.props.onPress(this.props.index)}
                        style={styles.buttonStyle} >
                        <Image
                            source={this.props.icon}
                            style={{ height: 20, width: 20, tintColor: Colors.themeColor, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity>


                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    cintainer: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red',
        width: Dimensions.get('window').width
    },
    txt: {
        fontSize: 20,
        marginBottom: 20
    },
    buttonStyle: {
        backgroundColor: "white",
        alignSelf: 'flex-end',
        padding: 5,
        position: 'absolute',
        bottom: 5,
        right: 5,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: Colors.themeColor,

    },

})

export default ProfilePickerComponent