
import React, { useState } from 'react';
// import * as ImagePicker from 'expo-image-picker';
import ImageCropPicker from 'react-native-image-crop-picker';
import {
  StyleSheet,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Alert,
  View,
  Text,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
} from 'react-native';
import QuillEditor, { QuillToolbar } from 'react-native-cn-quill';

import {
  SelectionChangeData,
  TextChangeData,
} from 'react-native-cn-quill';
import Colors from '../common/Colors';
import MyHeader from './MyHeader';
import { debugLog } from '../common/Constants';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setIsProfileDescription, setIsQuillEditorVideo1, setProfileDescription, setQuillEditorVideoUrl } from '../redux_store/actions/indexActionsJaswant';
import MyButton from './MyButton';
import { msStyle } from '../common/MyStyle';
import { launchImageLibrary, launchCamera } from 'react-native-image-picker';
import moment from 'moment';
import { RNS3 } from 'react-native-aws3';
import ProfilePopup from './ProfilePopup';
import { hitUploadVideoOnASW } from '../redux_store/actions/indexActionsApi';

class QuillEditorComponent1 extends React.PureComponent {

  constructor(props) {
    super(props);
    this._editor = React.createRef();
    this._editor1 = null
    this.state = {
      disabled: false,
      title: 'react-native-cn-quill',
      currentIndex: 0,
      selectedImage: null,
      changes: '',
      image: null,
      imageUrl: null,
      html: null
    };

  }


  openImagePickerAsync() {
    Alert.alert(
      'Upload Image',
      'Please choose Camera or Gallery',
      [
        {
          text: 'Camera',
          onPress: () => {
            this.openCameraImage1();
          },
        },
        {
          text: 'Gallery',
          onPress: () => {
            this.openGalleryImage1();
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  }

  openCameraImage1 = () => {
    let options = {
      mediaType: 'photo',
    };
    launchCamera(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('You cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      this.setState({ image: response })
    });
  };

  openGalleryImage1 = () => {
    let options = {
      mediaType: 'photo',
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('You cancelled library picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      this.setState({ image: response })
    });
  };

  uploadImageOnASW1 = (filePath) => {
    moment.locale('en');
    var currentTime = moment(new Date().toString()).milliseconds(0);
    var c2 = moment(currentTime).format('YYYYMMDDTHHMMSS') + 'Z'
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "image/png");
    myHeaders.append("X-Amz-Content-Sha256", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
    myHeaders.append("X-Amz-Date", c2);
    myHeaders.append("Connection", 'keep-alive');
    myHeaders.append("Authorization", "AWS4-HMAC-SHA256 Credential=AKIAUKGZKWGYS36AJUNW/20210624/us-east-1/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=ea76eba575264b05e3c30297e9688b7a5bd2cf367ae82dd9d08fa02016be18c0");

    if (Object.keys(filePath).length == 0) {
      alert('Please select image first');
      return;
    }
    RNS3.put(
      {
        // `uri` can also be a file system path (i.e. file://)
        uri: filePath.assets[0].uri,
        name: filePath.assets[0].fileName,
        type: filePath.assets[0].type,
        // uri: filePath.path,
        // type: filePath.mime,
        // name: 'pic.png'
      },
      {
        // keyPrefix: "bht/images/reseller/1010/", // Ex. myuploads/
        keyPrefix: `${this.props.loginResponse.platformId}/images/reseller/${this.props.loginResponse.storeNo}/`, // Ex. myuploads/
        bucket: "ecomnow-images", // Ex. aboutreact
        region: "us-east-1", // Ex. ap-south-1
        accessKey: this.props.accessAndSecretsKey.accessKey,
        secretKey: this.props.accessAndSecretsKey.secretKey,
        headers: myHeaders,
        successActionStatus: 201,
      },
    ).progress((progress) =>
      // this.setState({ uploadSuccessMessage: `Uploading: ${progress.loaded / progress.total} (${progress.percent}%)` },),
      debugLog(progress)

    ).catch(function (error) {
      throw error;
    }).then((response) => {

      if (response.status !== 201)
        alert('Failed to upload image to S3');
      let {
        bucket,
        etag,
        key,
        location
      } = response.body.postResponse;
      debugLog(location)
      this._editor.current?.insertEmbed(
        this.state.currentIndex,
        'image',
        location,
        
      );
      this.setState({ imageUrl: location, image: null })
    });
  }



  openVideoPicker = (index) => {
    let options = {
      mediaType: 'video',
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('You cancelled library picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      this.props.setIsQuillEditorVideo1(response)
    });
  };

  uploadVideoOnASW1 = (video) => {
    let params = {
      video: video,
      accessKey: this.props.accessAndSecretsKey.accessKey,
      secretKey: this.props.accessAndSecretsKey.secretKey,
      platformId: this.props.loginResponse.platformId,
      storeNo: this.props.loginResponse.storeNo
    }
    this.props.hitUploadVideoOnASW(params).then(res => {
      let { bucket, etag, key, location } = res.body.postResponse;
      debugLog(location)
      this._editor.current?.insertEmbed(
        0,
        'video',
        location,
        
      );
      this.props.setIsQuillEditorVideo1(null)
    })
  }

  componentDidMount() {
    this._editor1 = this._editor
    this.setState({
      changes: this.props.profileDescription,
      html: this.props.profileDescription,
    })
  }


  getCurrentDate() {
    let d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  handleEnable = () => {
    const { disabled } = this.state;
    this._editor.current?.enable(disabled);
    this.setState({ disabled: !disabled });
  };

  handleGetHtml = () => {
    this._editor.current?.getHtml().then((res) => {
      console.log('Html :', res);
      Alert.alert(res);
    });
  };
  /**
       * editor height change
       * @param {SelectionChangeData} data
       */
  handleSelectionChange = async (data) => {
    const { range } = data;
    if (range) {
      if (range.length === 0) {
        console.log('User cursor is on', range.index);
        this.currentIndex = range.index;
      } else {
        var text = await this._editor.current?.getText(
          range.index,
          range.length
        );
        console.log('User has highlighted', text);
      }
    } else {
      console.log('Cursor not in the editor');
    }
  };
  /**
       * editor height change
       * @param {TextChangeData} data
       */
  handleTextChange = (data) => {
    if (data.source === 'api') {
      console.log('An API call triggered this change.');
    } else if (data.source === 'user') {
      console.log('A user action triggered this change.');
      // debugLog(this._editor)
    }
  };

  customHandler = (name, value) => {
    console.log(`${name} cliccked with value: ${value}`);
    // this._editor.current?.insertEmbed(
    //   0,
    //   'image',
    //   'https://picsum.photos/200/300'
    // );
    console.log('jhjjjj', this.currentIndex);
    if (name === 'image') {
      this.openImagePickerAsync()
      // this._editor.current?.insertEmbed(
      //   0,
      //   'image',
      //   'https://picsum.photos/200/300'
      // );
    } else if (name === 'link') {
      // insert URL
      this._editor.current?.insertEmbed(
        this.currentIndex,
        'image',
        'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/100px-React-icon.svg.png',
      );
    } else if (name === 'video') {

      this.openVideoPicker()
      // this._editor.current?.insertEmbed(
      //   0,
      //   'video',
      //   'https://mdn.github.io/learning-area/html/multimedia-and-embedding/video-and-audio-content/rabbit320.mp4',
      // );
    } else if (name === 'clock') {
      this._editor.current?.insertText(0, `Today is ${this.getCurrentDate()}`, {
        bold: true,
        color: 'red',
      });
    } else {
      console.log(`${name} clicked with value: ${value}`);
    }

  };

  onGoBack = () => {
    this.props.navigation.goBack()
  }

  onSaveChanges = () => {
    this.props.setProfileDescription(this.state.html)
    this.props.navigation.goBack()
  }

  render() {
    const { title, disabled, currentIndex } = this.state;

    return (

      <SafeAreaView
        style={{ flex: 1, }}
      // onTouchStart={() => this._editor.current?.blur()}
      >
        <View style={{ flex: 1, backgroundColor: 'white' }} >

          <MyHeader
            title={this.props.title}
            icon={require('../../assets/back.png')}
            tintColor="white"
            onPress={() => this.onGoBack()}
            containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
            titleStyle={{ color: Colors.whiteText }}
          />


          <QuillEditor
            //  container={CustomContainer} // not required just to show how to pass cusom container

            style={[styles.input, styles.editor]}
            ref={this._editor}
            onSelectionChange={this.handleSelectionChange}
            onTextChange={this.handleTextChange}
            onHtmlChange={({ html }) => this.setState({ html: html })}

            quill={{
              // not required just for to show how to pass this props
              placeholder: 'Type here ...',
              modules: {
                toolbar: false, // this is default value
              },
              theme: 'snow', // this is default value
            }}
            import3rdParties="cdn" // default value is 'local'
            initialHtml={this.props.profileDescription}
          />
          <View style={styles.buttons}>
            {/* <TouchableOpacity onPress={this.handleEnable} style={styles.btn}>
          <Text>{disabled === true ? 'Enable' : 'Disable'}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleGetHtml} style={styles.btn}>
          <Text>Html</Text>
        </TouchableOpacity> */}
          </View>
          {/* <QuillEditor
          style={styles.editor}
          ref={this._editor}
          initialHtml="<h1>Quill Editor for react-native</h1>"
        /> */}
          {/* <QuillToolbar editor={this._editor} options="full" theme="light" /> */}
          {/* <QuillToolbar
          editor={this._editor}
          options={['image', 'clock']}
          theme="light"
          custom={{
            handler: this.customHandler,
            actions: ['image', 'clock'],
            icons: {
              clock: clockIcon,
            },
          }}
        /> */}

          {
            this.state.changes != this.state.html ?
              <TouchableOpacity
                onPress={() => { this.onSaveChanges() }}
                style={{ backgroundColor: Colors.themeColor, alignSelf: 'flex-end', margin: 10, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 5 }}
              >
                <Text style={[msStyle.txtStyle, { color: Colors.whiteText }]} >{'Save'}</Text>
              </TouchableOpacity> :
              null
          }


          <QuillToolbar
            editor={this._editor}

            options="full"
            //   options={[
            //     ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            //     ['blockquote', 'code-block'],

            //     [{ 'header': 1 }, { 'header': 2 }],               // custom button values
            //     [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            //     [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
            //     [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
            //     [{ 'direction': 'rtl' }],                         // text direction

            //     [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            //     [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

            //     [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            //     [{ 'font': [] }],
            //     [{ 'align': [] }],

            //     ['clean'],                                         // remove formatting button

            //     ['link', 'image', 'video']                         // link and image, video
            // ]}
            theme="light"
            custom={{
              handler: this.customHandler,
              actions: ['link', 'image', 'video'],
              // icons: {
              //   clock: clockIcon,
              // },
            }}
          />


          {
            this.state.image != null ?
              <ProfilePopup
                title='Image'
                image={this.state.image.assets[0].uri}
                onPress={() => {
                  this.uploadImageOnASW1(this.state.image)
                }}
              />
              : null
          }



          {
            this.props.isQuillEditorVideo != null ?
              <ProfilePopup
                title='Video'
                loading={this.props.is_loading}
                image={this.props.isQuillEditorVideo.assets[0].uri}
                onPress={() => {
                  this.uploadVideoOnASW1(this.props.isQuillEditorVideo)
                }}
              />
              : null
          }

        </View>

      </SafeAreaView>

    );
  }

}



const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  root: {
    backgroundColor: Colors.themeColor,
    borderRadius: 10,
    width, height,
    position: 'absolute',
    elevation: 10,
    zIndex: 5
  },
  input: {
    borderColor: 'gray',
    borderWidth: 1,
    marginHorizontal: 30,
    // marginVertical: 5,
    backgroundColor: 'white',
  },

  container: {

    height: 40,
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  otherContainer: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
  },
  topBox: {
    height: 40,
    flexDirection: 'row',
    backgroundColor: 'lightgray',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headline: {
    fontWeight: 'bold',
    fontSize: 18,
    width: 200,
    height: 40,

    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbox: {
    height: 40,
    paddingHorizontal: 20,
  },
  editor: {
    flex: 1,
    padding: 0,
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    alignItems: 'center',
    backgroundColor: '#ddd',
    padding: 10,
    margin: 3,
  },
});



const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {

    profileDescription: jaswant.profileDescription,
    isProfileDescription: jaswant.isProfileDescription,
    accessAndSecretsKey: jaswant.accessAndSecretsKey,
    loginResponse: common.loginResponse,
    isQuillEditorVideo: jaswant.isQuillEditorVideo,


  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({

    setProfileDescription: (txt) => setProfileDescription(txt),
    setIsProfileDescription: (isTrue) => setIsProfileDescription(isTrue),
    hitUploadVideoOnASW: (params) => hitUploadVideoOnASW(params),
    setIsQuillEditorVideo1: (params) => setIsQuillEditorVideo1(params),



  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(QuillEditorComponent1)