import React from 'react'
import { Component } from 'react'

import { StyleSheet, View, Text, TouchableOpacity, Dimensions, TextInput, Image, Platform, FlatList } from "react-native"
import Colors from '../common/Colors'
import Font from '../common/Font'
import { msStyle } from '../common/MyStyle'
import MyButton from './MyButton'
import { debugLog } from '../common/Constants';
import ProductScreen from '../screen/ProductScreen'




class SearchbyPopup extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sharedTo: [],
        }
    }

    renderItem = (item) => {
        return (
            <TouchableOpacity
                style={{ marginHorizontal: 10, margin: 10, justifyContent: 'center', flex: 1, alignItems: 'center' }}
                onPress={() => {
                    this.props.onPress(item.item)
                }}
            >
                <View style={{ justifyContent: 'flex-start', alignItems: 'center' }}>
                    <Text style={{
                        alignSelf: 'flex-start',
                        justifyContent: 'flex-start'
                    }}>{item.item.name}</Text>
                </View>
            </TouchableOpacity >

        )
    }

    render() {
        return (
            <View
                style={[styles.container]

                }        >
                <View style={{
                    backgroundColor: 'white', margin: 20,
                    padding: 20,
                    marginBottom: 0,
                    borderRadius: 10
                }} >
                                                      
                    <Text style={[msStyle.txtStyle]} >
                        {this.props.title}
                    </Text>

                    <FlatList
                        data={this.props.serachTypeArray}
                        // numColumns={4}
                        renderItem={this.renderItem}
                        keyExtractor={item => item.id}
                    />

                    <MyButton
                        title="Cancel"
                        titleStyle={{ color: "white", fontSize: 18 }}
                        containerStyle={{ paddingVertical: 10, marginTop: 30, marginHorizontal: 10 }}
                        onPress={this.props.onCancel}
                    />
                </View>
            </View>
        )
    }
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themeColor,
        borderRadius: 10,
        paddingVertical: 20,
        width, height,
        position: 'absolute',
        backgroundColor: '#0009',
        justifyContent: 'center',
        elevation: 5,
        zIndex: 5
    },
    submitText: {
        textAlign: 'center',
    },
})

export default SearchbyPopup