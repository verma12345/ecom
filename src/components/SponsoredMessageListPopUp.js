import React from 'react'

import { Dimensions, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { debugLog } from '../common/Constants'
import { msStyle } from '../common/MyStyle'

const SponsoredMessageListPopUp = (props) => {

    const renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                onPress={() => props.onSelectMessage(dataItem.index)}
                style={[msStyle.myshadow, {
                    backgroundColor: dataItem.item.selected ? '#1c80c9' : 'white',
                    paddingHorizontal: 10,
                    paddingVertical: 15,
                    marginHorizontal: 15,
                    marginTop: 10,
                    borderRadius: 15,
                    marginBottom: props.data.length - 1 == dataItem.index ? 50 : 0

                }]} >
                <Text style={[msStyle.txtStyle, { fontSize: 18, color: dataItem.item.selected ? 'white' : Colors.textColor, }]}  >
                    {dataItem.item.message}
                </Text>

              
            </TouchableOpacity>
        )
    }

    return (
        <View
            style={[{
                width, height,
                backgroundColor: 'white',
                position: 'absolute',

            }, props.containerStyle]}
            onPress={props.onPress}
        >
            <View style={[msStyle.myshadow, { flex: 1, backgroundColor: 'white', paddingVertical: 10, marginHorizontal:10,marginVertical:30, borderRadius: 20 }]} >
                {/* <View style={{}} > */}

                <FlatList
                    data={props.data}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => 'key' + index}
                />
                {/* </View> */}


                <TouchableOpacity
                    onPress={props.onOk}
                    style={[msStyle.myshadow, { padding: 15, position: 'absolute', bottom: 20, right: 20, backgroundColor: Colors.themeColor, borderRadius: 30 }]}
                >
                    <Text style={[msStyle.txtStyle, { color: 'white', fontSize: 18 }]} >{'OK'} </Text>
                </TouchableOpacity>


            </View>



        </View>
    )
}
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    mainRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
        width: '100%',
    },
    subRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconView: {
        height: 40,
        width: 40,
        borderRadius: 10,
        backgroundColor: Colors.themeColor,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default SponsoredMessageListPopUp