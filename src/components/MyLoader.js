import React from 'react'

import { ActivityIndicator, Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'

const MyLoader = (props) => {
    return (
        <View
            style={[styles.container, props.containerStyle]}
        >
            <View style={{ backgroundColor: 'white', borderRadius: 100, padding: 10 }} >
                <ActivityIndicator
                    size='large'
                    color={Colors.themeColor}
                />
            </View>
            <Text>{props.loading_percent + ' %'} </Text>

        </View>
    )
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        height, width,
        position: 'absolute',
        backgroundColor: '#0005',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 155
    },

})

export default MyLoader