import React from 'react'

import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import WebView from 'react-native-webview'
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'

import striptags from 'striptags'

const ProfileOption = (props) => {

    // const regex = /<\/?[^>]+(>|$)/g;
    const result = striptags(props.message, [], ' ');
const length = result.trim(' ')

    return (
        <View
            style={[styles.mainRow, props.containerStyle]} >
            <Text style={[msStyle.txtStyle, { fontSize: 16, }, props.titleStyle]}>
                {props.title}
            </Text>

            <View
                // onPress={props.onPressWebView}
                style={[styles.subRow, props.inputStyle]} >
                {/* {
                    props.isWeb ?
                        <WebView
                            originWhitelist={['*']}
                            source={{ html: props.message }}
                            scrollEnabled={true}
                            style={{ marginTop: 20, flex: 1 }}
                        />
                        : */}
                <Text numberOfLines={3} style={[
                    msStyle.txtStyle,
                    { fontSize: 16, }, props.titleStyle]}>
                    {/* {props.message != null ? props.message : "Enter Certificate Name"} */}
                    {props.message != null ? length.length != 0? result : 'Click the pencil icon to edit': 'Click the pencil icon to edit'}

                </Text>
                {props.isWeb ? result.length > 135 ?
                    <TouchableOpacity
                        style={{ position: 'absolute', bottom: 5, right: 5 }}
                        onPress={props.onPressReadMore} >
                        <Text style={[msStyle.txtStyle, { fontSize: 15, color: 'black', }, props.titleStyle]}>
                            {'Read More'}
                        </Text>
                    </TouchableOpacity>
                    : null : null}
                {/* } */}


                {props.disable ? null :
                    <TouchableOpacity
                        style={{ position: 'absolute', right: 5, top: 5, backgroundColor: 'white' }}
                        onPress={props.onPencil} >
                        <Image
                            source={require('../../assets/edit.png')}
                            style={{ width: 20, height: 20, tintColor: Colors.grayColor }}
                        />
                    </TouchableOpacity>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainRow: {
        marginTop: 10
    },
    subRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1.5,
        borderColor: Colors.grayColor,
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    iconView: {
        height: 60,
        width: 70,
        borderRadius: 15,
        backgroundColor: Colors.themeColor,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export const ProfileOptionMemo = React.memo(ProfileOption)