import React, { useState, useRef } from 'react';
import { StyleSheet, View, Platform, Text, TouchableOpacity, Image, Dimensions, ActivityIndicator } from 'react-native';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Video from 'react-native-video';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';
import { msStyle } from '../common/MyStyle';


const VideoPlayerComponent = (props) => {

    const video = { uri: props.url }

    const videoPlayer = useRef(null);
    const [duration, setDuration] = useState(0);
    const [paused, setPaused] = useState(true);

    const [currentTime, setCurrentTime] = useState(0);
    const [playerState, setPlayerState] = useState(PLAYER_STATES.PAUSED);
    const [isLoading, setIsLoading] = useState(true);

    const onSeek = (seek) => {
        videoPlayer?.current.seek(seek);
    };

    const onSeeking = (currentVideoTime) => setCurrentTime(currentVideoTime);

    const onPaused = (newState) => {
        setPaused(!paused);
        setPlayerState(newState);
    };

    const onReplay = () => {
        videoPlayer?.current.seek(0);
        setCurrentTime(0);
        setPlayerState(PLAYER_STATES.PLAYING);
        setPaused(false);
    };


    const onProgress = (data) => {
        if (!isLoading) {
            setCurrentTime(data.currentTime);
        }
    };

    const onLoad = (data) => {
        setDuration(Math.round(data.duration));
        setIsLoading(false);
        setPlayerState(PLAYER_STATES.PLAYING);
        setPaused(false);
    };

    const onLoadStart = () => setIsLoading(true);

    const onEnd = () => {
        setPlayerState(PLAYER_STATES.ENDED);
        setCurrentTime(duration);
    };


    return (
        
        <View style={{
            height, width,
            position: 'absolute',
            backgroundColor: 'black',
            elevation:20,
            zIndex:20
        }} >
            <View style={[{
                width: "100%",
                height: "100%",
                backgroundColor: 'black',
                alignItems: 'center',
                justifyContent: 'center'
            }]}>

                <Video
                    ref={(ref) => (videoPlayer.current = ref)}
                    source={{ uri: props.url }}

                    onEnd={onEnd}
                    onLoad={onLoad}
                    onLoadStart={onLoadStart}
                    onProgress={onProgress}

                    paused={paused}

                    posterResizeMode={'cover'}
                    resizeMode={'contain'}
                    ignoreSilentSwitch="ignore"
                    volume={1.0}
                    rate={1.0}

                    style={[styles.backgroundVideo, {
                        width: "100%",
                        height: "100%",
                        backgroundColor: 'black'
                    }]}
                />

                <MediaControls
                    isFullScreen={false}
                    duration={duration}
                    isLoading={isLoading}
                    progress={currentTime}
                    onPaused={onPaused}
                    onReplay={onReplay}
                    onSeek={onSeek}
                    onSeeking={onSeeking}
                    mainColor={Colors.themeColor}
                    playerState={playerState}
                    sliderStyle={{ containerStyle: { marginBottom: 10 }, thumbStyle: {}, trackStyle: {} }}
                >

                    {
                        props.is_end ? null :
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: "100%" }}>


                                <TouchableOpacity
                                    onPress={props.onGoBack}
                                    style={{ width: 50, height: 60, left: -10, justifyContent: 'center', alignItems: 'center' }}
                                >
                                    <Image
                                        source={require('../../assets/back.png')}
                                        style={{ height: 30, width: 30, resizeMode: 'contain' }}
                                    />
                                </TouchableOpacity>
                            </View>
                    }

                </MediaControls>
{/* 
                {
                    props.downloadedVideoSize == 'finished' ?
                        null :
                        <View style={{ flexDirection: 'row', justifyContent: 'center', position: 'absolute', elevation: 10, zIndex: 5, alignItems: 'center' }} >
                            <Text style={[msStyle.txtStyle, { fontSize: 12,marginTop:50, color: 'white' }]} >
                                {props.downloadedVideoSize == 'Saving...' ?
                                    props.downloadedVideoSize :
                                    props.downloadedVideoSize == 'Preparing...' ?
                                        props.downloadedVideoSize :
                                        props.downloadedVideoSize + "/" + props.totalVideoSize}</Text>
                        </View>

                } */}

            </View>
        </View>

    );
};
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    backgroundVideo: {

        // paddingHorizontal: 15,

    },
    mediaControls: {
        height: getWidth(200),
        flex: 1,
        alignSelf: 'center',
    },
});

export default VideoPlayerComponent;