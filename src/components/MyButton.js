import React from 'react'

import { StyleSheet, Text, TouchableOpacity } from "react-native"
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'

const MyButton = (props) => {
    return (
        <TouchableOpacity
            style={[styles.submit,msStyle.myshadow, props.containerStyle]}
            // onPress={() => this.submitSuggestion(this.props)}
            onPress={props.onPress}
            >
            <Text style={[styles.submitText, msStyle.txtStyle, props.titleStyle]}>
                {props.title}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    submit: {
        backgroundColor: Colors.themeColor,
        borderRadius: 10,
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:15
    },
    submitText: {
        textAlign: 'center',
    },
})

export default MyButton