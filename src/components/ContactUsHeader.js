import React from 'react'

import { Image, StyleSheet, Text, TouchableOpacity, ImageBackground, View } from "react-native"

const ContactUsHeader = (props) => {
    return (
        <ImageBackground
            source={require('../../assets/header.png')}
            style={[styles.container, props.containerStyle]}
            imageStyle={{padding:20}}
        >
            <TouchableOpacity
                onPress={props.onPress}
                style={{ }}
            >
                <Image
                    source={props.icon}
                    style={[{ height: 20, width: 20,marginLeft:20, resizeMode: 'contain', tintColor: props.tintColor }, props.iconStyle]}
                />

            </TouchableOpacity>

            <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                <View style={{ width: '75%',padding:20 }}>
                    <Text style={{ fontSize: 22, fontWeight: '700', color: 'white' }}>{'Contact Us'} </Text>
                    <Text style={{ fontSize: 14, fontWeight: '500', color: 'white' }}>{'Please write us your query and we will get back to you soon.'} </Text>
                </View>

                <View style={{ }}>
                    <Image
                        style={{ height: 70, width: 70 }}
                        source={require('../../assets/email.png')}
                    />
                </View>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1.5,
        // alignItems: 'center',
        // height: 50,
        paddingVertical: 15,
        elevation:5

    },

})

export default ContactUsHeader;