import React from 'react'

import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'

const MyHeader = (props) => {
    return (
        <View
            style={[styles.container, props.containerStyle]}
        >
            <TouchableOpacity
                onPress={props.onPress}
                style={{ position: 'absolute', alignSelf: 'baseline', paddingHorizontal: 15 }}
            >
                <Image
                    source={props.icon}
                    style={[{ height: 25, width: 25, resizeMode: 'contain', tintColor: props.tintColor }, props.iconStyle]}
                />

            </TouchableOpacity>

            <Text style={[msStyle.txtStyle, { fontSize: 20 }, props.titleStyle]}>
                {props.title}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // backgroundColor: 'red',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        elevation: 3,
        // backgroundColor: 'white'
    },

})

export default MyHeader