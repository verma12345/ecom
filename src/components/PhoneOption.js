import React from 'react'

import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'

const PhoneOption = (props) => {
    return (
        <View
            style={[styles.mainRow, props.containerStyle]}
            onPress={props.onPress}
        >
            <Text style={[msStyle.txtStyle, { fontSize: 16, }, props.titleStyle]}>
                {props.title}
            </Text>


            <View style={[styles.subRow, props.inputStyle]} >


                <View style={{ flexDirection: "row" }} >
                    <Text style={[msStyle.txtStyle, { fontSize: 14, }, props.titleStyle]}>
                        {props.country_code}
                    </Text>
                    <TouchableOpacity>
                        <Image
                            source={require('../../assets/drop_off.png')}
                            style={{ width: 20, height: 20, marginHorizontal: 10, tintColor: Colors.grayColor }}
                        />
                    </TouchableOpacity>
                    <Text style={[msStyle.txtStyle, { fontSize: 14, }, props.titleStyle]}>
                        {props.message}
                    </Text>
                </View>


                {props.disable ? null :
                    <TouchableOpacity onPress={props.onPencil} >
                        <Image
                            source={require('../../assets/edit.png')}
                            style={{ width: 20, height: 20, tintColor: Colors.grayColor }}
                        />
                    </TouchableOpacity>
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainRow: {
        marginTop: 10
    },
    subRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1.5,
        borderColor: Colors.grayColor,
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    iconView: {
        height: 60,
        width: 70,
        borderRadius: 15,
        backgroundColor: Colors.themeColor,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export const PhoneOptionMemo = React.memo(PhoneOption)