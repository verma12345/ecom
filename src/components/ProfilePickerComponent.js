import React, { Component } from 'react'

import { Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { msStyle } from '../common/MyStyle'



class ProfilePickerComponent extends Component {
    render() {
        return (
            <View
                style={[styles.cintainer, this.props.containerStyle]}
            >
                <View style={{
                    flexDirection: 'row', alignItems: 'center',
                    marginLeft: this.props.isHint ? 20 : 0
                }} >
                    <Text style={[msStyle.txtStyle, this.props.titleStyle]}>
                        {this.props.title}
                    </Text>

                    {this.props.isHint ?
                        <TouchableOpacity onPress={this.props.onHint}
                            style={{ marginHorizontal: 5, borderWidth: 1, borderColor: 'red', padding: 3, borderRadius: 30 }} >
                            <Image
                                style={{ height: 12, width: 12, tintColor: 'red' }}
                                source={require('../../assets/help.png')} />
                        </TouchableOpacity>

                        : null
                    }
                </View>

                <View style={{ padding: 5, backgroundColor: "white", elevation: 5, borderRadius: 100 }} >
                    {this.props.url != null ?
                        <ImageBackground
                            source={{ uri: this.props.url }}
                            borderRadius={100}
                            style={{ height: 70, width: 70, resizeMode: 'contain' }}
                        /> :
                        <View style={{ height: 60, width: 60, borderRadius: 360, backgroundColor: Colors.placeHolderColor }} />
                    }


                    <TouchableOpacity
                        onPress={this.props.onPress}
                        style={styles.buttonStyle} >
                        <Image
                            source={this.props.icon}
                            style={{ height: 20, width: 20, tintColor: Colors.themeColor, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity>


                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    cintainer: {
        flex: 1,
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonStyle: {
        backgroundColor: "white",
        alignSelf: 'flex-end',
        padding: 5,
        position: 'absolute',
        bottom: -5,
        right: -5,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: Colors.themeColor,

    },

})

export default ProfilePickerComponent