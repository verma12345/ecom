import React from 'react'
import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { msStyle } from '../common/MyStyle';
const SearchHeader = (props) => {
    return (
        <View style={[msStyle.myshadow2, { backgroundColor: Colors.backgroundColor2, flexDirection: 'row', height: 60, alignItems: 'center', justifyContent: 'space-between' }]}>
            <TouchableOpacity
                style={{
                    width: 50,
                    paddingLeft: 10,
                    justifyContent: "center"
                }}
                onPress={props.onGoBack}
            >
                <Image
                    source={require('../../assets/back2.png')}
                    resizeMode="contain"
                    style={{
                        width: 30,
                        height: 30,
                        paddingRight: 20
                    }}
                />
            </TouchableOpacity>

            {
                props.isInput ?
                    <TextInput
                        onChangeText={props.onChangeText}
                        placeholder={props.placeholder}
                        value={props.value}
                        keyboardType={props.keyboardType}
                        maxLength={props.maxLength}

                        // editable={props.editable == false ? false : true}
                        style={[styles.inputStyle, props.inputStyle]}
                    />
                    :
                    <Text style={[msStyle.txtStyle, {
                        color: 'black', fontSize: 20,
                        fontStyle: "normal",
                        fontWeight: '500'
                    }]}>{props.title}</Text>
            }

            <TouchableOpacity
                style={{
                    width: 50,
                    paddingRight: 20,
                    justifyContent: "center",
                    alignItems: 'flex-end'
                }}
                onPress={props.onSearch}
            >
                <Image
                    source={props.isInput ? require('../../assets/cancel.png') : require('../../assets/search1.png')}
                    resizeMode="contain"
                    style={{
                        width: props.isInput ? 20 : 30,
                        height: props.isInput ? 20 : 30,
                    }}
                />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    inputStyle: {
        textAlignVertical: 'top',
        borderWidth: 1, flex: 1,
        fontSize: 18,
        marginHorizontal: 10,
        paddingHorizontal: 10,
        marginVertical: 10,
        borderRadius: 10,
        borderColor: "gray",
        fontFamily: Font.SourceSansPro,
        fontWeight: "600",
        color: 'black'
    }
})

export default SearchHeader;