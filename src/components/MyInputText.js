import React from 'react'
import { TouchableOpacity, Text, StyleSheet, View, Image, TextInput } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';

const MyInputText = (props) => {
  return (

    <View style={[ props.style]}>
      <TextInput
        label={props.label}
        // returnKeyType = {"next"}
        color={Colors.textColor}
        placeholder={props.placeholder}
        keyboardType={props.keyboardType}
        secureTextEntry={props.secureTextEntry}
        multiline={props.multiline}
        textAlignVertical={props.textAlignVertical}
        placeholderTextColor={Colors.grayColor}
        activeColor={Colors.textColor}
        onChangeText={props.onChangeText}
        value={props.value}
        autoCapitalize={props.autoCapitalize}
        maxLength={props.maxLength}
        onFocus={props.onFocus}
        style={{ flex: 1, fontSize: 18, fontFamily: Font.SourceSansPro, fontWeight: "300" }}

      />
      <View style={{ height: 1.5, backgroundColor: Colors.placeHolderColor ,flex:1}} />

    </View>

  );
};

const styles = StyleSheet.create({
  mainStyle: {
    height: 71,

  },


  txt: {
    flex: 1,
    fontSize: 16,
  },
  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 10,
  },
});

export default MyInputText;

