import {
  SET_LOADING,
  SET_PLATFORM,
  SET_EMAIL,
  SET_DOMAIN,
  SET_IS_DRAWER,
  SET_LOGIN_RESPONSE,
  SET_PRODUCT_LIST,
  IS_SELECTE_ALL,
  SET_SELECTED_PRODUCT_LIST,
  SET_SPONSORED_VIDEO_LIST,
  SET_SPONSORED_IMAGE_LIST,
  SET_SELECTED_SPONSORED_VIDEO_LIST,
  SET_SELECTED_SPONSORED_IMAGE_LIST,
  SET_SPONSORED_MESSAGE_LIST,
  SET_SELECTED_SPONSORED_MESSAGE_LIST,
  SET_VIDEO_PROGRESS,
  SET_IMAGE_PROGRESS,
  SET_DOCUMENT_PROGRESS,
  SET_REFERESH_GET_PROFILE
} from '../actions/types';

const initialState = {
  is_loading: false,
  is_drawer: false,
  domain: '',
  email: '',
  platform: '',
  loginResponse: null,
  productList: [],
  selectedProductList: [],
  isSelectAll: false,
  sponsoredVideoList: [],

  sponsoredImageList: [],

  sponsoredMessageList: [],

  sponsoredSelectedMessageList: [],

  sponsoredSelectedVideoList: [],
  sponsoredSelectedImageList: [],
  video_progress: '0',
  image_progress: '0',
  document_progress: '0',
  refereshGetProfile: 0,
};

export default function (state = initialState, action) {
  switch (action.type) {
    // -----------------

    // ---------------
    case SET_LOADING:
      return {
        ...state,
        is_loading: action.payload,
      };

    // vendor registration:
    case SET_DOMAIN:
      return {
        ...state,
        domain: action.payload,
      };

    case SET_EMAIL:
      return {
        ...state,
        email: action.payload,
      };

    case SET_PLATFORM:
      return {
        ...state,
        platform: action.payload,
      };

    case SET_IS_DRAWER:
      return {
        ...state,
        is_drawer: action.payload,
      };

    case SET_LOGIN_RESPONSE:
      return {
        ...state,
        loginResponse: action.payload,
      };

    case SET_PRODUCT_LIST:
      return {
        ...state,
        productList: action.payload,
      };

    case IS_SELECTE_ALL:
      return {
        ...state,
        isSelectAll: action.payload,
      };


    case SET_SELECTED_PRODUCT_LIST:
      return {
        ...state,
        selectedProductList: action.payload,
      };


    case SET_SPONSORED_VIDEO_LIST:
      return {
        ...state,
        sponsoredVideoList: action.payload,
      };

    case SET_SELECTED_SPONSORED_VIDEO_LIST:
      return {
        ...state,
        sponsoredSelectedVideoList: action.payload,
      };


    case SET_SPONSORED_IMAGE_LIST:
      return {
        ...state,
        sponsoredImageList: action.payload,
      };

    case SET_SELECTED_SPONSORED_IMAGE_LIST:
      return {
        ...state,
        sponsoredSelectedImageList: action.payload,
      };


    case SET_SPONSORED_MESSAGE_LIST:
      return {
        ...state,
        sponsoredMessageList: action.payload,
      };

    case SET_SELECTED_SPONSORED_MESSAGE_LIST:
      return {
        ...state,
        sponsoredSelectedMessageList: action.payload,
      };

    case SET_VIDEO_PROGRESS:
      return {
        ...state,
        video_progress: action.payload,
      };

    case SET_IMAGE_PROGRESS:
      return {
        ...state,
        image_progress: action.payload,
      };

    case SET_DOCUMENT_PROGRESS:
      return {
        ...state,
        document_progress: action.payload,
      }; 

    case SET_REFERESH_GET_PROFILE:
      return {
        ...state,
        refereshGetProfile: action.payload,
      }; 

    default:
      return state;
  }
}
