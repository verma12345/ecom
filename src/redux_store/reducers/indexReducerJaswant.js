import {

} from '../actions/types';
import { IS_LOGIN_FAILED, IS_MY_MARKETING_DOCUMENT, IS_PROFILE_ABOUT_US, IS_PROFILE_CERTIFICATE_NAME, IS_PROFILE_DESCRIPTION, IS_PROFILE_DESCRIPTION_VIDEO, IS_PROFILE_EMAIL, IS_PROFILE_FIRST_NAME, IS_PROFILE_HEADER, IS_PROFILE_INSTAGRAM, IS_PROFILE_LAST_NAME, IS_PROFILE_LINKEDIN, IS_PROFILE_MESSENGER, IS_PROFILE_PHONE_NUMBER, IS_PROFILE_PINTEREST, IS_PROFILE_POPUP, IS_PROFILE_TIKTOK, IS_PROFILE_TWITTER, IS_PROFILE_WHATSAPP, IS_PROFILE_YOUTUBE, SET_ACCESSKEY_SECRETS_KEY, SET_CUSTOMER_REPORT_LIST, SET_CUSTOMER_TOTAL_COMMITION, SET_CUSTOMER_TOTAL_SALES, SET_DASHBOARD_LIST, SET_DASHBOARD_SALES_INFO, SET_DASHBOARD_TOTAL_COMMITION, SET_DASHBOARD_TOTAL_SALES, SET_FORGOTPASSWORD_URL_MESSAGE, SET_LOGIN_FAILD_MESSAGE, SET_MESSAGE_HISTORY_DETAILS, SET_MESSAGE_HISTORY_LIST, SET_MY_MARKETING_DOCUMENT, SET_MY_MARKETING_DOCUMENT_URL, SET_MY_MARKETING_IMAGE, SET_MY_MARKETING_IMAGE_LIST, SET_MY_MARKETING_INDEX, SET_MY_MARKETING_VIDEO, SET_MY_MARKETING_VIDEO_LIST, SET_PROFILE_ABOUT_US, SET_PROFILE_CERTIFICATE, SET_PROFILE_CERTIFICATE1, SET_PROFILE_CERTIFICATE2, SET_PROFILE_CERTIFICATE3, SET_PROFILE_CERTIFICATE4, SET_PROFILE_CERTIFICATE5, SET_PROFILE_CERTIFICATE6, SET_PROFILE_CERTIFICATE7, SET_PROFILE_CERTIFICATE_INDEX, SET_PROFILE_CERTIFICATE_LIST, SET_PROFILE_CERTIFICATE_NAME, SET_PROFILE_CERTIFICATE_NAME1, SET_PROFILE_CERTIFICATE_NAME2, SET_PROFILE_CERTIFICATE_NAME3, SET_PROFILE_CERTIFICATE_NAME4, SET_PROFILE_CERTIFICATE_NAME5, SET_PROFILE_CERTIFICATE_NAME6, SET_PROFILE_CERTIFICATE_NAME7, SET_PROFILE_DESCRIPTION, SET_PROFILE_DESCRIPTION_VIDEO, SET_PROFILE_EMAIL, SET_PROFILE_FIRST_NAME, SET_PROFILE_HEADER, SET_PROFILE_IMAGE1, SET_PROFILE_IMAGE1_URL, SET_PROFILE_IMAGE2, SET_PROFILE_IMAGE2_URL, SET_PROFILE_INSTAGRAM, SET_PROFILE_INSTAGRAM_ICON, SET_PROFILE_LAST_NAME, SET_PROFILE_LINKEDIN, SET_PROFILE_LINKEDIN_ICON, SET_PROFILE_MESSENGER, SET_PROFILE_MESSENGER_ICON, SET_PROFILE_PHONE_NUMBER, SET_PROFILE_PINTEREST, SET_PROFILE_PINTEREST_ICON, SET_PROFILE_TIKTOK, SET_PROFILE_TIKTOK_ICON, SET_PROFILE_TWITTER, SET_PROFILE_TWITTER_ICON, SET_PROFILE_WHATSAPP, SET_PROFILE_WHATSAPP_ICON, SET_PROFILE_YOUTUBE, SET_PROFILE_YOUTUBE_ICON, SET_RECOMMENDATION_HISTORY, SET_RECOMMENDATION_HISTORY_DETAILS, SET_SEARCH_MESSAGE_HISTORY_DETAILS, SET_SEARCH_MESSAGE_HISTORY_LIST, SET_SEARCH_RECOMMENDATION_HISTORY, SET_SEARCH_RECOMMENDATION_HISTORY_DETAILS, SET_SELECTED_PRODUCT_LIST_FOR_RECOMMEND, SET_SELECTED_VIDEO_URL } from '../actions/typesJaswant';

const initialState = {
  isSaveImage1: false,
  isSaveImage2: false,
  isProfileHeader: false,
  isProfileDescription: false,
  isProfileAboutUs: false,
  isProfileFirstName: false,
  isProfileLastName: false,
  isProfileEmail: false,
  isProfilePhoneNumber: false,


  profileImage1: null,
  profileImage2: null,
  profileImage1Url: null,
  profileImage2Url: null,
  profileHeader: '',
  profileDescription: null,
  profileAboutUs: null,
  profileFirstName: '',
  profileLastName: '',
  profileEmail: '',
  profilePhoneNumber: '',

  profileCertificate: null,
  profileCertificateName: '',
  profileCertificateList: [],
  isProfileCertificateName: false,
  profileCertificateIndex: null,

  is_whatsapp: false,
  is_twitter: false,
  is_messenger: false,
  is_linkedin: false,
  is_instagram: false,
  is_youtube: false,
  is_pinterest: false,
  is_tiktok: false,

  whatsapp: '',
  twitter: '',
  messenger: '',
  linkedin: '',
  instagram: '',
  youtube: '',
  pinterest: '',
  tiktok: '',

  whatsapp_icon: '',
  twitter_icon: '',
  messenger_icon: '',
  linkedin_icon: '',
  instagram_icon: '',
  youtube_icon: '',
  pinterest_icon: '',
  tiktok_icon: '',

  profile_certificate_image1: null,
  profile_certificate_image2: "",
  profile_certificate_image3: "",
  profile_certificate_image4: "",
  profile_certificate_image5: "",
  profile_certificate_image6: "",
  profile_certificate_image7: "",

  profile_certificate_name1: "",
  profile_certificate_name2: "",
  profile_certificate_name3: "",
  profile_certificate_name4: "",
  profile_certificate_name5: "",
  profile_certificate_name6: "",
  profile_certificate_name7: "",


  isProfilePopup: false,
  accessAndSecretsKey: null,
  myMarketingImageList: [{ image_url: null, isSelected: false, }],
  myMarketingVideoList: [{ video_url: null, isSelected: false, }],
  myMarketingImage: null,
  myMarketingVideo: null,
  myMarketingIndex: null,
  myMarketingDocument: null,
  myMarketingDocumentUrl: null,
  isMyMarketingDocument: false,
  selectedVideoUrl: null,

  darshboard_sales_info: [],
  totalSalesAmount: 0.0,
  totalCommitionAmount: 0.0,

  customerReportList: [],
  totalCustomerSalesAmount: 0.0,
  totalCustomerCommitionAmount: 0.0,
  dashboard_list: [],

  quillEditorVideoUrl: null,
  isQuillEditorVideo: null,

  // RECOMMENDATION
  recommendation_history_list: [],
  search_recommendation_history_list: [],

  selected_product_list_for_recommend: [],
  message_history_list: [],
  search_message_history_list: [],

  message_history_details: [],
  search_message_history_details: [],
  recommendation_history_details: [],
  search_recommendation_history_details: [],
  isLoginFailed: false,
  loginFailedMessage: '',
  forgotPassMessageUrl:'',
  
};

export default function (state = initialState, action) {
  switch (action.type) {
    // -----------------

    // ---------------
    case IS_PROFILE_HEADER:
      return {
        ...state,
        isProfileHeader: action.payload,
      };

    case IS_PROFILE_DESCRIPTION:
      return {
        ...state,
        isProfileDescription: action.payload,
      };

    case IS_PROFILE_ABOUT_US:
      return {
        ...state,
        isProfileAboutUs: action.payload,
      };

    case IS_PROFILE_FIRST_NAME:
      return {
        ...state,
        isProfileFirstName: action.payload,
      };

    case IS_PROFILE_LAST_NAME:
      return {
        ...state,
        isProfileLastName: action.payload,
      };

    case IS_PROFILE_EMAIL:
      return {
        ...state,
        isProfileEmail: action.payload,
      };

    case IS_PROFILE_PHONE_NUMBER:
      return {
        ...state,
        isProfilePhoneNumber: action.payload,
      };





    case SET_PROFILE_IMAGE1:
      return {
        ...state,
        profileImage1: action.payload,
      };

    case SET_PROFILE_IMAGE2:
      return {
        ...state,
        profileImage2: action.payload,
      };

    case SET_PROFILE_IMAGE1_URL:
      return {
        ...state,
        profileImage1Url: action.payload,
      };

    case SET_PROFILE_IMAGE2_URL:
      return {
        ...state,
        profileImage2Url: action.payload,
      };


    case SET_PROFILE_HEADER:
      return {
        ...state,
        profileHeader: action.payload,
      };

    case SET_PROFILE_DESCRIPTION:
      return {
        ...state,
        profileDescription: action.payload,
      };

    case SET_PROFILE_ABOUT_US:
      return {
        ...state,
        profileAboutUs: action.payload,
      };

    case SET_PROFILE_FIRST_NAME:
      return {
        ...state,
        profileFirstName: action.payload,
      };

    case SET_PROFILE_LAST_NAME:
      return {
        ...state,
        profileLastName: action.payload,
      };

    case SET_PROFILE_EMAIL:
      return {
        ...state,
        profileEmail: action.payload,
      };

    case SET_PROFILE_PHONE_NUMBER:
      return {
        ...state,
        profilePhoneNumber: action.payload,
      };




    case SET_PROFILE_CERTIFICATE:
      return {
        ...state,
        profileCertificate: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_LIST:
      return {
        ...state,
        profileCertificateList: action.payload,
      };


    case IS_PROFILE_CERTIFICATE_NAME:
      return {
        ...state,
        isProfileCertificateName: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_NAME:
      return {
        ...state,
        profileCertificateName: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_INDEX:
      return {
        ...state,
        profileCertificateIndex: action.payload,
      };

    // social media

    case SET_PROFILE_WHATSAPP:
      return {
        ...state,
        whatsapp: action.payload,
      };


    case SET_PROFILE_TWITTER:
      return {
        ...state,
        twitter: action.payload,
      };


    case SET_PROFILE_MESSENGER:
      return {
        ...state,
        messenger: action.payload,
      };


    case SET_PROFILE_LINKEDIN:
      return {
        ...state,
        linkedin: action.payload,
      };


    case SET_PROFILE_INSTAGRAM:
      return {
        ...state,
        instagram: action.payload,
      };


    case SET_PROFILE_YOUTUBE:
      return {
        ...state,
        youtube: action.payload,
      };


    case SET_PROFILE_PINTEREST:
      return {
        ...state,
        pinterest: action.payload,
      };


    case SET_PROFILE_TIKTOK:
      return {
        ...state,
        tiktok: action.payload,
      };

    // Social media boolean

    case IS_PROFILE_WHATSAPP:
      return {
        ...state,
        is_whatsapp: action.payload,
      };

    case IS_PROFILE_TWITTER:
      return {
        ...state,
        is_twitter: action.payload,
      };

    case IS_PROFILE_MESSENGER:
      return {
        ...state,
        is_messenger: action.payload,
      };

    case IS_PROFILE_LINKEDIN:
      return {
        ...state,
        is_linkedin: action.payload,
      };

    case IS_PROFILE_INSTAGRAM:
      return {
        ...state,
        is_instagram: action.payload,
      };

    case IS_PROFILE_YOUTUBE:
      return {
        ...state,
        is_youtube: action.payload,
      };

    case IS_PROFILE_PINTEREST:
      return {
        ...state,
        is_pinterest: action.payload,
      };

    case IS_PROFILE_TIKTOK:
      return {
        ...state,
        is_tiktok: action.payload,
      };

    // sm_icons


    case SET_PROFILE_WHATSAPP_ICON:
      return {
        ...state,
        whatsapp_icon: action.payload,
      };


    case SET_PROFILE_TWITTER_ICON:
      return {
        ...state,
        twitter_icon: action.payload,
      };


    case SET_PROFILE_MESSENGER_ICON:
      return {
        ...state,
        messenger_icon: action.payload,
      };


    case SET_PROFILE_LINKEDIN_ICON:
      return {
        ...state,
        linkedin_icon: action.payload,
      };


    case SET_PROFILE_INSTAGRAM_ICON:
      return {
        ...state,
        instagram_icon: action.payload,
      };


    case SET_PROFILE_YOUTUBE_ICON:
      return {
        ...state,
        youtube_icon: action.payload,
      };


    case SET_PROFILE_PINTEREST_ICON:
      return {
        ...state,
        pinterest_icon: action.payload,
      };


    case SET_PROFILE_TIKTOK_ICON:
      return {
        ...state,
        tiktok_icon: action.payload,
      };
    // certificate

    case SET_PROFILE_CERTIFICATE1:
      return {
        ...state,
        profile_certificate_image1: action.payload,
      };

    case SET_PROFILE_CERTIFICATE2:
      return {
        ...state,
        profile_certificate_image2: action.payload,
      };

    case SET_PROFILE_CERTIFICATE3:
      return {
        ...state,
        profile_certificate_image3: action.payload,
      };

    case SET_PROFILE_CERTIFICATE4:
      return {
        ...state,
        profile_certificate_image4: action.payload,
      };

    case SET_PROFILE_CERTIFICATE5:
      return {
        ...state,
        profile_certificate_image5: action.payload,
      };

    case SET_PROFILE_CERTIFICATE6:
      return {
        ...state,
        profile_certificate_image6: action.payload,
      };

    case SET_PROFILE_CERTIFICATE7:
      return {
        ...state,
        profile_certificate_image7: action.payload,
      };

    // certificate name

    case SET_PROFILE_CERTIFICATE_NAME1:
      return {
        ...state,
        profile_certificate_name1: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_NAME2:
      return {
        ...state,
        profile_certificate_name2: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_NAME3:
      return {
        ...state,
        profile_certificate_name3: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_NAME4:
      return {
        ...state,
        profile_certificate_name4: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_NAME5:
      return {
        ...state,
        profile_certificate_name5: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_NAME6:
      return {
        ...state,
        profile_certificate_name6: action.payload,
      };

    case SET_PROFILE_CERTIFICATE_NAME7:
      return {
        ...state,
        profile_certificate_name7: action.payload,
      };

    case SET_ACCESSKEY_SECRETS_KEY:
      return {
        ...state,
        accessAndSecretsKey: action.payload,
      };

    case SET_MY_MARKETING_IMAGE_LIST:
      return {
        ...state,
        myMarketingImageList: action.payload,
      };

    case SET_MY_MARKETING_VIDEO_LIST:
      return {
        ...state,
        myMarketingVideoList: action.payload,
      };

    case SET_MY_MARKETING_IMAGE:
      return {
        ...state,
        myMarketingImage: action.payload,
      };

    case SET_MY_MARKETING_VIDEO:
      return {
        ...state,
        myMarketingVideo: action.payload,
      };


    case SET_MY_MARKETING_INDEX:
      return {
        ...state,
        myMarketingIndex: action.payload,
      };

    case SET_MY_MARKETING_DOCUMENT:
      return {
        ...state,
        myMarketingDocument: action.payload,
      };

    case IS_MY_MARKETING_DOCUMENT:
      return {
        ...state,
        isMyMarketingDocument: action.payload,
      };

    case SET_MY_MARKETING_DOCUMENT_URL:
      return {
        ...state,
        myMarketingDocumentUrl: action.payload,
      };


    case SET_SELECTED_VIDEO_URL:
      return {
        ...state,
        selectedVideoUrl: action.payload,
      };


    case SET_DASHBOARD_SALES_INFO:
      return {
        ...state,
        darshboard_sales_info: action.payload,
      };

    case SET_DASHBOARD_TOTAL_SALES:
      return {
        ...state,
        totalSalesAmount: action.payload,
      };


    case SET_DASHBOARD_TOTAL_COMMITION:
      return {
        ...state,
        totalCommitionAmount: action.payload,
      };

    case SET_CUSTOMER_REPORT_LIST:
      return {
        ...state,
        customerReportList: action.payload,
      };

    case SET_CUSTOMER_TOTAL_SALES:
      return {
        ...state,
        totalCustomerSalesAmount: action.payload,
      };

    case SET_CUSTOMER_TOTAL_COMMITION:
      return {
        ...state,
        totalCustomerCommitionAmount: action.payload,
      };

    case SET_DASHBOARD_LIST:
      return {
        ...state,
        dashboard_list: action.payload,
      };

    case SET_PROFILE_DESCRIPTION_VIDEO:
      return {
        ...state,
        quillEditorVideoUrl: action.payload,
      };

    case IS_PROFILE_DESCRIPTION_VIDEO:
      return {
        ...state,
        isQuillEditorVideo: action.payload,
      };


    case SET_RECOMMENDATION_HISTORY:
      return {
        ...state,
        recommendation_history_list: action.payload,
      };

    case SET_SEARCH_RECOMMENDATION_HISTORY:
      return {
        ...state,
        search_recommendation_history_list: action.payload,
      };

    case SET_SELECTED_PRODUCT_LIST_FOR_RECOMMEND:
      return {
        ...state,
        selected_product_list_for_recommend: action.payload,
      };

    case SET_MESSAGE_HISTORY_LIST:
      return {
        ...state,
        message_history_list: action.payload,
      };

    case SET_SEARCH_MESSAGE_HISTORY_LIST:
      return {
        ...state,
        search_message_history_list: action.payload,
      };



    case SET_MESSAGE_HISTORY_DETAILS:
      return {
        ...state,
        message_history_details: action.payload,
      };

    case SET_SEARCH_MESSAGE_HISTORY_DETAILS:
      return {
        ...state,
        search_message_history_details: action.payload,
      };

    case SET_RECOMMENDATION_HISTORY_DETAILS:
      return {
        ...state,
        recommendation_history_details: action.payload,
      };

    case SET_SEARCH_RECOMMENDATION_HISTORY_DETAILS:
      return {
        ...state,
        search_recommendation_history_details: action.payload,
      };

    case IS_LOGIN_FAILED:
      return {
        ...state,
        isLoginFailed: action.payload,
      };

    case SET_LOGIN_FAILD_MESSAGE:
      return {
        ...state,
        loginFailedMessage: action.payload,
      };

    case SET_FORGOTPASSWORD_URL_MESSAGE:
      return {
        ...state,
        forgotPassMessageUrl: action.payload,
      };

    default:
      return state;
  }
}
