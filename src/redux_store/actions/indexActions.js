import { debugLog } from '../../common/Constants';
import store from '../store';
import {
  IS_SELECTE_ALL,
  SET_DOCUMENT_PROGRESS,
  SET_DOMAIN, SET_EMAIL, SET_IMAGE_PROGRESS, SET_IS_DRAWER, SET_LOADING, SET_LOGIN_RESPONSE, SET_PLATFORM, SET_PRODUCT_LIST, SET_REFERESH_GET_PROFILE, SET_SELECTED_PRODUCT_LIST, SET_SELECTED_SPONSORED_IMAGE_LIST, SET_SELECTED_SPONSORED_MESSAGE_LIST, SET_SELECTED_SPONSORED_VIDEO_LIST, SET_SPONSORED_IMAGE_LIST, SET_SPONSORED_MESSAGE_LIST, SET_SPONSORED_VIDEO_LIST, SET_VIDEO_PROGRESS,
} from './types';


export const setLoading = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_LOADING,
    payload: isTrue,
  });
};

export const setDomain = (domain) => (dispatch) => {
  let domain1 = domain.toLowerCase()

  dispatch({
    type: SET_DOMAIN,
    payload: domain1,
  });
};


export const setEmail = (email) => (dispatch) => {
  let email1 = email.toLowerCase()
  dispatch({
    type: SET_EMAIL,
    payload: email1,
  });
};

export const setPlatform = (platform) => (dispatch) => {
  dispatch({
    type: SET_PLATFORM,
    payload: platform,
  });
};

export const setIsDrawer = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_IS_DRAWER,
    payload: isTrue,
  });
};

export const setLoginRespons = (res) => (dispatch) => {

  dispatch({
    type: SET_LOGIN_RESPONSE,
    payload: res,
  });
};


export const setProductList = (list) => (dispatch) => {

  let newdata = []
  list.map((item, index) => {
    newdata.push({
      ...item,
      selected: false
    })
  })

  dispatch({
    type: SET_PRODUCT_LIST,
    payload: newdata,
  });
};


export const setUpdateProductList = (selectedIndex) => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.productList;
  let newdata = []
  data.map((item, index) => {
    if (selectedIndex == index) {
      newdata.push({
        ...item,
        selected: !item.selected
      })
    } else {
      newdata.push({
        ...item,
        selected: item.selected
      })
    }
  })

  dispatch({
    type: SET_PRODUCT_LIST,
    payload: newdata,
  });
};


export const setUpdateSelectAllProductList = (isTrue) => (dispatch) => {
  const state = store.getState().indexReducer

  dispatch({
    type: IS_SELECTE_ALL,
    payload: isTrue,
  });

  let data = state.productList;
  let newdata = []
  debugLog(data)
  data.map((item, index) => {
    if (isTrue) {
      newdata.push({
        ...item,
        selected: true
      })
    } else {
      newdata.push({
        ...item,
        selected: false
      })
    }
  })

  dispatch({
    type: SET_PRODUCT_LIST,
    payload: newdata,
  });
};


export const setSelectedProductList = () => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.productList;
  let newdata = []

  data.map((item, index) => {
    if (item.selected == true) {
      newdata.push({ item })
    }
  })

  dispatch({
    type: SET_SELECTED_PRODUCT_LIST,
    payload: newdata,
  });
  return newdata;
};


// Sponsored Video  ##################################################################################################
export const setSponsoredVideoList = (list) => (dispatch) => {
  let newdata = []
  if (list.length < 1) return

  list.map((item, index) => {
    if (item.Key.includes('.mp4')) {
      newdata.push({
        ...item,
        selected: false
      })
    }
  })

  dispatch({
    type: SET_SPONSORED_VIDEO_LIST,
    payload: newdata,
  });
};



export const setUpdateSelectAllSponsoredVideoList = (isTrue) => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredVideoList;
  let newdata = []
  debugLog(data)
  data.map((item, index) => {
      newdata.push({
        ...item,
        selected: isTrue
      })
  })

  dispatch({
    type: SET_SPONSORED_VIDEO_LIST,
    payload: newdata,
  });
};



export const setUpdateSponsoredVideoList = (selectedIndex) => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredVideoList;
  let newdata = []
  data.map((item, index) => {
    if (selectedIndex == index) {
      newdata.push({
        ...item,
        selected: !item.selected
      })
    } else {
      newdata.push({
        ...item,
        selected: item.selected
      })
    }
  })

  debugLog(newdata)
  dispatch({
    type: SET_SPONSORED_VIDEO_LIST,
    payload: newdata,
  });
};


export const setUpdateSponsoredVideoListURL = (url, selectedIndex) => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredVideoList;
  let newdata = []
  data.map((item, index) => {
    if (selectedIndex == index) {
      newdata.push({
        ...item,
        path: url
      })
    } else {
      newdata.push({
        ...item
      })
    }
  })

  debugLog(newdata)
  dispatch({
    type: SET_SPONSORED_VIDEO_LIST,
    payload: newdata,
  });
};


export const setSelectedSponsoredVideoList = () => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredVideoList;
  let newdata = []

  data.map((item, index) => {
    if (item.selected == true) {
      newdata.push({ ...item })
    }
  })

  dispatch({
    type: SET_SELECTED_SPONSORED_VIDEO_LIST,
    payload: newdata,
  });
  return newdata;
};


// #####################################################################################





// Sponsored Image  ##################################################################################################
export const setSponsoredImage_List = (list) => (dispatch) => {
  let newdata = []
  list.map((item, index) => {
    if (index != 0) {
      newdata.push({
        ...item,
        selected: false
      })
    }
  })

  dispatch({
    type: SET_SPONSORED_IMAGE_LIST,
    payload: newdata,
  });
};




export const setUpdateSelectAllSponsoredImage_List = (isTrue) => (dispatch) => {
  const state = store.getState().indexReducer

  // dispatch({
  //   type: IS_SELECTE_ALL,
  //   payload: isTrue,
  // });

  let data = state.sponsoredImageList;
  let newdata = []
  debugLog(data)
  data.map((item, index) => {
    // if (isTrue) {
    newdata.push({
      ...item,
      selected: isTrue
    })
    // } else {
    //   newdata.push({
    //     ...item,
    //     selected: false
    //   })
    // }
  })

  dispatch({
    type: SET_SPONSORED_IMAGE_LIST,
    payload: newdata,
  });
};



export const setUpdateSponsoredImage_List = (selectedIndex) => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredImageList;
  let newdata = []
  data.map((item, index) => {
    if (selectedIndex == index) {
      newdata.push({
        ...item,
        selected: !item.selected
      })
    } else {
      newdata.push({
        ...item,
        selected: item.selected
      })
    }
  })

  dispatch({
    type: SET_SPONSORED_IMAGE_LIST,
    payload: newdata,
  });
};


export const setUpdateSponsoredImage_ListURL = (list) => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredImageList;
  let newdata = []
  debugLog('Action')
  debugLog(list)
  list.map((item, index) => {
    if (item.name.includes('sp_img')) {
      newdata.push({
        ...item,
        uri: item.path
      })
    }
  })
  debugLog(newdata)
  dispatch({
    type: SET_SPONSORED_IMAGE_LIST,
    payload: newdata,
  });
};

export const setSelectedSponsoredImage_List = () => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredImageList;
  let newdata = []

  data.map((item, index) => {
    if (item.selected == true) {
      newdata.push({ ...item })
    }
  })

  dispatch({
    type: SET_SELECTED_SPONSORED_IMAGE_LIST,
    payload: newdata,
  });
  return newdata;
};
// #####################################################################################



// Sponsored Message ##################################################################################################

export const setSponsoredMessage_List = (list) => (dispatch) => {

  let newdata = []
  list.map((item, index) => {
    if (index != 0) {
      newdata.push({
        ...item,
        selected: false
      })
    }
  })

  dispatch({
    type: SET_SPONSORED_MESSAGE_LIST,
    payload: newdata,
  });
};




export const setUpdateSelectAllSponsoredMessage_List = (isTrue) => (dispatch) => {
  const state = store.getState().indexReducer

  // dispatch({
  //   type: IS_SELECTE_ALL,
  //   payload: isTrue,
  // });

  let data = state.sponsoredMessageList;
  let newdata = []
  debugLog(data)
  data.map((item, index) => {
    if (isTrue) {
      newdata.push({
        ...item,
        selected: true
      })
    } else {
      newdata.push({
        ...item,
        selected: false
      })
    }
  })

  dispatch({
    type: SET_SPONSORED_MESSAGE_LIST,
    payload: newdata,
  });
};



export const setUpdateSponsoredMessage_List = (selectedIndex) => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredMessageList;
  let newdata = []
  debugLog(selectedIndex)
  data.map((item, index) => {
    if (selectedIndex == index) {
      newdata.push({
        ...item,
        selected: !item.selected
      })
    } else {
      newdata.push({
        ...item,
        selected: item.selected
      })
    }
  })
  debugLog(newdata)
  dispatch({
    type: SET_SPONSORED_MESSAGE_LIST,
    payload: newdata,
  });
};


export const setSelectedSponsoredMessage_List = () => (dispatch) => {
  const state = store.getState().indexReducer
  let data = state.sponsoredMessageList;
  let newdata = []

  data.map((item, index) => {
    if (item.selected == true) {
      newdata.push({ ...item })
    }
  })

  dispatch({
    type: SET_SELECTED_SPONSORED_MESSAGE_LIST,
    payload: newdata,
  });
  return newdata;
};


export const setVideoProgress = (pr) => (dispatch) => {
  dispatch({
    type: SET_VIDEO_PROGRESS,
    payload: pr,
  });
};


export const setImageProgress = (pr) => (dispatch) => {
  dispatch({
    type: SET_IMAGE_PROGRESS,
    payload: pr,
  });
};


export const setDocumentProgress = (pr) => (dispatch) => {
  dispatch({
    type: SET_DOCUMENT_PROGRESS,
    payload: pr,
  });
};

export const setRefereshGetProfile = (referesh) => (dispatch) => {
  dispatch({
    type: SET_REFERESH_GET_PROFILE,
    payload: referesh,
  });
};