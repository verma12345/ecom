import { debugLog } from "../../common/Constants";
import store from "../store";
import {
  IS_LOGIN_FAILED,
  IS_MY_MARKETING_DOCUMENT,
  IS_PROFILE_ABOUT_US,
  IS_PROFILE_CERTIFICATE_NAME,
  IS_PROFILE_DESCRIPTION,
  IS_PROFILE_DESCRIPTION_VIDEO,
  IS_PROFILE_EMAIL,
  IS_PROFILE_FIRST_NAME,
  IS_PROFILE_HEADER,
  IS_PROFILE_INSTAGRAM,
  IS_PROFILE_LAST_NAME,
  IS_PROFILE_LINKEDIN,
  IS_PROFILE_MESSENGER,
  IS_PROFILE_PHONE_NUMBER,
  IS_PROFILE_PINTEREST,
  IS_PROFILE_POPUP,
  IS_PROFILE_TIKTOK,
  IS_PROFILE_TWITTER,
  IS_PROFILE_WHATSAPP,
  IS_PROFILE_YOUTUBE,
  IS_SAVE_IMAGE1,
  IS_SAVE_IMAGE2,
  SET_ACCESSKEY_SECRETS_KEY,
  SET_CUSTOMER_REPORT_LIST,
  SET_CUSTOMER_TOTAL_COMMITION,
  SET_CUSTOMER_TOTAL_SALES,
  SET_DASHBOARD_LIST,
  SET_DASHBOARD_SALES_INFO,
  SET_DASHBOARD_TOTAL_COMMITION,
  SET_DASHBOARD_TOTAL_SALES,
  SET_FORGOTPASSWORD_URL_MESSAGE,
  SET_LOGIN_FAILD_MESSAGE,
  SET_MESSAGE_HISTORY_DETAILS,
  SET_MESSAGE_HISTORY_LIST,
  SET_MY_MARKETING_DOCUMENT,
  SET_MY_MARKETING_DOCUMENT_URL,
  SET_MY_MARKETING_IMAGE,
  SET_MY_MARKETING_IMAGE_LIST,
  SET_MY_MARKETING_INDEX,
  SET_MY_MARKETING_VIDEO,
  SET_MY_MARKETING_VIDEO_LIST,
  SET_PROFILE_ABOUT_US,
  SET_PROFILE_CERTIFICATE,
  SET_PROFILE_CERTIFICATE1,
  SET_PROFILE_CERTIFICATE2,
  SET_PROFILE_CERTIFICATE3,
  SET_PROFILE_CERTIFICATE4,
  SET_PROFILE_CERTIFICATE5,
  SET_PROFILE_CERTIFICATE6,
  SET_PROFILE_CERTIFICATE7,
  SET_PROFILE_CERTIFICATE_INDEX,
  SET_PROFILE_CERTIFICATE_LIST,
  SET_PROFILE_CERTIFICATE_NAME,
  SET_PROFILE_CERTIFICATE_NAME1,
  SET_PROFILE_CERTIFICATE_NAME2,
  SET_PROFILE_CERTIFICATE_NAME3,
  SET_PROFILE_CERTIFICATE_NAME4,
  SET_PROFILE_CERTIFICATE_NAME5,
  SET_PROFILE_CERTIFICATE_NAME6,
  SET_PROFILE_CERTIFICATE_NAME7,
  SET_PROFILE_DESCRIPTION,
  SET_PROFILE_DESCRIPTION_VIDEO,
  SET_PROFILE_EMAIL,
  SET_PROFILE_FIRST_NAME,
  SET_PROFILE_HEADER,
  SET_PROFILE_IMAGE1,
  SET_PROFILE_IMAGE1_URL,
  SET_PROFILE_IMAGE2,
  SET_PROFILE_IMAGE2_URL,
  SET_PROFILE_INSTAGRAM,
  SET_PROFILE_INSTAGRAM_ICON,
  SET_PROFILE_LAST_NAME,
  SET_PROFILE_LINKEDIN,
  SET_PROFILE_LINKEDIN_ICON,
  SET_PROFILE_MESSENGER,
  SET_PROFILE_MESSENGER_ICON,
  SET_PROFILE_PHONE_NUMBER,
  SET_PROFILE_PINTEREST,
  SET_PROFILE_PINTEREST_ICON,
  SET_PROFILE_TIKTOK,
  SET_PROFILE_TIKTOK_ICON,
  SET_PROFILE_TWITTER,
  SET_PROFILE_TWITTER_ICON,
  SET_PROFILE_WHATSAPP,
  SET_PROFILE_WHATSAPP_ICON,
  SET_PROFILE_YOUTUBE,
  SET_PROFILE_YOUTUBE_ICON,
  SET_RECOMMENDATION_HISTORY,
  SET_RECOMMENDATION_HISTORY_DETAILS,
  SET_SEARCH_MESSAGE_HISTORY_DETAILS,
  SET_SEARCH_MESSAGE_HISTORY_LIST,
  SET_SEARCH_RECOMMENDATION_HISTORY,
  SET_SEARCH_RECOMMENDATION_HISTORY_DETAILS,
  SET_SELECTED_PRODUCT_LIST_FOR_RECOMMEND,
  SET_SELECTED_VIDEO_URL
} from "./typesJaswant";


export const setIsProfileHeader = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_HEADER,
    payload: isTrue,
  });
};

export const setIsProfileDescription = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_DESCRIPTION,
    payload: isTrue,
  });
};

export const setIsProfileAboutUs = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_ABOUT_US,
    payload: isTrue,
  });
};

export const setIsProfileFirstName = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_FIRST_NAME,
    payload: isTrue,
  });
};

export const setIsProfileLastName = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_LAST_NAME,
    payload: isTrue,
  });
};

export const setIsProfileEmail = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_EMAIL,
    payload: isTrue,
  });
};

export const setIsProfilePhoneNumber = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_PHONE_NUMBER,
    payload: isTrue,
  });
};



export const setIsSaveImage1 = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_SAVE_IMAGE1,
    payload: isTrue,
  });
};

export const setIsSaveImage2 = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_SAVE_IMAGE2,
    payload: isTrue,
  });
};


export const setProfileImage1 = (img1) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_IMAGE1,
    payload: img1,
  });

};

export const setProfileImage2 = (img2) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_IMAGE2,
    payload: img2,
  });

};

export const setProfileImage1Url = (url) => (dispatch) => {

  dispatch({
    type: SET_PROFILE_IMAGE1_URL,
    payload: url,
  });
};

export const setProfileImage2Url = (url) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_IMAGE2_URL,
    payload: url,
  });
};




export const setProfileHeader = (txt) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_HEADER,
    payload: txt,
  });
};

export const setProfileDescription = (txt) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_DESCRIPTION,
    payload: txt,
  });
};

export const setProfileAboutUs = (txt) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_ABOUT_US,
    payload: txt,
  });
};

export const setProfileFirstName = (txt) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_FIRST_NAME,
    payload: txt,
  });
};

export const setProfileLastName = (txt) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_LAST_NAME,
    payload: txt,
  });
};

export const setProfileEmail = (txt) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_EMAIL,
    payload: txt,
  });
};

export const setProfilePhoneNumber = (txt) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_PHONE_NUMBER,
    payload: txt,
  });
};


export const setProfileCertificate = (certificate) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE,
    payload: certificate,
  });
};



export const setIsProfileCertificateName = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_CERTIFICATE_NAME,
    payload: isTrue,
  });
};

export const setProfileCertificateName = (name) => (dispatch) => {

  dispatch({
    type: SET_PROFILE_CERTIFICATE_NAME,
    payload: name,
  });
};



export const setProfileCertificateEmptyList = (isEmpty) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;

  let empty_data = { cert_name: 'Enter Certificate Name', cert_icon_name: '' };
  let data = state.profileCertificateList;
  if (isEmpty) {
    data = [];
  }
  let neData = [...data, empty_data];
  dispatch({
    type: SET_PROFILE_CERTIFICATE_LIST,
    payload: neData,
  });
};

export const setProfileCertificateList = (list) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_LIST,
    payload: list,
  });
};

export const setProfileCertificateListName = (name, index1) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.profileCertificateList;
  let newData = [];

  data.map((item, index) => {
    if (index == index1) {
      newData.push({
        cert_name: name,
        cert_icon_name: item.cert_icon_name
      })
    } else {
      newData.push({
        cert_name: item.cert_name,
        cert_icon_name: item.cert_icon_name
      })
    }
  })

  dispatch({
    type: SET_PROFILE_CERTIFICATE_LIST,
    payload: newData,
  });
};


export const setProfileCertificateListImage = (image, index1) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.profileCertificateList;
  debugLog(image + " index " + index1)
  let newData = [];

  data.map((item, index) => {
    if (index == index1) {
      newData.push({
        cert_name: item.cert_name,
        cert_icon_name: image
      })
    } else {
      newData.push({
        cert_name: item.cert_name,
        cert_icon_name: item.cert_icon_name
      })
    }
  })

  dispatch({
    type: SET_PROFILE_CERTIFICATE_LIST,
    payload: newData,
  });
};

export const setProfileCertificateIndex = (index) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_INDEX,
    payload: index,
  });
};



// social media



export const setProfileWhatsapp = (number) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_WHATSAPP,
    payload: number,
  });
};


export const setProfileTwitter = (link) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_TWITTER,
    payload: link,
  });
};

export const setProfileMessenger = (link) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_MESSENGER,
    payload: link,
  });
};

export const setProfileLinkedin = (link) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_LINKEDIN,
    payload: link,
  });
};

export const setProfileInstagram = (link) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_INSTAGRAM,
    payload: link,
  });
};

export const setProfileYoutube = (link) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_YOUTUBE,
    payload: link,
  });
};

export const setProfilePinterest = (link) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_PINTEREST,
    payload: link,
  });
};

export const setProfileTiktok = (link) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_TIKTOK,
    payload: link,
  });
};


// sm_icon


export const setProfileWhatsappIcon = (icon) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_WHATSAPP_ICON,
    payload: icon,
  });
};


export const setProfileTwitterIcon = (icon) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_TWITTER_ICON,
    payload: icon,
  });
};

export const setProfileMessengerIcon = (icon) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_MESSENGER_ICON,
    payload: icon,
  });
};

export const setProfileLinkedinIcon = (icon) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_LINKEDIN_ICON,
    payload: icon,
  });
};

export const setProfileInstagramIcon = (icon) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_INSTAGRAM_ICON,
    payload: icon,
  });
};

export const setProfileYoutubeIcon = (icon) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_YOUTUBE_ICON,
    payload: icon,
  });
};

export const setProfilePinterestIcon = (icon) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_PINTEREST_ICON,
    payload: icon,
  });
};

export const setProfileTiktokIcon = (icon) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_TIKTOK_ICON,
    payload: icon,
  });
};


export const setIsProfilePopUp = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_POPUP,
    payload: isTrue,
  });
};

// social boolean


export const setIsProfileWhatsapp = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_WHATSAPP,
    payload: isTrue,
  });
};

export const setIsProfileTwitter = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_TWITTER,
    payload: isTrue,
  });
};

export const setIsProfileMessenger = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_MESSENGER,
    payload: isTrue,
  });
};

export const setIsProfileLinkedin = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_LINKEDIN,
    payload: isTrue,
  });
};

export const setIsProfileInstagram = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_INSTAGRAM,
    payload: isTrue,
  });
};

export const setIsProfileYoutube = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_YOUTUBE,
    payload: isTrue,
  });
};

export const setIsProfilePinterest = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_PINTEREST,
    payload: isTrue,
  });
};

export const setIsProfileTiktok = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_TIKTOK,
    payload: isTrue,
  });
};

// Certificate

export const setProfileCertificate1 = (certificate) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE1,
    payload: certificate,
  });
};


export const setProfileCertificate2 = (certificate) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE2,
    payload: certificate,
  });
};

export const setProfileCertificate3 = (certificate) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE3,
    payload: certificate,
  });
};

export const setProfileCertificate4 = (certificate) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE4,
    payload: certificate,
  });
};

export const setProfileCertificate5 = (certificate) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE5,
    payload: certificate,
  });
};

export const setProfileCertificate6 = (certificate) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE6,
    payload: certificate,
  });
};

export const setProfileCertificate7 = (certificate) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE7,
    payload: certificate,
  });
};

// certificate image

export const setProfileCertificateName1 = (name) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_NAME1,
    payload: name,
  });
};

export const setProfileCertificateName2 = (name) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_NAME2,
    payload: name,
  });
};

export const setProfileCertificateName3 = (name) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_NAME3,
    payload: name,
  });
};

export const setProfileCertificateName4 = (name) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_NAME4,
    payload: name,
  });
};

export const setProfileCertificateName5 = (name) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_NAME5,
    payload: name,
  });
};

export const setProfileCertificateName6 = (name) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_NAME6,
    payload: name,
  });
};

export const setProfileCertificateName7 = (name) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_CERTIFICATE_NAME7,
    payload: name,
  });
};


export const setAccesKeySecretsKey = (object) => (dispatch) => {
  dispatch({
    type: SET_ACCESSKEY_SECRETS_KEY,
    payload: object,
  });
};




export const setMyMarketingVideoEmptyList = (isEmpty) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;

  let empty_data = { video_url: null };
  let data = state.myMarketingVideoList;
  if (isEmpty) {
    data = [];
  }
  let neData = [...data, empty_data];
  dispatch({
    type: SET_MY_MARKETING_VIDEO_LIST,
    payload: neData,
  });
};

export const setMyMarketingImageEmptyList = (isEmpty) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;

  let empty_data = { image_url: null };
  let data = state.myMarketingImageList;
  if (isEmpty) {
    data = [];
  }
  let neData = [...data, empty_data];
  dispatch({
    type: SET_MY_MARKETING_IMAGE_LIST,
    payload: neData,
  });
};



export const setMyMarketingImage = (object) => (dispatch) => {
  dispatch({
    type: SET_MY_MARKETING_IMAGE,
    payload: object,
  });
};


export const setMyMarketingVideo = (object) => (dispatch) => {
  dispatch({
    type: SET_MY_MARKETING_VIDEO,
    payload: object,
  });
};

export const setMyMarketingImageList = (image, index1) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.myMarketingImageList;
  let newData = [];
  let empty_data = { image_url: null, isSelected: false, };
  data.map((item, index) => {
    if (index == index1) {
      newData.push({
        image_url: image,
        isSelected: item.isSelected,
      })
    } else {
      newData.push({
        image_url: item.image_url,
        isSelected: item.isSelected,

      })
    }
  })

  let newData1 = [...newData, empty_data]

  dispatch({
    type: SET_MY_MARKETING_IMAGE_LIST,
    payload: newData1,
  });
};

export const setMyMarketingUpdateImageList = (index1) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.myMarketingImageList;
  let newData = [];
  data.map((item, index) => {
    if (index == index1 && item.image_url != null) {
      newData.push({
        image_url: item.image_url,
        isSelected: !item.isSelected,
      })
    } else {
      newData.push({
        image_url: item.image_url,
        isSelected: item.isSelected,

      })
    }
  })

  dispatch({
    type: SET_MY_MARKETING_IMAGE_LIST,
    payload: newData,
  });
};



export const setMyMarketingSelectAllImageList = (isTrue) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.myMarketingImageList;
  let newData = [];

  data.map((item, index) => {
    if (item.image_url != null) {
      newData.push({
        image_url: item.image_url,
        isSelected: isTrue,
      })
    } else {
      newData.push({
        image_url: item.image_url,
        isSelected: item.isSelected,
      })
    }
  })

  dispatch({
    type: SET_MY_MARKETING_IMAGE_LIST,
    payload: newData,
  });
};



export const setMyMarketingVideoList = (video, index1) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.myMarketingVideoList;
  let newData = [];
  let empty_data = { video_url: null, isSelected: false };
  data.map((item, index) => {
    if (index == index1) {
      newData.push({
        video_url: video,
        isSelected: item.isSelected,
      })
    } else {
      newData.push({
        video_url: item.video_url,
        isSelected: item.isSelected,
      })
    }
  })

  let newData1 = [...newData, empty_data]

  dispatch({
    type: SET_MY_MARKETING_VIDEO_LIST,
    payload: newData1,
  });
};

export const setMyMarketingUpdateVideoList = (index1) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.myMarketingVideoList;
  let newData = [];
  data.map((item, index) => {
    if (index == index1 && item.video_url != null) {
      newData.push({
        video_url: item.video_url,
        isSelected: !item.isSelected,
      })
    } else {
      newData.push({
        video_url: item.video_url,
        isSelected: item.isSelected,
      })
    }
  })

  dispatch({
    type: SET_MY_MARKETING_VIDEO_LIST,
    payload: newData,
  });
};

export const setMyMarketingSelectAllVideoList = (isTrue) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.myMarketingVideoList;
  let newData = [];

  data.map((item, index) => {
    if (item.video_url != null) {
      newData.push({
        video_url: item.video_url,
        isSelected: isTrue,
      })
    } else {
      newData.push({
        video_url: item.video_url,
        isSelected: item.isSelected,
      })
    }
  })

  dispatch({
    type: SET_MY_MARKETING_VIDEO_LIST,
    payload: newData,
  });
};


export const setMarketingIndex = (index) => (dispatch) => {
  dispatch({
    type: SET_MY_MARKETING_INDEX,
    payload: index,
  });
};


export const setMarketingDocument = (document) => (dispatch) => {
  dispatch({
    type: SET_MY_MARKETING_DOCUMENT,
    payload: document,
  });
};


export const setIsMarketingDocument = (document) => (dispatch) => {
  dispatch({
    type: IS_MY_MARKETING_DOCUMENT,
    payload: document,
  });
};

export const setMyMarketingDocumentUrl = (url) => (dispatch) => {
  dispatch({
    type: SET_MY_MARKETING_DOCUMENT_URL,
    payload: url,
  });
};

export const setSelctedVideoUrl = (url) => (dispatch) => {
  dispatch({
    type: SET_SELECTED_VIDEO_URL,
    payload: url,
  });
};


export const setDashboardSalesInfo = (list) => (dispatch) => {
  dispatch({
    type: SET_DASHBOARD_SALES_INFO,
    payload: list,
  });
};

export const setTotalSales = (amt) => (dispatch) => {
  dispatch({
    type: SET_DASHBOARD_TOTAL_SALES,
    payload: amt,
  });
};


export const setTotalCommition = (amt) => (dispatch) => {
  dispatch({
    type: SET_DASHBOARD_TOTAL_COMMITION,
    payload: amt,
  });
};

export const setCustomeReportList = (list) => (dispatch) => {
  dispatch({
    type: SET_CUSTOMER_REPORT_LIST,
    payload: list,
  });
};


export const setCustomerTotalSales = (amt) => (dispatch) => {
  dispatch({
    type: SET_CUSTOMER_TOTAL_SALES,
    payload: amt,
  });
};


export const setCustomerTotalCommition = (amt) => (dispatch) => {
  dispatch({
    type: SET_CUSTOMER_TOTAL_COMMITION,
    payload: amt,
  });
};


export const setDashboardList = (list) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.darshboard_sales_info;

  // Adding only unique object
  var itemsObj = {};
  var itemsList = [];
  for (var i = 0; i < data.length; i++) {
    var item = data[i];
    if (!itemsObj[item.customerFirstName]) {
      itemsObj[item.customerFirstName] = item;
      itemsList.push(item);
    }
  }

  let orderAmount = 0
  let commitionAmount = 0
  let newData = []

  itemsList.map((item, index) => {
    data.map((item1, index) => {
      if (item.customerFirstName == item1.customerFirstName && item.customerLastName == item1.customerLastName) {
        // newData = [...newData, item1,]
        commitionAmount = commitionAmount + item1.commission
        orderAmount = orderAmount + item1.orderAmount
      }
    })

    newData.push({
      ...item,
      totalSales: commitionAmount + orderAmount
    })
    orderAmount = 0
    commitionAmount = 0
  })
  debugLog('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj')
  debugLog(newData)

  dispatch({
    type: SET_DASHBOARD_LIST,
    payload: newData,
  });
};




export const setIsQuillEditorVideo1 = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_PROFILE_DESCRIPTION_VIDEO,
    payload: isTrue,
  });
};


export const setQuillEditorVideoUrl = (url) => (dispatch) => {
  dispatch({
    type: SET_PROFILE_DESCRIPTION_VIDEO,
    payload: url,
  });
};


export const setRecommendationHistory = (list) => (dispatch) => {
  dispatch({
    type: SET_RECOMMENDATION_HISTORY,
    payload: list,
  });

  dispatch({
    type: SET_SEARCH_RECOMMENDATION_HISTORY,
    payload: list,
  });

};


export const setSelectedProductListForRecommend = (list) => (dispatch) => {
  dispatch({
    type: SET_SELECTED_PRODUCT_LIST_FOR_RECOMMEND,
    payload: list,
  });
};

export const serMessageHistoryList = (list) => (dispatch) => {
  dispatch({
    type: SET_MESSAGE_HISTORY_LIST,
    payload: list,
  });

  dispatch({
    type: SET_SEARCH_MESSAGE_HISTORY_LIST,
    payload: list,
  });
};


export const setMessageHistoryDetails = (list) => (dispatch) => {
  dispatch({
    type: SET_MESSAGE_HISTORY_DETAILS,
    payload: list,
  });

  dispatch({
    type: SET_SEARCH_MESSAGE_HISTORY_DETAILS,
    payload: list,
  });
};


export const setRecommendationHistoryDetails = (list) => (dispatch) => {
  dispatch({
    type: SET_RECOMMENDATION_HISTORY_DETAILS,
    payload: list,
  });

  dispatch({
    type: SET_SEARCH_RECOMMENDATION_HISTORY_DETAILS,
    payload: list,
  });

};



export const searchRecommendationHistory = (text) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let txt = text.toUpperCase();
  let holderlist = state.search_recommendation_history_list;
  let newData = holderlist.filter(function (item) {
    let name = item.subject.toUpperCase()
    return name.includes(txt);
  });
  if (txt == "") {
    dispatch({
      type: SET_RECOMMENDATION_HISTORY,
      payload: state.search_recommendation_history_list,
    });
  } else {
    dispatch({
      type: SET_RECOMMENDATION_HISTORY,
      payload: newData,
    });
  }
};

export const searchMessageHistory = (text) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let txt = text.toUpperCase();
  let holderlist = state.search_message_history_list;
  let newData = holderlist.filter(function (item) {
    let name = item.subject.toUpperCase()
    return name.includes(txt);
  });
  if (txt == "") {
    dispatch({
      type: SET_MESSAGE_HISTORY_LIST,
      payload: state.search_message_history_list,
    });
  } else {
    dispatch({
      type: SET_MESSAGE_HISTORY_LIST,
      payload: newData,
    });
  }
};


export const searchMessageHistoryDetails = (text) => (dispatch) => {
 
  let state = store.getState().indexReducerJaswant;
  let txt = text.toUpperCase();
  let holderlist = state.search_message_history_details;
  let newData = holderlist.filter(function (item) {
    let name = item.subject.toUpperCase()
    return name.includes(txt);
  });
  if (txt == "") {
    dispatch({
      type: SET_MESSAGE_HISTORY_DETAILS,
      payload: state.search_message_history_details,
    });
  } else {
    dispatch({
      type: SET_MESSAGE_HISTORY_DETAILS,
      payload: newData,
    });
  }
};


export const searchRecommendationHistoryDetails = (text) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let txt = text.toUpperCase();
  let holderlist = state.search_recommendation_history_details;
  let newData = holderlist.filter(function (item) {
    let name = item.subject.toUpperCase()
    return name.includes(txt);
  });
  if (txt == "") {
    dispatch({
      type: SET_RECOMMENDATION_HISTORY_DETAILS,
      payload: state.search_recommendation_history_details,
    });
  } else {
    dispatch({
      type: SET_RECOMMENDATION_HISTORY_DETAILS,
      payload: newData,
    });
  }
};


export const setIsLoginFailed = (isTrue) => (dispatch) => {
  dispatch({
    type: IS_LOGIN_FAILED,
    payload: isTrue,
  });
};


export const setLoginFailedMessage = (msg) => (dispatch) => {
  dispatch({
    type: SET_LOGIN_FAILD_MESSAGE,
    payload: msg,
  });
};

export const setForgotPassMessage = (msg) => (dispatch) => {
  dispatch({
    type: SET_FORGOTPASSWORD_URL_MESSAGE,
    payload: msg,
  });
};

