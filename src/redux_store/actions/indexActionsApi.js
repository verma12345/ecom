import moment from 'moment';
import { RNS3 } from 'react-native-aws3';
import Constants, { debugLog } from '../../common/Constants';


import {
  SET_DOCUMENT_PROGRESS,
  SET_IMAGE_PROGRESS,
  SET_LOADING, SET_VIDEO_PROGRESS,
} from './types';


export const hitSocialLogin = (param) => {
  return async (dispatch) => {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    let data = await fetch(Constants.AUTH_API_BASE_URL + "/test-sso/login?state=login->true,domain->" + param.domain + ",platform->" + param.platform + ",source->mobile"
      , {
        headers: {
          'Content-Type': 'application/json'
        },
      });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });

    let response = await JSON.parse(data);

    return response;
  };
};




export const hitGetProfileData = (param) => {

  return async (dispatch) => {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    let data = await fetch(
      // `https://www.${param.domainname}/restapi/storeprofileget`,
      `https://www.ecompaas.com/test-cbp/restapi/storeprofileget`,

      {
        method: 'post',
        body: JSON.stringify(param),
      },
    );
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();
    return response;
  };
};



export const hitSaveProfileData = (param) => {

  return async (dispatch) => {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    let data = await fetch(
      `${Constants.API_BASE_URL}/restapi/storeprofilesave`,
      param
    );
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();
    return response;
  };
};



export const hitUploadImageOnASW = (param) => {

  return async (dispatch) => {
    // dispatch({
    //   type: SET_LOADING,
    //   payload: true,
    // });


    moment.locale('en');
    var currentTime = moment(new Date().toString()).milliseconds(0);
    var c2 = moment(currentTime).format('YYYYMMDDTHHMMSS') + 'Z'
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "image/png");
    myHeaders.append("X-Amz-Content-Sha256", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
    myHeaders.append("X-Amz-Date", c2);
    myHeaders.append("Connection", 'keep-alive');
    myHeaders.append("Authorization", "AWS4-HMAC-SHA256 Credential=AKIAUKGZKWGYS36AJUNW/20210624/us-east-1/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=ea76eba575264b05e3c30297e9688b7a5bd2cf367ae82dd9d08fa02016be18c0");

    if (Object.keys(param).length == 0) {
      alert('Please select image first');
      return;
    }
    let res = await RNS3.put(
      {
        // `uri` can also be a file system path (i.e. file://)
        uri: param.image.assets[0].uri,
        name: param.image.assets[0].fileName,
        type: param.image.assets[0].type,
        // uri: param.path,
        // type: param.mime,
        // name: 'pic.png'
      },
      {
        keyPrefix: `${param.platformId}/images/reseller/${param.storeNo}/`, // Ex. myuploads/
        bucket: "ecomnow-images", // Ex. aboutreact
        region: "us-east-1", // Ex. ap-south-1
        accessKey: param.accessKey,
        secretKey: param.secretKey,
        headers: myHeaders,
        successActionStatus: 201,
      },
    ).progress((progress) => {

      var completed = progress.loaded;
      var total = progress.total;
      var pr = Math.ceil(completed / total * 100) + ' %';

      dispatch({
        type: SET_IMAGE_PROGRESS,
        payload: pr,
      });

    }
    )

    // dispatch({
    //   type: SET_LOADING,
    //   payload: false,
    // });

    return res
  };
};







export const hitUploadVideoOnASW = (param) => {

  return async (dispatch) => {
    // dispatch({
    //   type: SET_LOADING,
    //   payload: true,
    // });


    moment.locale('en');
    var dt = '2016-05-02T00:00:00';
    var currentTime = moment(new Date().toString()).milliseconds(0);
    var c2 = moment(currentTime).format('YYYYMMDDTHHMMSS') + 'Z'
    //   console.log(moment(new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString()).format('YYYYMMDDTHHMMSS'))
    console.log(currentTime)
    console.log(c2)

    //  var currentTime = moment("2021-06-24T11:05:10Z").format('YYYYMMDDTHHMMSS')+'Z'

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "video/mp4");
    myHeaders.append("X-Amz-Content-Sha256", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
    myHeaders.append("X-Amz-Date", c2);
    myHeaders.append("Connection", 'keep-alive');
    myHeaders.append("Authorization", "AWS4-HMAC-SHA256 Credential=AKIAUKGZKWGYS36AJUNW/20210624/us-east-1/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=ea76eba575264b05e3c30297e9688b7a5bd2cf367ae82dd9d08fa02016be18c0");
    console.log('>oooooo>>>')
    if (Object.keys(param.video).length == 0) {
      alert('Please select image first');
      return;
    }
    console.log('>>......>>')
    console.log('uuu', param.video.assets[0].uri)


    let res = await RNS3.put(
      {
        // `uri` can also be a file system path (i.e. file://)
        uri: param.video.assets[0].uri,
        name: `${param.video.assets[0].fileName}.mp4`,
        type: 'video/mp4'
      },
      {


        // keyPrefix: "bht/images/reseller/2336/videos", // Ex. myuploads/
        keyPrefix: 'bht/3/marketing/videos',
        // keyPrefix: `${param.platformId}/images/reseller/${param.storeNo}/`, // Ex. myuploads/
        bucket: "ecomnow-images", // Ex. aboutreact
        region: "us-east-1", // Ex. ap-south-1
        accessKey: "AKIAUKGZKWGYWRZ46EPO",
        secretKey: "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",
        headers: myHeaders,
        successActionStatus: 201,
      },
    ).progress((progress) => {
      var completed = progress.loaded;
      var total = progress.total;
      var pr = Math.ceil(completed / total * 100) + ' %';

      dispatch({
        type: SET_VIDEO_PROGRESS,
        payload: pr,
      });
    }
    )

    // dispatch({
    //   type: SET_LOADING,
    //   payload: false,
    // });

    return res
  };
};



export const hitUploadDocumentOnASW = (param) => {

  return async (dispatch) => {
    // dispatch({
    //   type: SET_LOADING,
    //   payload: true,
    // });

    moment.locale('en')
    var dt = '2016-05-02T00:00:00';
    var currentTime = moment(new Date().toString()).milliseconds(0);
    var c2 = moment(currentTime).format('YYYYMMDDTHHMMSS') + 'Z'
    //   console.log(moment(new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString()).format('YYYYMMDDTHHMMSS'))
    console.log(currentTime)
    console.log(c2)

    //  var currentTime = moment("2021-06-24T11:05:10Z").format('YYYYMMDDTHHMMSS')+'Z'

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    myHeaders.append("X-Amz-Content-Sha256", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
    myHeaders.append("X-Amz-Date", c2);
    myHeaders.append("Connection", 'keep-alive');
    myHeaders.append("Authorization", "AWS4-HMAC-SHA256 Credential=AKIAUKGZKWGYS36AJUNW/20210624/us-east-1/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=ea76eba575264b05e3c30297e9688b7a5bd2cf367ae82dd9d08fa02016be18c0");
    console.log('>oooooo>>>')
    if (Object.keys(param.video).length == 0) {
      alert('Please select image first');
      return;
    }
    console.log('>>......>>')
    console.log('uuu', param.video)

    let res = await RNS3.put(
      {
        // `uri` can also be a file system path (i.e. file://)
        uri: param.video.uri,
        name: param.video.name,
        type: param.video.type
      },
      {
        // keyPrefix: "bht/images/reseller/2336/message", // Ex. myuploads/
        keyPrefix: `${param.platformId}/images/reseller/${param.storeNo}/`, // Ex. myuploads/
        bucket: "ecomnow-images", // Ex. aboutreact
        region: "us-east-1", // Ex. ap-south-1
        accessKey: param.accessKey,
        secretKey: param.secretKey,
        headers: myHeaders,
        successActionStatus: 201,
      },
    ).progress((progress) => {
      var completed = progress.loaded;
      var total = progress.total;
      var pr = Math.ceil(completed / total * 100) + ' %';

      dispatch({
        type: SET_DOCUMENT_PROGRESS,
        payload: pr,
      });

    })


    return res
  };
};











export const hitWestornUnion = (param) => {

  return async (dispatch) => {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    debugLog(param)
    const url = `https://payee.globalpay.westernunion.com/PayeeManager/BeneficiaryEnrollment/CustomizedEnrollmentProcess.aspx?cid=45785B6194F6875B2732E7DF82A60114&sid=CSCr${param.storeNo}-${param.uniqueId}`

    let data = await fetch(url, {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    })
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.text();
    return response;
  };
};


export const hitGetDashboardSales = (param) => {

  return async (dispatch) => {
    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Cookie", "SESSION=YmM2YjI4NTctNjJjMC00YmZjLTg5NzYtM2JkYzhjZTY1MjFi");

    var raw = "";

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    // fetch("https://www.ecompaas.com/test-cbp/admin/store-sales?timePeriod=YEAR&platform=csc&storeNumber=2336", requestOptions)
    //   .then(response => response.json())
    //   .then(result => {
    //     console.log(result[3])
    //     this.props.setDashboardSalesInfo(result)

    //   })
    //   .catch(error => console.log('error', error));

    let data = await fetch(`${Constants.API_BASE_URL}/admin/store-sales?timePeriod=DAY&platform=csc&storeNumber=2336`, requestOptions);
    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();
    debugLog(response)
    return response;
  };
};























// Dummy

export const uplaodVideo = () => {


  moment.locale('en');
  var dt = '2016-05-02T00:00:00';
  var currentTime = moment(new Date().toString()).milliseconds(0);
  var c2 = moment(currentTime).format('YYYYMMDDTHHMMSS') + 'Z'
  //   console.log(moment(new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString()).format('YYYYMMDDTHHMMSS'))
  console.log(currentTime)
  console.log(c2)

  //  var currentTime = moment("2021-06-24T11:05:10Z").format('YYYYMMDDTHHMMSS')+'Z'

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "video/mp4");
  myHeaders.append("X-Amz-Content-Sha256", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
  myHeaders.append("X-Amz-Date", c2);
  myHeaders.append("Connection", 'keep-alive');
  myHeaders.append("Authorization", "AWS4-HMAC-SHA256 Credential=AKIAUKGZKWGYS36AJUNW/20210624/us-east-1/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=ea76eba575264b05e3c30297e9688b7a5bd2cf367ae82dd9d08fa02016be18c0");
  console.log('>oooooo>>>')
  if (Object.keys(filePath).length == 0) {
    alert('Please select image first');
    return;
  }
  console.log('>>......>>')
  console.log('uuu', filePath.assets[0].uri)


  RNS3.put(
    {
      // `uri` can also be a file system path (i.e. file://)
      uri: filePath.assets[0].uri,
      name: 'vid_6.mp4',
      type: 'video/mp4'
    },
    {
      keyPrefix: "bht/images/reseller/2336/videos", // Ex. myuploads/
      bucket: "ecomnow-images", // Ex. aboutreact
      region: "us-east-1", // Ex. ap-south-1
      accessKey: "AKIAUKGZKWGYWRZ46EPO",
      // Ex. AKIH73GS7S7C53M46OQ
      secretKey: "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",
      headers: myHeaders,
      // Ex. Pt/2hdyro977ejd/h2u8n939nh89nfdnf8hd8f8fd
      successActionStatus: 201,
    },
  )

    .progress((progress) =>
      // setUploadSuccessMessage(``, ),
      debugLog(progress)
    )
    .catch(function (error) {
      // console.log('Error', JSON.stringify(error.errorCode));
      // console.log('Error', JSON.stringify(error));
      throw error;
    })
    .then((response) => {
      // console.log('skldjlksajdkasjdlk', response.errorCode)
      if (response.status !== 201)
        alert('Failed to upload image to S3');
      let {
        bucket,
        etag,
        key,
        location
      } = response.body.postResponse;
      this.props.setMyMarketingVideoList(location, this.props.myMarketingIndex)
      this.props.setMyMarketingVideo(null)

      debugLog(location)
      // setUploadSuccessMessage(
      //   `Uploaded Successfully: 
      //   \n1. bucket => ${bucket}
      //   \n2. etag => ${etag}
      //   \n3. key => ${key}
      //   \n4. location => ${location}`,
      // );
      // console.log('###########>', uploadSuccessMessag)

      /**
       * {
       *   postResponse: {
       *     bucket: "your-bucket",
       *     etag : "9f620878e06d28774406017480a59fd4",
       *     key: "uploads/image.png",
       *     location: "https://bucket.s3.amazonaws.com/**.png"
       *   }
       * }
       */
    });
}



