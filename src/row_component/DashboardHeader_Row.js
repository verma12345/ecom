import React, { Component } from "react"
import { Text, TouchableOpacity, View } from "react-native"
import LinearGradient from "react-native-linear-gradient"
import { msStyle } from "../common/MyStyle";

class DashboardHeader_Row extends Component {
    constructor(props) {
        super(props);
     
    }



    render() {
        return (
            <View>
                <View style={[msStyle.myshadow,{
                    height: 35,
                    width: '90%',
                    backgroundColor: '#fafaff',
                    alignSelf: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                    borderRadius: 20,
                    marginTop: 20,
                    // elevation: 3,
                    marginVertical: 15
                }]}>
                    <TouchableOpacity
                        onPress={this.props.onPressToday}
                        style={{ height: 35, flex: 1, borderBottomLeftRadius: 20, borderTopLeftRadius: 20, alignItems: 'center', backgroundColor: this.props.is_today ? '#055485' : 'white', justifyContent: 'center', borderRightWidth: 1, borderColor: 'gray' }}>
                        <Text style={{
                            color: this.props.is_today ? 'white' : 'black',
                            fontWeight: '700',
                            fontSize: 14
                        }}>
                            {'Today'}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.props.onPressWeek}
                        style={{ height: 35, flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: this.props.is_week ? '#055485' : 'white', borderRightWidth: 1, borderColor: 'gray' }}>
                        <Text style={{
                            color: this.props.is_week ? 'white' : 'black',
                            fontWeight: '700',
                            fontSize: 14
                        }}>
                            {'Week'}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.props.onPressMonth}
                        style={{ height: 35, flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: this.props.is_month ? '#055485' : 'white', borderRightWidth: 1, borderColor: 'gray' }}>
                        <Text style={{
                            color: this.props.is_month ? 'white' : 'black',
                            fontWeight: '700',
                            fontSize: 14
                        }}>
                            {'Month'}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.props.onPressYear}
                        style={{ height: 35, flex: 1, alignItems: 'center', borderBottomRightRadius: 20, borderTopRightRadius: 20, justifyContent: 'center', backgroundColor: this.props.is_year ? '#055485' : 'white' }}>
                        <Text style={{
                            color: this.props.is_year ? 'white' : 'black',
                            fontWeight: '700',
                            fontSize: 14
                        }}>
                            {'Year'}
                        </Text>
                    </TouchableOpacity>
                </View>

                {/* card view */}
                <View style={{
                    flexDirection: 'row',
                    backgroundColor: 'white',
                    marginVertical: 15,
                    marginHorizontal: 10,
                    height:120
                }}>

                    <TouchableOpacity style={[msStyle.myshadow,{
                        flex: 1,
                        justifyContent: 'space-around',
                        borderRadius: 7,
                        marginRight: 10,

                    }]}>
                        <LinearGradient
                            start={{ x: 1, y: 1 }}
                            end={{ x: 0, y: 0 }}
                            colors={['#1c80c9', '#074674']}
                            style={{
                                flex: 1,
                                justifyContent: 'space-between',
                                padding: 10,
                                borderRadius: 7,
                                paddingVertical:15
                            }}>
                            <Text style={{
                                color: 'white',
                                fontWeight: '500',
                                fontSize: 14
                            }}>
                                {'Sales Total'}
                            </Text>

                            <Text style={{
                                color: 'white',
                                fontWeight: '700',
                                fontSize: 25
                            }}>
                                {'$ '}{this.props.totalSalesAmount.toFixed(2)}
                            </Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    <TouchableOpacity style={[msStyle.myshadow,{
                        flex: 1,
                        justifyContent: 'space-around',
                        borderRadius: 7,
                    }]}>
                        <LinearGradient
                            start={{ x: 1, y: 1 }}
                            end={{ x: 0, y: 0 }}
                            colors={['#1c80c9', '#074674']}
                            style={{
                                padding: 10,
                                flex: 1,
                                justifyContent: 'space-between',
                                borderRadius: 7,
                                paddingVertical:15

                            }}>
                            <Text style={{
                                color: 'white',
                                fontWeight: '500',
                                fontSize: 14
                            }}>
                                {'Commission Total'}
                            </Text>

                            <Text style={{
                                color: 'white',
                                fontWeight: '700',
                                fontSize: 25
                            }}>
                                {'$ '}{this.props.totalCommitionAmount.toFixed(2)}
                            </Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>


                <Text style={{
                    color: '#28293d',
                    fontWeight: '700',
                    fontSize: 18,
                    paddingHorizontal: 20
                }}>
                    {'Customers'}
                </Text>
            </View>
        )
    }
}


export default DashboardHeader_Row;