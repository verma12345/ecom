// import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage'
import Constants from './Constants';


const KEY_SOUND = "key_sound" + Constants.RN_CODEBASE_VERSION;
const KEY_RATE = "key_rate" + Constants.RN_CODEBASE_VERSION;
const VENDER_ID = "VENDER_ID" + Constants.RN_CODEBASE_VERSION;
const IS_REGISTER = "IS_REGISTER" + Constants.RN_CODEBASE_VERSION;
const IS_ONETIME_REGISTER = "IS_REGISTER" + Constants.RN_CODEBASE_VERSION;


export default {
    KEY_SOUND, KEY_RATE, VENDER_ID, IS_REGISTER, IS_ONETIME_REGISTER
};

export async function setPrefs(key, value) {
    return AsyncStorage.setItem(key, value);
}


export async function getPrefs(key) {
    return await AsyncStorage.getItem(key);
}
