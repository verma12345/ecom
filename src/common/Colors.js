const tintColor = '#66ffff';
const whiteText = '#ffffff';
const statusBarColor = '#1050E6';
const backgroundColor = '#1050E6';
const backgroundColor2='#f8faff'
const placeHolderColor = '#C4C4C4';
const textColor = '#343434';
const textDarkColor = "#3D3D3D"
const progressColor = '##9E9E9E';
const grayColor = '#737373';
const themeColor='#026bb8';


export default {
  tintColor,
  tabIconDefault: '#384659',
  tabIconSelected: tintColor,
  whiteText,
  progressColor,
  statusBarColor,
  red: "red",
  grayColor,
  backgroundColor,
  placeHolderColor,
  textColor,
  textDarkColor,
  themeColor,
  backgroundColor2,
};
