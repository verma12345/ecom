import { StyleSheet } from "react-native";
import Colors from "./Colors";
import Font from "./Font";

export const msStyle = StyleSheet.create({
    txtStyle: {
        fontFamily: Font.SourceSansPro,
        fontWeight: "600",
    },
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor2
    },
    myshadow: {
        shadowColor: "#1050e6",
        shadowOpacity: 0.15,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
        zIndex: 5
    },

    myshadow2: {
        shadowColor: "#1050e6",
        shadowOpacity: 0.15,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 4,
        zIndex: 4
    },
})