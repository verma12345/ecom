import React, { Component } from 'react';
import {
  Button, View, Text, SafeAreaView, Dimensions, StyleSheet,
  TouchableOpacity, PixelRatio, Alert,
  ImageBackground, Image, ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import { msStyle } from '../common/MyStyle';
import MyHeader from '../components/MyHeader';
import { ProfileOptionMemo } from '../components/ProfileOption';
import ProfilePopup from '../components/ProfilePopup';
import {
  setIsProfileCertificateName,
  setProfileCertificate,
  setProfileCertificate1,
  setProfileCertificateEmptyList,
  setProfileCertificateIndex,
  setProfileCertificateListImage,
  setProfileCertificateListName,
  setProfileCertificateName,

} from '../redux_store/actions/indexActionsJaswant';

import ImageCropPicker from 'react-native-image-crop-picker';
import MyButton from '../components/MyButton';
import { RNS3 } from 'react-native-aws3';
import { launchImageLibrary, launchCamera } from 'react-native-image-picker';
import moment from 'moment';
import { hitUploadImageOnASW } from '../redux_store/actions/indexActionsApi';
import MyLoader from '../components/MyLoader';

class CertificatesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCertificat: false,

    }
  }

  showAlert(index) {
    Alert.alert(
      'Upload Image',
      'Please choose Camera or Gallery',
      [
        {
          text: 'Camera',
          onPress: () => {
            this.openCameraImage1(index);
          },
        },
        {
          text: 'Gallery',
          onPress: () => {
            this.openGalleryImage1(index);
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  }

  openCameraImage1 = (index) => {
    let options = {
      mediaType: 'photo',
    };
    launchCamera(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        alert('You cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      this.props.setProfileCertificateIndex(index)
      this.props.setProfileCertificate1(response);
    });
  };

  openGalleryImage1 = (index) => {
    let options = {
      mediaType: 'photo',
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        alert('You cancelled library picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      this.props.setProfileCertificateIndex(index)
      this.props.setProfileCertificate1(response);
    });
  };

  uploadImageOnASW1 = (filePath) => {

    let params = {
      image: filePath,
      accessKey: this.props.accessAndSecretsKey.accessKey,
      secretKey: this.props.accessAndSecretsKey.secretKey,
      platformId: this.props.loginResponse.platformId,
      storeNo: this.props.loginResponse.storeNo
    }
    this.setState({ isCertificat: true })
    this.props.hitUploadImageOnASW(params).then(res => {
      let { bucket, etag, key, location } = res.body.postResponse;
      // debugLog(location)
      this.props.setProfileCertificateListImage(location, this.props.profileCertificateIndex);
      this.props.setProfileCertificate1(null);
      this.setState({ isCertificat: false })

    })
  }


  onHint = () => {
    alert("Coming soon...")
  }

  onGoBack = () => {
    this.props.navigation.goBack()
  }

  componentDidMount() {
    if (this.props.profileCertificateList.length == 0) {
      this.props.setProfileCertificateEmptyList(true)
      // this.props.setProfileCertificate1(null);
    }
  }

  onSave = () => {
    this.props.setProfileCertificate(this.props.profileCertificateList[0].cert_icon_name)
    this.props.navigation.goBack()
  }

  addMore = () => {
    if (this.props.profileCertificateList.length == 7) {
      alert("Sorry! Only seven certificate can be add")
    } else {
      this.props.setProfileCertificateEmptyList(false)
    }
  }

  onPressCertificateName = () => {
    let isDuplicate = 1

    this.props.profileCertificateList.map((item, index) => {
      if (item.cert_name == this.props.profileCertificateName) {
        isDuplicate = isDuplicate + 1
      }
    })

    if (isDuplicate >= 2) {
      alert('Certificate name is already exists!')
    } else {
      this.props.setProfileCertificateListName(this.props.profileCertificateName, this.props.profileCertificateIndex)
      this.props.setIsProfileCertificateName(!this.props.isProfileCertificateName)
    }
  }

  render() {
    debugLog(this.props.profileCertificateList)
    let input = this.props.profileCertificateList.map((item, index) => {
      return (
        <View
          key={index}
          style={{ paddingHorizontal: 15, marginTop: index == 0 ? 30 : 0 }} >


          <ProfileOptionMemo
            title='Certficate Name'
            message={item.cert_name}
            onPencil={() => {
              this.props.setProfileCertificateName(item.cert_name)
              this.props.setProfileCertificateIndex(index)
              this.props.setIsProfileCertificateName(!this.props.isProfileCertificateName)
            }}
            inputStyle={{ paddingVertical: 10, marginVertical: 10 }}
          />

          <Text style={[msStyle.txtStyle, { marginVertical: 15 }]}>
            {"Certificate Image"}
          </Text>

          <View style={[msStyle.myshadow,{ padding: 5, height: 100, width: 100, backgroundColor: "white", elevation: 5, borderRadius: 100 }]} >
            {item.cert_icon_name != '' ?
              <ImageBackground
                source={{ uri: item.cert_icon_name }}
                borderRadius={100}
                style={{ height: 90, width: 90, resizeMode: 'contain' }}
              /> :
              <View style={{ height: 90, width: 90, borderRadius: 360, backgroundColor: Colors.placeHolderColor }} />
            }


            <TouchableOpacity
              onPress={() => this.showAlert(index)}
              style={styles.buttonStyle} >
              <Image
                source={require('../../assets/edit.png')}
                style={{ height: 20, width: 20, tintColor: Colors.themeColor, resizeMode: 'contain' }}
              />
            </TouchableOpacity>


          </View>
          {index == this.props.profileCertificateList.length - 1 ? null :
            <View style={{ height: 1.5, backgroundColor: 'lightgray', marginTop: 30, marginBottom: 20 }} />
          }

        </View>
      )
    })

    return (
      <SafeAreaView style={msStyle.container}>
        <View style={msStyle.container}>

          {/* Need to change bg color */}
          <View style={{
            flex: 1,
            backgroundColor: Colors.themeColor
          }} >
            <MyHeader
              title='Upload Certificate'
              icon={require('../../assets/back.png')}
              tintColor="white"
              onPress={() => { this.onGoBack() }}
              containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
              titleStyle={{ color: Colors.whiteText }}
            />



          </View>

          <View style={[msStyle.myshadow, {borderRadius:10,paddingVertical:5, backgroundColor: 'white', paddingHorizontal: 10, position: 'absolute', alignSelf: 'flex-end', right: 20, top: 70, elevation: 33 }]} >
            <TouchableOpacity onPress={() => { this.addMore() }} >
              <Text style={[msStyle.txtStyle, { fontSize: 20, color: Colors.themeColor }]} >{"+ Add More"}</Text>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 5, backgroundColor: Colors.whiteText }} >

            {/* Absolute UI */}
            <View style={[msStyle.myshadow, styles.portrateMode]} >





              <ScrollView
                //  ref={(ref) => { this.scrollListReftop = ref; }}
                keyboardDismissMode={'on-drag'}
                alwaysBounceVertical={true}
                // keyboardShouldPersistTaps={this.props.service_index >= 0 ? 'always' : 'never'}
                // scrollEnabled={this.props.service_index >= 0 ? false : true}
                contentContainerStyle={{
                  flexGrow: 1,// use this to solve this problem
                }}>
                {input}

                <MyButton
                  title='SAVE'
                  onPress={() => { this.onSave() }}
                  titleStyle={{ color: Colors.whiteText }}
                  containerStyle={{ marginVertical: 50, marginTop: 80, marginHorizontal: 20 }}
                />
              </ScrollView>

            </View>
          </View>

          {
            this.props.isProfileCertificateName ?
              <ProfilePopup
                title='Certificate Name'
                value={this.props.profileCertificateName}
                onChangeText={(txt) => { this.props.setProfileCertificateName(txt) }}
                onPress={() => { this.onPressCertificateName() }}
                onCancel={() => { this.props.setIsProfileCertificateName(!this.props.isProfileCertificateName) }}
              />
              : null
          }


          {
            this.props.profile_certificate_image1 != null ?
              <ProfilePopup
                title='Image'
                loading={this.props.is_loading}
                image={this.props.profile_certificate_image1.assets[0].uri}
                onPress={() => {
                  this.uploadImageOnASW1(this.props.profile_certificate_image1)
                }}
                onCancel={() => { this.props.setProfileCertificate1(null); }}

              />
              : null
          }


          {
            this.state.isCertificat ?
              <MyLoader />
              : null
          }

        </View>
      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  portrateMode: {
    flex: 1,
    backgroundColor: Colors.backgroundColor2,
    margin: 10,
    padding: 3,
    elevation: 10,
    borderRadius: 10,
    marginTop: -PixelRatio.getPixelSizeForLayoutSize(25)
  },
  buttonStyle: {
    backgroundColor: "white",
    alignSelf: 'flex-end',
    padding: 5,
    position: 'absolute',
    bottom: -5,
    right: -5,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: Colors.themeColor,

  },
})

const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_loading: common.is_loading,
    is_drawer: common.is_drawer,
    profileCertificate: jaswant.profileCertificate,
    profileCertificateList: jaswant.profileCertificateList,
    isProfileCertificateName: jaswant.isProfileCertificateName,
    profileCertificateName: jaswant.profileCertificateName,
    profileCertificateIndex: jaswant.profileCertificateIndex,
    profile_certificate_image1: jaswant.profile_certificate_image1,
    accessAndSecretsKey: jaswant.accessAndSecretsKey,
    loginResponse: common.loginResponse,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({

    setProfileCertificate: (certificate) => setProfileCertificate(certificate),
    setIsProfileCertificateName: (isTrue) => setIsProfileCertificateName(isTrue),
    setProfileCertificateEmptyList: (isTrue) => setProfileCertificateEmptyList(isTrue),
    setProfileCertificateListName: (name, index) => setProfileCertificateListName(name, index),
    setProfileCertificateListImage: (image, index) => setProfileCertificateListImage(image, index),
    setProfileCertificateName: (name) => setProfileCertificateName(name),
    setProfileCertificateIndex: (index) => setProfileCertificateIndex(index),
    setProfileCertificate1: (certificate) => setProfileCertificate1(certificate),
    hitUploadImageOnASW: (params) => hitUploadImageOnASW(params),


  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(CertificatesComponent)