import React, { Component } from "react";
import { Image, StyleSheet, TextInput, Text, TouchableOpacity, View, SafeAreaView, ScrollView, FlatList } from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Constants, { debugLog } from "../common/Constants";
import { msStyle } from "../common/MyStyle";
import MyLoader from "../components/MyLoader";
import { hitGetDashboardSales } from "../redux_store/actions/indexActionsApi";
import { setCustomeReportList, setCustomerTotalCommition, setCustomerTotalSales, setDashboardList, setDashboardSalesInfo, setTotalCommition, setTotalSales } from "../redux_store/actions/indexActionsJaswant";
import DashboardHeader_Row from "../row_component/DashboardHeader_Row";

class DashboardScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {

            is_today: false,
            is_week: false,
            is_month: false,
            is_year: true,
            is_loading: false,
        }
    }

    

    onPressToday = () => {
        this.setState({
            is_today: true,
            is_week: false,
            is_month: false,
            is_year: false
        })
    }

    onPressWeek = () => {
        this.setState({
            is_today: false,
            is_week: true,
            is_month: false,
            is_year: false
        })
    }

    onPressMonth = () => {
        this.setState({
            is_today: false,
            is_week: false,
            is_month: true,
            is_year: false
        })
    }

    onPressYear = () => {
        this.setState({
            is_today: false,
            is_week: false,
            is_month: false,
            is_year: true
        })
    }


    componentDidMount() {
        this.onGetDashboardApi('YEAR')
    }

    onGetDashboardApi = (period) => {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
        myHeaders.append("Cookie", "SESSION=YmM2YjI4NTctNjJjMC00YmZjLTg5NzYtM2JkYzhjZTY1MjFi");

        var raw = "";

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        this.setState({ is_loading: true })
        fetch(`${Constants.API_BASE_URL}/admin/store-sales?timePeriod=${period}&platform=csc&storeNumber=2336`, requestOptions)
            .then(response => response.json())
            .then(result => {

                this.props.setDashboardSalesInfo(result)
                this.props.setDashboardList(result)

                let orderAmount = 0
                let commitionAmount = 0
                let filteredList = []

                result.map((item, index) => {
                    commitionAmount = commitionAmount + item.commission
                    orderAmount = orderAmount + item.orderAmount


                })


                this.props.setTotalSales(orderAmount)
                this.props.setTotalCommition(commitionAmount)

                this.setState({ is_loading: false })

            })
            .catch(error => console.log('error', error));
    }

    _renderHeader = (data) => {
        return (
            <DashboardHeader_Row
                is_today={this.state.is_today}
                is_week={this.state.is_week}
                is_month={this.state.is_month}
                is_year={this.state.is_year}
                totalCommitionAmount={this.props.totalCommitionAmount}
                totalSalesAmount={this.props.totalSalesAmount}
                onPressYear={() => {
                    this.onPressYear()
                    this.onGetDashboardApi('YEAR')
                }}
                onPressMonth={() => {
                    this.onPressMonth()
                    this.onGetDashboardApi('MONTH')

                }}
                onPressWeek={() => {
                    this.onPressWeek()
                    this.onGetDashboardApi('WEEK')
                }}
                onPressToday={() => {
                    this.onPressToday()
                    this.onGetDashboardApi('TODAY')

                }}
            />


        )
    }

    onPressCustomer = (data) => {
        let orderAmount = 0
        let commitionAmount = 0
        let newData = []
        this.props.darshboard_sales_info.map((item, index) => {
            if (data.customerFirstName == item.customerFirstName && data.customerLastName == item.customerLastName) {
                newData = [...newData, item]
                commitionAmount = commitionAmount + item.commission
                orderAmount = orderAmount + item.orderAmount
            }
        })

        this.props.setCustomeReportList(newData)

        this.props.setCustomerTotalSales(orderAmount)
        this.props.setCustomerTotalCommition(commitionAmount)
        // return
        this.props.navigation.navigate('CustomerReportScreen')
    }


    _renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                onPress={() => this.onPressCustomer(dataItem.item)}
                style={[msStyle.myshadow,{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    backgroundColor: '#fafaff',
                    // elevation: 13,
                    paddingHorizontal: 15,
                    paddingVertical: 15,
                    marginVertical: 7,
                    marginHorizontal: 10,
                    alignItems: 'center',
                    borderRadius: 5,
                    height: 80
                }]}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: "50%"
                }}>
                    {dataItem.item.customer_pic == undefined ?
                        <View style={{ height: 40, width: 40, borderRadius: 100, backgroundColor: 'gray', justifyContent: 'center', alignItems: 'center', marginRight: 10 }} >

                            <LinearGradient
                                start={{ x: 1, y: 1 }}
                                end={{ x: 0, y: 0 }}
                                colors={['#62a4d5', '#074674']}

                                style={{ flex: 1, width: '100%', borderRadius: 100, backgroundColor: 'gray', justifyContent: 'center', alignItems: 'center', }}
                            >
                                <Text style={[msStyle.txtStyle, { color: 'white', fontSize: 16 }]}>
                                    {dataItem.item.customerFirstName != null ? dataItem.item.customerFirstName.charAt(0) : null}
                                    {dataItem.item.customerLastName != null ? dataItem.item.customerLastName.charAt(0) : null}
                                </Text>
                            </LinearGradient>

                        </View>
                        :
                        <Image
                            style={{ height: 30, width: 30, borderRadius: 100 }}
                            source={dataItem.item.customer_pic}
                        />


                    }
                    <Text style={[msStyle.txtStyle, { color: '#302f33', fontSize: 16 }]}>
                        {dataItem.item.customerFirstName} {dataItem.item.customerLastName}
                    </Text>
                </View>

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <Text style={[msStyle.txtStyle, { color: '#302f33', fontSize: 16 }]}>
                        <Text style={[msStyle.txtStyle, { color: '#302f33', fontSize: 16 }]}>{'$'} </Text> {dataItem.item.totalSales.toFixed(2)}
                    </Text>
                    <TouchableOpacity>
                        <Image
                            style={{ height: 15, width: 15, marginLeft: 20 }}
                            source={require('../../assets/navigate.png')}
                        />
                    </TouchableOpacity>

                </View>
            </TouchableOpacity>
        )
    }

    onPressMenu = () => {
        this.props.navigation.openDrawer()
    }
    listFooterItem = (data) => {
        return (
            <View style={{ marginBottom: 200 }} />
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>

                    {/*********************************************** header ******************************************/}
                    <View style={[msStyle.myshadow2,{
                        height: 50,
                        width: '100%',
                        backgroundColor: 'white',
                        justifyContent: "center",
                        alignItems: "center",
                        flexDirection: 'row',
                        paddingHorizontal: 15
                    }]}>
                        <TouchableOpacity
                            onPress={() => { this.onPressMenu() }}
                            style={{ position: 'absolute', left: 20 }}>
                            <Image
                                source={require('../../assets/menu2.png')}
                                style={{ height: 20, width: 20, resizeMode: 'contain' }}
                            />
                        </TouchableOpacity>
                        <Text style={[msStyle.txtStyle, { fontSize: 20, color: 'black' }]}>
                            {'Dashboard'}
                        </Text>
                    </View>



                    {/* ******************************* *customers ************************************* */}

                    <View>
                        <FlatList
                            data={this.props.dashboard_list}
                            renderItem={this._renderItem}
                            keyExtractor={(item, index) => index + 'key'}
                            ListHeaderComponent={this._renderHeader}
                            ListFooterComponent={this.listFooterItem}
                        />

                    </View>

                    {
                        this.state.is_loading ?
                            <MyLoader />
                            : null
                    }
                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f8faff',
    },
    linearGradient: {
        flex: 1,
        // paddingLeft: 15,
        // paddingRight: 15,
        borderRadius: 5
    },

})



const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let jaswant = state.indexReducerJaswant;

    return {
        is_loading: common.is_loading,
        darshboard_sales_info: jaswant.darshboard_sales_info,
        dashboard_list: jaswant.dashboard_list,
        totalSalesAmount: jaswant.totalSalesAmount,
        totalCommitionAmount: jaswant.totalCommitionAmount,

    }
}

const mapDispatchToProps = (dispatch) => {

    return bindActionCreators({

        setDashboardSalesInfo: (list) => setDashboardSalesInfo(list),
        setDashboardList: (list) => setDashboardList(list),
        hitGetDashboardSales: (param) => hitGetDashboardSales(param),
        setTotalSales: (amt) => setTotalSales(amt),
        setTotalCommition: (amt) => setTotalCommition(amt),
        setCustomeReportList: (list) => setCustomeReportList(list),

        setCustomerTotalSales: (amt) => setCustomerTotalSales(amt),
        setCustomerTotalCommition: (amt) => setCustomerTotalCommition(amt),


    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen)