import React, { Component } from 'react';
import {
  Share, Image, Button, View, Text, SafeAreaView, TextInput, Dimensions, StyleSheet,
  ScrollView, SectionList, TouchableOpacity, flexDirection, FlatList, Alert, Linking
} from 'react-native';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import MyHeader from '../components/MyHeader';
import ProfilePickerComponent from '../components/ProfilePickerComponent';
import Constants, { debugLog } from '../common/Constants';
import MyButton from '../components/MyButton';
import MyInputText from '../components/MyInputText';
import { setDomain, setEmail, setLoginRespons, setPlatform } from '../redux_store/actions/indexActions';
import ProfilePopup from '../components/ProfilePopup';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SharePopup from '../components/SharePopup';
import { setRecommendationHistory, setSelectedProductListForRecommend } from '../redux_store/actions/indexActionsJaswant';




class RecommendationScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      subject: '',
      subject2: '',
      message: '',
      message2: '',
      loginData: {},
      productList: [],
      contacts: [],
      cartdetails: {},
      isMessage: false,
      isSubject: false,
      formProduct: false,
      isRedayToShare: false,
      appList: [
        { id: '1', image: require('../../assets/mail.png'), name: 'mail' },
        { id: '2', image: require('../../assets/messenger.png'), name: 'messenger' },
        { id: '3', image: require('../../assets/twitter.png'), name: 'twitter' },
        { id: '4', image: require('../../assets/pinterest.png'), name: 'pinterest' },
        { id: '5', image: require('../../assets/instagram.png'), name: 'instagram' },
        { id: '6', image: require('../../assets/whatsapp.png'), name: 'whatsapp' },
        { id: '7', image: require('../../assets/linkedin.png'), name: 'linkedin' },
        { id: '8', image: require('../../assets/mail2.png'), name: 'Text Message' },

        //  {id: '2', name: 'messenger'},
        // {id: '3', name: 'twitter'}, {id: '4', name: 'pintrest'}, 
        // {id: '5', name: 'instagram'},{id: '6', name: 'whatsapp'},
        // {id: '7', name: 'mail'},{id: '8', name: 'linkedin'}
      ],
      sharedTo: {}

    }
  }

  renderList(appCheckResults) {
    let installedApp = []
    // appcheckResults.forEach(element => {
    //    if (element.name === 'facebook') {
    //     installedApp.push({element})

    //    } else if (element.name === 'facebook messenger') {
    //     installedApp.push({element})

    //    } else if (element.name === 'twitter') {
    //     installedApp.push({element})
    //   }
    //   else if (element.name === 'pinterest') {
    //     installedApp.push({element})
    //   }
    //   else if (element.name === 'instagram') {
    //     installedApp.push({element})
    //   }
    //   else if (element.name === 'whatsapp') {
    //      installedApp.push({element})
    //   }

    // });

    // installedApp.push({id: '7', name: 'message', })
    // installedApp.push({id: '8', name: 'mail', })
    //  this.setState({ appList: installedApp })
    this.setState({ appList: appcheckResults })
    debugLog('Data-------------------------------------------------')
    debugLog(this.state.appList)
    debugLog(appCheckResults)
  }


  renderItem = ({ item }) => (

    <View style={[msStyle.myshadow2, {
      flex: 1,
      padding: 15,
      marginHorizontal: 5,
      marginVertical: 5,
      backgroundColor: 'white',
      borderRadius: 7,
    }]}>


      <Text
        style={{ flex: 1, fontWeight: 'bold', alignItems: 'flex-start', paddingBottom: 15 }}>{item.item.itemname}</Text>


      <Text >{item.item.brand}</Text>
    </View>

  );


  componentDidMount() {

  }



  onShare = async (item) => {

    var body = this.state.message.trim() + ' Products: ' + `https://www.`+`${this.state.cartdetails[0].url}`

    this.setState({ isRedayToShare: false })

    if (item.name === 'mail') {
      Linking.openURL(`mailto:${''}?subject=${this.state.subject.trim()}&body=${body}`)

      this.saveRecommendations(item.name)
      // this.props.navigation.navigate("RecommendationHistoryScreen")
    }
    else {

      try {
        const result = await Share.share({

          subject: { default: this.state.subject.trim() },

          title: this.state.subject.trim(),

          // message: this.state.subject.trim() + `\n` + this.state.message.trim() + '\n' + 'Products: ' + '\n' + productsUrl,
          // message: this.state.message.trim() + '\n' + 'Products: ' + '\n' + `${this.state.cartdetails[0].url}`,
          message: body

        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
            debugLog('activity log')

          } else {
            // shared
            debugLog('recommendation saved')
            this.saveRecommendations(item.name)
            // this.props.navigation.navigate("RecommendationHistoryScreen")

          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {
        alert(error.message);
      }
    }
  };



  createCartUrl = async () => {
    debugLog('ccccc')
    let newData = []
    this.props.selected_product_list_for_recommend.map((item, index) => {
      newData.push({
        sku: item.item.sku,
        product_id: item.item.productId,
        quantity: "1"
      })
    })

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({
      // "products": [{ "sku": "m2", "product_id": "161", "quantity": "1" }],
      "products": newData,
      "store_id": this.props.loginResponse.storeNo,
      "first_name": this.props.loginResponse.userFirstName,
      "last_name": this.props.loginResponse.userLastName,
      "email": this.props.loginResponse.userEmail,
      "platformId": this.props.loginResponse.platformId,
      // "auth_token": "3k82/9NwEQfLif1IIKvQMg==",
      "CBP": this.props.loginResponse.cbp,
      "domainname": this.props.loginResponse.domainName.includes(`www.`) ? this.props.loginResponse.domainName : `www.${this.props.loginResponse.domainName}`
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch(`${Constants.API_BASE_URL}/restapi/createshoppingcart`, requestOptions)
      .then(response => response.text())
      .then(result => {
        // debugLog('rrrrrrrrrrrrrrrrrr')
        // console.log(result)
        let json = JSON.parse(result)
        this.setState({ cartdetails: json })
        if (this.state.cartdetails[0].msg === 'successful') {
          this.setState({ isRedayToShare: !this.state.isRedayToShare })
          // debugLog(this.state.cartdetails[0].url)
        }
      })
      .catch(error => console.log('error', error));
  }



  saveRecommendations = async (name) => {

    let newData = []
    this.props.selected_product_list_for_recommend.map((item, index) => {
      newData.push({
        product_id: item.item.productId,
        brandName: item.item.brand,
        sku: item.item.sku,
        price: item.item.price,
        category_name: item.item.categories[0].category
      })
    })

    // "products": [{
    //   "productId": "1122",
    //   "brandName": "Test Brand",
    //   "sku": "11223344",
    //   "price": "1122",
    //   "category_name": "test cat"
    // }]

    let param = {
      domainName: this.props.loginResponse.domainName,
      platformId: this.props.loginResponse.platformId,
      cbpNumber: this.props.loginResponse.cbp,
      storeNumber: this.props.loginResponse.storeNo,
      uniqueId: this.props.loginResponse.uniqueId,
      subject: this.state.subject.trim(),
      cartName: "abc",// proudctList cart
      message: this.state.message.trim(),
      // message: this.props.recomendMessage.trim(),
      // mobileapi_key: "3k82\/9NwEQfLif1IIKvQMg==",
      contacts: [],
      products: newData,
      deliveryMethods: [{ "methodName": name, "count": 1 }]
    }
    debugLog(param)
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");
    // myHeaders.append("Cookie", "AWSALBTG=aIAK/mtl9EPfyyBg+vvkhUXM8sNFFRuJGYQPwTHpDLN4JlsWSyhxC5pm48KGOqsyfLTYd0UzvN8Xt3hjHSNrYpewKvNf8Lc7yQ44JYdiYugow2c3RogLvqj3izNhE+7dx7upPD29o5+Klyc5vEeHJOnuHnJP/vKf3vGaXRRlH3k2; AWSALBTGCORS=aIAK/mtl9EPfyyBg+vvkhUXM8sNFFRuJGYQPwTHpDLN4JlsWSyhxC5pm48KGOqsyfLTYd0UzvN8Xt3hjHSNrYpewKvNf8Lc7yQ44JYdiYugow2c3RogLvqj3izNhE+7dx7upPD29o5+Klyc5vEeHJOnuHnJP/vKf3vGaXRRlH3k2");

    var raw = JSON.stringify(param)
    var rawTesting = JSON.stringify({
      "domainName": "http://www.cscestorexpress.com/",
      "platformId": "BHT",
      "cbpNumber": 3,
      "storeNumber": 2336,
      "uniqueId": "001",
      "subject": "XXXX",
      "cartName": "abc",
      "message": "message123",
      "contacts": [{ "firstName": "shahzad", "lastName": "Test", "phoneNumber": "112233445566" }],
      "products": [{ "productId": "1122", "brandName": "Test Brand", "sku": "11223344", "price": "1122", "category_name": "test cat" },
      { "productId": "5566", "brandName": "Test 2", "sku": "000000", "price": "0000", "category_name": "xxxx" }],
      "deliveryMethods": [{ "methodName": "WhatsApp", "count": 1 }]
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch(`${Constants.API_BASE_URL}/admin/recommendations/save`, requestOptions)
      .then(response => response.text())
      .then(result => {
        console.log(result)
        debugLog('sucess:---------')
        alert(result)
        if (result === 'Recommendation saved.') {
          this.setState({ subject: '', subject2: '' })
          this.setState({ message: '', message2: '' })
          this.getRecommendationHistory()
          console.log('saved:::::::')
          debugLog(result)
        }
      })
      .catch(error => console.log('error', error));
  }


  getRecommendationHistory = () => {

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Cookie", "AWSALBTG=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; AWSALBTGCORS=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; SESSION=Mjg3Y2Q5ODctMzI0OS00NjhkLThhNDEtOTRkNzEzNDEwYzBl");
    var raw = ''
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    // {"cbp": 3, "createdAt": 1625066191, "domainName": "cscestorexpress.com", "platformId": "bht", "product": 
    // null, "randomkey": null, "source": "mobile", "storeNo": 2336, "uniqueId": "001", "updatedAt": 
    // 1625066194, "userEmail": "barneyrubble@testemail.com", "userFirstName": "test", "userLastName": "Testing"}

    let url_testing = `${Constants.API_BASE_URL}/test-cbp/admin/recommendations/history?uniqueId=001&cbpNumber=3&storeNumber=2336`

    // let url = `${Constants.API_BASE_URL}/admin/recommendations/history?uniqueId=${this.state.loginData.uniqueId}&cbpNumber=${this.state.loginData.cbp}&storeNumber=${this.state.loginData.storeNo}`
    let url = Constants.API_BASE_URL + "/admin/recommendations/history?uniqueId=" + this.props.loginResponse.uniqueId + "&cbpNumber=" + this.props.loginResponse.cbp + "&storeNumber=" + this.props.loginResponse.storeNo

    debugLog("my url")
    fetch(url, requestOptions)
      .then(response => response.text())
      .then(result => {
        let json = JSON.parse(result)
        this.setState({ data: json })
        this.props.setRecommendationHistory(json)

        debugLog('recommendation history navigated successfully!')
        this.props.setSelectedProductListForRecommend([])
        this.props.navigation.navigate("RecommendationHistoryScreen")
        debugLog(json)

      })
      .catch(error => console.log('error', error));

  }




  onProceed = () => {

    if (this.state.subject == '') {
      alert('Please enter Subject');
      return
    }

    else if (this.state.message == '') {
      alert('Please add message');
      return
    }
    // appListPKG: [],
    else if (this.props.selected_product_list_for_recommend.length === 0) {
      alert('Please add products to recommend ');
      return
    }

    this.createCartUrl()
    // this.setState({ isRedayToShare: !this.state.isRedayToShare })
    // this.onShare()


    //alert('Going to share');
    //  this.onShare()
    // this.props.navigation.navigate("RecommendationHistoryScreen")


  }


  render() {
    return (
      <SafeAreaView style={msStyle.container}>
        <View style={{ flexDirection: 'row', height: 60, justifyContent: 'space-between' }}>
          <TouchableOpacity
            style={{
              width: 50,
              paddingLeft: 10,
              justifyContent: "center"
            }}
            onPress={() => { //this.props.navigation.goBack();
              this.props.navigation.openDrawer()
            }}

          >
            <Image
              source={require('../../assets/menu2.png')}
              resizeMode="contain"
              style={{
                width: 30,
                height: 30
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              width: '75%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 5
            }}
          >
            <Text style={[msStyle.txtStyle, { color: 'black', fontSize: 20, fontStyle: "normal", fontWeight: '500' }]}>{"Recommendation"}</Text>

          </View>
          <TouchableOpacity style={{
            width: 50,
            paddingRight: 20,
            justifyContent: "center"
          }}
            onPress={() => { this.props.navigation.navigate("RecommendationHistoryScreen") }}

          >
            <Image
              source={require('../../assets/historyIcon.png')}
              resizeMode="contain"
              style={{
                width: 30,
                height: 30
              }}
            />
          </TouchableOpacity>
        </View>

        {/* MARK:  body */}
        <View style={{ flex: 1 }} >

          <View style={msStyle.container}>
            <View style={{
              margin: 20,
              padding: 10,
              // flex: 1

            }}>

              <Text style={[msStyle.txtStyle, { color: 'black', fontWeight: 'bold', paddingBottom: 10 }]}>
                {"Subject"}
              </Text>
              <TouchableOpacity
                onPress={() => this.setState({ isSubject: !this.state.isSubject })}
                style={{ height: 40, borderWidth: 1, borderRadius: 7, padding: 10, justifyContent: 'center' }} >
                <Text>
                  {this.state.subject.trim()}
                </Text>
              </TouchableOpacity>

              <Text style={[msStyle.txtStyle, { color: 'black', fontWeight: 'bold' }]}>{"Product"}</Text>
              <View style={{
                borderWidth: 1.0,
                borderRadius: 7,
                padding: 5,
                height: 40,
                marginTop: 10,
                marginBottom: 10,
                flexDirection: "row",
              }}>
                <TouchableOpacity
                  style={{
                    width: 50,
                    paddingLeft: 10,
                    justifyContent: "center"
                  }}
                >
                  <Image
                    source={require('../../assets/downArrow.png')}
                    style={{
                      width: 15,
                      height: 15,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>

                <Text style={[msStyle.txtStyle,
                { color: 'black', fontSize: 15, flex: 1 }]}>
                  {"Product Search"}</Text>


                <TouchableOpacity
                  style={{
                    width: 30,
                    justifyContent: "center",
                  }}
                  onPress={() => { //alert('Product Screen')
                    this.props.navigation.navigate('ProductScreen')

                  }}
                >
                  <Image
                    source={require('../../assets/searchIcon.png')}
                    style={{
                      width: 30,
                      height: 30,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>

              </View>


              <Text style={[msStyle.txtStyle, { color: 'black', fontWeight: 'bold' }]}>{"Message"}</Text>
              <TouchableOpacity
                onPress={() => { this.setState({ isMessage: !this.state.isMessage }) }}
                style={{ borderWidth: 2, padding: 8, marginVertical: 5, borderColor: 'gray', 
                backgroundColor: 'white', borderRadius: 10, flexDirection: 'row', minHeight: 40 }} >
                <Text style={[msStyle.txtStyle, { fontSize: 18 }]}>{this.state.message.trim()}</Text>
                <Image
                  source={require('../../assets/add.png')}
                  style={{ width: 30, height: 30, resizeMode: 'contain', position: 'absolute', right: 5, bottom: 2.5 }}
                />
              </TouchableOpacity>



              <View >
                <FlatList
                  contentContainerStyle={{ marginTop: 10, paddingBottom: 20, }}
                  data={this.props.selected_product_list_for_recommend}
                  renderItem={this.renderItem}
                  keyExtractor={(item, index) => "key" + index}
                  showsVerticalScrollIndicator={false}
                />

              </View>



            </View>
            <TouchableOpacity
              style={{
                position: 'absolute',
                bottom: 20,
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: Colors.themeColor,
                borderRadius: 10,
                elevation: 10,
                zIndex: 5,
                height: 50,
                width: Dimensions.get('window').width - 50
              }}
              onPress={() => { this.onProceed() }} >

              <Text style={[msStyle.txtStyle, { color: 'white', fontSize: 18 }]} >
                {'DELIVERY METHOD'}
              </Text>

            </TouchableOpacity>
          </View>
        </View>



        {
          this.state.isSubject ?
            <ProfilePopup
              title='Subject'
              maxLength={50}
              value={this.state.subject2}
              inputStyle={{ height: 100 }}
              onChangeText={(txt) => {
                var regex = /^[0-9a-zA-Z(-/  %)]*$/
                regex.test(txt) ? this.setState({ subject2: txt })
                  : (txt = txt.substring(0, txt.length - 1),
                    this.setState({ subject: txt }),
                    alert('Please enter aplhabate only'))
              }}
              onPress={() => {
                this.setState({ subject: this.state.subject2 })
                this.setState({ isSubject: !this.state.isSubject })
              }}
              onCancel={() => {
                // this.setState({ subject: '' })
                this.setState({ isSubject: !this.state.isSubject })
              }}
            />
            : null
        }

        {
          this.state.isMessage ?
            <ProfilePopup
              title='Type Here'
              maxLength={250}
              value={this.state.message2}
              inputStyle={{ height: 100 }}
              onChangeText={(txt) => { this.setState({ message2: txt }) }}
              onPress={() => {
                this.setState({ message: this.state.message2 })
                this.setState({ isMessage: !this.state.isMessage })

              }}
              onCancel={() => {
                // this.setState({ message: '' })
                this.setState({ isMessage: !this.state.isMessage })
              }}
            />
            : null
        }
        {
          this.state.isRedayToShare ?
            <SharePopup
              title='Select Share Method'
              appList={this.state.appList}
              inputStyle={{ height: 100 }}
              onPress={this.onShare}
              onCancel={() => this.setState({ isRedayToShare: false })}
            />
            : null
        }

      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  absoluteStyle: {
    position: 'absolute',
    backgroundColor: Colors.whiteText,
    alignSelf: 'center',
    width: width - 30,
    height: height - 70,
    elevation: 10,
    zIndex: 5,
    bottom: 15,
    borderRadius: 5
  },

})


const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    loginResponse: common.loginResponse,
    selected_product_list_for_recommend: jaswant.selected_product_list_for_recommend,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    // setIsDrawer: (domain) => setIsDrawer(domain),

    setLoginRespons: (res) => setLoginRespons(res),
    setRecommendationHistory: (list) => setRecommendationHistory(list),
    setSelectedProductListForRecommend: (list) => setSelectedProductListForRecommend(list)

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(RecommendationScreen)