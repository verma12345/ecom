import { StackActions } from '@react-navigation/routers';
import React, { Component } from 'react';
import { View, Image, BackHandler, StatusBar, SafeAreaView, } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { debugLog } from '../common/Constants';
import Prefs, { getPrefs, setPrefs } from '../common/Prefs';
import { setDomain, setEmail, setPlatform } from '../redux_store/actions/indexActions';
import { Platform } from 'react-native';

class SplashScreen extends React.Component {

  componentWillUnmount() {

    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
  }
  backPressed = () => {
    this.props.navigation.goBack();
    return true;
  };



  componentDidMount() {
   

    BackHandler.addEventListener('hardwareBackPress', this.backPressed);

    setTimeout(() => {
      getPrefs('ONE_TIME_SCREEN').then((value) => {
        if (value != '' && value != undefined) {
          getPrefs('SIGINED_IN').then((isRegister) => {
            if (isRegister != '' && isRegister != undefined) {
              this.props.navigation.navigate('ProfileScreen1')
            } else {
              this.props.navigation.navigate('SignInScreen')
            }
          });
        } else {
          this.props.navigation.navigate('OneTimeScreen')
        }
      });
    }, 1000);
  }


  render() {
    return (

      <SafeAreaView style={{ flex: 1, }}>
        <View style={{ flex: 1, }} >
          <StatusBar hidden />
          <Image
            source={require('../../assets/splash_bg.png')}
            style={{ flex: 1, width: '100%', resizeMode: 'cover' }}
          />
        </View>
      </SafeAreaView>
    )
  }
}


const mapStateToProps = (state) => {
  let common = state.indexReducer
  return {
    domain: common.domain,
    email: common.email,
    platform: common.platform,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    setDomain: (domain) => setDomain(domain),
    setEmail: (email) => setEmail(email),
    setPlatform: (platform) => setPlatform(platform),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen)



//32, 34, 35, 57, 58, 62, 88, 103,