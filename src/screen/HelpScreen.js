import React, { Component } from 'react';
import {
  Button, View, Text, SafeAreaView, Dimensions, StyleSheet,
  TouchableOpacity, PixelRatio, Alert, Image, ActivityIndicator,
} from 'react-native';
import WebView from 'react-native-webview';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';

class HelpScreen extends Component {
  constructor(props) {
    super(props);

  }


  renderLoadingView() {
    return (
      <View style={{
        borderRadius: 10,
        backgroundColor: "white",
        flex: 1,

      }}>

        <ActivityIndicator
          size={"large"}
          color={Colors.themeColor}
          hidesWhenStopped={true}
        />

      </View>
    );
  }

  onGoBack = () => {
    this.props.navigation.goBack()
  }
9628808129
  render() {
    return (
      <SafeAreaView style={msStyle.container}>
        <View style={msStyle.container}>

          <View style={{ height: 40, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', elevation: 3, zIndex: 2 }} >
            <TouchableOpacity
              onPress={() => { this.onGoBack() }}
              style={{ position: 'absolute', left: 10 }} >
              <Image
                source={require('../../assets/back.png')}
                style={{ height: 25, width: 25, tintColor: 'gray', resizeMode: 'contain' }} />
            </TouchableOpacity>
            <Text style={[msStyle.txtStyle, { fontSize: 18 }]}>{'Help'}</Text>
          </View>


          <WebView
            // userAgent="Mozilla/5.0 Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"
            source={{ uri: "https://commercescience.freshdesk.com/support/solutions" }}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            originWhitelist={['*']}
            renderLoading={this.renderLoadingView}
            // onNavigationStateChange={this.handleWebViewNavigationStateChange}
            startInLoadingState={true}

          // onLoadEnd={this.loadEnd.bind(this)}

          />
        </View>
      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  portrateMode: {
    flex: 1,
    backgroundColor: 'white',
    margin: 10,
    padding: 3,
    elevation: 10,
    borderRadius: 10,
    marginTop: -PixelRatio.getPixelSizeForLayoutSize(25)
  },

})

const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({



  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(HelpScreen)