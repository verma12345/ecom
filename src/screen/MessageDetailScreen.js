import React, { Component, useState } from 'react';
import { Image, Button, View, Text, SafeAreaView, TextInput, Dimensions, StyleSheet, ScrollView, SectionList, TouchableOpacity, flexDirection, FlatList } from 'react-native';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import { debugLog } from '../common/Constants';
import { connect } from 'react-redux';
import { searchMessageHistoryDetails, setMessageHistoryDetails } from '../redux_store/actions/indexActionsJaswant';
import { bindActionCreators } from 'redux';
import SearchHeader from '../components/SearchHeader';


class MessageDetailScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.message_history_details,
            deliveryMethod: this.props.message_history_details.deliveryMethods,
            whatsappCount: 0,
            messengerCount: 0,
            instagramCount: 0,
            pinterestCount: 0,
            mailCount: 0,
            messageCount: 0,
            isSearch: false
        }
    }

    getDeliveryMethod_count = () => {

        this.state.deliveryMethod.map((y) => {
            debugLog(y.methodName)
            switch (y.methodName) {
                case 'WhatsApp':
                    this.setState({ whatsappCount: y.count })
                    break;
                case 'Massenger':
                    this.setState({ messengerCount: y.count })
                    break;
                case 'InstaGram':
                    this.setState({ instagramCount: y.count })
                    break;
                case 'Pintrest':
                    this.setState({ pintrestCount: y.count })
                    break;
                case 'Mail':
                    this.setState({ mailCount: y.count })
                    break;
                case 'Message':
                    this.setState({ messageCount: y.count })
                    break;
                default:
                    debugLog('Nothing')
                    break;
            }

        })
    }


    getParsedDate(strDate) {

        var strSplitDate = String(strDate).split(' ');
        var date = new Date(strSplitDate[0]);
        // alert(date);
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!

        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        date = dd + "/" + mm + "/" + yyyy;
        return date.toString();
    }


    renderContactsItem = (contact) => {
        return (
            <View style={{ flex: 1 }}>
                <Text style={{ fontWeight: 'normal', color: 'grey' }}>
                    {contact.item.firstName + ' ' + contact.item.lastName + '  ' + contact.item.phoneNumber}</Text>
            </View>
        )
    }

    componentDidMount() {
        debugLog(this.props.message_history_details,)
        this.getDeliveryMethod_count()
    }


    render() {
        return (
            <SafeAreaView style={msStyle.container}>



                {/* MARK:  body */}
                <View style={{ flex: 1 }}>

                    <SearchHeader
                        title='Message Details'
                        placeholder="Search"
                        isInput={this.state.isSearch}
                        onChangeText={(txt) => { this.props.searchMessageHistoryDetails(txt) }}
                        onGoBack={() => this.props.navigation.goBack()}
                        onSearch={() => { this.setState({ isSearch: !this.state.isSearch }) }} />
                        

                    <Text style={{ justifyContent: 'flex-end', alignSelf: 'flex-end', padding: 10, color: 'gray' }}>
                        {this.getParsedDate(this.props.message_history_details.createdAt)}</Text>
                    <View style={{ flex: .5, padding: 25 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{'Contacts'}</Text>
                        <View style={(msStyle.container)}>
                            <FlatList style={styles.list}
                                data={this.props.message_history_details.contacts}
                                renderItem={this.renderContactsItem}
                                keyExtractor={item => item.id}
                            />
                        </View>

                    </View>
                    <View style={{ flex: .3, padding: 25 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{'Message'}</Text>
                        <Text>{this.props.message_history_details.message}</Text>
                    </View>

                    <View style={{ flex: .3, padding: 25 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'gray' }}>{'Delivery Method'}</Text>
                        <View
                            style={{ flex: .1, padding: 10, flexDirection: 'row', justifyContent: 'space-evenly' }}>
                            {/* item 1 */}
                            <TouchableOpacity>
                                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                                    <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                                        {this.state.whatsappCount}
                                    </Text>
                                </View>
                                <Image
                                    source={require('../../assets/whatsapp.png')}
                                    resizeMode="contain"
                                    style={{
                                        width: 30,
                                        height: 30
                                    }}
                                />
                            </TouchableOpacity>

                            {/* item 2 */}
                            <TouchableOpacity>
                                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                                    <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                                        {this.state.messengerCount}
                                    </Text>
                                </View>
                                <Image
                                    source={require('../../assets/messenger.png')}
                                    resizeMode="contain"
                                    style={{
                                        width: 30,
                                        height: 30
                                    }}
                                />
                            </TouchableOpacity>
                            {/* item 3 */}
                            <TouchableOpacity>
                                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                                    <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                                        {this.state.instagramCount}
                                    </Text>
                                </View>
                                <Image
                                    source={require('../../assets/instagram.png')}
                                    resizeMode="contain"
                                    style={{
                                        width: 30,
                                        height: 30
                                    }}
                                />
                            </TouchableOpacity>
                            {/* item 4 */}
                            <TouchableOpacity>
                                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                                    <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                                        {this.state.pinterestCount}
                                    </Text>
                                </View>
                                <Image
                                    source={require('../../assets/pinterest.png')}
                                    resizeMode="contain"
                                    style={{
                                        width: 30,
                                        height: 30
                                    }}
                                />
                            </TouchableOpacity>
                            {/* item 5 */}
                            <TouchableOpacity>
                                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                                    <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                                        {this.state.mailCount}
                                    </Text>
                                </View>
                                <Image
                                    source={require('../../assets/mail.png')}
                                    resizeMode="contain"
                                    style={{
                                        width: 30,
                                        height: 30
                                    }}
                                />
                            </TouchableOpacity>
                            {/* item 6 */}
                            <TouchableOpacity>
                                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                                    <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                                        {this.state.messageCount}
                                    </Text>
                                </View>
                                <Image
                                    source={require('../../assets/mail2.png')}
                                    resizeMode="contain"
                                    style={{
                                        width: 30,
                                        height: 30
                                    }}
                                />
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>

            </SafeAreaView>
        )
    }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
    absoluteStyle: {
        position: 'absolute',
        backgroundColor: Colors.whiteText,
        alignSelf: 'center',
        width: width - 30,
        height: height - 70,
        elevation: 10,
        zIndex: 5,
        bottom: 15,
        borderRadius: 5
    },
    list: {
        flex: 1,
        padding: 15
    },
    nofitfy: {
        zIndex: 2,
        elevation: 2,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 7,
        right: -7,
        top: -7,
        borderRadius: 30,
        backgroundColor: 'red'
    }

})


const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let jaswant = state.indexReducerJaswant;

    return {
        loginResponse: common.loginResponse,
        message_history_details: jaswant.message_history_details,
    }
}

const mapDispatchToProps = (dispatch) => {

    return bindActionCreators({
        setMessageHistoryDetails: (list) => setMessageHistoryDetails(list),
        searchMessageHistoryDetails: (txt) => searchMessageHistoryDetails(txt),

    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(MessageDetailScreen)
