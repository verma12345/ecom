import React, { Component } from 'react';
import {
  Share, Image, Button, View, Text, SafeAreaView, TextInput, Dimensions, StyleSheet,
  ScrollView, SectionList, TouchableOpacity, flexDirection, FlatList, Alert, Linking
} from 'react-native';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import MyHeader from '../components/MyHeader';
import ProfilePickerComponent from '../components/ProfilePickerComponent';
import Constants, { debugLog } from '../common/Constants';
import MyButton from '../components/MyButton';
import MyInputText from '../components/MyInputText';
import { setDomain, setEmail, setLoginRespons, setPlatform } from '../redux_store/actions/indexActions';
import { getPixelSizeForLayoutSize } from 'react-native/Libraries/Utilities/PixelRatio';
import { NativeViewGestureHandler } from 'react-native-gesture-handler';
import { ProfileOptionMemo } from '../components/ProfileOption';
import ProfilePopup from '../components/ProfilePopup';
import { set } from 'react-native-reanimated';
import { getPrefs } from '../common/Prefs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SharePopup from '../components/SharePopup';
import { serMessageHistoryList } from '../redux_store/actions/indexActionsJaswant';



class MessageScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {

      // DATA: this.props.route.params.data,

      subject: '',
      message: '',
      subject2: '',
      message2: '',
      loginData: {},
      productList: [],
      contacts: [],
      isMessage: false,
      isSubject: false,
      formProduct: false,
      isRedayToShare: false,
      appList: [
        { id: '1', image: require('../../assets/mail.png'), name: 'mail' },
        { id: '2', image: require('../../assets/messenger.png'), name: 'messenger' },
        { id: '3', image: require('../../assets/twitter.png'), name: 'twitter' },
        { id: '4', image: require('../../assets/pinterest.png'), name: 'pinterest' },
        { id: '5', image: require('../../assets/instagram.png'), name: 'instagram' },
        { id: '6', image: require('../../assets/whatsapp.png'), name: 'whatsapp' },
        { id: '7', image: require('../../assets/linkedin.png'), name: 'linkedin' },
        { id: '8', image: require('../../assets/mail2.png'), name: 'Text Message' },

        //  {id: '2', name: 'messenger'},
        // {id: '3', name: 'twitter'}, {id: '4', name: 'pintrest'}, 
        // {id: '5', name: 'instagram'},{id: '6', name: 'whatsapp'},
        // {id: '7', name: 'mail'},{id: '8', name: 'linkedin'}
      ],
    }
  }

  renderItem = ({ item }) => (
    <View style={{
      flex: 1,
      padding: 0,
      justifyContent: 'center',
      paddingBottom: 6,
      // height: 110
    }}>
      <View style={{
        flex: 1,
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 7,
        justifyContent: 'space-evenly',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.4,
        marginBottom: 10,
        elevation: 2,
        position: 'relative'
      }}>


        <Text
          style={{ flex: 1, fontWeight: 'bold', alignItems: 'flex-start', paddingBottom: 15 }}>{item.item.itemname}</Text>
        <Text >{item.item.brand}</Text>
      </View>
    </View>
  );


  componentDidMount() {
    // this.getLoginCrediential()
  }


  onShare = async (item) => {

    this.setState({ isRedayToShare: false })

    if (item.name === 'mail') {
      var body = this.state.message.trim()

      Linking.openURL(`mailto:${''}?subject=${this.state.subject.trim()}&body=${body}`)
      this.saveMessage(item)

    }
    else {


      try {
        const result = await Share.share({
          // title: this.state.subject,
          subject: this.state.subject,
          message: `${this.state.subject.trim()}\n${this.state.message.trim()}`

        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType

          } else {
            // shared
            // debugLog('recommendation saved')
            this.saveMessage(item)

          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {
        alert(error.message);
      }
    }
  };

  saveMessage = async (item) => {

    // {"cbp": 3, "createdAt": 1625066191, "domainName": "cscestorexpress.com", "platformId": "bht", "product": null, "randomkey": null, "source": "mobile", "storeNo": 2336, "uniqueId": "001", "updatedAt": 
    // 1625066194, "userEmail": "barneyrubble@testemail.com", "userFirstName": "test", "userLastName": "Testing"}

    let param = {
      domainName: this.props.loginResponse.domainName,
      platformId: this.props.loginResponse.platformId,
      cbpNumber: this.props.loginResponse.cbp,
      storeNumber: this.props.loginResponse.storeNo,
      uniqueId: this.props.loginResponse.uniqueId,
      subject: this.state.subject.trim(),
      cartName: "abc",// proudctList cart
      message: this.state.message.trim(),
      contacts: [],
      deliveryMethods: [{ "methodName": item.name, "count": 1 }]
    }

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");
    // myHeaders.append("Cookie", "AWSALBTG=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; AWSALBTGCORS=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; SESSION=YTNhZmFiMjItOWI0Yy00NGQxLWJmNDItYmE3MDI2NjY5OWQy");

    var rawdata = JSON.stringify(param)
    var raw = JSON.stringify({
      "domainName": "http://www.cscestorexpress.com/",
      "platformId": "BHT",
      "cbpNumber": 3,
      "storeNumber": 2336,
      "uniqueId": "001",
      "subject": "XXXX",
      "cartName": "abc",
      "message": "message",
      "contacts": [{ "firstName": "shahzad", "lastName": "Test", "phoneNumber": "112233445566" }],
      "deliveryMethods": [{ "methodName": "WhatsApp", "count": 5 }]
    })

    debugLog(param)

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: rawdata,
      redirect: 'follow'
    };

    fetch(`${Constants.API_BASE_URL}/admin/recommendations/save`, requestOptions)
      .then(response => response.text())
      .then(result => {
        console.log(result)
        alert(result)
        this.setState({ subject: '', subject2: '' })
        this.setState({ message: '', message2: '' })
        this.getMessageHistory()
        console.log('saved++++++++++++++')
        debugLog(result)
      })
      .catch(error => console.log('error', error));
  }


  getMessageHistory = () => {

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Cookie", "AWSALBTG=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; AWSALBTGCORS=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; SESSION=Mjg3Y2Q5ODctMzI0OS00NjhkLThhNDEtOTRkNzEzNDEwYzBl");
    var raw = ''
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    // {"cbp": 3, "createdAt": 1625066191, "domainName": "cscestorexpress.com", "platformId": "bht", "product": 
    // null, "randomkey": null, "source": "mobile", "storeNo": 2336, "uniqueId": "001", "updatedAt": 
    // 1625066194, "userEmail": "barneyrubble@testemail.com", "userFirstName": "test", "userLastName": "Testing"}

    // let urlTest = `${Constants.API_BASE_URL}/cbp/admin/recommendations/history?uniqueId=001&cbpNumber=3&storeNumber=2336`

    let url = `${Constants.API_BASE_URL}/admin/recommendations/history?uniqueId=${this.props.loginResponse.uniqueId}&cbpNumber=${this.props.loginResponse.cbp}&storeNumber=${this.props.loginResponse.storeNo}`

    debugLog("my url")

    fetch(url, requestOptions)
      .then(response => response.text())
      .then(result => {
        let json = JSON.parse(result)
        // this.setState({ data: json })  set thi in redux
        this.props.serMessageHistoryList(json)
        this.props.navigation.navigate("MessageHistoryScreen")

        debugLog(json)

      })
      .catch(error => console.log('error', error));

  }


  onProceed = () => {

    if (this.state.subject == '') {
      alert('Please enter Subject');
    }

    else if (this.state.message == '') {
      alert('Please add message');
      return
    }

    else {
      this.setState({ isRedayToShare: !this.state.isRedayToShare })
    }


    //alert('Going to share');
    //  this.onShare()
    // this.props.navigation.navigate("RecommendationHistoryScreen")


  }

  render() {
    debugLog('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    debugLog(this.props.loginResponse)
    return (
      <SafeAreaView style={msStyle.container}>
        <View style={{ flexDirection: 'row', height: 60, justifyContent: 'space-between' }}>
          <TouchableOpacity
            style={{
              width: 50,
              paddingLeft: 10,
              justifyContent: "center"
            }}
            onPress={() => { //this.props.navigation.goBack();
              this.props.navigation.openDrawer()
            }}

          >
            <Image
              source={require('../../assets/menu2.png')}
              resizeMode="contain"
              style={{
                width: 30,
                height: 30
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              width: '75%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 5
            }}
          >
            <Text style={[msStyle.txtStyle, { color: 'black', fontSize: 20, fontStyle: "normal", fontWeight: '500' }]}>{"Message"}</Text>

          </View>
          <TouchableOpacity style={{
            width: 50,
            paddingRight: 20,
            justifyContent: "center"
          }}
            onPress={() => { this.props.navigation.navigate("MessageHistoryScreen") }}

          >
            <Image
              source={require('../../assets/historyIcon.png')}
              resizeMode="contain"
              style={{
                width: 30,
                height: 30
              }}
            />
          </TouchableOpacity>
        </View>

        {/* MARK:  body */}
        <ScrollView>

          <View style={msStyle.container}>
            <View style={{
              margin: 20,
              padding: 10,
              // flex: 1

            }}>

              <Text style={[msStyle.txtStyle, { color: 'black', fontWeight: 'bold', paddingBottom: 10 }]}>
                {"Subject"}
              </Text>
              <TouchableOpacity
                onPress={() => this.setState({ isSubject: !this.state.isSubject })}
                style={{ height: 40, borderWidth: 1, borderRadius: 7, padding: 10, justifyContent: 'center' }} >
                <Text>
                  {this.state.subject.trim()}
                </Text>
              </TouchableOpacity>

              <View>
                <Text style={[msStyle.txtStyle, {
                  color: 'black', fontWeight: 'bold',
                  paddingTop: 10,
                }]}>{"Message"}</Text>

                <TouchableOpacity
                  onPress={() => { this.setState({ isMessage: !this.state.isMessage }) }}
                >
                  <View style={{
                    borderWidth: 1.0,
                    borderRadius: 7,
                    padding: 5,
                    height: 175,
                    marginTop: 10,
                    marginBottom: 10,
                    flexDirection: "row",
                  }}>

                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                        borderRadius: 5,
                        flex: 1
                      }}
                    >
                      <Text style={[msStyle.txtStyle, {
                        color: 'black', fontSize: 15,
                        fontStyle: "normal", flex: 1
                      }]}>{this.state.message.trim()}</Text>

                    </View>
                    <TouchableOpacity
                      style={{
                        width: 30,
                        justifyContent: "center",
                        bottom: -65,

                      }}
                      onPress={() => { this.setState({ isMessage: !this.state.isMessage }) }}
                    >
                      <Image
                        source={require('../../assets/add.png')}
                        style={{
                          width: 30,
                          height: 30,
                          resizeMode: 'contain'
                        }}
                      />
                    </TouchableOpacity>

                  </View>
                </TouchableOpacity>
              </View>


              <MyButton
                title='Share'
                onPress={() => { this.onProceed() }}
                titleStyle={{ color: Colors.whiteText, fontWeight: 'bold' }}
                containerStyle={{ marginTop: 100, }}
              />
            </View>
          </View>
        </ScrollView>
        {
          this.state.isSubject ?
            <ProfilePopup
              title='Subject'
              maxLength={50}
              value={this.state.subject2}
              inputStyle={{ height: 100 }}
              onChangeText={(txt) => {
                var regex = /^[0-9a-zA-Z(-/  %)]*$/
                regex.test(txt) ? this.setState({ subject2: txt })
                  : (txt = txt.substring(0, txt.length - 1),
                    this.setState({ subject: txt }),
                    alert('Please enter aplhabate only'))
              }}
              onPress={() => {
                this.setState({ subject: this.state.subject2 })
                this.setState({ isSubject: !this.state.isSubject })
              }}
              onCancel={() => {
                // this.setState({ subject: '' })
                this.setState({ isSubject: !this.state.isSubject })
              }}
            />
            : null
        }

        {
          this.state.isMessage ?
            <ProfilePopup
              title='Type Here'
              maxLength={250}
              value={this.state.message2}
              inputStyle={{ height: 100 }}
              onChangeText={(txt) => { this.setState({ message2: txt }) }}
              onPress={() => {
                this.setState({ message: this.state.message2 })
                this.setState({ isMessage: !this.state.isMessage })

              }}
              onCancel={() => {
                // this.setState({ message: '' })
                this.setState({ isMessage: !this.state.isMessage })
              }}
            />
            : null
        }
        {
          this.state.isRedayToShare ?
            <SharePopup
              title='Select Share Method'
              appList={this.state.appList}
              inputStyle={{ height: 100 }}
              onPress={this.onShare}
              onCancel={() => this.setState({ isRedayToShare: false })}
            />
            : null
        }

      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  absoluteStyle: {
    position: 'absolute',
    backgroundColor: Colors.whiteText,
    alignSelf: 'center',
    width: width - 30,
    height: height - 70,
    elevation: 10,
    zIndex: 5,
    bottom: 15,
    borderRadius: 5
  },

})


const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    loginResponse: common.loginResponse,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    serMessageHistoryList: (list) => serMessageHistoryList(list),

    setLoginRespons: (res) => setLoginRespons(res),
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(MessageScreen)