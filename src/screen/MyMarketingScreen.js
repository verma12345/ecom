import React, { Component } from 'react';
import {
  Button, View, Text, SafeAreaView, Dimensions, StyleSheet,
  TouchableOpacity, PixelRatio, Alert,
  ImageBackground, Image, ScrollView, FlatList, Share, PermissionsAndroid
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import MyHeader from '../components/MyHeader';
import MarketingComponent from '../components/MarketingComponent';
import { launchImageLibrary, launchCamera } from 'react-native-image-picker';
import {
  setMyMarketingImage, setMyMarketingImageEmptyList, setMyMarketingImageList, setMyMarketingVideo, setMyMarketingVideoEmptyList,
  setMarketingIndex,
  setMyMarketingVideoList,
  setMarketingDocument,
  setIsMarketingDocument,
  setMyMarketingDocumentUrl,
  setSelctedVideoUrl,
  setMyMarketingUpdateVideoList,
  setMyMarketingUpdateImageList,
  setMyMarketingSelectAllImageList,
  setMyMarketingSelectAllVideoList
} from '../redux_store/actions/indexActionsJaswant';
import ProfilePopup from '../components/ProfilePopup';
import Constants, { debugLog } from '../common/Constants';
import { hitUploadDocumentOnASW, hitUploadImageOnASW, hitUploadVideoOnASW } from '../redux_store/actions/indexActionsApi';
import moment from 'moment';
import { RNS3 } from 'react-native-aws3';
import DocumentPicker from 'react-native-document-picker';
import VideoPlayerComponent from '../components/VideoPlayerComponent';
import { sendEmail, shareViaWhatsapp } from '../common/common';
import SharePopupJaswant from '../components/SharePopupJaswant';
import { setDocumentProgress, setImageProgress, setVideoProgress } from '../redux_store/actions/indexActions';
import { msStyle } from '../common/MyStyle';

class MyMarketingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVideoPlayer: false,
      video_url: null,
      isVideo: false,
      isImage: false,
      isFile: false,
      isRedayToShare: false,


      appList: [
        { id: '1', image: require('../../assets/whatsapp.png'), name: 'WhatsApp' },
        { id: '2', image: require('../../assets/messenger.png'), name: 'messenger' },
        { id: '3', image: require('../../assets/twitter.png'), name: 'twitter' },
        { id: '4', image: require('../../assets/pinterest.png'), name: 'pinterest' },
        { id: '5', image: require('../../assets/instagram.png'), name: 'instagram' },
        { id: '6', image: require('../../assets/mail.png'), name: 'gmail' },
        { id: '7', image: require('../../assets/linkedin.png'), name: 'linkedin' },
        // {id: '1', image: require('../../assets/whatsapp.png'),name:'WhatsApp'},

        //  {id: '2', name: 'messenger'},
        // {id: '3', name: 'twitter'}, {id: '4', name: 'pintrest'}, 
        // {id: '5', name: 'instagram'},{id: '6', name: 'whatsapp'},
        // {id: '7', name: 'mail'},{id: '8', name: 'linkedin'}
      ],
    }

    this.selectAllImage = false
    this.selectAllVideo = false
  }

  onGoBack = () => {
    this.props.navigation.goBack()
  }


  showAlert(index) {
    Alert.alert(
      'Upload Image',
      'Please choose Camera or Gallery',
      [
        {
          text: 'Camera',
          onPress: () => {
            this.openCameraPicker(index);
          },
        },
        {
          text: 'Gallery',
          onPress: () => {
            this.openImagePicker(index);
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  }


  openCameraPicker = (index) => {
    let options = {
      mediaType: 'photo',
    };
    launchCamera(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        alert('You cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      this.props.setMyMarketingImage(response)
      this.props.setMarketingIndex(index)

    });
  };

  openImagePicker = (index) => {
    debugLog(index)
    let options = {
      mediaType: 'photo',
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        alert('You cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      this.props.setMyMarketingImage(response)
      this.props.setMarketingIndex(index)
    });
  };

  openVideoPicker = (index) => {
    let options = {
      mediaType: 'video',
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('You cancelled library picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      this.props.setMyMarketingVideo(response)
      this.props.setMarketingIndex(index)
    });
  };

  uploadImageOnASW1 = (image) => {
    let params = {
      image: image,
      accessKey: this.props.accessAndSecretsKey.accessKey,
      secretKey: this.props.accessAndSecretsKey.secretKey,
      platformId: this.props.loginResponse.platformId,
      storeNo: this.props.loginResponse.storeNo
    }
    this.setState({ isImage: true })
    this.props.hitUploadImageOnASW(params).then(res => {
      let { bucket, etag, key, location } = res.body.postResponse;
      // debugLog(location)
      this.props.setMyMarketingImageList(location, this.props.myMarketingIndex)
      this.props.setMyMarketingImage(null)
      this.setState({ isImage: false })


    })
  }


  uploadVideoOnASW1 = (video) => {

    let params = {
      video: video,
      accessKey: this.props.accessAndSecretsKey.accessKey,
      secretKey: this.props.accessAndSecretsKey.secretKey,
      platformId: this.props.loginResponse.platformId,
      storeNo: this.props.loginResponse.storeNo
    }
    this.setState({ isVideo: true })

    this.props.hitUploadVideoOnASW(params).then(res => {
      let { bucket, etag, key, location } = res.body.postResponse;
      debugLog('response video>>>>')
      debugLog(location)
      debugLog('response video>>>>')

      this.props.setMyMarketingVideoList(location, this.props.myMarketingIndex)
      this.props.setMyMarketingVideo(null)
      this.props.setVideoProgress(0)
      this.setState({ isVideo: false })

    })
  }

  uploadDocumentOnASW1 = (filePath) => {

    let params = {
      video: filePath,
      accessKey: this.props.accessAndSecretsKey.accessKey,
      secretKey: this.props.accessAndSecretsKey.secretKey,
      platformId: this.props.loginResponse.platformId,
      storeNo: this.props.loginResponse.storeNo
    }
    this.setState({ isFile: true })
    this.props.hitUploadDocumentOnASW(params).then(res => {
      let { bucket, etag, key, location } = res.body.postResponse;
      // debugLog(location)
      this.props.setMyMarketingDocumentUrl(location)
      this.props.setIsMarketingDocument(false)
      this.setState({ isFile: false })

      debugLog(location)
    })
  }


  renderItemImage = (data) => {
    return (
      <View
        style={[styles.listStyle, this.props.containerStyle]}
      >

        <Text style={[styles.txt, this.props.titleStyle]}>
          {'Image'}
        </Text>

        <View style={{ padding: 10, backgroundColor: data.item.isSelected ? Colors.themeColor : "white", elevation: 5, borderRadius: 100 }} >
          {data.item.image_url != null ?
            <TouchableOpacity
              onLongPress={() => this.props.setMyMarketingUpdateImageList(data.index)}
            >
              <ImageBackground
                source={{ uri: data.item.image_url }}
                borderRadius={100}
                style={{ height: 150, width: 150, resizeMode: 'contain' }}
              />
            </TouchableOpacity> :
            <View style={{ height: 130, width: 130, borderRadius: 360, backgroundColor: Colors.placeHolderColor }} />
          }


          <TouchableOpacity
            onPress={() => this.showAlert(data.index)}
            style={styles.buttonStyleCam} >
            <Image
              source={require('../../assets/camera.png')}
              style={{ height: 20, width: 20, tintColor: Colors.themeColor, resizeMode: 'contain' }}
            />
          </TouchableOpacity>



        </View>

        {data.item.image_url != null ?
          <TouchableOpacity
            onPress={() => { this.onSelectImage(data.index) }}
            style={{ elevation: 6, zIndex: 1, position: 'absolute', bottom: 0, right: 10, padding: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >

            <View style={{ marginRight: 10, width: 18, height: 18, borderRadius: 30, backgroundColor: "gray", justifyContent: 'center', alignItems: 'center' }} >
              <View style={{ width: 15, height: 15, borderRadius: 30, backgroundColor: data.item.isSelected ? Colors.themeColor : 'white' }} />
            </View>

            <Text style={[msStyle.txtStyle, { fontSize: 18, color: 'black' }]} >{'Select'} </Text>
          </TouchableOpacity>
          : null
        }

        {
          this.props.myMarketingImageList.length > 1 && data.index != this.props.myMarketingImageList.length - 1 ?
            <View style={{ position: 'absolute', right: 0, backgroundColor: 'lightgray', height: '100%', width: 40, justifyContent: 'center', alignItems: 'center' }} >
              <Image
                source={require('../../assets/drawer/navigate.png')}
                style={{ width: 20, height: 20, resizeMode: 'contain', }}
              />
            </View>
            : null
        }
      </View>
    )
  }

  onPressVideo = (video_url) => {
    this.props.setSelctedVideoUrl(video_url)
    this.setState({
      isVideoPlayer: !this.state.isVideoPlayer
    })
  }

  renderItemVideo = (data) => {
    return (
      <View
        style={[styles.listStyle, this.props.containerStyle]}
      >

        <Text style={[styles.txt, this.props.titleStyle]}>
          {'Video'}
        </Text>
        <View style={{ padding: 10, backgroundColor: data.item.isSelected ? Colors.themeColor : "white", elevation: 5, borderRadius: 100 }} >
          {data.item.video_url != null ?
            <TouchableOpacity
              onPress={() => this.onPressVideo(data.item.video_url)}
              onLongPress={() => this.props.setMyMarketingUpdateVideoList(data.index)}
              // source={{ uri: data.item.video_url }}
              borderRadius={100}
              style={{ height: 150, width: 150, backgroundColor: 'gray', borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}
            >
              <Image
                source={require('../../assets/play.png')}
                style={{ height: 50, width: 50, resizeMode: 'contain' }}
              />

            </TouchableOpacity> :
            <View style={{ height: 130, width: 130, borderRadius: 360, backgroundColor: Colors.placeHolderColor }} />
          }
          <TouchableOpacity
            onPress={() => this.openVideoPicker(data.index)}
            style={styles.buttonStyleCam} >
            <Image
              source={require('../../assets/camera.png')}
              style={{ height: 20, width: 20, tintColor: Colors.themeColor, resizeMode: 'contain' }}
            />
          </TouchableOpacity>

        </View>

        {data.item.video_url != null ?
          <TouchableOpacity
            onPress={() => { this.onSelectVideo(data.index) }}
            style={{ position: 'absolute', elevation: 6, zIndex: 1, bottom: 0, right: 10, padding: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
            <View style={{ marginRight: 10, width: 18, height: 18, borderRadius: 30, backgroundColor: "gray", justifyContent: 'center', alignItems: 'center' }} >
              <View style={{ width: 15, height: 15, borderRadius: 30, backgroundColor: data.item.isSelected ? Colors.themeColor : 'white' }} />
            </View>
            <Text style={[msStyle.txtStyle, { fontSize: 18, color: 'black' }]} >{'Select'} </Text>
          </TouchableOpacity>
          : null
        }


        {
          this.props.myMarketingVideoList.length > 1 && data.index != this.props.myMarketingVideoList.length - 1 ?
            <View style={{ position: 'absolute', right: 0, backgroundColor: 'lightgray', height: '100%', width: 40, justifyContent: 'center', alignItems: 'center' }} >
              <Image
                source={require('../../assets/drawer/navigate.png')}
                style={{ width: 20, height: 20, resizeMode: 'contain', }}
              />
            </View>
            : null
        }
      </View>
    )
  }


  _onShare = async () => {


    let image = ''
    let video = ''

    if (this.props.myMarketingImageList.length <= 1 && this.props.myMarketingVideoList.length <= 1 && this.props.myMarketingDocumentUrl == null) {
      alert('Plaese Upload an Iamge, Video or Attach producct.')
      return
    }

    this.props.myMarketingImageList.map((item, index) => {
      if (item.isSelected == true && item.image_url != null) {
        // debugLog(item.image_url)
        image += "\n\n" + item.image_url
      }
    })

    this.props.myMarketingVideoList.map((item, index) => {
      if (item.isSelected == true && item.video_url != null) {
        // debugLog(item.image_url)
        video += "\n\n" + item.video_url
      }
    })

    if (image == '' && video == '' && this.props.myMarketingDocumentUrl == null) {
      alert('Plaese select an Image, Video or Attach Product.')
      return
    }
    this.setState({ isRedayToShare: true })
  }


  shareOnEmail = (urls, mediaType) => {

    let email_to = [''];
    let subject = 'My Marketing';
    let body = urls[0].image + urls[0].video + urls[0].document

    let res = sendEmail(email_to, subject, body)
    debugLog(res)
    // return
    this.saveMediaCount(mediaType, urls)
    this.setState({ isRedayToShare: false })

  }



  onSelectSocialMedia = async (mediaType) => {

    let image = ''
    let video = ''
    let document = ''
    this.props.myMarketingImageList.map((item, index) => {
      if (item.isSelected == true && item.image_url != null) {
        // debugLog(item.image_url)
        image += "\n\n" + item.image_url
      }
    })

    this.props.myMarketingVideoList.map((item, index) => {
      if (item.isSelected == true && item.video_url != null) {
        // debugLog(item.image_url)
        video += "\n\n" + item.video_url
      }
    })

    if (image == '' && video == '' && this.props.myMarketingDocumentUrl == null) {
      alert('Plaese select an Image or Video.')
      return
    }

    // if (image != '') {
    //   image = "\n\n" + image

    // }

    // if (video != '') {
    //   video = "\n\n" + video

    // }

    if (this.props.myMarketingDocumentUrl != null) {
      document = "\n\n" + this.props.myMarketingDocumentUrl

    }



    let urls = []
    urls.push({
      image: image,
      video: video,
      document: document
    })


    if (mediaType.name == 'gmail') {
      this.shareOnEmail(urls, mediaType)
      return
    }
    // shareViaWhatsapp('eComNow\n' + image + "\n\n" + video + "\n\nDocument\n" + this.props.myMarketingDocumentUrl)
    // return
    try {
      const result = await Share.share({
        message: 'eComNow\n' + image + "" + video + "" + document,
        url: image,
        title: `eComNow`
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
          this.saveMediaCount(mediaType, urls)
          this.setState({ isRedayToShare: false })
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  saveMediaCount = async (mediaType, urls) => {

    let param = {
      domainname: this.props.loginResponse.domainName.includes(`www.`) ? this.props.loginResponse.domainName : this.props.loginResponse.domainName,
      platformid: this.props.loginResponse.platformId,
      cbpnumber: this.props.loginResponse.cbp,
      storenumber: this.props.loginResponse.storeNo,
      uniqueid: this.props.loginResponse.uniqueId,
      subject: 'My Marketing',
      cartName: "abc",// proudctList cart
      message: mediaType.name,
      contacts: [],
      deliveryMethods: [{ "methodName": mediaType.name, "count": 1 }]
    }

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Cookie", "AWSALBTG=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; AWSALBTGCORS=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; SESSION=YTNhZmFiMjItOWI0Yy00NGQxLWJmNDItYmE3MDI2NjY5OWQy");

    var rawdata = JSON.stringify(param)
    var raw = JSON.stringify({
      "domainName": "http://www.cscestorexpress.com/",
      "platformId": "BHT",
      "cbpNumber": 3,
      "storeNumber": 2336,
      "uniqueId": "001",
      "subject": "XXXX",
      "cartName": "abc",
      "message": "message",
      "contacts": [{ "firstName": "shahzad", "lastName": "Test", "phoneNumber": "112233445566" }],
      "products": [{ "productId": "1122", "brandName": "Test Brand", "sku": "11223344", "price": "1122", "category_name": "test cat" },
      { "productId": "5566", "brandName": "Test 2", "sku": "000000", "price": "0000", "category_name": "xxxx" }],
      "deliveryMethods": [{ "methodName": mediaType, "count": 5 }]
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: rawdata,
      redirect: 'follow'
    };

    fetch(`${Constants.API_BASE_URL}/admin/recommendations/save`, requestOptions)
      .then(response => response.text())
      .then(result => {
        debugLog('success::::::::::::::')
        debugLog(result)
        alert(result)
        debugLog(result)
        this.props.setMyMarketingSelectAllImageList(false)
        this.props.setMyMarketingSelectAllVideoList(false)
        this.props.setMyMarketingDocumentUrl(null)
        this.props.navigation.navigate('ProfileScreen1')
      })
      .catch(error => {
        debugLog('error:::::::::::::::::')
        debugLog(error)
      });

  }


  async componentDidMount() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          'title': 'Cool App ...',
          'message': 'App needs access to external storage'
        }
      );
      return granted == PermissionsAndroid.RESULTS.GRANTED
    }
    catch (err) {
      return false;
    }
  }



  onSelectAllVideo = () => {
    // this.setState({ selectAllVideo: !this.state.selectAllVideo })

    if (this.props.myMarketingVideoList.length <= 1) {
      alert('Please Upload at least one Video')
      return
    }

    this.selectAllVideo = !this.selectAllVideo
    this.props.setMyMarketingSelectAllVideoList(this.selectAllVideo)
  }

  onSelectVideo = (index) => {
    // this.setState({ selectAllVideo: !this.state.selectAllVideo })

    if (this.props.myMarketingVideoList.length <= 1) {
      alert('Please Upload at least one Video')
      return
    }

    this.selectAllVideo = !this.selectAllVideo
    this.props.setMyMarketingUpdateVideoList(index)
  }


  onSelectAllImage = () => {
    // this.setState({ selectAllImage: !this.state.selectAllImage })
    if (this.props.myMarketingImageList.length <= 1) {
      alert('Please Upload at least one Image')
      return
    }

    this.selectAllImage = !this.selectAllImage
    this.props.setMyMarketingSelectAllImageList(this.selectAllImage)
  }

  onSelectImage = (index) => {
    // this.setState({ selectAllImage: !this.state.selectAllImage })
    if (this.props.myMarketingImageList.length <= 1) {
      alert('Please Upload at least one Image')
      return
    }

    this.selectAllImage = !this.selectAllImage
    this.props.setMyMarketingUpdateImageList(index)
  }


  onAttachProduct = () => {
    this.props.navigation.navigate('ProductListScreen', { screen: 'MyMarketingScreen' })
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>

          {/* Need to change bg color */}
          <View style={{
            backgroundColor: '#026bb8'
          }} >
            <MyHeader
              title='My Marketing'
              icon={require('../../assets/back.png')}
              tintColor="white"
              onPress={() => { this.onGoBack() }}
              containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
              titleStyle={{ color: Colors.whiteText }}
            />

          </View>

          <View
            style={{
              flexGrow: 1,// use this to solve this problem
            }}>



            <View style={{ flex: 1 }} >
              <FlatList
                data={this.props.myMarketingVideoList}
                renderItem={this.renderItemVideo}
                horizontal
                keyExtractor={(item, index) => 'key' + index}
              />
              {/* <TouchableOpacity
                onPress={() => { this.onSelectAllVideo() }}
                style={{ position: 'absolute', bottom: 0, right: 10, padding: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                <View style={{ marginRight: 10, width: 18, height: 18, borderRadius: 30, backgroundColor: "gray", justifyContent: 'center', alignItems: 'center' }} >
                  <View style={{ width: 15, height: 15, borderRadius: 30, backgroundColor: this.selectAllVideo ? Colors.themeColor : 'white' }} />
                </View>
                <Text style={[msStyle.txtStyle, { fontSize: 18, color: 'black' }]} >{'ALL'} </Text>
              </TouchableOpacity> */}
            </View>

            <View style={{ height: 2, backgroundColor: 'lightgray', alignSelf: 'center', width: "80%" }} />

            <View style={{ flex: 1 }} >
              <FlatList
                data={this.props.myMarketingImageList}
                renderItem={this.renderItemImage}
                horizontal
                keyExtractor={(item, index) => 'key' + index}
              />

              {/* <TouchableOpacity
                onPress={() => { this.onSelectAllImage() }}
                style={{ position: 'absolute', bottom: 0, right: 10, padding: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >

                <View style={{ marginRight: 10, width: 18, height: 18, borderRadius: 30, backgroundColor: "gray", justifyContent: 'center', alignItems: 'center' }} >
                  <View style={{ width: 15, height: 15, borderRadius: 30, backgroundColor: this.selectAllImage ? Colors.themeColor : 'white' }} />
                </View>

                <Text style={[msStyle.txtStyle, { fontSize: 18, color: 'black' }]} >{'ALL'} </Text>
              </TouchableOpacity> */}
            </View>


            <TouchableOpacity
              onPress={() => this.onAttachProduct()}
              style={{ paddingVertical: 10, flexDirection: 'row', marginLeft: 20, marginBottom: 20 }} >
              <Image
                source={require('../../assets/link.png')}
                style={{ height: 25, width: 25, resizeMode: 'contain' }}
              />
              <Text style={{ fontSize: 16, marginLeft: 10, paddingHorizontal: 20 }} >
                {this.props.myMarketingDocumentUrl != null ? this.props.myMarketingDocumentUrl : 'Attach Product'}
              </Text>
            </TouchableOpacity>



            <TouchableOpacity
              onPress={() => this._onShare()}
              style={{ paddingVertical: 10, width: '90%', backgroundColor: '#026bb8', borderRadius: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', marginBottom: 50 }} >
              <Text style={{ color: 'white', fontSize: 20 }} >
                {'SHARE'}
              </Text>
            </TouchableOpacity>

          </View>


          {
            this.props.myMarketingImage != null ?
              <ProfilePopup
                loading_percent={this.props.image_progress}
                title='Image'
                loading={this.state.isImage}
                image={this.props.myMarketingImage.assets[0].uri}
                onPress={() => {
                  this.uploadImageOnASW1(this.props.myMarketingImage)
                }}
                onCancel={() => { this.props.setMyMarketingImage(null) }}
              />
              : null
          }


          {
            this.props.myMarketingVideo != null ?
              <ProfilePopup
                title='Video'
                loading_percent={this.props.video_progress}
                loading={this.state.isVideo}
                image={this.props.myMarketingVideo.assets[0].uri}
                onPress={() => {
                  this.uploadVideoOnASW1(this.props.myMarketingVideo)
                }}
                onCancel={() => { this.props.setMyMarketingVideo(null) }}

              />
              : null
          }


          {
            this.props.isMyMarketingDocument ?
              <ProfilePopup
                title='Document'
                loading_percent={this.props.document_progress}
                loading={this.state.isFile}
                editable={false}
                value={this.props.myMarketingDocument.name}
                onPress={() => {
                  this.uploadDocumentOnASW1(this.props.myMarketingDocument)
                }}
                onCancel={() => { this.props.setIsMarketingDocument(false) }}
              />
              : null
          }

          {this.state.isVideoPlayer ?
            <VideoPlayerComponent
              downloadedVideoSize='finished'
              url={this.props.selectedVideoUrl}
              onGoBack={() => this.setState({ isVideoPlayer: !this.state.isVideoPlayer })}
            />
            : null
          }

          {/* {
            this.state.isVideo ?
              <MyLoader
                loading_percent={this.props.video_progress}
              />
              : null
          } */}

          {/* {
            this.state.isImage ?
              <MyLoader />
              : null
          } */}

          {/* {
            this.state.isFile ?
              <MyLoader />
              : null
          } */}

          {
            this.state.isRedayToShare ?
              <SharePopupJaswant
                title='Select Share Method'
                appList={this.state.appList}
                inputStyle={{ height: 100 }}
                onPress={this.onSelectSocialMedia}
                onCancel={() => this.setState({ isRedayToShare: false })}
              />
              : null
          }

        </View>
      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width
  },
  txt: {
    fontSize: 20,
    marginBottom: 20
  },
  buttonStyleCam: {
    backgroundColor: "white",
    alignSelf: 'flex-end',
    padding: 5,
    position: 'absolute',
    bottom: 5,
    right: 5,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: Colors.themeColor,

  },
  portrateMode: {
    flex: 1,
    backgroundColor: 'white',
    margin: 10,
    padding: 3,
    elevation: 10,
    borderRadius: 10,
    marginTop: -PixelRatio.getPixelSizeForLayoutSize(25)
  },
  buttonStyle: {
    backgroundColor: "white",
    alignSelf: 'flex-end',
    padding: 5,
    position: 'absolute',
    bottom: -5,
    right: -5,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: Colors.themeColor,

  },
})

const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_loading: common.is_loading,
    myMarketingImageList: jaswant.myMarketingImageList,
    myMarketingVideoList: jaswant.myMarketingVideoList,
    myMarketingImage: jaswant.myMarketingImage,
    myMarketingVideo: jaswant.myMarketingVideo,
    myMarketingIndex: jaswant.myMarketingIndex,

    accessAndSecretsKey: jaswant.accessAndSecretsKey,
    loginResponse: common.loginResponse,
    myMarketingDocument: jaswant.myMarketingDocument,
    isMyMarketingDocument: jaswant.isMyMarketingDocument,
    myMarketingDocumentUrl: jaswant.myMarketingDocumentUrl,
    selectedVideoUrl: jaswant.selectedVideoUrl,

    video_progress: common.video_progress,
    image_progress: common.image_progress,
    document_progress: common.document_progress,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    setMarketingIndex: (index) => setMarketingIndex(index),
    setMyMarketingVideoEmptyList: (isTrue) => setMyMarketingVideoEmptyList(isTrue),
    setMyMarketingImageEmptyList: (isTrue) => setMyMarketingImageEmptyList(isTrue),
    setMyMarketingImage: (url) => setMyMarketingImage(url),
    setMyMarketingVideo: (url) => setMyMarketingVideo(url),
    setMyMarketingImageList: (image_url, index) => setMyMarketingImageList(image_url, index),
    setMyMarketingVideoList: (image_url, index) => setMyMarketingVideoList(image_url, index),
    hitUploadImageOnASW: (params) => hitUploadImageOnASW(params),
    hitUploadVideoOnASW: (params) => hitUploadVideoOnASW(params),
    hitUploadDocumentOnASW: (params) => hitUploadDocumentOnASW(params),
    setMarketingDocument: (document) => setMarketingDocument(document),
    setIsMarketingDocument: (isTrue) => setIsMarketingDocument(isTrue),
    setMyMarketingDocumentUrl: (url) => setMyMarketingDocumentUrl(url),
    setSelctedVideoUrl: (url) => setSelctedVideoUrl(url),

    setVideoProgress: (pr) => setVideoProgress(pr),
    setImageProgress: (pr) => setImageProgress(pr),
    setDocumentProgress: (pr) => setDocumentProgress(pr),

    setMyMarketingUpdateVideoList: (index) => setMyMarketingUpdateVideoList(index),
    setMyMarketingUpdateImageList: (index) => setMyMarketingUpdateImageList(index),
    setMyMarketingSelectAllImageList: (isTrue) => setMyMarketingSelectAllImageList(isTrue),
    setMyMarketingSelectAllVideoList: (isTrue) => setMyMarketingSelectAllVideoList(isTrue),


  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(MyMarketingScreen)