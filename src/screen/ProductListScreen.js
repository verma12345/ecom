import React, { Component, useState } from 'react';
import { Image, Button, View, Text, SafeAreaView, TextInput, Dimensions, StyleSheet, ScrollView, TouchableOpacity, flexDirection, FlatList, Keyboard } from 'react-native';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import Constants, { debugLog } from '../common/Constants';
import MyButton from '../components/MyButton';
import { setDomain, setEmail, setPlatform, setProductList, setSelectedProductList, setUpdateProductList, setUpdateSelectAllProductList } from '../redux_store/actions/indexActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MyLoader from "../components/MyLoader";
import { setIsDrawer, setLoginRespons } from '../redux_store/actions/indexActions';
import { getPrefs } from '../common/Prefs';
import SearchbyPopup from '../components/SearchbyPopup';
import { setMyMarketingDocumentUrl } from '../redux_store/actions/indexActionsJaswant';


class ProductListScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchTxt: '',
      serachTypeArray: [{ id: '1', name: 'Catagory', type: 'categories' }, { id: '2', name: 'SKU', type: 'sku' },
      { id: '3', name: 'Brand Name', type: 'brandname' }, { id: '4', type: 'itemname', name: 'Item Name' }],
      data: [],
      loginData: {},
      recommendList1: [],
      isSelectAll: false,
      is_loading: false,
      isSearching: false,
      is_dropdown: false,
      search_by: 'Item Name',
      search_type: 'itemname',
      index: '',
    }

  }

  onSearchBy = async (item) => {

    this.setState({ is_dropdown: false })
    this.setState({ search_by: item.name })
    this.setState({ search_type: item.type })
  };



  searchProduct = () => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");

    var raw_testing = JSON.stringify({
      "domainname": "www.${param.domainname}",
      "platformid": "BHT",
      "cbpnumber": 3,
      "storenumber": 2336,
      "uniqueid": "001",
      "numbersofrows": 100,
      "searchType": this.state.search_type,
      "searchString": '%' + this.state.searchTxt + '%',
    });
    // {"cbp": 3, "createdAt": 1625066191, "domainName": "${param.domainname}", "platformId": "bht", "product": 
    // null, "randomkey": null, "source": "mobile", "storeNo": 2336, "uniqueId": "001", "updatedAt": 
    // 1625066194, "userEmail": "barneyrubble@testemail.com", "userFirstName": "test", "userLastName": "Testing"}


    var raw = JSON.stringify({
      "domainname": this.state.loginData.domainName.includes(`www.`) ? this.state.loginData.domainName : `www.${this.state.loginData.domainName}`,
      "platformid": this.state.loginData.platformId,
      "cbpnumber": this.state.loginData.cbp,
      "storenumber": this.state.loginData.storeNo,
      "uniqueid": this.state.loginData.uniqueId,
      "numbersofrows": 100,
      "searchType": "itemname",
      "searchString": '%' + this.state.searchTxt + '%',
    });


    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    this.setState({ is_loading: true })
    fetch(`${Constants.API_BASE_URL}/restapi/storeproducts`, requestOptions)
      .then(response => response.text())
      .then(result => {
        this.setState({ is_loading: false })
        let json = JSON.parse(result)
        if (json.Products.product) {
          this.props.setProductList(json.Products.product)
        } else {
          debugLog('else care')
          this.props.setProductList([])
          // alert('No Product found')
        }
        // this.props.setProductList(json.Products.product)
        this.setState({ is_loading: false, isSearching: true })
      })
      .catch(error => console.log('error', error));
  }

  getLoginCrediential = async () => {
    getPrefs('ACCESS_TOKEN').then((value) => {
      debugLog(value)
      let json = JSON.parse(value)
      // this.props.setLoginRespons(json)
      this.setState({ loginData: json })
      // debugLog(json.domainName)
      this.getProductList()

    })
  }

  getProductList = () => {
    this.setState({ isSearching: false })
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");

    var raw_testing = JSON.stringify({
      "domainname": "www.cscestorexpress.com",
      "platformid": "BHT",
      "cbpnumber": 3,
      "storenumber": 2336,
      "uniqueid": "001",
      "numbersofrows": 100,
      "searchType": "",
      "searchString": '',
    });
    // {"cbp": 3, "createdAt": 1625066191, "domainName": "cscestorexpress.com", "platformId": "bht", "product": 
    // null, "randomkey": null, "source": "mobile", "storeNo": 2336, "uniqueId": "001", "updatedAt": 
    // 1625066194, "userEmail": "barneyrubble@testemail.com", "userFirstName": "test", "userLastName": "Testing"}


    var raw = JSON.stringify({
      "domainname": this.state.loginData.domainName.includes(`www.`) ? this.state.loginData.domainName : `www.${this.state.loginData.domainName}`,
      "platformid": this.state.loginData.platformId,
      "cbpnumber": this.state.loginData.cbp,
      "storenumber": this.state.loginData.storeNo,
      "uniqueid": this.state.loginData.uniqueId,
      "numbersofrows": 100,
      "searchType": "",
      "searchString": this.state.searchTxt,
      // "mobileapi_key": "3k82/9NwEQfLif1IIKvQMg=="
    });


    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    this.setState({ is_loading: true })
    let url = Constants.API_BASE_URL + "/restapi/storeproducts"

    fetch(url, requestOptions)
      .then(response => response.text())
      .then(result => {
        let json = JSON.parse(result)
        this.props.setProductList(json.Products.product)
        this.setState({ is_loading: false, })

      })
      .catch(error => console.log('error', error),
        this.setState({ is_loading: false })
      );
  }



  componentDidMount() {
    this.getLoginCrediential()
    // this.getProductList()
  }

  onProductDetailScreen = (data) => {
    debugLog(data)
    this.props.navigation.navigate('ProductDetailScreen', { data: data })
  }

  selectAll = () => {
    this.props.setUpdateSelectAllProductList(!this.props.isSelectAll)
  }

  onPressCheck = (index) => {
    this.props.setUpdateProductList(index)
  }

  searchingProduct = () => {
    if (this.state.searchTxt === '') {
      this.getProductList()
      this.setState({ isSearching: false })
      debugLog('Listing ......')
    } else {
      debugLog('searching ......')
      this.setState({ isSearching: true })
      this.searchProduct()
    }

  }

  onRecommendProduct = async () => {

    let selectedData = this.props.setSelectedProductList()
    debugLog(selectedData)
    if (selectedData.length == 0) {
      alert(" Select Product to Recommend")
      return
    }



    let newData = []
    selectedData.map((item, index) => {
      newData.push({
        sku: item.item.sku,
        product_id: item.item.productId,
        quantity: "1"
      })
    })

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({
      // "products": [{ "sku": "m2", "product_id": "161", "quantity": "1" }],
      "products": newData,
      "store_id": this.props.loginResponse.storeNo,
      "first_name": this.props.loginResponse.userFirstName,
      "last_name": this.props.loginResponse.userLastName,
      "email": this.props.loginResponse.userEmail,
      "platformId": this.props.loginResponse.platformId,
      "CBP": this.props.loginResponse.cbp,
      "domainname": this.props.loginResponse.domainName.includes(`www.`) ? this.props.loginResponse.domainName : `www.${this.props.loginResponse.domainName}`
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch(`${Constants.API_BASE_URL}/restapi/createshoppingcart`, requestOptions)
      .then(response => response.text())
      .then(result => {
        debugLog('rrrrrrrrrrrrrrrrrr')
        console.log(result)
        debugLog('ppppppppppppppppppppp')

        let json = JSON.parse(result)
        this.setState({ cartdetails: json })
        // debugLog(this.state.cartdetails[0].url)
        this.props.setMyMarketingDocumentUrl(`https://www.` + json[0].url)
        this.props.navigation.navigate(this.props.route.params.screen)
      })
      .catch(error => console.log('error', error));
  }


  renderItem = (product) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.onProductDetailScreen(product.item)
        }}
        style={[{ flexDirection: 'row', marginHorizontal: 15, marginTop: 10, backgroundColor: 'white', }]}
      >

        <View style={[msStyle.myshadow2, { flex: 1, paddingRight: 10, borderRadius: 10, flexDirection: 'row', backgroundColor: 'white', justifyContent: 'center', }]}     >

          <View style={[{
            flex: 1,
            borderWidth: 1,
            borderColor: 'lightgray',
            borderTopLeftRadius: 6,
            borderBottomLeftRadius: 6,
            borderBottomRightRadius: 6,
            backgroundColor: 'white',
            margin: 5,
            marginRight: 10
          }]} >
            <Image style={{
              width: Dimensions.get('window').width / 4,
              height: Dimensions.get('window').width / 4,
              borderTopLeftRadius: 6,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6,
            }}
              source={{ uri: product.item.image }} />
          </View>

          <View style={{ flex: 1, }} >
            <Text numberOfLines={1} style={[msStyle.txtStyle, { fontSize: 15, paddingVertical: 15 }]}>{product.item.itemname}</Text>
            <Text numberOfLines={1} style={[msStyle.txtStyle, { fontSize: 15 }]}>{product.item.description}</Text>
            <Text numberOfLines={1} style={[msStyle.txtStyle, { fontSize: 15, }]}>{'Price - $'}{product.item.price}</Text>
          </View>

          <View style={{ flex: 1, alignItems: 'flex-end' }} >
            <Text numberOfLines={1} style={[msStyle.txtStyle, { fontSize: 15, paddingVertical: 15 }]}>{product.item.categories[0].category}</Text>
            <Text numberOfLines={1} style={[msStyle.txtStyle, { fontSize: 15 }]}>{product.item.sku}</Text>
            <Text numberOfLines={1} style={[msStyle.txtStyle, { fontSize: 15, }]}>{product.item.brand}</Text>
          </View>

        </View>

        <TouchableOpacity
          onPress={() => {
            this.onPressCheck(product.index)
          }}
          style={{ marginLeft: 10 }} >
          <Image
            source={product.item.selected ? require('../../assets/check.png') : require('../../assets/uncheck.png')}
            style={{ height: 25, width: 25 }}
          />
        </TouchableOpacity>

      </TouchableOpacity>
    )
  }




  render() {
    return (
      <SafeAreaView style={msStyle.container}>


        <View style={{ flexDirection: 'row', height: 60, justifyContent: 'center', alignItems: 'center' }}>
          <TouchableOpacity
            style={{ position: 'absolute', left: 20, }}
            onPress={() => { this.props.navigation.openDrawer() }}
          >
            <Image
              source={require('../../assets/menu2.png')}
              resizeMode="contain"
              style={{ width: 30, height: 30, resizeMode: 'contain' }}
            />
          </TouchableOpacity>

          <Text style={[msStyle.txtStyle, {
            color: 'black', fontSize: 20,
            fontStyle: "normal",
            fontWeight: '500'
          }]}>{"Product"}</Text>
        </View>

        {/* MARK:  body */}

        <View style={(msStyle.container)}>
          <View style={{ padding: 15 }}>
            <View
              // onPress={() => this.setState({ isSubject: !this.state.isSubject })}
              style={{
                flexDirection: 'row', height: 50, borderWidth: 1, borderRadius: 25, paddingHorizontal: 10,
                justifyContent: 'flex-start',
                alignItems: 'center'
              }} >
              <TouchableOpacity
                // onPress={() => this.searchingProduct()
                onPress={() => {
                  this.getProductList()
                  this.setState({ searchTxt: '' })
                }}
              >
                <Image
                  source={
                    this.state.isSearching ? require('../../assets/cancel.png') : require('../../assets/search1.png')}
                  resizeMode="contain"
                  style={{ width: 20, height: 20, }}
                />
              </TouchableOpacity>
              <TextInput
                placeholder="Catagory / SKU / Brand Name / Item Name"
                placeholderTextColor='gray'
                value={this.state.searchTxt}
                style={{ fontSize: 15, paddingLeft: 10, justifyContent: 'flex-start' }}
                onChangeText={(txt) => {
                  // this.state.searchType.map((y) => {
                  //   this.searchingProduct(y.item.name)
                  // })
                  this.setState({ searchTxt: txt })
                  this.searchingProduct(),
                    this.setState({ isSearching: true })
                }} />

            </View>
          </View>
          {/* Select All */}
          <View style={{ height: 30, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 15, alignItems: 'center' }}>
              <Text>{'Search By: '}</Text>
              <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                onPress={() => {
                  this.setState({ is_dropdown: !this.state.is_dropdown })
                }}
              >
                <Text style={{ color: 'gray', paddingRight: 5, fontWeight: '500' }}> {this.state.search_by}</Text>
                <Image
                  source={require('../../assets/drop_off.png')}
                  resizeMode="contain"
                  style={{
                    width: 20,
                    height: 20,
                    justifyContent: 'center',
                    alignContent: 'center'
                  }}
                />
              </TouchableOpacity>
            </View>

            <View style={{ flex: .5, height: 30, padding: 5, paddingRight: 10, justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ paddingRight: 10, }}>
                {'Select All'}
              </Text>
              <TouchableOpacity
                onPress={() => this.selectAll()}
                style={{ padding: 5 }} >
                <Image
                  source={
                    this.props.isSelectAll ? require('../../assets/check.png') : require('../../assets/uncheck.png')
                  }
                  resizeMode="contain"
                  style={{
                    width: 25,
                    height: 25,
                  }}
                />

              </TouchableOpacity>
            </View>
          </View>


          <FlatList
            contentContainerStyle={{ marginTop: 20, paddingBottom: 100 }}
            data={this.props.productList}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => 'key' + index}
            showsVerticalScrollIndicator={false}
          />

          {/* Button */}

          <MyButton
            title='RECOMMEND'
            onPress={() => { this.onRecommendProduct() }}
            titleStyle={{ color: Colors.whiteText, fontWeight: 'bold' }}
            containerStyle={{
              marginHorizontal: 25,
              marginBottom: 25,
            }}
          />


          {
            this.state.is_loading ?
              <MyLoader />
              : null
          }
          {
            this.state.is_dropdown ?
              <SearchbyPopup
                title='Select Category'
                serachTypeArray={this.state.serachTypeArray}
                inputStyle={{ height: 100 }}
                onPress={this.onSearchBy}
                onCancel={() => this.setState({ is_dropdown: false })}
              />
              : null
          }
        </View>


      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  absoluteStyle: {
    position: 'absolute',
    backgroundColor: Colors.whiteText,
    alignSelf: 'center',
    width: width - 30,
    height: height - 70,
    elevation: 10,
    zIndex: 5,
    bottom: 15,
    borderRadius: 5
  },
  list: {
    flex: 1,
    padding: 15

  }

})


const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    loginResponse: common.loginResponse,
    productList: common.productList,
    isSelectAll: common.isSelectAll,
    selectedProductList: common.selectedProductList,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    setProductList: (domain) => setProductList(domain),
    setUpdateProductList: (index) => setUpdateProductList(index),
    setUpdateSelectAllProductList: (list) => setUpdateSelectAllProductList(list),
    setLoginRespons: (res) => setLoginRespons(res),
    setSelectedProductList: () => setSelectedProductList(),

    setMyMarketingDocumentUrl: (url) => setMyMarketingDocumentUrl(url),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductListScreen)