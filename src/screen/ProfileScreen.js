import React, { Component } from 'react';
import { Button, View, Text, SafeAreaView, Dimensions, StyleSheet, ScrollView, TouchableWithoutFeedback, TouchableOpacity, PixelRatio, Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import { msStyle } from '../common/MyStyle';
import { getPrefs, setPrefs } from '../common/Prefs';
import MyHeader from '../components/MyHeader';
import { PhoneOptionMemo } from '../components/PhoneOption';
import { ProfileOptionMemo } from '../components/ProfileOption';
import ProfilePickerComponent from '../components/ProfilePickerComponent';
import ProfilePopup from '../components/ProfilePopup';
import { SocialMediaOptionMemo } from '../components/SocialMediaOption';
import { setIsDrawer } from '../redux_store/actions/indexActions';
import {
  setIsProfileAboutUs,
  setIsProfileDescription,
  setIsProfileEmail,
  setIsProfileFirstName,
  setIsProfileHeader,
  setIsProfileInstagram,
  setIsProfileLastName,
  setIsProfileLinkedin,
  setIsProfileMessenger,
  setIsProfilePhoneNumber,
  setIsProfilePinterest,
  setIsProfileTiktok,
  setIsProfileTwitter,
  setIsProfileWhatsapp,
  setIsProfileYoutube,
  setProfileAboutUs,
  setProfileCertificate,
  setProfileDescription,
  setProfileEmail,
  setProfileFirstName,
  setProfileHeader,
  setProfileImage1,
  setProfileImage2,
  setProfileInstagram,
  setProfileLastName,
  setProfileLinkedin,
  setProfileMessenger,
  setProfilePhoneNumber,
  setProfilePinterest,
  setProfileTiktok,
  setProfileTwitter,
  setProfileWhatsapp,
  setProfileYoutube,
} from '../redux_store/actions/indexActionsJaswant';

import ImageCropPicker from 'react-native-image-crop-picker';
import MyButton from '../components/MyButton';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    // this.props.navigation.openDrawer()

    this.getLoginCrediential()
    this.state = {
      orientation: '',
      isLoading: false
    }
  }

  getOrientation = () => {
    if (this.refs.rootView) {
      if (Dimensions.get('window').width < Dimensions.get('window').height) {
        this.setState({ orientation: 'portrait' });
      } else {
        this.setState({ orientation: 'landscape' });
      }
    }
  }

  componentDidMount() {
    setPrefs("SIGINED_IN", "1")

    this.getOrientation();
    Dimensions.addEventListener('change', () => {
      this.getOrientation();
    });
  }


  getLoginCrediential = async () => {
    try {
      const value = await AsyncStorage.getItem('ACCESS_TOKEN');
      if (value !== null) {

        let loginData = JSON.parse(JSON.parse(value))

        // console.log(loginData)
      }
    } catch (error) {
      // Error retrieving data
    }
  }


  openPickerGallery = (option) => {
    ImageCropPicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      isCamera: true,
    }).then((image) => {
      if (option == 1) {
        this.props.setProfileImage1(image.path);
      }
      if (option == 2) {
        this.props.setProfileImage2(image.path);
      }

    });
  };

  openPickerCamera = (option) => {
    ImageCropPicker.openCamera({
      width: 300,
      height: 300,
      cropping: true,
    }).then((image) => {
      if (option == 1) {
        this.props.setProfileImage1(image.path);
      }
      if (option == 2) {
        this.props.setProfileImage2(image.path);
      }

    });
  };

  showAlert(option) {
    Alert.alert(
      'Upload Image',
      'Please choose Camera or Gallery',
      [
        {
          text: 'Camera',
          onPress: () => {
            this.openPickerCamera(option);
          },
        },
        {
          text: 'Gallery',
          onPress: () => {
            this.openPickerGallery(option);
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  }


  putImage = () => {

    moment.locale('en');
    // var dt = '2016-05-02T00:00:00';
    var currentTime = moment(new Date().toString()).milliseconds(0);
    var c2 = moment(currentTime).format('YYYYMMDDTHHMMSS') + 'Z'
    //   console.log(Moment(new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString()).format('YYYYMMDDTHHMMSS'))
    // console.log(currentTime)
    // console.log(c2)

    // //  var currentTime = Moment("2021-06-24T11:05:10Z").format('YYYYMMDDTHHMMSS')+'Z'

    // console.log(c2);
    debugLog(c2)
    // return

    this.setState({ isLoading: true })
    var myHeaders = new Headers();
    myHeaders.append("X-Amz-Content-Sha256", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
    myHeaders.append("X-Amz-Date", c2);
    myHeaders.append("Authorization", "AWS4-HMAC-SHA256 Credential=AKIAUKGZKWGYS36AJUNW/20210624/us-east-1/s3/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=b90b5d1ba8d14cd9d1945089bd6d9e8d17a2451e0b67da4645efdc9267958d11");
    myHeaders.append("Content-Type", "image/png");


    let imageData = {
      uri: this.props.profileImage1.path,
      type: this.props.profileImage1.mime,
      name: 'pic.png'
    }




    var file = imageData;

    var requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: file,
      redirect: 'follow'
    };
    debugLog(requestOptions)
    fetch("http://ecomnow-images.s3.us-east-1.amazonaws.com/bht/images/reseller/1010/icon.png", requestOptions)
      .then(response => response.text())
      .then(result => {
        console.log(result)
        this.setState({ isLoading: false })

      })
      .catch(error => console.log('error', error));
  }






  onHint = () => {
    alert("Coming soon...")
  }

  onMenu = () => {
    // this.props.setIsDrawer(!this.props.is_drawer)
    this.props.navigation.openDrawer()

  }

  onCertificate = () => {
    this.props.navigation.navigate('CertificatesComponent')
  }

  onSave = () => {
    alert('Coming soon...')
  }

  render() {
    debugLog(this.state.orientation)
    return (
      <SafeAreaView
        ref="rootView"
        style={msStyle.container}>
        <View style={msStyle.container}>

          <View style={{ flex: 1, backgroundColor: Colors.themeColor, }} />
          <View style={{ flex: 5, backgroundColor: "white", }} />


          <View style={{
            position: 'absolute',
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
            backgroundColor: "#0000",
            elevation: 1
          }} >


            <View>
              <MyHeader
                title='Profile'
                icon={require('../../assets/menu.png')}
                tintColor="white"
                onPress={() => { this.onMenu() }}
                containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
                titleStyle={{ color: Colors.whiteText }}
              />
            </View>

            <View style={{ flex: 1, backgroundColor: 'white', margin: 15, paddingHorizontal: 20, paddingVertical: 3, borderRadius: 5, elevation: 5, zIndex: 5 }} >

              <ScrollView  >

                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', paddingVertical: 10, }} >

                  <ProfilePickerComponent
                    title='Image 1'
                    icon={require('../../assets/camera.png')}
                    image={this.props.profileImage1}
                    onPress={() => { this.showAlert(1) }}
                    isHint={true}
                    onHint={() => { this.onHint() }}
                  />

                  <ProfilePickerComponent
                    title='Image 2'
                    icon={require('../../assets/camera.png')}
                    onPress={() => { this.showAlert(2) }}
                    image={this.props.profileImage2}
                    isHint={true}
                    onHint={() => { this.onHint() }}

                  />
                  <ProfilePickerComponent
                    title='Certificate'
                    icon={require('../../assets/edit.png')}
                    onPress={() => { this.onCertificate() }}
                    image={this.props.profileCertificate.path}
                  />
                </View>


                <ProfileOptionMemo
                  title='Profile Header'
                  message={this.props.profileHeader}
                  onPencil={() => {
                    this.props.setIsProfileHeader(!this.props.isProfileHeader)
                  }}
                />

                <ProfileOptionMemo
                  title='Profile Description'
                  message={this.props.profileDescription}

                  onPencil={() => {
                    this.props.setIsProfileDescription(!this.props.isProfileDescription)
                  }}
                />

                <ProfileOptionMemo
                  title='About Us'
                  message={this.props.profileAboutUs}
                  inputStyle={{ height: 100 }}
                  onPencil={() => {
                    this.props.setIsProfileAboutUs(!this.props.isProfileAboutUs)
                  }}
                />

                <ProfileOptionMemo
                  title='First Name'
                  disable={true}
                  message={this.props.profileFirstName}
                // onPencil={() => {
                //   this.props.setIsProfileFirstName(!this.props.isProfileFirstName)
                // }}
                />

                <ProfileOptionMemo
                  title='Last Name'
                  disable={true}
                  message={this.props.profileLastName}
                // onPencil={() => {
                //   this.props.setIsProfileLastName(!this.props.isProfileLastName)
                // }}
                />

                <ProfileOptionMemo
                  title='Email'
                  disable={true}
                  message={this.props.profileEmail}
                // onPencil={() => {
                //   this.props.setIsProfileEmail(!this.props.isProfileEmail)
                // }}
                />

                <PhoneOptionMemo
                  title='Phone Number'
                  country_code={'+1'}
                  disable={true}
                  message={this.props.profilePhoneNumber}
                // onPencil={() => {
                //   this.props.setIsProfilePhoneNumber(!this.props.isProfilePhoneNumber)
                // }}
                />




                {/* ********************************************************* * social media section****************************************************************** */}





                <Text style={[msStyle.txtStyle, { fontSize: 18, padding: 10, marginTop: 20 }]} >{'Social Media'}</Text>

                <View style={{ flexDirection: 'row', }} >
                  <SocialMediaOptionMemo
                    value={this.props.whatsapp}
                    onPress={() => {

                      this.props.setIsProfileWhatsapp(!this.props.is_whatsapp)
                    }}
                    icon={require('../../assets/whatsapp.png')}
                    title="Whatsapp"
                    containerStyle={{ alignSelf: 'flex-start' }}

                  />

                  <SocialMediaOptionMemo
                    value={this.props.twitter}
                    onPress={() => { this.props.setIsProfileTwitter(!this.props.is_twitter) }}
                    icon={require('../../assets/twitter.png')}
                    title="Twitter"
                    containerStyle={{ marginLeft: 30 }}

                  />
                </View>

                <View style={{ flexDirection: 'row', }} >
                  <SocialMediaOptionMemo
                    value={this.props.messenger}
                    onPress={() => { this.props.setIsProfileMessenger(!this.props.is_messenger) }}
                    icon={require('../../assets/messenger.png')}
                    title="Messenger"
                    containerStyle={{ alignSelf: 'flex-start' }}
                  />

                  <SocialMediaOptionMemo
                    value={this.props.linkedin}
                    onPress={() => { this.props.setIsProfileLinkedin(!this.props.is_linkedin) }}
                    icon={require('../../assets/linkedin.png')}
                    title="Linkedin"
                    containerStyle={{ marginLeft: 30 }}
                  />
                </View>

                <View style={{ flexDirection: 'row', }} >
                  <SocialMediaOptionMemo
                    value={this.props.instagram}
                    onPress={() => { this.props.setIsProfileInstagram(!this.props.is_instagram) }}
                    icon={require('../../assets/instagram.png')}
                    title="Instagram"
                    containerStyle={{ alignSelf: 'flex-start' }}

                  />

                  <SocialMediaOptionMemo
                    value={this.props.youtube}
                    onPress={() => { this.props.setIsProfileYoutube(!this.props.is_youtube) }}
                    icon={require('../../assets/youtube.png')}
                    title="Youtube"
                    containerStyle={{ marginLeft: 30 }}

                  />
                </View>

                <View style={{ flexDirection: 'row', }} >
                  <SocialMediaOptionMemo
                    value={this.props.pinterest}
                    onPress={() => { this.props.setIsProfilePinterest(!this.props.is_pinterest) }}
                    icon={require('../../assets/pinterest.png')}
                    title="Pinterest"
                    containerStyle={{ alignSelf: 'flex-start' }}
                  />

                  <SocialMediaOptionMemo
                    value={this.props.tiktok}
                    onPress={() => { this.props.setIsProfileTiktok(!this.props.is_tiktok) }}
                    icon={require('../../assets/tiktok.png')}
                    title="Tiktok"
                    containerStyle={{ marginLeft: 30 }}
                  />
                </View>

                <MyButton
                  title='SAVE'
                  onPress={() => { this.putImage() }}
                  titleStyle={{ color: Colors.whiteText, fontSize: 20 }}
                  containerStyle={{ marginVertical: 50, paddingVertical: 10 }}
                />
              </ScrollView>
            </View>
          </View>


          {
            this.props.isProfileHeader ?
              <ProfilePopup
                title='Profile Header'
                value={this.props.profileHeader}
                onChangeText={(txt) => { this.props.setProfileHeader(txt) }}
                onPress={() => {
                  this.props.setIsProfileHeader(!this.props.isProfileHeader)
                }}
              />
              : null
          }

          {
            this.props.isProfileDescription ?
              <ProfilePopup
                title='Profile Description'
                value={this.props.profileDescription}
                onChangeText={(txt) => { this.props.setProfileDescription(txt) }}
                onPress={() => {
                  this.props.setIsProfileDescription(!this.props.isProfileDescription)
                }}
              />
              : null
          }

          {
            this.props.isProfileAboutUs ?
              <ProfilePopup
                title='About Us'
                value={this.props.profileAboutUs}
                onChangeText={(txt) => { this.props.setProfileAboutUs(txt) }}
                onPress={() => {
                  this.props.setIsProfileAboutUs(!this.props.isProfileAboutUs)
                }}
              />
              : null
          }

          {/* {
            this.props.isProfileFirstName ?
              <ProfilePopup
                title='First Name'
                value={this.props.profileFirstName}
                onChangeText={(txt) => { this.props.setProfileFirstName(txt) }}
                onPress={() => {
                  this.props.setIsProfileFirstName(!this.props.isProfileFirstName)
                }}
              />
              : null
          }

          {
            this.props.isProfileLastName ?
              <ProfilePopup
                title='Last Name'
                value={this.props.profileLastName}
                onChangeText={(txt) => { this.props.setProfileLastName(txt) }}
                onPress={() => {
                  this.props.setIsProfileLastName(!this.props.isProfileLastName)
                }}
              />
              : null
          }


          {
            this.props.isProfileEmail ?
              <ProfilePopup
                title='Email'
                value={this.props.profileEmail}
                onChangeText={(txt) => { this.props.setProfileEmail(txt) }}
                onPress={() => {
                  this.props.setIsProfileEmail(!this.props.isProfileEmail)
                }}
              />
              : null
          }

          {
            this.props.isProfilePhoneNumber ?
              <ProfilePopup
                title='Phone Number'
                value={this.props.profilePhoneNumber}
                keyboardType="number-pad"
                maxLength={10}
                onChangeText={(txt) => { this.props.setProfilePhoneNumber(txt) }}
                onPress={() => {
                  this.props.setIsProfilePhoneNumber(!this.props.isProfilePhoneNumber)
                }}
              />
              : null
          } */}


          {/* SOCIAL MEDIA++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}

          {
            this.props.is_whatsapp ?
              <ProfilePopup
                title='WhatsApp'
                value={this.props.whatsapp}
                keyboardType="number-pad"
                maxLength={10}
                onChangeText={(txt) => { this.props.setProfileWhatsapp(txt) }}
                onPress={() => {
                  this.props.setIsProfileWhatsapp(!this.props.is_whatsapp)
                }}
              />
              : null
          }


          {
            this.props.is_twitter ?
              <ProfilePopup
                title='Twitter'
                value={this.props.twitter}
                // keyboardType="number-pad"
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileTwitter(txt) }}
                onPress={() => {
                  this.props.setIsProfileTwitter(!this.props.is_twitter)
                }}
              />
              : null
          }


          {
            this.props.is_messenger ?
              <ProfilePopup
                title='Messenger'
                value={this.props.messenger}
                // keyboardType="number-pad"
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileMessenger(txt) }}
                onPress={() => {
                  this.props.setIsProfileMessenger(!this.props.is_messenger)
                }}
              />
              : null
          }


          {
            this.props.is_linkedin ?
              <ProfilePopup
                title='Linkedin'
                value={this.props.linkedin}
                // keyboardType="number-pad"
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileLinkedin(txt) }}
                onPress={() => {
                  this.props.setIsProfileLinkedin(!this.props.is_linkedin)
                }}
              />
              : null
          }


          {
            this.props.is_instagram ?
              <ProfilePopup
                title='Instagram'
                value={this.props.instagram}
                // keyboardType="number-pad"
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileInstagram(txt) }}
                onPress={() => {
                  this.props.setIsProfileInstagram(!this.props.is_instagram)
                }}
              />
              : null
          }


          {
            this.props.is_youtube ?
              <ProfilePopup
                title='Youtube'
                value={this.props.youtube}
                // keyboardType="number-pad"
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileYoutube(txt) }}
                onPress={() => {
                  this.props.setIsProfileYoutube(!this.props.is_youtube)
                }}
              />
              : null
          }


          {
            this.props.is_pinterest ?
              <ProfilePopup
                title='Pinterest'
                value={this.props.pinterest}
                // keyboardType="number-pad"
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfilePinterest(txt) }}
                onPress={() => {
                  this.props.setIsProfilePinterest(!this.props.is_pinterest)
                }}
              />
              : null
          }

          {
            this.props.is_tiktok ?
              <ProfilePopup
                title='Tiktok'
                value={this.props.tiktok}
                // keyboardType="number-pad"
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileTiktok(txt) }}
                onPress={() => {
                  this.props.setIsProfileTiktok(!this.props.is_tiktok)
                }}
              />
              : null
          }

          {
            this.state.isLoading ?
              <View style={{ height, width, position: 'absolute', backgroundColor: "#0000", elevation: 10, justifyContent: 'center', alignItems: 'center' }} >
                <View style={{ height: 100, width: 100, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }} >
                  <Text style={{ fontSize: 30 }} >{"Loading..."}</Text>
                </View>
              </View>
              : null
          }
        </View>
      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  portrateMode: {
    flex: 1,
    backgroundColor: 'white',
    margin: 10,
    padding: 3,
    elevation: 10,
    borderRadius: 10,
    // marginTop: -PixelRatio.getPixelSizeForLayoutSize(25),
    // transform: [{ translateY: -PixelRatio.getPixelSizeForLayoutSize(0) }]
  },
})

const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_drawer: common.is_drawer,

    isProfileHeader: jaswant.isProfileHeader,
    isProfileDescription: jaswant.isProfileDescription,
    isProfileAboutUs: jaswant.isProfileAboutUs,
    isProfileFirstName: jaswant.isProfileFirstName,
    isProfileLastName: jaswant.isProfileLastName,
    isProfileEmail: jaswant.isProfileEmail,
    isProfilePhoneNumber: jaswant.isProfilePhoneNumber,

    profileImage1: jaswant.profileImage1,
    profileImage2: jaswant.profileImage2,
    profileCertificate: jaswant.profileCertificate,

    profileHeader: jaswant.profileHeader,
    profileDescription: jaswant.profileDescription,
    profileAboutUs: jaswant.profileAboutUs,
    profileFirstName: jaswant.profileFirstName,
    profileLastName: jaswant.profileLastName,
    profileEmail: jaswant.profileEmail,
    profilePhoneNumber: jaswant.profilePhoneNumber,

    is_whatsapp: jaswant.is_whatsapp,
    is_twitter: jaswant.is_twitter,
    is_messenger: jaswant.is_messenger,
    is_linkedin: jaswant.is_linkedin,
    is_instagram: jaswant.is_instagram,
    is_youtube: jaswant.is_youtube,
    is_pinterest: jaswant.is_pinterest,
    is_tiktok: jaswant.is_tiktok,

    whatsapp: jaswant.whatsapp,
    twitter: jaswant.twitter,
    messenger: jaswant.messenger,
    linkedin: jaswant.linkedin,
    instagram: jaswant.instagram,
    youtube: jaswant.youtube,
    pinterest: jaswant.pinterest,
    tiktok: jaswant.tiktok,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    // setIsDrawer: (domain) => setIsDrawer(domain),
    setIsProfileHeader: (isTrue) => setIsProfileHeader(isTrue),
    setIsProfileDescription: (isTrue) => setIsProfileDescription(isTrue),
    setIsProfileAboutUs: (isTrue) => setIsProfileAboutUs(isTrue),
    setIsProfileFirstName: (isTrue) => setIsProfileFirstName(isTrue),
    setIsProfileLastName: (isTrue) => setIsProfileLastName(isTrue),
    setIsProfileEmail: (isTrue) => setIsProfileEmail(isTrue),
    setIsProfilePhoneNumber: (isTrue) => setIsProfilePhoneNumber(isTrue),

    setProfileImage1: (img) => setProfileImage1(img),
    setProfileImage2: (img) => setProfileImage2(img),
    setProfileCertificate: (certificate) => setProfileCertificate(certificate),
    setProfileHeader: (txt) => setProfileHeader(txt),
    setProfileDescription: (txt) => setProfileDescription(txt),
    setProfileAboutUs: (txt) => setProfileAboutUs(txt),
    setProfileFirstName: (txt) => setProfileFirstName(txt),
    setProfileLastName: (txt) => setProfileLastName(txt),
    setProfileEmail: (txt) => setProfileEmail(txt),
    setProfilePhoneNumber: (txt) => setProfilePhoneNumber(txt),


    setIsProfileWhatsapp: (number) => setIsProfileWhatsapp(number),
    setIsProfileTwitter: (link) => setIsProfileTwitter(link),
    setIsProfileMessenger: (link) => setIsProfileMessenger(link),
    setIsProfileLinkedin: (link) => setIsProfileLinkedin(link),
    setIsProfileInstagram: (link) => setIsProfileInstagram(link),
    setIsProfileYoutube: (link) => setIsProfileYoutube(link),
    setIsProfilePinterest: (link) => setIsProfilePinterest(link),
    setIsProfileTiktok: (link) => setIsProfileTiktok(link),

    setProfileWhatsapp: (number) => setProfileWhatsapp(number),
    setProfileTwitter: (link) => setProfileTwitter(link),
    setProfileMessenger: (link) => setProfileMessenger(link),
    setProfileLinkedin: (link) => setProfileLinkedin(link),
    setProfileInstagram: (link) => setProfileInstagram(link),
    setProfileYoutube: (link) => setProfileYoutube(link),
    setProfilePinterest: (link) => setProfilePinterest(link),
    setProfileTiktok: (link) => setProfileTiktok(link),





  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)