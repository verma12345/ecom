import React, { Component } from 'react';
import {
  Button, View, Text, SafeAreaView, Dimensions, StyleSheet,
  TouchableOpacity, PixelRatio, Alert,
  ImageBackground, Image, ScrollView, TextInput, KeyboardAvoidingView, Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import ProfilePopup from '../components/ProfilePopup';

import ContactUsHeader from '../components/ContactUsHeader';
import Font from '../common/Font';
import { sendEmail } from '../common/common';
import { debugLog } from '../common/Constants';

class ContactUsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      comments: '',
      attached_file: '',
      isFocused: false
    }

  }

  handleAttachedFile = () => {
    this.setState({ attached_file: '' })
  }

  onChangeName = (txt) => {
    this.setState({
      name: txt
    })
  }

  onChangeEmail = (txt) => {
    this.setState({
      email: txt
    })
  }

  onChangeComments = (txt) => {
    this.setState({
      comments: txt
    })
  }

  onGoBack = () => {
    this.props.navigation.goBack()
  }

  onEmailPress = () => {
    let email_to = ['admin@ecomnow.com'];
    let subject = 'Contact us';
    let body = this.state.name + "\n" + this.state.comments + "Tkank you!\n" + this.state.email
    sendEmail(email_to, subject, body)
    this.setState({ name: '', email: '', comments: '' })
    this.onGoBack()
  }


  render() {
    debugLog(this.state.isFocused)
    return (

      <SafeAreaView style={msStyle.container}>
        <View style={msStyle.container}>

          {/* {/ Need to change bg color /} */}
          <View style={{
            flex: 1,
            backgroundColor: Colors.themeColor
          }} >
            <ContactUsHeader
              title='Contact Us'
              icon={require('../../assets/back.png')}
              tintColor="white"
              onPress={() => { this.onGoBack() }}
              containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
              titleStyle={{ color: Colors.whiteText }}
            />
          </View>



          <View style={{ flex: 2.3, backgroundColor: Colors.whiteText }} >

            {/* {/ Absolute UI /} */}
            <View style={styles.portrateMode} >

              <KeyboardAvoidingView
                style={{ flex: 1 }}
                //  keyboardVerticalOffset={100}
                behavior={Platform.OS === "ios" ? "position" : "height"}
              >
                <ScrollView
                  //  ref={(ref) => { this.scrollListReftop = ref; }}
                  // keyboardDismissMode={'on-drag'}
                  //alwaysBounceVertical={true}
                  // keyboardShouldPersistTaps={this.props.service_index >= 0 ? 'always' : 'never'}
                  // scrollEnabled={this.props.service_index >= 0 ? false : true}
                  contentContainerStyle={{
                    // flexGrow: 1,// use this to solve this problem
                    paddingBottom: this.state.isFocused ? 300 : 0,

                  }}>


                  {/* {/ {input} /} */}

                  <Text style={[msStyle.txtStyle, { fontSize: 25, paddingHorizontal: 20, paddingTop: 15 }]}>
                    {'eComNow'}
                  </Text>

                  <Text style={[msStyle.txtStyle, { fontSize: 16, fontWeight: '500', paddingHorizontal: 20 }]}>
                    {'support@ecomnow.com'}
                  </Text>


                  <View style={{ paddingHorizontal: 20, paddingTop: 15 }}>
                    <Text style={[msStyle.txtStyle, { paddingBottom: 6, color: 'gray', fontSize: 14 }]}>
                      {'Full Name'}
                    </Text>
                    <TextInput
                      onFocus={() => { this.setState({ isFocused: true }) }}
                      onBlur={() => { this.setState({ isFocused: false }) }}
                      placeholder="David Mathew"
                      placeholderTextColor='lightgray'
                      value={this.state.name}
                      onChangeText={this.onChangeName}
                      keyboardType="default"
                      style={[styles.inputStyle]}

                    />
                  </View>

                  <View style={{ paddingHorizontal: 20, marginTop: 15, backgroundColor: 'white' }}>
                    <Text style={[msStyle.txtStyle, { marginBottom: 6, color: 'gray', fontSize: 14 }]}>
                      {'Email'}
                    </Text>
                    <TextInput
                      onFocus={() => { this.setState({ isFocused: true }) }}
                      onBlur={() => { this.setState({ isFocused: false }) }}
                      placeholder="david@ecomnow.com"
                      placeholderTextColor='lightgray'
                      autoCapitalize='none'
                      keyboardType="email-address"
                      value={this.state.email}
                      onChangeText={this.onChangeEmail}
                      style={[styles.inputStyle]}
                    />
                  </View>

                  <View style={{ paddingHorizontal: 20, paddingTop: 15 }}>
                    <Text style={[msStyle.txtStyle, { marginBottom: 6, color: 'gray', fontSize: 14 }]}>
                      {'Comments'}
                    </Text>
                    <TextInput
                      onFocus={() => { this.setState({ isFocused: true }) }}
                      onBlur={() => { this.setState({ isFocused: false }) }}
                      placeholder="Vivamus ferentum consequat mouris porta pharetra quam porta quis class aptent taciti sociosquad litora torquent per conubia nostra, per inceptos himenaos."
                      placeholderTextColor='lightgray'
                      multiline={true}
                      value={this.state.comments}
                      keyboardType="default"
                      onChangeText={this.onChangeComments}
                      style={[styles.inputStyle, { height: 100, fontSize: 14, textAlignVertical: 'top' }]}
                    />
                  </View>

                </ScrollView>

              </KeyboardAvoidingView>

              <TouchableOpacity style={{
                paddingVertical: 10,
                // marginVertical: 50,
                width: '90%',
                backgroundColor: '#026BB8',
                borderRadius: 10,
                alignSelf: 'center',
                justifyContent: 'center', alignItems: 'center',
                // marginBottom: 50
                marginBottom: this.state.isFocused ? 0 : 50,
              }}
                onPress={() => this.onEmailPress()}
              >
                <Text style={{ color: 'white', fontSize: 20 }} >
                  {'SEND'}
                </Text>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </SafeAreaView>

    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  portrateMode: {
    flex: 1,
    backgroundColor: 'white',
    margin: 10,
    padding: 3,
    elevation: 10,
    borderRadius: 10,
    marginTop: -PixelRatio.getPixelSizeForLayoutSize(25)
  },
  buttonStyle: {
    backgroundColor: "white",
    alignSelf: 'flex-end',
    padding: 5,
    position: 'absolute',
    bottom: -5,
    right: -5,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: Colors.themeColor,

  },
  inputStyle: {
    borderWidth: 1,
    fontSize: 18,
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    borderRadius: 10,
    fontFamily: Font.SourceSansPro,
    fontWeight: "300",
    color: '#000000'
  }
})

const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_drawer: common.is_drawer,

    profileCertificate: jaswant.profileCertificate,
    profileCertificateList: jaswant.profileCertificateList,
    isProfileCertificateName: jaswant.isProfileCertificateName,
    profileCertificateName: jaswant.profileCertificateName,
    profileCertificateIndex: jaswant.profileCertificateIndex,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({

    setProfileCertificate: (certificate) => setProfileCertificate(certificate),
    setIsProfileCertificateName: (isTrue) => setIsProfileCertificateName(isTrue),
    setProfileCertificateEmptyList: (isTrue) => setProfileCertificateEmptyList(isTrue),
    setProfileCertificateListName: (name, index) => setProfileCertificateListName(name, index),
    setProfileCertificateListImage: (image, index) => setProfileCertificateListImage(image, index),
    setProfileCertificateName: (name) => setProfileCertificateName(name),
    setProfileCertificateIndex: (index) => setProfileCertificateIndex(index),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactUsScreen)