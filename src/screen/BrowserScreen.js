

import React, { Component } from 'react'
import { WebView } from 'react-native-webview'
import { ActivityIndicator, StyleSheet } from 'react-native'

export default class BrowserScreen extends Component {

  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.state = {
      weburl: ''
    };
  }
  ActivityIndicatorLoadingView() {

    return (

      <ActivityIndicator
        color='#009688'
        size='large'
        style={styles.ActivityIndicatorStyle}
      />
    );
  }

  injectjs() {
    let jsCode = `const bodyData = JSON.stringify({
          title: 'foo',
          body: 'bar',
          userId: 1
        });
        fetch('https://jsonplaceholder.typicode.com/posts', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: bodyData,
        }).then(response => response.text()).then(valueText => {
          alert(JSON.stringify(valueText));
        });`;
    return jsCode;
  }
  render() {
    const jsCode = "window.postMessage(document.getElementById('gb-main').innerHTML)"
    let domainName = this.params.domain
    let statictestweburl = 'https://www.ecompaas.com/test-sso/login?state=login->true,domain->cscestorexpress.com,platform->BHT,source->mobile'
    this.state.weburl = "https://www.ecompaas.com/test-sso/login?state=login->true,domain->" + this.params.domain + ",platform->" + this.params.platform + ",source->mobile"
    // const runFirst = window.ReactNativeWebView.postMessage("this is mffffessage from web");

    return (
      <WebView

        source={{ uri: this.state.weburl }}

        //renderLoading={this.LoadingIndicatorView}
        startInLoadingState={true}
        onNavigationStateChange={this.handleWebViewNavigationStateChange}
        renderLoading={this.ActivityIndicatorLoadingView}
        injectedJavaScript={jsCode}
        //  domStorageEnabled={true}

        //   injectedJavaScript={runFirst}

        // injectedJavaScript="window.postMessage(document.title)"

        //  onMessage={(event)=> console.log(JSON.parse(event.nativeEvent.data))}

        javaScriptEnabled={true}
      // onMessage={({ nativeEvent: state }) => {
      //     const { url } = nativeEvent;
      //     console.log('test it');
      //     if (url.includes('mobile-login-success')) {
      //         console.log(JSON.parse(nativeEvent.data))
      //     }
      //   }}

      // onMessage={event => console.log('Received: ', event.nativeEvent.data)}

      />
    )


  }
  handleWebViewNavigationStateChange = newNavState => {
    // newNavState looks something like this:
    // {
    //   url?: string;
    //   title?: string;
    //   loading?: boolean;
    //   canGoBack?: boolean;
    //   canGoForward?: boolean;
    // }

    const { url } = newNavState;

    if (!url) return;
    console.log('hhhhhhhh', url)

    // redirect somewhere else
    if (url.includes('mobile-login-success')) {
      // this.webview.stopLoading();

      console.log('Hi from Reatttct Native');


      const loginurl = this.state.weburl
      fetch(loginurl, {
        headers: {

          'Content-Type': 'application/json'
        },
      }).then(response => response.text()).then(valueText => {

        console.log('>>>>>', JSON.stringify(valueText))

      });
      /* fetch(loginurl)
       .then((response) => response.json())
       .then((json) => {
         console.log(json)
       })
 
       .catch((error) => console.error(error))*/
    }
  };
}

const styles = StyleSheet.create(
  {

    WebViewStyle:
    {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      marginTop: (Platform.OS) === 'ios' ? 20 : 0
    },

    ActivityIndicatorStyle: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center'

    }
  });