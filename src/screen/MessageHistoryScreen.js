import React, { Component } from 'react';
import { Image, Button, View, Text, SafeAreaView, TextInput, Dimensions, StyleSheet, ScrollView, SectionList, TouchableOpacity, flexDirection, FlatList } from 'react-native';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getPrefs } from '../common/Prefs';
import { searchMessageHistory, serMessageHistoryList, setMessageHistoryDetails } from '../redux_store/actions/indexActionsJaswant';
import SearchHeader from '../components/SearchHeader';
import Constants, { debugLog } from "../common/Constants";


class MessageHistoryScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isSearch: false
    }
  }



  getMessageHistory = () => {

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Cookie", "AWSALBTG=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; AWSALBTGCORS=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; SESSION=Mjg3Y2Q5ODctMzI0OS00NjhkLThhNDEtOTRkNzEzNDEwYzBl");
    var raw = ''
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    // {"cbp": 3, "createdAt": 1625066191, "domainName": "cscestorexpress.com", "platformId": "bht", "product": 
    // null, "randomkey": null, "source": "mobile", "storeNo": 2336, "uniqueId": "001", "updatedAt": 
    // 1625066194, "userEmail": "barneyrubble@testemail.com", "userFirstName": "test", "userLastName": "Testing"}

    //let urlTest = `${Constants.API_BASE_URL}/admin/recommendations/history?uniqueId=001&cbpNumber=3&storeNumber=2336`

    let url = `${Constants.API_BASE_URL}/admin/recommendations/history?uniqueId=${this.state.loginData.uniqueId}&cbpNumber=${this.state.loginData.cbp}&storeNumber=${this.state.loginData.storeNo}`


    // debugLog("my url")
    // debugLog(this.state.loginData.uniqueId)
    fetch(url, requestOptions)
      .then(response => response.text())
      .then(result => {
        let json = JSON.parse(result)
        // this.setState({ data: json })
        this.props.serMessageHistoryList(json)
        // debugLog(json)

      })
      .catch(error => console.log('error', error));

  }

  getLoginCrediential = async () => {
    getPrefs('ACCESS_TOKEN').then((value) => {
      // debugLog(value)
      let json = JSON.parse(value)
      // this.props.setLoginRespons(json)
      this.setState({ loginData: json })
      // debugLog(json.domainName)
      this.getMessageHistory()

    })
  }
  getParsedDate(strDate) {
    var strSplitDate = String(strDate).split(' ');
    var date = new Date(strSplitDate[0]);
    // alert(date);
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!

    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    date = dd + "/" + mm + "/" + yyyy;
    return date.toString();
  }



  componentDidMount() {
    this.getLoginCrediential()
    // this.getMessageHistory()

  }

  onMessageDetailScreen = (data) => {
    // debugLog('item----------------------------')
    // debugLog(data)
    this.props.setMessageHistoryDetails(data)
    this.props.navigation.navigate('MessageDetailScreen')
  }

  renderItem = (dataItem) => {
    return (
      <TouchableOpacity
        onPress={() => { this.onMessageDetailScreen(dataItem.item) }}
      >

        <View style={[msStyle.myshadow2, {
          flex: 1,
          padding: 15,
          backgroundColor: 'white',
          borderRadius: 7,
          marginHorizontal: 5,
          marginTop: 10
        }]}>

          <View style={{
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'space-evenly',
            paddingBottom: 15
          }}>
            <Text
              style={{ flex: .99, fontWeight: 'bold', alignItems: 'flex-start' }}>{dataItem.item.subject}</Text>
            <TouchableOpacity
              onPress={() => { }}
            >
              <Image style={{
                height: 15,
                width: 15,
                resizeMode: 'contain'
              }}
                source={require('../../assets/rightArrow.png')} />
            </TouchableOpacity>
          </View>
          <View>
            <Text >{dataItem.item.message}</Text>
            <Text style={{ color: "grey" }}>{this.getParsedDate(dataItem.item.createdAt)}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }


  render() {
    return (
      <SafeAreaView style={msStyle.container}>


        {/* MARK:  body */}

        <View style={(msStyle.container)}>

          <SearchHeader
            title='Message History'
            placeholder="Search"
            isInput={this.state.isSearch}
            onChangeText={(txt) => { this.props.searchMessageHistory(txt) }}
            onGoBack={() => this.props.navigation.goBack()}
            onSearch={() => { this.setState({ isSearch: !this.state.isSearch }) }} />

          <FlatList
            contentContainerStyle={{ paddingBottom: 100 }}
            style={styles.list}
            data={this.props.message_history_list}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />
        </View>

      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  absoluteStyle: {
    position: 'absolute',
    backgroundColor: Colors.whiteText,
    alignSelf: 'center',
    width: width - 30,
    height: height - 70,
    elevation: 10,
    zIndex: 5,
    bottom: 15,
    borderRadius: 5
  },
  list: {
    flex: 1,
    padding: 15

  }

})


const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    loginResponse: common.loginResponse,
    message_history_list: jaswant.message_history_list,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    serMessageHistoryList: (list) => serMessageHistoryList(list),
    searchMessageHistory: (txt) => searchMessageHistory(txt),
    setLoginRespons: (res) => setLoginRespons(res),
    setMessageHistoryDetails: (list) => setMessageHistoryDetails(list),
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(MessageHistoryScreen)