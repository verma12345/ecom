import React, { Component } from "react";
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, TouchableOpacity, ScrollView, ActivityIndicator } from "react-native";
import WebView from "react-native-webview";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { msStyle } from "../common/MyStyle";
import MyLoader from "../components/MyLoader";
import { hitWestornUnion } from "../redux_store/actions/indexActionsApi";

class WesternUnionCreateAccount extends Component {
  constructor(props) {
    super(props)
  }
  onGoBack = () => {
    this.props.navigation.goBack()
  }




  renderLoadingView() {
    return (
      <MyLoader />
    );
  }


  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container} >

          {/* header */}
          <View style={{ height: 40, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }} >
            <TouchableOpacity
              onPress={() => this.onGoBack()}
              style={{ position: 'absolute', left: 10 }} >
              <Image
                source={require('../../assets/back.png')}
                style={{ height: 25, width: 25, tintColor: '#141212', resizeMode: 'contain' }}
              />
            </TouchableOpacity>
            <Text style={[msStyle.txtStyle, { fontSize: 22, color: '#141212' }]} >{'Western Union'}</Text>
          </View>


          <WebView
            source={{ uri: `https://payee.globalpay.westernunion.com/PayeeManager/BeneficiaryEnrollment/CustomizedEnrollmentProcess.aspx?cid=45785B6194F6875B2732E7DF82A60114&sid=CSCr${this.props.loginResponse.storeNo}-${this.props.loginResponse.uniqueId}` }}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            originWhitelist={['*']}
            renderLoading={this.renderLoadingView}
            startInLoadingState={true}
          />

        
        </View>
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000a0"
  }
});



const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_loading: common.is_loading,

    accessAndSecretsKey: jaswant.accessAndSecretsKey,
    loginResponse: common.loginResponse,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    hitWestornUnion: (params) => hitWestornUnion(params),
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(WesternUnionCreateAccount)