import React, { Component } from 'react';
import { Button, View, Text, ActivityIndicator, Dimensions, BackHandler, Alert, SafeAreaView, ScrollView, Keyboard } from 'react-native';
import WebView from 'react-native-webview';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import { msStyle } from '../common/MyStyle';
import { getPrefs, setPrefs } from '../common/Prefs';
import { setLoading, setLoginRespons, setRefereshGetProfile } from '../redux_store/actions/indexActions';
import { hitSocialLogin } from '../redux_store/actions/indexActionsApi';
import { setIsLoginFailed, setLoginFailedMessage } from '../redux_store/actions/indexActionsJaswant';

class SignInScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loading: false,
      isResponse: null,
      isError: null,
      loginResponse: null,
    }
  }



  onValidCredintial = () => {
    debugLog(this.state.loginResponse)
    debugLog('verification')
    if (this.state.loginResponse == null) {
      this.onLoginFailed('Ooops Something went wrong!')
      return
    }



    if (this.state.loginResponse.cbp == null ||
      this.state.loginResponse.domainName == null ||
      this.state.loginResponse.uniqueId == null ||
      this.state.loginResponse.platformId == null ||
      this.state.loginResponse.storeNo == null
    ) {
      this.onLoginFailed('Ooops something went wrong in login response please try again!')
      return
    }



    setPrefs("ACCESS_TOKEN", JSON.stringify(this.state.loginResponse))
    this.props.setLoginRespons(this.state.loginResponse)

    if (this.state.loginResponse.domainName.toLowerCase() != this.props.domain.toLowerCase()) {
      this.showAlert1('Please enter valid platform')
      return
    }


    if (this.state.loginResponse.userEmail != this.props.email) {
      this.showAlert1('Please enter valid email')
      return
    }

    if (this.state.loginResponse.platformId.toLowerCase() != this.props.platform.toLowerCase()) {
      this.showAlert1('Please enter valid platform')
      return
    }

    this.setState({ isResponse: false })
    if (this.props.refereshGetProfile == 1) {
      this.props.setRefereshGetProfile(2)
    }

    this.props.navigation.navigate('ProfileScreen1')
    return
  }

  renderLoadingView() {
    return (
      <View style={{
        borderRadius: 10,
        backgroundColor: "white",
        flex: 1,

      }}>

        <ActivityIndicator
          size={"large"}
          color={Colors.themeColor}
          hidesWhenStopped={true}
        />

      </View>
    );
  }

  showAlert1(msg) {
    Alert.alert(
      'Login Error',
      msg,
      [
        {
          text: 'Go Back',
          onPress: () => {
            this.onOpenOneTimeScreen()
          }
        },
      ]
    );
  }

  onOpenOneTimeScreen = () => {
    this.props.navigation.navigate('OneTimeScreen')
  }

  onLoginFailed = (msg) => {
    this.props.setIsLoginFailed(true)
    this.props.setLoginFailedMessage(msg)
    this.props.navigation.navigate('OneTimeScreen')
  }

  handleWebViewNavigationStateChange = newNavState => {

    debugLog(newNavState)
    debugLog('After Log in')

    this.setState({ isLoading: true })
    const { url } = newNavState;
    if (!url) return;

    console.log('urlllll>>>>>', url)
    if (url.includes('/sso/error') || url.includes('test-sso/error/') ){   

      this.onLoginFailed('Please contact your Support adminstrator or Submit Contact E-mail as you currently do not have a user Credentials to access the system!')
      return

    }
    if (url.includes("mobile-login-success?authUser=")) {

      const authUserId = url.substr(url.indexOf('=') + 1)

      var apiUrl = `https://www.ecompaas.com/test-sso/get-mobile-user?authUser=${authUserId}`
      debugLog(apiUrl)

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
      };

      this.setState({ is_loading: true })
      fetch(apiUrl, requestOptions).then(async response => {
        try {
          let res = await response.json();
          this.setState({ loginResponse: res })
          this.onValidCredintial()

        } catch (error) {
          debugLog(error)
        }
      }).catch((error) => {
        debugLog(error)
      })
    }
  };



  render() {

    const jsCode = "window.waitForBridge = function(fn) { return (window.postMessage.length === 1) ? fn() : setTimeout(function() { window.waitForBridge(fn) }, 5) }; window.waitForBridge(function() { window.postMessage(document.body.innerText });"

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <WebView
          incognito={true}
          userAgent="Mozilla/5.0 Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"
          source={{ uri: "https://www.ecompaas.com/sso/login?state=login->true,domain->" + this.props.domain + ",platform->" + this.props.platform + ",source->mobile" }}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          originWhitelist={['*']}
          renderLoading={this.renderLoadingView}
          onNavigationStateChange={this.handleWebViewNavigationStateChange}
          startInLoadingState={true}
          onMessage={event => console.log('Received: ', event.nativeEvent.data)}
          injectedJavaScript={jsCode}
          javaScriptEnabledAndroid={true}
        />

        {
          this.state.is_loading ?
            <View style={{
              position: 'absolute',
              width: Dimensions.get('window').width,
              height: Dimensions.get('window').height,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center'
            }} >
              <ActivityIndicator
                size={"large"}
                color={Colors.themeColor}
              />
              <Text style={msStyle.txtStyle} >{"Please wait for a while..."}</Text>
            </View>
            : null
        }

      </SafeAreaView>
    )
  }


}



const mapStateToProps = (state) => {
  let common = state.indexReducer
  return {
    is_loading: common.is_loading,
    domain: common.domain,
    email: common.email,
    platform: common.platform,
    is_drawer: common.is_drawer,
    refereshGetProfile: common.refereshGetProfile,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    hitSocialLogin: (domain) => hitSocialLogin(domain),
    setLoading: (isLoading) => setLoading(isLoading),
    setLoginRespons: (res) => setLoginRespons(res),
    setRefereshGetProfile: (referesh) => setRefereshGetProfile(referesh),
    setIsLoginFailed: (isTrue) => setIsLoginFailed(isTrue),
    setLoginFailedMessage: (msg) => setLoginFailedMessage(msg)

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen)