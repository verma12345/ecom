import React, { Component } from 'react';
import {
    Button, View, Text, SafeAreaView, Dimensions, StyleSheet,
    TouchableOpacity, PixelRatio, Alert,
    ImageBackground, Image, ScrollView, FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';

import MyHeader from '../components/MyHeader';
import LinearGradient from 'react-native-linear-gradient';
import { msStyle } from '../common/MyStyle';
import { setCustomerTotalCommition, setCustomerTotalSales } from '../redux_store/actions/indexActionsJaswant';


class CustomerReportScreen extends Component {
    constructor(props) {
        super(props);

    }

    onGoBack = () => {
        this.props.navigation.goBack()
    }


    renderItem = (dataItem) => {
        return (
            <View style={[msStyle.myshadow,{
                flexDirection: 'row',
                justifyContent: 'space-between',
                elevation: 5,
                zIndex: 5,
                backgroundColor: "white",
                paddingHorizontal: 10,
                paddingVertical: 20,
                margin: 10,
                borderRadius: 5,
                // marginBottom: dataItem.index == this.props.customerReportList.length - 1 ? 200 : 10
            }]} >

                <View style={{
                    flexDirection: 'row',
                }} >
                    <Text style={[msStyle.txtStyle, { fontSize: 14, fontWeight: '500', color: '#302f33' }]}  >{'Order Number:'}</Text>
                    <Text style={[msStyle.txtStyle, { fontSize: 14, fontWeight: '500', color: '#302f33' }]}  >{dataItem.item.orderNumber}</Text>
                </View>

                <View style={{
                    flexDirection: 'row',
                }} >
                    <Text style={[msStyle.txtStyle, { fontSize: 14, fontWeight: '500', color: '#302f33' }]}  >{'Order Date:'}</Text>
                    <Text style={[msStyle.txtStyle, { fontSize: 14, fontWeight: '500', color: '#302f33' }]}  >{dataItem.item.issueDate.substring(1, dataItem.item.issueDate.indexOf(" "))}</Text>
                </View>

            </View>
        )
    }

    listHeaderItem = (data) => {
        return (
            <View style={{ alignItems: 'center' }} >
                <TouchableOpacity
                    style={[msStyle.myshadow,{ borderRadius: 360, marginTop: 30, backgroundColor: 'white',  }]}
                >
                    <View style={{ height: 100, margin: 10, width: 100, borderRadius: 100, backgroundColor: 'gray', justifyContent: 'center', alignItems: 'center', }} >
                        <Text style={[msStyle.txtStyle, { color: 'white', fontSize: 25 }]}>
                            {this.props.customerReportList[0].customerFirstName != null ? this.props.customerReportList[0].customerFirstName.charAt(0) : null}
                            {this.props.customerReportList[0].customerLastName != null ? this.props.customerReportList[0].customerLastName.charAt(0) : null}
                        </Text>
                    </View>
                    {/* <Image
                    source={require('../../assets/icon.png')}
                    style={{ height: 100, width: 100, resizeMode: 'contain', borderRadius: 300, margin: 5 }}
                /> */}
                </TouchableOpacity>

                <Text style={{ fontSize: 16, marginTop: 10, fontWeight: '700', }} >
                    {this.props.customerReportList[0].customerFirstName} {this.props.customerReportList[0].customerLastName} </Text>

                <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'center' }} >
                    <Text style={{ fontSize: 14, fontWeight: '500' }} >{'Sale'} </Text>
                    <Text style={{ fontSize: 16, fontWeight: '700' }} >{'$'}{this.props.totalCustomerSalesAmount.toFixed(2)} </Text>
                </View>

                <View style={{ flexDirection: 'row', marginVertical: 10, alignItems: 'center', justifyContent: 'center' }} >
                    <Text style={{ fontSize: 14, fontWeight: '500' }} >{'Commission'} </Text>
                    <Text style={{ fontSize: 16, fontWeight: '700' }} >{'$'} {this.props.totalCustomerCommitionAmount.toFixed(2)}</Text>
                </View>

            </View>
        )
    }


    render() {

        return (
            <SafeAreaView style={[styles.container]}>
                <View style={styles.container}>

                    {/* Need to change bg color */}

                    <LinearGradient
                        start={{ x: 1, y: 1 }}
                        end={{ x: 0, y: 0 }}
                        colors={['#0b6fbd', '#064673']}
                        style={{
                            // height: 90,
                            flex: 1,
                            justifyContent: 'space-around',
                            // padding: 10,

                        }}>
                        <View style={{
                            flex: 1,
                            // backgroundColor: "blue"
                        }} >
                            <MyHeader
                                title='Customer Report'
                                icon={require('../../assets/back.png')}
                                tintColor="white"
                                onPress={() => { this.onGoBack() }}
                                containerStyle={{ elevation: 0 }}
                                titleStyle={{ color: Colors.whiteText }}
                            />



                        </View>
                    </LinearGradient>

                    <View style={{ flex: 5, backgroundColor: Colors.whiteText }} >

                        {/* Absolute UI */}
                        <View style={[msStyle.myshadow, styles.portrateMode]} >

                            {/* profile section */}


                            <FlatList
                                contentContainerStyle={{ paddingBottom: 50 }}
                                data={this.props.customerReportList}
                                renderItem={this.renderItem}
                                ListHeaderComponent={ this.listHeaderItem}
                                keyExtractor={(item, index) => 'key' + index}
                            />

                        </View>
                    </View>



                </View>
            </SafeAreaView>
        )
    }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    portrateMode: {
        flex: 1,
        backgroundColor: 'white',
        margin: 10,
        padding: 3,
        elevation: 10,
        borderRadius: 10,
        marginTop: -PixelRatio.getPixelSizeForLayoutSize(25)
    },
    buttonStyle: {
        backgroundColor: "white",
        alignSelf: 'flex-end',
        padding: 5,
        position: 'absolute',
        bottom: -5,
        right: -5,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: Colors.themeColor,

    },
})

const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let jaswant = state.indexReducerJaswant;

    return {
        customerReportList: jaswant.customerReportList,
        totalCustomerSalesAmount: jaswant.totalCustomerSalesAmount,
        totalCustomerCommitionAmount: jaswant.totalCustomerCommitionAmount,
    }
}

const mapDispatchToProps = (dispatch) => {

    return bindActionCreators({

        setCustomerTotalSales: (amt) => setCustomerTotalSales(amt),
        setCustomerTotalCommition: (amt) => setCustomerTotalCommition(amt),


    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(CustomerReportScreen)