import React, { Component, useState } from 'react';
import { Image, Button, View, Text, SafeAreaView, TextInput, Dimensions, StyleSheet, ScrollView, SectionList, TouchableOpacity, flexDirection, FlatList } from 'react-native';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import MyHeader from '../components/MyHeader';
import ProfilePickerComponent from '../components/ProfilePickerComponent';
import { debugLog } from '../common/Constants';
import MyButton from '../components/MyButton';
import MyInputText from '../components/MyInputText';
import { setDomain, setEmail, setPlatform } from '../redux_store/actions/indexActions';
import { getPixelSizeForLayoutSize } from 'react-native/Libraries/Utilities/PixelRatio';
import { NativeViewGestureHandler } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';




class RecommendationDetailScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          subject: 'First Item',
          image: 'https://picsum.photos/200',
          createdAt: 'sale up to 50%',
          message: 'this is test message this is test messagethis is test messagethis is test messagethis is test messagethis is test messagethis is test messagethis is test message'
        },
        {
          id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
          subject: 'Second Item',
          image: 'https://picsum.photos/200',
          createdAt: 'sale up to 50%',
          message: 'this is test message'

        },
        {
          id: '58694a0f-3da1-471f-bd96-145571e29d72',
          subject: 'Third Item',
          image: 'https://picsum.photos/200',
          createdAt: 'sale up to 50%',
          message: 'this is test message'

        },
      ],
      data: this.props.recommendation_history_details,
      deliveryMethod: this.props.recommendation_history_details.deliveryMethods,
      productList: {},
      whatsappCount: 0,
      messengerCount: 0,
      instagramCount: 0,
      pinterestCount: 0,
      mailCount: 0,
      messageCount: 0,


    }
  }

  getDeliveryMethod_count = () => {

    this.state.deliveryMethod.map((y) => {
      debugLog(y.methodName)
      switch (y.methodName) {
        case 'WhatsApp':
          this.setState({ whatsappCount: y.count })
          break;
        case 'Massenger':
          this.setState({ messengerCount: y.count })
          break;
        case 'InstaGram':
          this.setState({ instagramCount: y.count })
          break;
        case 'Pintrest':
          this.setState({ pintrestCount: y.count })
          break;
        case 'Mail':
          this.setState({ mailCount: y.count })
          break;
        case 'Message':
          this.setState({ messageCount: y.count })
          break;
        default:
          debugLog('Nothing')
          break;
      }

    })
  }

  getParsedDate(strDate) {
    var strSplitDate = String(strDate).split(' ');
    var date = new Date(strSplitDate[0]);
    // alert(date);
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!

    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    date = dd + "/" + mm + "/" + yyyy;
    return date.toString();
  }

  renderItem = (product) => {
    return (
      <View style={{
        flex: 1,
        // justifyContent: 'center',
        paddingBottom: 6, flexDirection: 'row',
        alignItems: 'center', shadowColor: '#000', shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.4,
        marginBottom: 10,
        elevation: 2,
        position: 'relative',
        justifyContent: 'space-between',
        // shadowColor: 'gray',
        // borderColor: 'black',
        // borderWidth:1

      }}>
        <View style={[{
            flex: 1,
            borderWidth: 1,
            borderColor: 'lightgray',
            borderTopLeftRadius: 6,
            borderBottomLeftRadius: 6,
            borderBottomRightRadius: 6,
            backgroundColor: 'white',
            margin: 5,
            marginRight: 10
          }]} >
            <Image style={{
              width: Dimensions.get('window').width / 4,
              height: Dimensions.get('window').width / 4,
              borderTopLeftRadius: 6,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6,
            }}
              source={{ uri: product.item.image }} />
          </View>
        <View style={{
          flex: 2, padding: 5, alignItems: 'center'
        }}>
          <View style={{
            flex: 3,
            // justifyContent: 'space-around',
            flexDirection: 'row', alignItems: 'center', paddingLeft: 5
          }}>
            <View style={{
              flex: .5,
              // justifyContent: 'space-around',
              alignItems: 'flex-start',
              paddingLeft: 5
            }}>
              <Text style={{ fontWeight: 'normal', paddingBottom: 10 }}>{'Date: ' + this.getParsedDate(this.props.recommendation_history_details.createdAt)}</Text>
              <Text style={{ fontWeight: 'bold', paddingBottom: 5 }}>{product.item.brandName}</Text>
              <Text style={{ fontWeight: 'normal', paddingBottom: 10 }}>{'Brand Name'}</Text>
              <Text style={{ fontWeight: 'normal', paddingBottom: 5 }}>{product.item.sku}</Text>
            </View>
            <View style={{
              flex: .5,
              // justifyContent: 'space-around',
              alignItems: 'flex-end',
              paddingLeft: 5
            }}>
              <Text style={{ fontWeight: 'normal', paddingBottom: 10, }} numberOfLines={1}>{product.item.message}</Text>
              <Text style={{ fontWeight: 'normal', paddingBottom: 10 }}>{product.item.id}</Text>
              <Text style={{ fontWeight: 'bold', paddingBottom: 5 }}>{product.item.category_name}</Text>
              <Text style={{
                fontWeight: 'normal', paddingBottom: 10, bottom: -24, right: -2,
                color: 'grey'
              }}>{'Price: $' + product.item.price}</Text>
            </View>

          </View>
        </View>

      </View >
    )
  }

  renderContactsItem = (contact) => {
    return (
      <View style={{ flex: 1 }}>
        <Text style={{ fontWeight: 'normal', color: 'grey' }}>{contact.item.firstName + ' ' + contact.item.lastName + '   ' + contact.item.phoneNumber}</Text>
      </View >
    )
  }

  componentDidMount() {
    debugLog('my data????????????????????????????')
    // debugLog(this.state.data.products)
    this.setState({ productList: this.props.recommendation_history_details.products })
    // / this.setState({ productList: this.state.data })
    debugLog(this.state.data)

    debugLog(':::::::::::::::::::::::::::::::::::')
    this.getDeliveryMethod_count()
    debugLog(':::::::::::::::::::::::::::::::::::')

  }


  render() {
    return (
      <SafeAreaView style={msStyle.container}>
        <View style={{
          flexDirection: 'row', height: 60,
          // justifyContent: 'space-between' 
        }}>
          <TouchableOpacity
            style={{
              width: 50,
              paddingLeft: 10,
              justifyContent: "center"
            }}
            onPress={() => { this.props.navigation.goBack(); }}
          >
            <Image
              source={require('../../assets/back2.png')}
              resizeMode="contain"
              style={{
                width: 30,
                height: 30,
                paddingRight: 20
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              width: '70%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 5
            }}
          >
            <Text style={[msStyle.txtStyle, {
              color: 'black', fontSize: 20,
              fontStyle: "normal",
              fontWeight: '500'
            }]}>{"Recommendation Detail"}</Text>

          </View>
          {/* <TouchableOpacity
            style={{
              width: 50,
              paddingRight: 20,
              justifyContent: "center",
              alignItems: 'flex-end'
            }}
          >
            <Image
              source={require('../../assets/searchIcon.png')}
              resizeMode="contain"
              style={{
                width: 25,
                height: 30
              }}
            />
          </TouchableOpacity> */}
        </View>

        {/* MARK:  body */}
        <View style={{ flex: 1 }}>

          <View style={{ flex: 1 }}>
            <FlatList style={styles.list}
              data={this.props.recommendation_history_details.products}
              renderItem={this.renderItem}
              keyExtractor={item => item.id}
              showsVerticalScrollIndicator={false}

            />

          </View>
          <View style={{ flex: .5, padding: 25 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{'Contacts'}</Text>
            <View style={(msStyle.container)}>
              <FlatList style={styles.list}
                data={this.props.recommendation_history_details.contacts}
                renderItem={this.renderContactsItem}
                keyExtractor={item => item.id}
              />
            </View>

          </View>
          <View style={{ flex: .3, padding: 25 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{'Message'}</Text>
            <Text>{this.props.recommendation_history_details.message}</Text>
          </View>

          <View style={{ flex: .3, padding: 25 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'gray' }}>{'Delivery Method'}</Text>
            <View
              style={{ flex: .1, padding: 10, flexDirection: 'row', justifyContent: 'space-evenly' }}>
              {/* item 1 */}
              <TouchableOpacity>
                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                  <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                    {this.state.whatsappCount}
                  </Text>
                </View>
                <Image
                  source={require('../../assets/whatsapp.png')}
                  resizeMode="contain"
                  style={{
                    width: 30,
                    height: 30
                  }}
                />
              </TouchableOpacity>

              {/* item 2 */}
              <TouchableOpacity>
                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                  <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                    {this.state.messengerCount}
                  </Text>
                </View>
                <Image
                  source={require('../../assets/messenger.png')}
                  resizeMode="contain"
                  style={{
                    width: 30,
                    height: 30
                  }}
                />
              </TouchableOpacity>
              {/* item 3 */}
              <TouchableOpacity>
                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                  <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                    {this.state.instagramCount}
                  </Text>
                </View>
                <Image
                  source={require('../../assets/instagram.png')}
                  resizeMode="contain"
                  style={{
                    width: 30,
                    height: 30
                  }}
                />
              </TouchableOpacity>
              {/* item 4 */}
              <TouchableOpacity>
                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                  <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                    {this.state.pinterestCount}
                  </Text>
                </View>
                <Image
                  source={require('../../assets/pinterest.png')}
                  resizeMode="contain"
                  style={{
                    width: 30,
                    height: 30
                  }}
                />
              </TouchableOpacity>
              {/* item 5 */}
              <TouchableOpacity>
                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                  <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                    {this.state.mailCount}
                  </Text>
                </View>
                <Image
                  source={require('../../assets/mail.png')}
                  resizeMode="contain"
                  style={{
                    width: 30,
                    height: 30
                  }}
                />
              </TouchableOpacity>
              {/* item 6 */}
              <TouchableOpacity>
                <View style={{ zIndex: 2, elevation: 2, position: 'absolute', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 7, right: -7, top: -7, borderRadius: 30, backgroundColor: 'red' }} >
                  <Text style={[msStyle.txtStyle, { color: 'white', marginBottom: 3 }]} >
                    {this.state.messageCount}
                  </Text>
                </View>
                <Image
                  source={require('../../assets/mail2.png')}
                  resizeMode="contain"
                  style={{
                    width: 30,
                    height: 30
                  }}
                />
              </TouchableOpacity>

            </View>
          </View>
        </View>

      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  absoluteStyle: {
    position: 'absolute',
    backgroundColor: Colors.whiteText,
    alignSelf: 'center',
    width: width - 30,
    height: height - 70,
    elevation: 10,
    zIndex: 5,
    bottom: 15,
    borderRadius: 5
  },
  list: {
    flex: 1,
    padding: 15
  },
  nofitfy: {
    zIndex: 2,
    elevation: 2,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 7,
    right: -7,
    top: -7,
    borderRadius: 30,
    backgroundColor: 'red'
  }

})



const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
      loginResponse: common.loginResponse,
      recommendation_history_details: jaswant.recommendation_history_details,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
      setMessageHistoryDetails: (list) => setMessageHistoryDetails(list),
      searchMessageHistoryDetails: (txt) => searchMessageHistoryDetails(txt),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(RecommendationDetailScreen)