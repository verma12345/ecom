import React, { Component, useState } from 'react';
import { Image, Button, View, Text, SafeAreaView, TextInput, Dimensions, StyleSheet, ScrollView, SectionList, TouchableOpacity, flexDirection, FlatList } from 'react-native';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import MyHeader from '../components/MyHeader';
import Constants, { debugLog } from '../common/Constants';
import MyButton from '../components/MyButton';
import MyInputText from '../components/MyInputText';
import { setDomain, setEmail, setPlatform } from '../redux_store/actions/indexActions';
import { getPixelSizeForLayoutSize } from 'react-native/Libraries/Utilities/PixelRatio';
import { NativeViewGestureHandler, RawButton } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setIsDrawer, setLoginRespons } from '../redux_store/actions/indexActions';
import { getPrefs } from '../common/Prefs';
import { searchRecommendationHistory, setRecommendationHistoryDetails, setRecommendationHistory } from '../redux_store/actions/indexActionsJaswant';
import SearchHeader from '../components/SearchHeader';


class RecommendationHistoryScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loginData: {},
      isSearch: false
    }
  }

  getRecommendationHistory = () => {

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Cookie", "AWSALBTG=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; AWSALBTGCORS=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; SESSION=Mjg3Y2Q5ODctMzI0OS00NjhkLThhNDEtOTRkNzEzNDEwYzBl");
    var raw = ''
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    // {"cbp": 3, "createdAt": 1625066191, "domainName": "cscestorexpress.com", "platformId": "bht", "product": 
    // null, "randomkey": null, "source": "mobile", "storeNo": 2336, "uniqueId": "001", "updatedAt": 
    // 1625066194, "userEmail": "barneyrubble@testemail.com", "userFirstName": "test", "userLastName": "Testing"}

    let url_testing = `${Constants.API_BASE_URL}/admin/recommendations/history?uniqueId=001&cbpNumber=3&storeNumber=2336`

    // let url = `${Constants.API_BASE_URL}/admin/recommendations/history?uniqueId=${this.state.loginData.uniqueId}&cbpNumber=${this.state.loginData.cbp}&storeNumber=${this.state.loginData.storeNo}`
    let url = Constants.API_BASE_URL + "/admin/recommendations/history?uniqueId=" + this.state.loginData.uniqueId + "&cbpNumber=" + this.state.loginData.cbp + "&storeNumber=" + this.state.loginData.storeNo

    debugLog("my url++++++++++++++++++++++++++++++++++++++++++++++++++++" + url)
    fetch(url, requestOptions)
      .then(response => response.text())
      .then(result => {
        let json = JSON.parse(result)
        // this.setState({ data: json })
        this.props.setRecommendationHistory(json)
        debugLog(json)

      })
      .catch(error => console.log('error', error));

  }

  getLoginCrediential = async () => {
    getPrefs('ACCESS_TOKEN').then((value) => {
      debugLog(value)
      let json = JSON.parse(value)
      // this.props.setLoginRespons(json)
      this.setState({ loginData: json })
      // debugLog(json.domainName)
      this.getRecommendationHistory()

    })
  }

  getParsedDate(strDate) {
    var strSplitDate = String(strDate).split(' ');
    var date = new Date(strSplitDate[0]);
    // alert(date);
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!

    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    date = dd + "/" + mm + "/" + yyyy;
    return date.toString();
  }


  componentDidMount() {
    this.getLoginCrediential()
    // this.getRecommendationHistory() // for testing

  }

  onRecommendationDetailScreen = (data) => {
    this.props.setRecommendationHistoryDetails(data)
    this.props.navigation.navigate('RecommendationDetailScreen')
  }

  renderItem = (dataItem) => {
    // debugLog(dataItem.item)
    return (
      <TouchableOpacity
        onPress={() => { this.onRecommendationDetailScreen(dataItem.item) }}
      >

        <View style={[msStyle.myshadow2, {
          flex: 1,
          padding: 15,
          backgroundColor: 'white',
          borderRadius: 7,
          marginTop: 10,
          marginHorizontal: 5
        }]}>

          <View style={{
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'space-evenly',
            paddingBottom: 15
          }}>
            <Text
              style={{ flex: .99, fontWeight: 'bold', alignItems: 'flex-start' }}>{dataItem.item.subject}</Text>
            <TouchableOpacity
              onPress={() => { }}
            >
              <Image style={{
                height: 15,
                width: 15,
                resizeMode: 'contain'
              }}
                source={require('../../assets/rightArrow.png')} />
            </TouchableOpacity>
          </View>
          <View style={{
            paddingTop: 10,
            flex: 1,
            justifyContent: 'space-evenly'
          }}>
            <Text >{dataItem.item.message}</Text>
            <Text style={{ color: "grey" }}>{this.getParsedDate(dataItem.item.createdAt)}</Text>


          </View>
        </View>

      </TouchableOpacity>
    )
  }

  render() {
    return (
      <SafeAreaView style={msStyle.container}>



        {/* MARK:  body */}

        <View style={(msStyle.container)}>

          <SearchHeader
            title='Recommendation History'
            placeholder="Search"
            isInput={this.state.isSearch}
            onChangeText={(txt) => { this.props.searchRecommendationHistory(txt) }}
            onGoBack={() => this.props.navigation.goBack()}
            onSearch={() => { this.setState({ isSearch: !this.state.isSearch }) }} />

          <FlatList contentContainerStyle={styles.list}
            data={this.props.recommendation_history_list}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}

          />
          {/* <TouchableOpacity
            style={{ position: 'absolute', bottom: 10, right: 20, zIndex: 5, elevation: 5 }}
            onPress={() => { debugLog(this.state.loginData.uniqueId) }}
          >
            <Image
              source={require('../../assets/contactUsIcon.png')}
              resizeMode="center"
              style={{
                width: 80,
                height: 80
              }}
            />

            </TouchableOpacity>*/}
        </View>

      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  absoluteStyle: {
    position: 'absolute',
    backgroundColor: Colors.whiteText,
    alignSelf: 'center',
    width: width - 30,
    height: height - 70,
    elevation: 10,
    zIndex: 5,
    bottom: 15,
    borderRadius: 5
  },
  list: {
    paddingBottom: 100
  }

})


const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    loginResponse: common.loginResponse,
    recommendation_history_list: jaswant.recommendation_history_list,
    search_recommendation_history_list: jaswant.search_recommendation_history_list

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    setRecommendationHistory: (domain) => setRecommendationHistory(domain),
    searchRecommendationHistory: (txt) => searchRecommendationHistory(txt),
    setLoginRespons: (res) => setLoginRespons(res),
    setRecommendationHistoryDetails: (list) => setRecommendationHistoryDetails(list),
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(RecommendationHistoryScreen)