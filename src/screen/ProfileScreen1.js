import React, { Component } from 'react';
import { Button, View, Text, SafeAreaView, Dimensions, StyleSheet, ScrollView, TouchableWithoutFeedback, TouchableOpacity, PixelRatio, Alert, BackHandler, Image, PermissionsAndroid, Platform, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import Constants, { debugLog } from '../common/Constants';
import { msStyle } from '../common/MyStyle';
import { getPrefs, setPrefs } from '../common/Prefs';
import MyHeader from '../components/MyHeader';
import { PhoneOptionMemo } from '../components/PhoneOption';
import { ProfileOptionMemo } from '../components/ProfileOption';
import ProfilePickerComponent from '../components/ProfilePickerComponent';
import ProfilePopup from '../components/ProfilePopup';
import { SocialMediaOptionMemo } from '../components/SocialMediaOption';
import { setLoginRespons, setRefereshGetProfile } from '../redux_store/actions/indexActions';
import {
  setAccesKeySecretsKey,
  setIsProfileAboutUs,
  setIsProfileDescription,
  setIsProfileEmail,
  setIsProfileFirstName,
  setIsProfileHeader,
  setIsProfileInstagram,
  setIsProfileLastName,
  setIsProfileLinkedin,
  setIsProfileMessenger,
  setIsProfilePhoneNumber,
  setIsProfilePinterest,
  setIsProfileTiktok,
  setIsProfileTwitter,
  setIsProfileWhatsapp,
  setIsProfileYoutube,
  setIsSaveImage1,
  setIsSaveImage2,
  setProfileAboutUs,
  setProfileCertificate,
  setProfileCertificate1,
  setProfileCertificate2,
  setProfileCertificate3,
  setProfileCertificate4,
  setProfileCertificate5,
  setProfileCertificate6,
  setProfileCertificate7,
  setProfileCertificateList,
  setProfileCertificateName1,
  setProfileCertificateName2,
  setProfileCertificateName3,
  setProfileCertificateName4,
  setProfileCertificateName5,
  setProfileCertificateName6,
  setProfileCertificateName7,
  setProfileDescription,
  setProfileEmail,
  setProfileFirstName,
  setProfileHeader,
  setProfileImage1,
  setProfileImage1Url,
  setProfileImage2,
  setProfileImage2Url,
  setProfileInstagram,
  setProfileInstagramIcon,
  setProfileLastName,
  setProfileLinkedin,
  setProfileLinkedinIcon,
  setProfileMessenger,
  setProfileMessengerIcon,
  setProfilePhoneNumber,
  setProfilePinterest,
  setProfilePinterestIcon,
  setProfileTiktok,
  setProfileTiktokIcon,
  setProfileTwitter,
  setProfileTwitterIcon,
  setProfileWhatsapp,
  setProfileWhatsappIcon,
  setProfileYoutube,
  setProfileYoutubeIcon,
} from '../redux_store/actions/indexActionsJaswant';

import MyButton from '../components/MyButton';
import { hitGetProfileData, hitSaveProfileData, hitUploadImageOnASW } from '../redux_store/actions/indexActionsApi';
import { launchImageLibrary, launchCamera } from 'react-native-image-picker';
import MyLoader from '../components/MyLoader';
import WebView from 'react-native-webview';

class ProfileScreen1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orientation: '',
      loginData: {},
      uploadSuccessMessage: '',
      secrets: {},
      isImage1: false,
      isImage2: false,
      isGetProfile: false,
      isSaveProfile: false,
      isWebView1: false,
      isWebView2: false,

      input_whatsapp: null,
      input_twitter: null,
      input_messanger: null,
      input_linkedin: null,
      input_instagram: null,
      input_youtube: null,
      input_pinterest: null,
      input_tiktok: null,

      input_header: null,

    }
    this.refreshGetProfile = 0
  }

  // ACCESS KEY AND SECRETS KEY
  getSecrets = () => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Cookie", "SESSION=ZTM1NDAzNTQtMmU4YS00ZmI2LWIxYzMtNjUyZDUwYmExOWFi");

    var raw = "";

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch(`${Constants.API_BASE_URL}/admin/awsS3Credentials`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let json = JSON.parse(result)
        debugLog(json)
        this.props.setAccesKeySecretsKey(json)
      }
      )
      .catch(error => console.log('error', error));
  }
  //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  getOrientation = () => {
    if (this.refs.rootView) {
      if (Dimensions.get('window').width < Dimensions.get('window').height) {
        this.setState({ orientation: 'portrait' });
      } else {
        this.setState({ orientation: 'landscape' });
      }
    }
  }


  getLoginCrediential = async () => {
    this.props.setRefereshGetProfile(0)

    getPrefs('ACCESS_TOKEN').then((value) => {
      debugLog(value)
      let json = JSON.parse(value)
      this.getProfileData(json)
      this.props.setLoginRespons(json)
      debugLog(json)
    })
  }

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "App Camera Permission",
          message: "App needs access to your camera ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Camera permission given");
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    if (Platform.OS == 'android') {
      this.requestCameraPermission()
    }
    this.getSecrets()
    this.getLoginCrediential()

    // BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    setPrefs("SIGINED_IN", "1")
    setPrefs("ONE_TIME_SCREEN", "1")

    this.getOrientation();
    Dimensions.addEventListener('change', () => {
      this.getOrientation();
    });

  }

  getProfileData = (json) => {
    let params = {
      domainname: json.domainName.includes(`www.`) ? json.domainName : `www.${json.domainName}`,
      platformid: json.platformId,
      cbpnumber: json.cbp,
      storenumber: json.storeNo,
      uniqueid: json.uniqueId,
      // mobileapi_key: "3k82\/9NwEQfLif1IIKvQMg=="
    }
    this.setState({ isGetProfile: true })

    this.props.hitGetProfileData(params).then((response) => {
      this.props.navigation.openDrawer()
      this.setState({ isGetProfile: false })

      this.props.setProfileImage1Url(response.image1_name)
      this.props.setProfileImage2Url(response.image2_name)

      this.props.setProfileAboutUs(response.ld_html_tag_content)
      this.props.setProfileDescription(response.sd_html_tag_content)

      this.props.setProfileHeader(response.profile_header)

      this.props.setProfileFirstName(json.userFirstName)
      this.props.setProfileLastName(json.userLastName)

      this.props.setProfileEmail(json.userEmail)
      this.props.setProfilePhoneNumber(response.contact_phone)
      this.props.setProfileCertificateList(response.certificates)
      this.props.setProfileCertificate(response.certificates[0].cert_icon_name)

      // social media
      this.props.setProfileWhatsapp(response.sm_url1)
      this.props.setProfileTwitter(response.sm_url2)
      this.props.setProfileMessenger(response.sm_url3)
      this.props.setProfileLinkedin(response.sm_url4)
      this.props.setProfileInstagram(response.sm_url5)
      this.props.setProfileYoutube(response.sm_url6)
      this.props.setProfilePinterest(response.sm_url7)
      this.props.setProfileTiktok(response.sm_url8)

      this.props.setProfileWhatsappIcon(response.sm_icon_name1)
      this.props.setProfileTwitterIcon(response.sm_icon_name2)
      this.props.setProfileMessengerIcon(response.sm_icon_name3)
      this.props.setProfileLinkedinIcon(response.sm_icon_name4)
      this.props.setProfileInstagramIcon(response.sm_icon_name5)
      this.props.setProfileYoutubeIcon(response.sm_icon_name6)
      this.props.setProfilePinterestIcon(response.sm_icon_name7)
      this.props.setProfileTiktokIcon(response.sm_icon_name8)
    });

  }

  showAlert1() {
    Alert.alert(
      'Upload Image',
      'Please choose Camera or Gallery',
      [
        {
          text: 'Camera',
          onPress: () => {
            this.openCameraImage1();
          },
        },
        {
          text: 'Gallery',
          onPress: () => {
            this.openGalleryImage1();
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  }

  openCameraImage1 = () => {
    let options = {
      mediaType: 'photo',
    };
    launchCamera(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('You cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      // setFilePath(response);
      this.props.setProfileImage1(response)
    });
  };

  openGalleryImage1 = () => {
    let options = {
      mediaType: 'photo',
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('You cancelled library picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      // setFilePath(response);
      this.props.setProfileImage1(response)
    });
  };


  showAlert2() {
    Alert.alert(
      'Upload Image',
      'Please choose Camera or Gallery',
      [
        {
          text: 'Camera',
          onPress: () => {
            this.openCameraImage2();
          },
        },
        {
          text: 'Gallery',
          onPress: () => {
            this.openGalleryImage2();
          },
        },
      ],
      {
        cancelable: true,
      },
    );
  }

  openCameraImage2 = () => {
    let options = {
      mediaType: 'photo',
    };
    launchCamera(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('You cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      // setFilePath(response);
      this.props.setProfileImage2(response)
    });
  };

  openGalleryImage2 = () => {
    let options = {
      mediaType: 'photo',
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      // setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('You cancelled library picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      // setFilePath(response);
      this.props.setProfileImage2(response)
    });
  };

  uploadImageOnASW1 = (image) => {
    let params = {
      image: image,
      accessKey: this.props.accessAndSecretsKey.accessKey,
      secretKey: this.props.accessAndSecretsKey.secretKey,
      platformId: this.props.loginResponse.platformId,
      storeNo: this.props.loginResponse.storeNo
    }
    this.setState({ isImage1: true })

    this.props.hitUploadImageOnASW(params).then(res => {
      let { bucket, etag, key, location } = res.body.postResponse;
      debugLog(location)
      this.props.setProfileImage1Url(location)
      this.props.setProfileImage1(null)
      this.setState({ isImage1: false })

    })
  }

  uploadImageOnASW2 = (image) => {
    let params = {
      image: image,
      accessKey: this.props.accessAndSecretsKey.accessKey,
      secretKey: this.props.accessAndSecretsKey.secretKey,
      platformId: this.props.loginResponse.platformId,
      storeNo: this.props.loginResponse.storeNo
    }
    this.setState({ isImage2: true })

    this.props.hitUploadImageOnASW(params).then(res => {
      let { bucket, etag, key, location } = res.body.postResponse;
      // debugLog(location)
      this.props.setProfileImage2Url(location)
      this.props.setProfileImage2(null)
      this.setState({ isImage2: false })

    })
  }


  onHint1 = () => {
    alert("This image will represent your logo or profile picture on your store / profile page")
  }

  onHint2 = () => {
    alert("This image will be used to add a donation image to your favorite charity or cause")
  }


  onMenu = () => {
    // this.props.setIsDrawer(!this.props.is_drawer)
    this.props.navigation.openDrawer()

  }

  onCertificate = () => {
    this.props.navigation.navigate('CertificatesComponent')

  }

  onSave = () => {

    let param = {
      domainname: this.props.loginResponse.domainName.includes(`www.`) ? this.props.loginResponse.domainName : `www.${this.props.loginResponse.domainName}`,
      platformid: this.props.loginResponse.platformId,
      cbpnumber: this.props.loginResponse.cbp,
      storenumber: this.props.loginResponse.storeNo,
      uniqueid: this.props.loginResponse.uniqueId,
      // mobileapi_key: "3k82\/9NwEQfLif1IIKvQMg==",

      image1_name: this.props.profileImage1Url,
      image2_name: this.props.profileImage2Url,
      contact_phone: this.props.profilePhoneNumber,
      sm_icon_name1: this.props.whatsapp_icon,
      sm_icon_name2: this.props.twitter_icon,
      sm_icon_name3: this.props.messenger_icon,
      sm_icon_name4: this.props.linkedin_icon,
      sm_icon_name5: this.props.instagram_icon,
      sm_icon_name6: this.props.youtube_icon,
      sm_icon_name7: this.props.pinterest_icon,
      sm_icon_name8: this.props.tiktok_icon,
      sm_url1: this.props.whatsapp,
      sm_url2: this.props.twitter,
      sm_url3: this.props.messenger,
      sm_url4: this.props.linkedin,
      sm_url5: this.props.instagram,
      sm_url6: this.props.youtube,
      sm_url7: this.props.pinterest,
      sm_url8: this.props.tiktok,
      certificates: this.props.profileCertificateList,
      profile_header: this.props.profileHeader,
      sd_html_tag_content: this.props.profileDescription != null ? this.props.profileDescription.toString() : this.props.profileDescription,
      ld_html_tag_content: this.props.profileAboutUs != null ? this.props.profileAboutUs.toString() : this.props.profileAboutUs

    }


    // this.props.hitSaveProfileData(requestOptions).then((response) => {
    //   alert(response)
    //   debugLog(response)
    // })
    // return

    debugLog(param)
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");

    var raw1 = JSON.stringify(param)
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw1,
      redirect: 'follow'
    };
    this.setState({ isSaveProfile: true })
    fetch(`${Constants.API_BASE_URL}/restapi/storeprofilesave`, requestOptions)
      .then(response => response.text())
      .then(result => {
        console.log(result)
        this.setState({ isSaveProfile: false })
        this.getProfileData(this.props.loginResponse)
        alert(result)
      })
      .catch(error => console.log('error', error));
  }

  onWhatsApp = () => {
    if (isNaN(this.props.whatsapp) == true) {
      alert('Please enter valid number')
    } else {
      if (this.props.whatsapp.length < 10) {
        alert('Number is not correct')
        return
      }
      this.props.setIsProfileWhatsapp(!this.props.is_whatsapp)
    }
  }

  onURLValidation = (url1) => {

    if (url1 == null) {
      alert('Please entre URL')
      return
    }

    let url = url1.toLowerCase()

    if (url.includes('https://') || url.includes('http://')) {
      return true
    }
    else {
      alert('Please enter valid URL')
    }
  }


  onSaveHeader = () => {
    this.props.setIsProfileHeader(!this.props.isProfileHeader)
    this.props.setProfileHeader(this.state.input_header)
  }
  render() {

    if (this.props.refereshGetProfile == 2) {
      this.getLoginCrediential()
    }

    return (
      <SafeAreaView
        ref="rootView"
        style={msStyle.container}>
        <StatusBar backgroundColor={Colors.themeColor}
          barStyle={"light-content"}
        />
        <View style={msStyle.container}>

          <View style={{ flex: 1, backgroundColor: Colors.themeColor, }} />
          <View style={{ flex: 5, backgroundColor: "white", }} />


          <View style={{
            position: 'absolute',
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
            backgroundColor: "#0000",
            elevation: 1
          }} >


            <View>
              <MyHeader
                title='Profile'
                icon={require('../../assets/menu.png')}
                tintColor="white"
                onPress={() => { this.onMenu() }}
                containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
                titleStyle={{ color: Colors.whiteText }}
              />
            </View>

            <View style={[msStyle.myshadow, { flex: 1, backgroundColor: Colors.backgroundColor2, margin: 15, paddingHorizontal: 20, paddingVertical: 3, borderRadius: 5 }]} >

              <ScrollView showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false} >

                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', paddingVertical: 10, }} >

                  <ProfilePickerComponent
                    title='Image 1'
                    icon={require('../../assets/camera.png')}
                    url={this.props.profileImage1Url}
                    onPress={() => { this.showAlert1() }}
                    isHint={true}
                    onHint={() => {
                      this.onHint1()
                    }}
                  />

                  <ProfilePickerComponent
                    title='Image 2'
                    icon={require('../../assets/camera.png')}
                    onPress={() => { this.showAlert2() }}
                    url={this.props.profileImage2Url}
                    isHint={true}
                    onHint={() => {
                      this.onHint2()
                    }}

                  />
                  <ProfilePickerComponent
                    title='Certificate'
                    icon={require('../../assets/edit.png')}
                    onPress={() => { this.onCertificate() }}
                    url={this.props.profileCertificate}
                  />
                </View>


                <ProfileOptionMemo
                  title='Profile Header'
                  message={this.props.profileHeader}
                  onPencil={() => {
                    this.setState({ input_header: this.props.profileHeader })
                    this.props.setIsProfileHeader(!this.props.isProfileHeader)
                  }}
                />

                <ProfileOptionMemo
                  title='Profile Description'
                  isWeb={true}
                  message={this.props.profileDescription}
                  inputStyle={{ height: 100 }}
                  onPencil={() => {
                    // this.props.setIsProfileDescription(!this.props.isProfileDescription)
                    this.props.navigation.navigate('QuillEditorComponent1')
                  }}
                  onPressReadMore={() => { this.props.navigation.navigate('QuillEditorComponent1More') }}
                  onPressWebView={() => this.setState({ isWebView1: !this.state.isWebViewq })}

                />

                <ProfileOptionMemo
                  title='About Us'
                  isWeb={true}
                  message={this.props.profileAboutUs}
                  inputStyle={{ height: 100, }}
                  onPencil={() => {
                    // this.props.setIsProfileAboutUs(!this.props.isProfileAboutUs)
                    this.props.navigation.navigate('QuillEditorComponent')
                  }}
                  onPressReadMore={() => { this.props.navigation.navigate('QuillEditorComponent2More') }}
                  onPressWebView={() => this.setState({ isWebView2: !this.state.isWebView2 })}
                />

                <ProfileOptionMemo
                  title='First Name'
                  disable={true}
                  message={this.props.profileFirstName}
                // onPencil={() => {
                //   this.props.setIsProfileFirstName(!this.props.isProfileFirstName)
                // }}
                />

                <ProfileOptionMemo
                  title='Last Name'
                  disable={true}
                  message={this.props.profileLastName}
                // onPencil={() => {
                //   this.props.setIsProfileLastName(!this.props.isProfileLastName)
                // }}
                />

                <ProfileOptionMemo
                  title='Email'
                  disable={true}
                  message={this.props.profileEmail}
                // onPencil={() => {
                //   this.props.setIsProfileEmail(!this.props.isProfileEmail)
                // }}
                />

                <PhoneOptionMemo
                  title='Phone Number'
                  country_code={'+1'}
                  disable={true}
                  message={this.props.profilePhoneNumber}
                // onPencil={() => {
                //   this.props.setIsProfilePhoneNumber(!this.props.isProfilePhoneNumber)
                // }}
                />




                {/* ********************************************************* * social media section****************************************************************** */}





                <Text style={[msStyle.txtStyle, { fontSize: 18, padding: 10, marginTop: 20 }]} >{'Social Media'}</Text>

                <View style={{ flexDirection: 'row', }} >
                  <SocialMediaOptionMemo
                    value={this.props.whatsapp}
                    onPress={() => {
                      this.setState({ input_whatsapp: this.props.whatsapp })
                      this.props.setIsProfileWhatsapp(!this.props.is_whatsapp)
                    }}
                    icon={require('../../assets/whatsapp.png')}
                    title="Whatsapp"
                    containerStyle={{ alignSelf: 'flex-start' }}

                  />

                  <SocialMediaOptionMemo
                    value={this.props.twitter}
                    onPress={() => {
                      this.setState({ input_twitter: this.props.twitter })
                      this.props.setIsProfileTwitter(!this.props.is_twitter)
                    }}
                    icon={require('../../assets/twitter.png')}
                    title="Twitter"
                    containerStyle={{ marginLeft: 30 }}

                  />
                </View>

                <View style={{ flexDirection: 'row', }} >
                  <SocialMediaOptionMemo
                    value={this.props.messenger}
                    onPress={() => {
                      this.setState({ input_messanger: this.props.messenger })
                      this.props.setIsProfileMessenger(!this.props.is_messenger)
                    }}
                    icon={require('../../assets/messenger.png')}
                    title="Messenger"
                    containerStyle={{ alignSelf: 'flex-start' }}
                  />

                  <SocialMediaOptionMemo
                    value={this.props.linkedin}
                    onPress={() => {
                      this.setState({ input_linkedin: this.props.linkedin })
                      this.props.setIsProfileLinkedin(!this.props.is_linkedin)
                    }}
                    icon={require('../../assets/linkedin.png')}
                    title="Linkedin"
                    containerStyle={{ marginLeft: 30 }}
                  />
                </View>

                <View style={{ flexDirection: 'row', }} >
                  <SocialMediaOptionMemo
                    value={this.props.instagram}
                    onPress={() => {
                      this.setState({ input_instagram: this.props.instagram })
                      this.props.setIsProfileInstagram(!this.props.is_instagram)
                    }}
                    icon={require('../../assets/instagram.png')}
                    title="Instagram"
                    containerStyle={{ alignSelf: 'flex-start' }}

                  />

                  <SocialMediaOptionMemo
                    value={this.props.youtube}
                    onPress={() => {
                      this.setState({ input_youtube: this.props.youtube })
                      this.props.setIsProfileYoutube(!this.props.is_youtube)
                    }}
                    icon={require('../../assets/youtube.png')}
                    title="Youtube"
                    containerStyle={{ marginLeft: 30 }}

                  />
                </View>

                <View style={{ flexDirection: 'row', }} >
                  <SocialMediaOptionMemo
                    value={this.props.pinterest}
                    onPress={() => {
                      this.setState({ input_pinterest: this.props.pinterest })
                      this.props.setIsProfilePinterest(!this.props.is_pinterest)
                    }}
                    icon={require('../../assets/pinterest.png')}
                    title="Pinterest"
                    containerStyle={{ alignSelf: 'flex-start' }}
                  />

                  <SocialMediaOptionMemo
                    value={this.props.tiktok}
                    onPress={() => {
                      this.setState({ input_tiktok: this.props.tiktok })
                      this.props.setIsProfileTiktok(!this.props.is_tiktok)
                    }}
                    icon={require('../../assets/tiktok.png')}
                    title="Tiktok"
                    containerStyle={{ marginLeft: 30 }}
                  />
                </View>

                <MyButton
                  title='SAVE'
                  onPress={() => { this.onSave() }}
                  titleStyle={{ color: Colors.whiteText, fontSize: 20 }}
                  containerStyle={{ marginVertical: 50, paddingVertical: 10 }}
                />
              </ScrollView>
            </View>
          </View>

          {
            this.props.profileImage1 != null ?
              <ProfilePopup
                title='Image 1'
                loading={this.props.is_loading}
                image={this.props.profileImage1.assets[0].uri}
                onPress={() => {
                  this.uploadImageOnASW1(this.props.profileImage1)
                }}
                onCancel={() => { this.props.setProfileImage1(null) }}

              />
              : null
          }

          {
            this.props.profileImage2 != null ?
              <ProfilePopup
                title='Image 2'
                loading={this.props.is_loading}
                image={this.props.profileImage2.assets[0].uri}
                onPress={() => {
                  this.uploadImageOnASW2(this.props.profileImage2)
                }}
                onCancel={() => { this.props.setProfileImage2(null) }}

              />
              : null
          }

          {
            this.props.isProfileHeader ?
              <ProfilePopup
                title='Profile Header'
                value={this.state.input_header}
                onChangeText={(txt) => { this.setState({ input_header: txt }) }}
                onPress={() => {
                  var regex = /^[a-zA-Z ]*$/;
                  regex.test(this.state.input_header) ?
                    this.onSaveHeader() :
                    alert('Please enter valid name!')
                }}
                onCancel={() => { this.props.setIsProfileHeader(!this.props.isProfileHeader) }}
              />
              : null
          }

          {/* {
            this.props.isProfileDescription ?

              <QuillEditorComponent
                title='Profile Description'
                onGoBack={() => { this.props.setIsProfileDescription(!this.props.isProfileDescription) }}
                // title='About Us'
                // value={this.props.profileAboutUs}
                onHtmlChange={(Html) => { this.props.setProfileDescription(Html) }}
              // onPress={() => {
              //   this.props.setIsProfileDescription(!this.props.isProfileDescription)
              // }}
              />
              : null
          } */}

          {/* {
            this.props.isProfileAboutUs ?
              <QuillEditorComponent
                title='About Us'
                onGoBack={() => { this.props.setIsProfileAboutUs(!this.props.isProfileAboutUs) }}
                // title='About Us'
                // value={this.props.profileAboutUs}
                onHtmlChange={(Html) => { this.props.setProfileAboutUs(Html) }}
              // onPress={() => {
              //   this.props.setIsProfileAboutUs(!this.props.isProfileAboutUs)
              // }}
              />
              : null
          } */}

          {/* {
            this.props.isProfileFirstName ?
              <ProfilePopup
                title='First Name'
                value={this.props.profileFirstName}
                onChangeText={(txt) => { this.props.setProfileFirstName(txt) }}
                onPress={() => {
                  this.props.setIsProfileFirstName(!this.props.isProfileFirstName)
                }}
              />
              : null
          }

          {
            this.props.isProfileLastName ?
              <ProfilePopup
                title='Last Name'
                value={this.props.profileLastName}
                onChangeText={(txt) => { this.props.setProfileLastName(txt) }}
                onPress={() => {
                  this.props.setIsProfileLastName(!this.props.isProfileLastName)
                }}
              />
              : null
          }


          {
            this.props.isProfileEmail ?
              <ProfilePopup
                title='Email'
                value={this.props.profileEmail}
                onChangeText={(txt) => { this.props.setProfileEmail(txt) }}
                onPress={() => {
                  this.props.setIsProfileEmail(!this.props.isProfileEmail)
                }}
              />
              : null
          }

          {
            this.props.isProfilePhoneNumber ?
              <ProfilePopup
                title='Phone Number'
                value={this.props.profilePhoneNumber}
                keyboardType="number-pad"
                maxLength={10}
                onChangeText={(txt) => { this.props.setProfilePhoneNumber(txt) }}
                onPress={() => {
                  this.props.setIsProfilePhoneNumber(!this.props.isProfilePhoneNumber)
                }}
              />
              : null
          } */}


          {/* SOCIAL MEDIA++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */}

          {
            this.props.is_whatsapp ?
              <ProfilePopup
                title='WhatsApp'
                value={this.props.whatsapp}
                keyboardType="number-pad"
                maxLength={10}
                onChangeText={(txt) => { this.props.setProfileWhatsapp(txt) }}
                onPress={() => { this.onWhatsApp() }}
                onCancel={() => {
                  this.props.setProfileWhatsapp(this.state.input_whatsapp)
                  this.props.setIsProfileWhatsapp(!this.props.is_whatsapp)
                }}
              />
              : null
          }


          {
            this.props.is_twitter ?
              <ProfilePopup
                title='Twitter'
                value={this.props.twitter}
                autoCapitalize='none'
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileTwitter(txt) }}
                onPress={() => {
                  if (this.onURLValidation(this.props.twitter)) {
                    this.props.setIsProfileTwitter(!this.props.is_twitter)
                    this.props.setProfileTwitter(this.props.twitter != null ? this.props.twitter.toLowerCase() : this.props.twitter)
                  }
                }}
                onCancel={() => {
                  this.props.setProfileTwitter(this.state.input_twitter)
                  this.props.setIsProfileTwitter(!this.props.is_twitter)
                }}
              />
              : null
          }


          {
            this.props.is_messenger ?
              <ProfilePopup
                title='Messenger'
                value={this.props.messenger}
                autoCapitalize='none'
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileMessenger(txt) }}

                onPress={() => {
                  if (this.onURLValidation(this.props.messenger)) {
                    this.props.setProfileMessenger(this.props.messenger != null ? this.props.messenger.toLowerCase() : this.props.messenger)
                    this.props.setIsProfileMessenger(!this.props.is_messenger)
                  }
                }}
                onCancel={() => {
                  this.props.setProfileMessenger(this.state.input_messanger)
                  this.props.setIsProfileMessenger(!this.props.is_messenger)
                }}
              />
              : null
          }


          {
            this.props.is_linkedin ?
              <ProfilePopup
                title='Linkedin'
                value={this.props.linkedin}
                autoCapitalize='none'
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileLinkedin(txt) }}

                onPress={() => {
                  if (this.onURLValidation(this.props.linkedin)) {
                    this.props.setProfileLinkedin(this.props.linkedin != null ? this.props.linkedin.toLowerCase() : this.props.linkedin)
                    this.props.setIsProfileLinkedin(!this.props.is_linkedin)
                  }
                }}
                onCancel={() => {
                  this.props.setProfileLinkedin(this.state.input_linkedin)
                  this.props.setIsProfileLinkedin(!this.props.is_linkedin)
                }}
              />
              : null
          }


          {
            this.props.is_instagram ?
              <ProfilePopup
                title='Instagram'
                value={this.props.instagram}
                autoCapitalize='none'
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileInstagram(txt) }}

                onPress={() => {
                  if (this.onURLValidation(this.props.instagram)) {
                    this.props.setProfileInstagram(this.props.instagram != null ? this.props.instagram.toLowerCase() : this.props.instagram)
                    this.props.setIsProfileInstagram(!this.props.is_instagram)
                  }
                }}
                onCancel={() => {
                  this.props.setProfileInstagram(this.state.input_instagram)
                  this.props.setIsProfileInstagram(!this.props.is_instagram)
                }}
              />
              : null
          }


          {
            this.props.is_youtube ?
              <ProfilePopup
                title='Youtube'
                value={this.props.youtube}
                autoCapitalize='none'
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileYoutube(txt) }}

                onPress={() => {
                  if (this.onURLValidation(this.props.youtube)) {
                    this.props.setProfileYoutube(this.props.youtube != null ? this.props.youtube.toLowerCase() : this.props.youtube)
                    this.props.setIsProfileYoutube(!this.props.is_youtube)
                  }
                }}
                onCancel={() => {
                  this.props.setProfileYoutube(this.state.input_youtube)
                  this.props.setIsProfileYoutube(!this.props.is_youtube)
                }}
              />
              : null
          }


          {
            this.props.is_pinterest ?
              <ProfilePopup
                title='Pinterest'
                value={this.props.pinterest}
                autoCapitalize='none'
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfilePinterest(txt) }}

                onPress={() => {
                  if (this.onURLValidation(this.props.pinterest)) {
                    this.props.setProfilePinterest(this.props.pinterest != null ? this.props.pinterest.toLowerCase() : this.props.pinterest)
                    this.props.setIsProfilePinterest(!this.props.is_pinterest)
                  }
                }}
                onCancel={() => {
                  this.props.setProfilePinterest(this.state.input_pinterest)
                  this.props.setIsProfilePinterest(!this.props.is_pinterest)
                }}
              />
              : null
          }

          {
            this.props.is_tiktok ?
              <ProfilePopup
                title='Tiktok'
                value={this.props.tiktok}
                autoCapitalize='none'
                // maxLength={10}
                onChangeText={(txt) => { this.props.setProfileTiktok(txt) }}

                onPress={() => {
                  if (this.onURLValidation(this.props.tiktok)) {
                    this.props.setProfileTiktok(this.props.tiktok != null ? this.props.tiktok.toLowerCase() : this.props.tiktok)
                    this.props.setIsProfileTiktok(!this.props.is_tiktok)
                  }
                }}
                onCancel={() => {
                  this.props.setProfileTiktok(this.state.input_tiktok)
                  this.props.setIsProfileTiktok(!this.props.is_tiktok)
                }}
              />
              : null
          }

          {
            this.state.isImage1 ?
              <MyLoader />
              : null
          }

          {
            this.state.isImage2 ?
              <MyLoader />
              : null
          }

          {
            this.state.isGetProfile ?
              <MyLoader />
              : null
          }

          {
            this.state.isSaveProfile ?
              <MyLoader />
              : null
          }

          {/* {
            this.state.isWebView1 ?
              <View
                style={{ position: 'absolute', elevation: 20, zIndex: 10, height, width, backgroundColor: "#0009" }}
              >
                <View
                  style={{ flex: 1, margin: 15, borderRadius: 20, backgroundColor: 'white' }}
                >
                  <MyHeader
                    title='Profile Description'
                    icon={require('../../assets/back.png')}
                    tintColor="white"
                    onPress={() => { this.setState({ isWebView1: !this.state.isWebView1 }) }}
                    containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
                    titleStyle={{ color: Colors.whiteText }}
                  />
                  <WebView
                    originWhitelist={['*']}
                    source={{ html: this.props.profileDescription }}
                    scrollEnabled={true}
                    style={{ flex: 1, }}
                  />
                </View>

              </View>
              : null
          } */}

          {/* {
            this.state.isWebView2 ?
              <View
                style={{ position: 'absolute', elevation: 20, zIndex: 10, height, width, backgroundColor: "#0009" }}
              >
                <View
                  style={{ flex: 1, margin: 15, borderRadius: 20, backgroundColor: 'white' }}
                >
                  <MyHeader
                    title='About Us'
                    icon={require('../../assets/back.png')}
                    tintColor="white"
                    onPress={() => { this.setState({ isWebView2: !this.state.isWebView2 }) }}
                    containerStyle={{ backgroundColor: Colors.themeColor, elevation: 0 }}
                    titleStyle={{ color: Colors.whiteText }}
                  />
                  <WebView
                    originWhitelist={['*']}
                    source={{ html: this.props.profileAboutUs }}
                    scrollEnabled={true}
                    style={{ flex: 1, }}
                  />
                </View>

              </View>
              : null
          } */}

        </View>
      </SafeAreaView>
    )
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  portrateMode: {
    flex: 1,
    backgroundColor: 'white',
    margin: 10,
    padding: 3,
    elevation: 10,
    borderRadius: 10,
    // marginTop: -PixelRatio.getPixelSizeForLayoutSize(25),
    // transform: [{ translateY: -PixelRatio.getPixelSizeForLayoutSize(0) }]
  },
})

const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    refereshGetProfile: common.refereshGetProfile,

    is_loading: common.is_loading,
    is_drawer: common.is_drawer,
    isSaveImage1: jaswant.isSaveImage1,
    isSaveImage2: jaswant.isSaveImage2,
    isProfileHeader: jaswant.isProfileHeader,
    isProfileDescription: jaswant.isProfileDescription,
    isProfileAboutUs: jaswant.isProfileAboutUs,
    isProfileFirstName: jaswant.isProfileFirstName,
    isProfileLastName: jaswant.isProfileLastName,
    isProfileEmail: jaswant.isProfileEmail,
    isProfilePhoneNumber: jaswant.isProfilePhoneNumber,

    profileImage1: jaswant.profileImage1,
    profileImage2: jaswant.profileImage2,
    profileImage1Url: jaswant.profileImage1Url,
    profileImage2Url: jaswant.profileImage2Url,

    profileCertificate: jaswant.profileCertificate,

    profileHeader: jaswant.profileHeader,
    profileDescription: jaswant.profileDescription,
    profileAboutUs: jaswant.profileAboutUs,
    profileFirstName: jaswant.profileFirstName,
    profileLastName: jaswant.profileLastName,
    profileEmail: jaswant.profileEmail,
    profilePhoneNumber: jaswant.profilePhoneNumber,

    is_whatsapp: jaswant.is_whatsapp,
    is_twitter: jaswant.is_twitter,
    is_messenger: jaswant.is_messenger,
    is_linkedin: jaswant.is_linkedin,
    is_instagram: jaswant.is_instagram,
    is_youtube: jaswant.is_youtube,
    is_pinterest: jaswant.is_pinterest,
    is_tiktok: jaswant.is_tiktok,

    whatsapp: jaswant.whatsapp,
    twitter: jaswant.twitter,
    messenger: jaswant.messenger,
    linkedin: jaswant.linkedin,
    instagram: jaswant.instagram,
    youtube: jaswant.youtube,
    pinterest: jaswant.pinterest,
    tiktok: jaswant.tiktok,

    whatsapp_icon: jaswant.whatsapp_icon,
    twitter_icon: jaswant.twitter_icon,
    messenger_icon: jaswant.messenger_icon,
    linkedin_icon: jaswant.linkedin_icon,
    instagram_icon: jaswant.instagram_icon,
    youtube_icon: jaswant.youtube_icon,
    pinterest_icon: jaswant.pinterest_icon,
    tiktok_icon: jaswant.tiktok_icon,

    profileCertificateList: jaswant.profileCertificateList,


    profile_certificate_image1: jaswant.profile_certificate_image1,
    profile_certificate_image2: jaswant.profile_certificate_image2,
    profile_certificate_image3: jaswant.profile_certificate_image3,
    profile_certificate_image4: jaswant.profile_certificate_image4,
    profile_certificate_image5: jaswant.profile_certificate_image5,
    profile_certificate_image6: jaswant.profile_certificate_image6,
    profile_certificate_image7: jaswant.profile_certificate_image7,

    profile_certificate_name1: jaswant.profile_certificate_name1,
    profile_certificate_name2: jaswant.profile_certificate_name2,
    profile_certificate_name3: jaswant.profile_certificate_name3,
    profile_certificate_name4: jaswant.profile_certificate_name4,
    profile_certificate_name5: jaswant.profile_certificate_name5,
    profile_certificate_name6: jaswant.profile_certificate_name6,
    profile_certificate_name7: jaswant.profile_certificate_name7,

    accessAndSecretsKey: jaswant.accessAndSecretsKey,
    loginResponse: common.loginResponse,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    // setIsDrawer: (domain) => setIsDrawer(domain),
    setIsSaveImage1: (isTrue) => setIsSaveImage1(isTrue),
    setIsSaveImage2: (isTrue) => setIsSaveImage2(isTrue),

    setIsProfileHeader: (isTrue) => setIsProfileHeader(isTrue),
    setIsProfileDescription: (isTrue) => setIsProfileDescription(isTrue),
    setIsProfileAboutUs: (isTrue) => setIsProfileAboutUs(isTrue),
    setIsProfileFirstName: (isTrue) => setIsProfileFirstName(isTrue),
    setIsProfileLastName: (isTrue) => setIsProfileLastName(isTrue),
    setIsProfileEmail: (isTrue) => setIsProfileEmail(isTrue),
    setIsProfilePhoneNumber: (isTrue) => setIsProfilePhoneNumber(isTrue),

    setProfileImage1: (img) => setProfileImage1(img),
    setProfileImage2: (img) => setProfileImage2(img),
    setProfileImage1Url: (url) => setProfileImage1Url(url),
    setProfileImage2Url: (url) => setProfileImage2Url(url),

    setProfileCertificate: (certificate) => setProfileCertificate(certificate),
    setProfileHeader: (txt) => setProfileHeader(txt),
    setProfileDescription: (txt) => setProfileDescription(txt),
    setProfileAboutUs: (txt) => setProfileAboutUs(txt),
    setProfileFirstName: (txt) => setProfileFirstName(txt),
    setProfileLastName: (txt) => setProfileLastName(txt),
    setProfileEmail: (txt) => setProfileEmail(txt),
    setProfilePhoneNumber: (txt) => setProfilePhoneNumber(txt),


    setIsProfileWhatsapp: (number) => setIsProfileWhatsapp(number),
    setIsProfileTwitter: (link) => setIsProfileTwitter(link),
    setIsProfileMessenger: (link) => setIsProfileMessenger(link),
    setIsProfileLinkedin: (link) => setIsProfileLinkedin(link),
    setIsProfileInstagram: (link) => setIsProfileInstagram(link),
    setIsProfileYoutube: (link) => setIsProfileYoutube(link),
    setIsProfilePinterest: (link) => setIsProfilePinterest(link),
    setIsProfileTiktok: (link) => setIsProfileTiktok(link),

    setProfileWhatsapp: (number) => setProfileWhatsapp(number),
    setProfileTwitter: (link) => setProfileTwitter(link),
    setProfileMessenger: (link) => setProfileMessenger(link),
    setProfileLinkedin: (link) => setProfileLinkedin(link),
    setProfileInstagram: (link) => setProfileInstagram(link),
    setProfileYoutube: (link) => setProfileYoutube(link),
    setProfilePinterest: (link) => setProfilePinterest(link),
    setProfileTiktok: (link) => setProfileTiktok(link),


    setProfileWhatsappIcon: (number) => setProfileWhatsappIcon(number),
    setProfileTwitterIcon: (link) => setProfileTwitterIcon(link),
    setProfileMessengerIcon: (link) => setProfileMessengerIcon(link),
    setProfileLinkedinIcon: (link) => setProfileLinkedinIcon(link),
    setProfileInstagramIcon: (link) => setProfileInstagramIcon(link),
    setProfileYoutubeIcon: (link) => setProfileYoutubeIcon(link),
    setProfilePinterestIcon: (link) => setProfilePinterestIcon(link),
    setProfileTiktokIcon: (link) => setProfileTiktokIcon(link),

    setProfileCertificateList: (list) => setProfileCertificateList(list),

    setProfileCertificate1: (certificate) => setProfileCertificate1(certificate),
    setProfileCertificate2: (certificate) => setProfileCertificate2(certificate),
    setProfileCertificate3: (certificate) => setProfileCertificate3(certificate),
    setProfileCertificate4: (certificate) => setProfileCertificate4(certificate),
    setProfileCertificate5: (certificate) => setProfileCertificate5(certificate),
    setProfileCertificate6: (certificate) => setProfileCertificate6(certificate),
    setProfileCertificate7: (certificate) => setProfileCertificate7(certificate),

    setProfileCertificateName1: (name) => setProfileCertificateName1(name),
    setProfileCertificateName2: (name) => setProfileCertificateName2(name),
    setProfileCertificateName3: (name) => setProfileCertificateName3(name),
    setProfileCertificateName4: (name) => setProfileCertificateName4(name),
    setProfileCertificateName5: (name) => setProfileCertificateName5(name),
    setProfileCertificateName6: (name) => setProfileCertificateName6(name),
    setProfileCertificateName7: (name) => setProfileCertificateName7(name),

    setAccesKeySecretsKey: (object) => setAccesKeySecretsKey(object),

    hitGetProfileData: (param) => hitGetProfileData(param),
    hitSaveProfileData: (param) => hitSaveProfileData(param),
    setLoginRespons: (res) => setLoginRespons(res),

    hitUploadImageOnASW: (params) => hitUploadImageOnASW(params),

    setRefereshGetProfile: (referesh) => setRefereshGetProfile(referesh),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen1)