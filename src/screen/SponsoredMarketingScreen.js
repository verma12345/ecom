import React, { useEffect, useState } from 'react';
import { View, Image, Text, Share, ImageBackground, StyleSheet, SafeAreaView, TouchableOpacity, FlatList, Dimensions, PixelRatio, Platform, ActivityIndicator, ScrollView } from "react-native";
import AWS from 'aws-sdk';
import VideoPlayerComponent from '../components/VideoPlayerComponent';
import { setMyMarketingDocumentUrl, setSelctedVideoUrl } from '../redux_store/actions/indexActionsJaswant';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import base64 from 'react-native-base64'
import Constants, { debugLog } from '../common/Constants';
import { setSelectedSponsoredImage_List, setSelectedSponsoredMessage_List, setSelectedSponsoredVideoList, setSponsoredImage_List, setSponsoredMessage_List, setSponsoredVideoList, setUpdateSelectAllSponsoredImage_List, setUpdateSelectAllSponsoredMessage_List, setUpdateSelectAllSponsoredVideoList, setUpdateSponsoredImage_List, setUpdateSponsoredMessage_List, setUpdateSponsoredVideoList, setUpdateSponsoredVideoListURL } from "../redux_store/actions/indexActions";
import SponsoredMessageListPopUp from '../components/SponsoredMessageListPopUp';
import { msStyle } from '../common/MyStyle';
import Colors from '../common/Colors';
import RNFS from 'react-native-fs'
import IsEmptyList from '../components/IsEmptyList';
import SharePopupJaswant from '../components/SharePopupJaswant';
import { sendEmail } from '../common/common';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';


// SETTINGS
var AWS_KEY = 'AKIAUKGZKWGYWRZ46EPO';
var AWS_SECRET = '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';
var BUCKET = 'ecomnow-images';
// var PREFIX_VIDEO = 'bht/images/reseller/2336/videos';
var PREFIX_VIDEO = 'bht/3/marketing/videos';
var PREFIX_TEXT = 'bht/3/marketing/messages';

var PREFIX_IMAGE = 'bht/3/marketing/images';

const SponsoredMarketingScreen = (props) => {
  const [isVideoPlayer, setVideoPlayer] = useState(false);
  const [isMessagePopUp, setMessagePopUp] = useState(false);
  const [videoProgressIndex, setVideoProgressIndex] = useState(null);
  const [totalVideoSize, setTotalVideoSize] = useState(null);
  const [downloadedVideoSize, setDownloadedVideoSize] = useState(null);
  const [isRedayToShare, setSharePopUp] = useState(false);
  const appList = [
    { id: '1', image: require('../../assets/whatsapp.png'), name: 'WhatsApp' },
    { id: '2', image: require('../../assets/messenger.png'), name: 'messenger' },
    { id: '3', image: require('../../assets/twitter.png'), name: 'twitter' },
    { id: '4', image: require('../../assets/pinterest.png'), name: 'pinterest' },
    { id: '5', image: require('../../assets/instagram.png'), name: 'instagram' },
    { id: '6', image: require('../../assets/mail.png'), name: 'gmail' },
    { id: '7', image: require('../../assets/linkedin.png'), name: 'linkedin' },
    // { id: '8', image: require('../../assets/mail2.png'), name: 'Text Message' },
  ]


  const downloadVideo = () => {
    //var fs = require('fs');
    // var AWS = require('aws-sdk');
    AWS.config.update({ accessKeyId: AWS_KEY, secretAccessKey: AWS_SECRET });

    var s3 = new AWS.S3();

    var params = {
      Bucket: BUCKET,
      // Prefix:PREFIX_VIDEO
      Prefix: `${props.loginResponse.platformId}/${props.loginResponse.cbp}/marketing/videos`
    }


    // debugLog(params)

    s3.listObjects(params, function (err, data) {
      if (err) return debugLog(err);

      // debugLog(data.Contents)
      // debugLog('listObject"""""')
      onSetVideo(data.Contents)
    });
  };



  const onSelectVideo = (video_url, index) => {
    debugLog(video_url.Key)

    props.setUpdateSponsoredVideoList(index)
  }


  const onPlayVideo = (video_url, index) => {
    props.setSelctedVideoUrl('https://ecomnow-images.s3.amazonaws.com/' + video_url.Key)
    setVideoPlayer(!isVideoPlayer)
  }

  // const onPlayVideoCopy = (video_url, index) => {

  //   if (video_url.path != undefined && video_url.path.includes('.mp4')) {
  //     props.setSelctedVideoUrl(video_url.path)
  //     setVideoPlayer(!isVideoPlayer)
  //     return
  //   } else {

  //     debugLog(video_url);
  //     // return

  //     setDownloadedVideoSize('Preparing...')
  //     setVideoProgressIndex(index)

  //     var s3 = new AWS.S3();
  //     var key = video_url.Key;
  //     debugLog('Downloading: ' + key);


  //     let videoId = video_url.Key.slice(video_url.Key.lastIndexOf('/') + 1)
  //     debugLog(videoId)

  //     var fileParams = {
  //       Bucket: BUCKET,
  //       Key: key
  //     }
  //     setVideoPlayer(!isVideoPlayer)
  //     props.setSelctedVideoUrl('/storage/emulated/0/Android/data/com.ecomenow/files/')


  //     s3.getObject(fileParams, async function (err, fileContents) {
  //       if (err) {
  //         callback(err);
  //       } else {
  //         var contents = await fileContents.Body.toString('base64');

  //         const LOCAL_PATH_TO_VIDEO = Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/${videoId}` : `${RNFS.ExternalDirectoryPath}/${videoId}`
  //         const LOCAL_PATH_TO_READ_VIDEO = Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}` : `${RNFS.ExternalDirectoryPath}`

  //         debugLog('FILE Writing: ')
  //         debugLog(LOCAL_PATH_TO_VIDEO)
  //         props.setSelctedVideoUrl(LOCAL_PATH_TO_VIDEO)
  //         // setVideoPlayer(!isVideoPlayer)
  //         props.setUpdateSponsoredVideoListURL(LOCAL_PATH_TO_VIDEO, index)
  //         setDownloadedVideoSize('finished')

  //         RNFS.writeFile(LOCAL_PATH_TO_VIDEO, contents, 'base64')
  //           .then(async () => {
  //             debugLog('FILE WRITTEN: ')
  //             debugLog(videoProgressIndex)
  //             let fileList = await RNFS.readDir(`${LOCAL_PATH_TO_READ_VIDEO}`, 'base64');
  //             // debugLog(fileList)

  //             // onSetVideo(fileList, index)
  //           })
  //           .catch(err => {
  //             console.log('File Write Error: ', err.message)
  //           })
  //       }
  //     }).on('httpDownloadProgress', function (evt) {
  //       calculateProgress(evt)
  //     })

  //   }
  // }


  const calculateProgress = (evt) => {
    // {"isTrusted": false, "lengthComputable": true, "loaded": 10523, "percent": 1, "total": 10523}

    var completed = evt.loaded;
    var total = evt.total;
    var progress = Math.ceil(completed / total * 100);
    // console.log(progress);
    // setVideoProgress(progress + "%")

    // return
    console.log('progress:', evt);

    let target = evt.total
    let loaded = evt.loaded

    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (target == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(target) / Math.log(1024)));
    let size = Math.round(target / Math.pow(1024, i), 2) + ' ' + sizes[i];
    setTotalVideoSize(size)
    var j = parseInt(Math.floor(Math.log(loaded) / Math.log(1024)));
    let downloaded = Math.round(loaded / Math.pow(1024, j), 2) + ' ' + sizes[j];
    setDownloadedVideoSize(downloaded)

    if (progress == 100) {
      setDownloadedVideoSize('Saving...')
    }
  }


  const onSetVideo = (videoList) => {
    props.setSponsoredVideoList(videoList)
  }


  const downloadImageFile = () => {

    AWS.config.update({ accessKeyId: AWS_KEY, secretAccessKey: AWS_SECRET });

    var s3 = new AWS.S3();

    var params = {
      Bucket: BUCKET,
      // Prefix: `${props.loginResponse.platformId}/${props.loginResponse.cbp}/marketing/images`
      Prefix: 'bht/3/marketing/images'
    }

    s3.listObjects(params, function (err, data) {
      if (err) return debugLog(err);

      // let imageList = []
      debugLog(data.Contents)
      props.setSponsoredImage_List(data.Contents)

      // async.eachSeries(data.Contents, function (fileObj, callback) {
      //   var key = fileObj.Key;

      //   debugLog('Downloading: ' + key);


      //   var fileParams = {
      //     Bucket: BUCKET,
      //     Key: key
      //   }

      //   s3.getObject(fileParams, function (err, fileContents) {
      //     if (err) {
      //       callback(err);
      //     } else {
      //       var contents = fileContents.Body.toString('base64');
      //       imageList = [...imageList, { uri: 'data:image/jpeg;base64,' + contents, key: key }]
      //       callback();
      //     }
      //   });
      // }, function (err) {
      //   if (err) {
      //     debugLog('Failed: ' + err);
      //   } else {
      //     debugLog('Finished');
      //     // debugLog(imageList)
      //     props.setSponsoredImage_List(imageList)
      //   }
      // });
    });
  };


  const downloadText = () => {
    var async = require('async');
    AWS.config.update({ accessKeyId: AWS_KEY, secretAccessKey: AWS_SECRET });

    var s3 = new AWS.S3();

    var params = {
      Bucket: BUCKET,
      Prefix: `${props.loginResponse.platformId}/${props.loginResponse.cbp}/marketing/messages`
    }

    s3.listObjects(params, function (err, data) {
      if (err) return debugLog(err);
      let messageList = []
      debugLog('ddddddddddddddddddddd')
      debugLog(data.Contents)

      // props.setSponsoredMessage_List(data.Contents)

      async.eachSeries(data.Contents, function (fileObj, callback) {
        var key = fileObj.Key;

        debugLog('Downloading: ' + key)
        var fileParams = {
          Bucket: BUCKET,
          Key: key
        }

        s3.getObject(fileParams, async function (err, fileContents) {
          if (err) {
            callback(err);
          } else {
            debugLog('bbbbbbbbb')
            var contents = fileContents.Body.toString('base64');
            // debugLog('encoded', contents)

            // setMessage([contents]);
            let txt = base64.decode(contents);
            messageList = [...messageList, { message: txt, Key: key }]

            // const LOCAL_PATH_TO_VIDEO = Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/test.txt` : '/storage/emulated/0/EcomeText'

            // var file_path = LOCAL_PATH_TO_VIDEO + '/' + 'test.txt'


            // RNFetchBlob.fs.isDir(path).then((isDir) => {
            //   if (isDir) {
            //     RNFetchBlob.fs.createFile(file_path, txt, 'utf8').then((res)=>{
            //       debugLog('file created')
            //     })
            //   } else {
            //     RNFetchBlob.fs.mkdir(path)
            //   }
            // })
            // RNFS.writeFile(file_path, txt, 'utf8')
            //   .then((success) => {
            //     console.log('FILE WRITTEN!');
            //     debugLog(LOCAL_PATH_TO_VIDEO)
            //   })
            //   .catch((err) => {
            //     console.log(err.message);
            //   });

            // let fileList = await RNFS.readFile(file_path, 'utf8');

            // // debugLog(path)
            // debugLog('file>>>>>>>>')
            // debugLog(fileList)



            callback();
          }
        });
      }, function (err) {
        if (err) {
          debugLog('Failed: ' + err);
        } else {
          debugLog('Finished');
          props.setSponsoredMessage_List(messageList)

        }
      });
    });
  };



  useEffect(() => {
    downloadVideo()
    downloadText()
    downloadImageFile()

    // props.setSponsoredVideoList(props.sponsoredVideoList)
    // props.setSponsoredImage_List(props.sponsoredImageList)
  }, [])






  const renderItemVideo = (dataItem) => {
    // debugLog(dataItem.item)
    return (

      <TouchableOpacity
        onPress={() => onPlayVideo(dataItem.item, dataItem.index)}

        style={[msStyle.myshadow, {
          height: PixelRatio.getPixelSizeForLayoutSize(63),
          width: PixelRatio.getPixelSizeForLayoutSize(63),
          borderRadius: 5,
          marginHorizontal: 10,
          marginVertical: 20,
          backgroundColor: '#262626',
          justifyContent: 'center',
          // alignItems: 'center',
          marginLeft: dataItem.index == 0 ? 20 : 0

        }]}

      >

        <ImageBackground
          imageStyle={{ borderRadius: 6 }}
          style={[{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',

          }]}
          source={dataItem.item.path != undefined ? require('../../assets/thumb.png') : require('../../assets/thumb1.png')}
        >
          <Image
            source={require('../../assets/play.png')}
            style={{ height: 35, width: 35, resizeMode: 'contain' }} />


          <TouchableOpacity
            onPress={() => { onSelectVideo(dataItem.item, dataItem.index) }}
            style={{ borderRadius: 30, elevation: 6, zIndex: 2, padding: 2, backgroundColor: 'gray', position: 'absolute', right: 5, bottom: 5 }}  >
            <TouchableOpacity
              onPress={() => { onSelectVideo(dataItem.item, dataItem.index) }}
              style={{ borderRadius: 30, backgroundColor: 'white' }} >

              {
                dataItem.item.selected ?
                  <Image
                    source={require('../../assets/check.png')}
                    style={{ height: 30, width: 30 }}
                  />
                  :
                  <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "white" }} />
              }

            </TouchableOpacity>
          </TouchableOpacity>

          {
            dataItem.index == 1 ?
              <TouchableOpacity style={{ position: 'absolute', right: 0, borderRadius: 5, backgroundColor: '#3c3c3c', height: '100%', width: 40, justifyContent: 'center', alignItems: 'center' }} >
                <Image
                  source={require('../../assets/drawer/navigate.png')}
                  style={{ width: 20, height: 20, resizeMode: 'contain', }}
                />
              </TouchableOpacity>
              : null
          }
          {
            downloadedVideoSize == 'finished' ?
              null : videoProgressIndex == dataItem.index ?
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                  <ActivityIndicator
                    color='white'
                  />
                  <Text style={[msStyle.txtStyle, { fontSize: 12, color: 'white' }]} >
                    {downloadedVideoSize == 'Saving...' ?
                      downloadedVideoSize :
                      downloadedVideoSize == 'Preparing...' ?
                        downloadedVideoSize :
                        downloadedVideoSize + "/" + totalVideoSize}</Text>
                </View>
                : null
          }

          <Text numberOfLines={1} style={[msStyle.txtStyle, {
            fontSize: 18,
            position: 'absolute',
            bottom: 3,
            left: 5,
            width: '95%',
            color: 'black',
            opacity: 0.6,
            alignSelf: 'center',
            backgroundColor: 'white'
          }]} >
            {dataItem.item.Key.slice(dataItem.item.Key.lastIndexOf('/') + 1)}</Text>

        </ImageBackground>
      </TouchableOpacity>
    )
  }


  const onCheckImage = (index) => {
    props.setUpdateSponsoredImage_List(index)

  }

  const renderItemImage = (dataItem) => {
    // debugLog(dataItem.item.selected)
    return (
      <TouchableWithoutFeedback
        style={[msStyle.myshadow, {
          height: PixelRatio.getPixelSizeForLayoutSize(63),
          width: PixelRatio.getPixelSizeForLayoutSize(63),
          borderRadius: 5,
          marginHorizontal: 10,
          marginTop: 20,
          marginBottom: 30,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: dataItem.index == 0 ? 20 : 0

        }]} >
        <ImageBackground
          source={{ uri: 'https://ecomnow-images.s3.amazonaws.com/' + dataItem.item.Key }}
          borderRadius={5}
          style={{ width: '98%', height: "98%", margin: 5, resizeMode: 'contain', flexDirection: 'row', justifyContent: 'flex-end' }} >



          <TouchableOpacity
            onPress={() => { onCheckImage(dataItem.index) }}
            style={{ borderRadius: 30, elevation: 6, zIndex: 2, padding: 1, backgroundColor: 'gray', position: 'absolute', right: 5, bottom: 5 }}  >
            <TouchableOpacity
              onPress={() => { onCheckImage(dataItem.index) }}
              style={{ borderRadius: 30, backgroundColor: 'white' }} >
              {
                dataItem.item.selected ?
                  <Image
                    source={require('../../assets/check.png')}
                    style={{ height: 30, width: 30 }}
                  />
                  :
                  <View style={{ height: 30, width: 30, borderRadius: 30, backgroundColor: "white" }} />
              }
            </TouchableOpacity>
          </TouchableOpacity>



          {
            dataItem.index == 1 ?
              <View style={{ backgroundColor: '#eaeaea', height: '100%', borderRadius: 4, width: 40, justifyContent: 'center', alignItems: 'center' }} >
                <Image
                  source={require('../../assets/drawer/navigate.png')}
                  style={{ width: 20, height: 20, resizeMode: 'contain', }}
                />
              </View>
              : null
          }

          <Text numberOfLines={1} style={[msStyle.txtStyle, {
            fontSize: 18,
            position: 'absolute',
            bottom: 3,
            left: 5,
            width: '95%',
            color: 'black',
            opacity: 0.6,
            alignSelf: 'center',
            backgroundColor: 'white'
          }]} >
            {dataItem.item.Key.slice(dataItem.item.Key.lastIndexOf('/') + 1)}</Text>

        </ImageBackground>
      </TouchableWithoutFeedback>
    )
  }


  const onRecommendation = async (mediaType) => {

    let selectedVideo = props.setSelectedSponsoredVideoList()
    let selectedImage = props.setSelectedSponsoredImage_List()
    let selectedMessage = props.setSelectedSponsoredMessage_List()


    let image = ''
    let video = ''
    let document = ''

    let selectedData = null

    selectedImage.map((item, index) => {

      if (item.selected == true) {
        // debugLog(item.image_url)
        selectedData += "\n\n" + 'https://ecomnow-images.s3.amazonaws.com/' + item.Key
      }
    })


    selectedVideo.map((item, index) => {
      if (item.selected == true) {
        // debugLog(item.image_url)
        selectedData += "\n\n" + 'https://ecomnow-images.s3.amazonaws.com/' + item.Key
      }
    })



    selectedMessage.map((item, index) => {
      if (item.selected == true) {
        // debugLog(item.image_url)
        selectedData += "\n\n" + 'https://ecomnow-images.s3.amazonaws.com/' + item.Key
      }
    })


    if (props.myMarketingDocumentUrl != null) {
      selectedData = "\n\n" + props.myMarketingDocumentUrl

    }


    if (selectedData == null) {
      alert('Plaese select Image, Video or Message.')
      return
    }



    // if (image != '') {
    //   image = "\n\nImages" + image

    // }

    // if (video != '') {
    //   video = "\n\nVideos" + video

    // }

    // if (props.myMarketingDocumentUrl != null) {
    //   document = "\n\nDocument\n\n" + props.myMarketingDocumentUrl

    // }



    // let urls = []
    // urls.push({
    //   image: image,
    //   video: video,
    //   document: document
    // })


    if (mediaType.name == 'gmail') {
      shareOnEmail(selectedData, mediaType)
      return
    }
    // shareViaWhatsapp('eComNow\n' + image + "\n\n" + video + "\n\nDocument\n" + props.myMarketingDocumentUrl)
    // return
    try {
      const result = await Share.share({
        message: 'eComNow\n' + selectedData,
        url: image,
        title: `eComNow`
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
          saveMediaCount(mediaType, selectedData)
          // setState({ isRedayToShare: false })
          setSharePopUp(!isRedayToShare)
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }

  };

  const onGoBack = () => {
    props.navigation.goBack()
  }

  const onMessage = () => {
    setMessagePopUp(!isMessagePopUp)
  }

  const onOk = () => {
    props.setSelectedSponsoredMessage_List()
    setMessagePopUp(!isMessagePopUp)

  }



  const onSelectSocialMedia = (mediaType) => {
    setSharePopUp(!isRedayToShare)
    onRecommendation(mediaType)
  }


  const shareOnEmail = (urls, mediaType) => {

    let email_to = [''];
    let subject = 'Sponsored Marketing';
    // let body = urls[0].image + urls[0].video + urls[0].document

    sendEmail(email_to, subject, urls)

    saveMediaCount(mediaType, urls)
    setSharePopUp(!isRedayToShare)


  }

  const saveMediaCount = async (mediaType, urls) => {

    let param = {
      domainname: props.loginResponse.domainName.includes(`www.`) ? props.loginResponse.domainName : `www.${props.loginResponse.domainName}`,
      platformid: props.loginResponse.platformId,
      cbpnumber: props.loginResponse.cbp,
      storenumber: props.loginResponse.storeNo,
      uniqueid: props.loginResponse.uniqueId,
      subject: 'Sponsored Marketing',
      cartName: "abc",// proudctList cart
      message: mediaType.name,
      // mobileapi_key: "3k82\/9NwEQfLif1IIKvQMg==",
      contacts: [],
      deliveryMethods: [{ "methodName": mediaType.name, "count": 1 }]
    }

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Basic YWRtaW46YWRtaW4=");
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Cookie", "AWSALBTG=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; AWSALBTGCORS=ssCeX7vKWy0a5VaQl1WjHXZ/xELnQPaR2b1CpnKXOx3ic4FsRlI+6W7CEHvH0Osa31/6B2nPRzXKgK/LiCyFsRQggy2oIh8uQu+sTVzVJA45hzJhqkZ1A2DTLgwnE635QZKvD5heoMbVK05hNiij+UuMyd5SQfpfCSq91sEIXsJF; SESSION=YTNhZmFiMjItOWI0Yy00NGQxLWJmNDItYmE3MDI2NjY5OWQy");

    var rawdata = JSON.stringify(param)
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: rawdata,
      redirect: 'follow'
    };

    fetch(`${Constants.API_BASE_URL}/admin/recommendations/save`, requestOptions)
      .then(response => response.text())
      .then(result => {
        debugLog('success::::::::::::::')
        debugLog(result)
        alert(result)
        debugLog(result)
        props.setUpdateSelectAllSponsoredVideoList(false)
        props.setUpdateSelectAllSponsoredImage_List(false)
        props.setUpdateSelectAllSponsoredMessage_List(false)
        props.setMyMarketingDocumentUrl(null)

        props.setSelectedSponsoredMessage_List()
        props.navigation.navigate('ProfileScreen1')
      })
      .catch(error => {
        debugLog('error:::::::::::::::::')
        debugLog(error)
      });

  }


  const onSelectMessage = (index) => {

    props.setUpdateSponsoredMessage_List(index)
  }

  const onAttachProduct = () => {
    props.navigation.navigate('ProductListScreen', { screen: 'SponsoredMarketingScreen' })
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={[styles.container,]} >


        <View style={{ height: 40, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }} >
          <TouchableOpacity
            onPress={() => { onGoBack() }}
            style={{ position: 'absolute', left: 10 }} >
            <Image
              source={require('../../assets/back.png')}
              style={{ height: 25, width: 25, tintColor: 'gray', resizeMode: 'contain' }} />
          </TouchableOpacity>
          <Text style={[msStyle.txtStyle, { fontSize: 18 }]}>{'Sponsored Marketing'}</Text>
        </View>

        <ScrollView contentContainerStyle={{paddingBottom:200}} >

          <View style={{}} >
            <Text style={[msStyle.txtStyle, { fontSize: 18, marginLeft: 20 }]} >{"Videos"}</Text>

            {props.sponsoredVideoList.length == 0 ? <IsEmptyList /> : null}

            <FlatList
              data={props.sponsoredVideoList}
              renderItem={renderItemVideo}
              keyExtractor={(item, index) => 'key' + index}
              horizontal
              showsHorizontalScrollIndicator={false}
            />
          </View>

          <View style={{}} >
            <Text style={[msStyle.txtStyle, { fontSize: 18, marginLeft: 20 }]}>{"Images"}</Text>

            {props.sponsoredImageList.length == 0 ? <IsEmptyList /> : null}

            <FlatList
              data={props.sponsoredImageList}
              renderItem={renderItemImage}
              keyExtractor={(item, index) => 'key' + index}
              horizontal
              showsHorizontalScrollIndicator={false}
            />
          </View>


          <View style={{ marginHorizontal: 20, }} >
            <Text style={[msStyle.txtStyle, { fontSize: 18 }]}>{"Message"}</Text>
            <TouchableOpacity
              onPress={() => onMessage()}
              style={{ borderWidth: 1, paddingVertical: 10, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, marginVertical: 20, borderRadius: 5 }} >
              <Image
                source={require('../../assets/downArrow.png')}
                style={{ height: 20, width: 20, resizeMode: 'contain' }} />

              <Text style={[msStyle.txtStyle, { fontSize: 14, paddingLeft: 10 }]}>{props.sponsoredSelectedMessageList.length > 0 ?
                props.sponsoredSelectedMessageList.length + " Message selected" : 'Select message'}</Text>

              {
                props.sponsoredMessageList.length == 0 ?
                  <ActivityIndicator color={Colors.themeColor} size='small' />
                  : null
              }
            </TouchableOpacity>
          </View>


          <TouchableOpacity
            onPress={() => onAttachProduct()}
            style={{ paddingVertical: 10, flexDirection: 'row', marginLeft: 20, marginBottom: 20 }} >
            <Image
              source={require('../../assets/link.png')}
              style={{ height: 25, width: 25, resizeMode: 'contain' }}
            />
            <Text style={{ fontSize: 16, marginLeft: 10, paddingHorizontal: 20 }} >
              {props.myMarketingDocumentUrl != null ? props.myMarketingDocumentUrl : 'Attach Product'}
            </Text>
          </TouchableOpacity>



          <TouchableOpacity
            onPress={() => {
              setSharePopUp(!isRedayToShare)
              // onRecommendation()
            }}
            style={{ marginHorizontal: 30, borderRadius: 8, marginBottom: 50, paddingVertical: 12, backgroundColor: Colors.themeColor, alignItems: 'center', justifyContent: 'center' }} >
            <Text style={[msStyle.txtStyle, { color: 'white', fontSize: 16 }]}>{"RECOMMEND"}</Text>
          </TouchableOpacity>
        </ScrollView>

        {
          isMessagePopUp ?
            <SponsoredMessageListPopUp
              data={props.sponsoredMessageList}
              onSelectMessage={onSelectMessage}
              onOk={() => { onOk() }}
            />
            : null
        }

        {isVideoPlayer ?
          <VideoPlayerComponent
            // downloadedVideoSize={downloadedVideoSize}
            // totalVideoSize={totalVideoSize}
            url={props.selectedVideoUrl}
            // url={videoUrl}
            onGoBack={() => setVideoPlayer(!isVideoPlayer)}
          />
          : null
        }

        {
          isRedayToShare ?
            <SharePopupJaswant
              title='Select Share Method'
              appList={appList}
              inputStyle={{ height: 100 }}
              onPress={onSelectSocialMedia}
              onCancel={() => setSharePopUp(!isRedayToShare)}
            />
            : null
        }
      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});


const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_loading: common.is_loading,
    sponsoredVideoList: common.sponsoredVideoList,
    sponsoredImageList: common.sponsoredImageList,
    sponsoredSelectedImageList: common.sponsoredSelectedImageList,
    sponsoredSelectedVideoList: common.sponsoredSelectedVideoList,
    sponsoredMessageList: common.sponsoredMessageList,
    sponsoredSelectedMessageList: common.sponsoredSelectedMessageList,


    selectedVideoUrl: jaswant.selectedVideoUrl,

    accessAndSecretsKey: jaswant.accessAndSecretsKey,
    loginResponse: common.loginResponse,

    myMarketingDocumentUrl: jaswant.myMarketingDocumentUrl,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({

    setSelectedSponsoredVideoList: (index) => setSelectedSponsoredVideoList(index),
    setUpdateSelectAllSponsoredVideoList: (index) => setUpdateSelectAllSponsoredVideoList(index),
    setUpdateSponsoredVideoList: (index) => setUpdateSponsoredVideoList(index),
    setSponsoredVideoList: (list) => setSponsoredVideoList(list),
    setUpdateSponsoredVideoListURL: (path, index) => setUpdateSponsoredVideoListURL(path, index),

    setSelectedSponsoredImage_List: (index) => setSelectedSponsoredImage_List(index),
    setUpdateSelectAllSponsoredImage_List: (index) => setUpdateSelectAllSponsoredImage_List(index),
    setUpdateSponsoredImage_List: (index) => setUpdateSponsoredImage_List(index),
    setSponsoredImage_List: (index) => setSponsoredImage_List(index),

    setSelectedSponsoredMessage_List: (index) => setSelectedSponsoredMessage_List(index),
    setUpdateSelectAllSponsoredMessage_List: (index) => setUpdateSelectAllSponsoredMessage_List(index),
    setUpdateSponsoredMessage_List: (index) => setUpdateSponsoredMessage_List(index),
    setSponsoredMessage_List: (index) => setSponsoredMessage_List(index),

    setSelctedVideoUrl: (url) => setSelctedVideoUrl(url),
    setMyMarketingDocumentUrl: (url) => setMyMarketingDocumentUrl(url),


  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(SponsoredMarketingScreen)

