import React, { Component } from 'react';
import { Button, View, Text, ActivityIndicator, Dimensions, BackHandler, Alert, SafeAreaView, ScrollView, Keyboard } from 'react-native';
import WebView from 'react-native-webview';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import { msStyle } from '../common/MyStyle';
import { getPrefs, setPrefs } from '../common/Prefs';
import { setLoading, setLoginRespons, setRefereshGetProfile } from '../redux_store/actions/indexActions';
import { hitSocialLogin } from '../redux_store/actions/indexActionsApi';

class SignInScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loading: false,
      isResponse: null,
      isError: null,
      height: Dimensions.get('window').height,
      loginResponse: null,
    }
    this.isResponse = 0
  }


  onSignIn = (res) => {
    setTimeout(() => {
      this.onValidCredintial()
    }, 5000)
  }




  onValidCredintial = () => {
    debugLog('success########')
    debugLog(this.state.loginResponse)
    setPrefs("ACCESS_TOKEN", JSON.stringify(this.state.loginResponse))
    this.props.setLoginRespons(this.state.loginResponse)
    // return
    if (this.state.loginResponse == null) {
      this.showAlert1('Something went wrong please login again!')
      return
    }

    if (this.state.loginResponse.domainName.toLowerCase() != this.props.domain.toLowerCase()) {
      this.showAlert1('Please enter valid platform')
      return
    }


    if (this.state.loginResponse.userEmail != this.props.email) {
      this.showAlert1('Please enter valid email')
      return
    }

    if (this.state.loginResponse.platformId.toLowerCase() != this.props.platform.toLowerCase()) {
      this.showAlert1('Please enter valid platform')
      return
    }


    // return
    this.setState({ isResponse: false })
    if (this.props.refereshGetProfile == 1) {
      this.props.setRefereshGetProfile(2)
    }

    this.props.navigation.navigate('ProfileScreen1')
    return
  }

  renderLoadingView() {
    return (
      <View style={{
        borderRadius: 10,
        backgroundColor: "white",
        flex: 1,

      }}>

        <ActivityIndicator
          size={"large"}
          color={Colors.themeColor}
          hidesWhenStopped={true}
        />

      </View>
    );
  }

  showAlert1(msg) {
    Alert.alert(
      'Login Error',
      msg,
      [
        {
          text: 'Go Back',
          onPress: () => {
            this.onLoginFailed()
          }
        },
      ]
    );
  }

  onSuccess = () => {
    // setPrefs("domain", this.props.domain)
    // setPrefs("email", this.props.email)
    // setPrefs("platform", this.props.platform)
  }

  onLoginFailed = () => {
    // setPrefs("domain", '')
    // setPrefs("email", '')
    // setPrefs("platform", '')
    this.props.navigation.navigate('OneTimeScreen')
  }

  handleWebViewNavigationStateChange = newNavState => {

    debugLog(newNavState)
    debugLog('After Log in')
    console.log(newNavState.jsEvaluationValue)

    // this.setState({ isLoading: true })
    // const { url } = newNavState;
    // debugLog(url)
    // debugLog('??????????????????????????')
    // if (!url) return;
    // // redirect somewhere else
    // if (url.includes('mobile-login-success')) {
    //   const url = "https://www.ecompaas.com/test-sso/login?state=login->true,domain->" + this.props.domain + ",platform->" + this.props.platform + ",source->mobile"
    //   this.setState({ is_loading: true })

    //   // So you mean url should look like this
    //   // right? ok ok
    //   fetch(url
    //     // {
    //     //   headers: {
    //     //     'Content-Type': 'application/json'
    //     //   },
    //     // }
    //   ).then(async response => {

    //     debugLog("")
    //     debugLog(response)
    //     this.isResponse = this.isResponse + 1
    //     if (this.isResponse == 1) {

    //       debugLog('Request for SSO Login:')
    //       debugLog(url)
    //       debugLog("END::::::::")

    //       this.onSignIn()

    //       debugLog("RESPONSE::::::::")

    //       debugLog(response)
    //       debugLog("END::::::::")
    //     }


    //     try {

    //       let res = await response.json()
    //       debugLog('Before State')
    //       debugLog(res)
    //       this.setState({ loginResponse: res })
    //       // this.onNavigate(res)
    //       debugLog('After State')
    //       debugLog(res)

    //     } catch (error) {
    //       debugLog('Error::::::')
    //       debugLog(error)
    //     }
    //   })
    // }
  };

  handleMessage = ({ nativeEvent: { data } }) => {
    // console.log('got message, data:', data);

    debugLog('>>>>>>>>>>>>')
    debugLog(data)
  }

  render() {
    return (
      <SafeAreaView

        style={{ flex: 1 }}>
        {/* <ScrollView contentContainerStyle={{ marginBottom: 200 ,flex:1}} > */}

        <WebView
          incognito={true}
          userAgent="Mozilla/5.0 Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"
          source={{ uri: "https://www.ecompaas.com/test-sso/login?state=login->true,domain->" + this.props.domain + ",platform->" + this.props.platform + ",source->mobile" }}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          originWhitelist={['*']}
          renderLoading={this.renderLoadingView}
          onNavigationStateChange={this.handleWebViewNavigationStateChange}
          startInLoadingState={true}

          onMessage={this.handleMessage}
          injectedJavaScript={"window.postMessage()"}
          javaScriptEnabledAndroid={true}

        // injectedJavaScript={'(function(){return "Send me back!"}());'}

        // onLoadEnd={this.loadEnd.bind(this)}
        />

        {/* </ScrollView> */}

        {
          this.state.is_loading ?
            <View style={{
              position: 'absolute',
              width: Dimensions.get('window').width,
              height: Dimensions.get('window').height,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center'
            }} >
              <ActivityIndicator
                size={"large"}
                color={Colors.themeColor}
              // hidesWhenStopped={true}
              />
              <Text style={msStyle.txtStyle} >{"Please wait for a while..."}</Text>
            </View>
            : null
        }
        {/* </ScrollView> */}

      </SafeAreaView>
    )
  }


}



const mapStateToProps = (state) => {
  let common = state.indexReducer
  return {
    is_loading: common.is_loading,
    domain: common.domain,
    email: common.email,
    platform: common.platform,
    is_drawer: common.is_drawer,
    refereshGetProfile: common.refereshGetProfile,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    hitSocialLogin: (domain) => hitSocialLogin(domain),
    // setEmail: (email) => setEmail(email),
    // setPlatform: (platform) => setPlatform(platform),
    setLoading: (isLoading) => setLoading(isLoading),
    setLoginRespons: (res) => setLoginRespons(res),
    setRefereshGetProfile: (referesh) => setRefereshGetProfile(referesh),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen)


// apendex b 