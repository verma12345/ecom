import React, { Component } from 'react';

import { Button, ScrollView, Text, View, SafeAreaView, Alert } from 'react-native';

import MenuOption from '../components/MenuOption';
import MyHeader from '../components/MyHeader';
import { msStyle } from '../common/MyStyle';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setDomain, setEmail, setIsDrawer, setPlatform, setRefereshGetProfile } from '../redux_store/actions/indexActions';
import MyLoader from '../components/MyLoader';
import { setPrefs } from '../common/Prefs';
import { debugLog } from '../common/Constants';
import { StackActions } from '@react-navigation/routers';

class DrawerContent extends Component {
  constructor(props) {
    super(props);
  }

  onMenu = () => {
    this.props.navigation.closeDrawer()
  }

  onProfile = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('ProfileScreen1')

  }

  onRecommendation = () => {
    this.props.navigation.closeDrawer()
    // this.props.navigation.navigate('OneTimeScreen')
    this.props.navigation.navigate('RecommendationScreen')
  }

  onMessage = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('MessageScreen')
  }

  onDashboard = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('DashboardScreen')
  }

  onProduct = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('ProductScreen')
  }

  onWestornUnionPortal = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('WesternUnionScreen')
  }

  onSponsoredMarketing = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('SponsoredMarketingScreen')
    // this.props.navigation.navigate('Testing')
  }

  onMyMarketing = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('MyMarketingScreen')
  }


  onContactUs = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('ContactUsScreen')
  }


  onHelp = () => {
    this.props.navigation.closeDrawer()
    this.props.navigation.navigate('HelpScreen')
  }

  cleanAndLogout = () => {
    setPrefs("SIGINED_IN", "")
    setPrefs("ONE_TIME_SCREEN", "")
    this.props.navigation.closeDrawer()
    // this.props.setDomain('')
    // this.props.setEmail('')
    // this.props.setPlatform('')
    this.props.navigation.navigate('OneTimeScreen')
    this.props.setRefereshGetProfile(1)

    // this.props.navigation.dispatch(
    //     StackActions.replace('OneTimeScreen'),
    //   )
  }

  onLogout = () => {
    Alert.alert(
      'Logout',
      'Are sure want to Logout?',
      [
        // { text: 'Rate App', onPress: () => rateApp() },
        {
          text: 'Cancel',
          onPress: () => debugLog('Cancel Pressed'),
          style: 'cancel',
        },
        { text: 'Yes', onPress: () => this.cleanAndLogout() },
      ],
      { cancelable: false },
    );
  }


  render() {

    return (
      <SafeAreaView style={msStyle.container} >
        <View style={msStyle.container} >
          {
            this.props.is_loading ?
              <MyLoader />
              : null
          }
          <MyHeader
            title='Menu'
            icon={require('../../assets/drawer/menu.png')}
            onPress={() => { this.onMenu() }}
          />
          <View style={{ paddingHorizontal: 10 }} >
            <ScrollView  >

              <MenuOption
                title='Profile'
                onPress={() => this.onProfile()}
                icon={require('../../assets/drawer/profile.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15, marginTop: 50 }}
              />

              <MenuOption
                title='Recommendation'
                onPress={() => this.onRecommendation()}
                icon={require('../../assets/drawer/recommendation.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15 }}

              />

              <MenuOption
                title='Message'
                onPress={() => this.onMessage()}
                icon={require('../../assets/drawer/message.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15 }}
              />

              <MenuOption
                title='Dashboard'
                onPress={() => this.onDashboard()}
                icon={require('../../assets/drawer/dashboard.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15 }}

              />

              <MenuOption
                title='Product'
                onPress={() => this.onProduct()}
                icon={require('../../assets/drawer/product.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15 }}
              />

              <MenuOption
                title='Western Union Portal'
                onPress={() => this.onWestornUnionPortal()}
                icon={require('../../assets/drawer/portal.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15 }}

              />

              <MenuOption
                title='Sponsored Marketing'
                onPress={() => this.onSponsoredMarketing()}
                icon={require('../../assets/drawer/call.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15 }}
              />

              <MenuOption
                title='My Marketing'
                onPress={() => this.onMyMarketing()}
                icon={require('../../assets/drawer/marketing.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15 }}

              />

              <MenuOption
                title='Contact Us'
                onPress={() => this.onContactUs()}
                icon={require('../../assets/contact_us.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15 }}
                iconStyle={{ width: 22, height: 22, tintColor: 'white' }}

              />

              <MenuOption
                title='Logout'
                onPress={() => this.onLogout()}
                icon={require('../../assets/logout.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15, }}
                iconStyle={{ width: 25, height: 25, tintColor: 'white' }}
              />

              <MenuOption
                title='Help'
                onPress={() => this.onHelp()}
                icon={require('../../assets/drawer/help.png')}
                containerStyle={{ paddingRight: 20, marginVertical: 15, marginBottom: 50 }}
              />


              <Text style={[msStyle.txtStyle, { marginBottom: 70 }]} >{"App version 1.0"}</Text>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>

    );
  }
}



const mapStateToProps = (state) => {
  let common = state.indexReducer
  return {
    is_drawer: common.is_drawer,
    is_loading: common.is_loading,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    setIsDrawer: (domain) => setIsDrawer(domain),
    setDomain: (domain) => setDomain(domain),
    setEmail: (email) => setEmail(email),
    setPlatform: (platform) => setPlatform(platform),
    setRefereshGetProfile: (referesh) => setRefereshGetProfile(referesh),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent)