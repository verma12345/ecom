import React, { Component, useState } from 'react';
import { Image, Button, View, Text, SafeAreaView, TextInput, Dimensions, StyleSheet, ScrollView, SectionList, TouchableOpacity, flexDirection, FlatList } from 'react-native';
import Colors from '../common/Colors';
import { msStyle } from '../common/MyStyle';
import MyHeader from '../components/MyHeader';
import { debugLog } from '../common/Constants';
import MyButton from '../components/MyButton';
import MyInputText from '../components/MyInputText';
import { setDomain, setEmail, setPlatform } from '../redux_store/actions/indexActions';
import { getPixelSizeForLayoutSize } from 'react-native/Libraries/Utilities/PixelRatio';
import { NativeViewGestureHandler, RawButton } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setIsDrawer, setLoginRespons } from '../redux_store/actions/indexActions';
import { getPrefs } from '../common/Prefs';


class ProductDetailScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            searchTxt: '',
            data: this.props.route.params.data,

        }
    }

    componentDidMount() {
        // this.getProductList()
        // debugLog(this.state.data)
        // this.setState.data({ data: this.props.route.params.data })
    }


    render() {
        return (
            <SafeAreaView style={msStyle.container}>
                <View style={{ flexDirection: 'row', height: 60, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity
                        style={{ position: 'absolute', left: 10 }}
                        onPress={() => { this.props.navigation.goBack(); }}
                    >
                        <Image
                            source={require('../../assets/back2.png')}
                            resizeMode="contain"
                            style={{
                                width: 30,
                                height: 30,
                                paddingRight: 20,
                            }}
                        />
                    </TouchableOpacity>

                    <Text style={[msStyle.txtStyle, {
                        fontSize: 20,
                        fontWeight: '500'
                    }]}>{"Product Detail"}</Text>
                </View>

                {/* MARK:  body */}

                <View style={(msStyle.container)}>
                    <View style={{ padding: 10, flex: 1, flexDirection: 'row', justifyContent: "space-between" }}>

                        <View style={[{
                            borderWidth: 1,
                            borderColor: 'lightgray',
                            borderTopLeftRadius: 15,
                            borderBottomLeftRadius: 15,
                            borderBottomRightRadius: 15,
                            backgroundColor: 'white',
                            margin: 5,
                            width: Dimensions.get('window').width / 2,
                            height: Dimensions.get('window').width / 2,
                        }]} >
                            <Image style={{
                                flex: 1,
                                borderTopLeftRadius: 15,
                                borderBottomLeftRadius: 15,
                                borderBottomRightRadius: 15,
                            }}
                                source={{ uri: this.props.route.params.data.image }} />

                        </View>


                        <View style={{
                            flex: 1,
                            padding: 5,
                            alignItems: 'center',
                            left: 20,
                            top: -5

                        }}>
                            <Text style={{ fontSize:16 }}>{this.props.route.params.data.description}</Text>
                        </View>
                    </View>

                    <MyButton
                        title={'Price $' + this.props.route.params.data.price}
                        //onPress={() => { this.onProceed() }}
                        titleStyle={{ color: Colors.whiteText, fontWeight: 'bold' }}
                        containerStyle={{ margin: 25, width: 100 }}
                    />

                </View>

            </SafeAreaView>
        )
    }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
    absoluteStyle: {
        position: 'absolute',
        backgroundColor: Colors.whiteText,
        alignSelf: 'center',
        width: width - 30,
        height: height - 70,
        elevation: 10,
        zIndex: 5,
        bottom: 15,
        borderRadius: 5
    },
    list: {
        flex: 1,
        padding: 15

    }

})


const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let jaswant = state.indexReducerJaswant;

    return {
        loginResponse: common.loginResponse,

    }
}

const mapDispatchToProps = (dispatch) => {

    return bindActionCreators({
        // setIsDrawer: (domain) => setIsDrawer(domain),

        setLoginRespons: (res) => setLoginRespons(res),
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailScreen)