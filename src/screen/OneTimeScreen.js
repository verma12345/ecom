
import React, { Component } from 'react';
import { Image, View, Text, TextInput, StyleSheet, TouchableHighlight, ScrollView, SafeAreaView, BackHandler, ActivityIndicator } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setIsDrawer } from '../../App';
import Colors from '../common/Colors';
import { isValidEmail } from '../common/common';
import Constants, { debugLog } from '../common/Constants';
import { msStyle } from '../common/MyStyle';
import { getPrefs, setPrefs } from '../common/Prefs';
import MyButton from '../components/MyButton';
import MyInputText from '../components/MyInputText';
import { setDomain, setEmail, setPlatform } from '../redux_store/actions/indexActions';
import { setIsLoginFailed } from '../redux_store/actions/indexActionsJaswant';

class OneTimeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailError: '',
      domainError: '',
      platformError: '',
    }

  }


  componentDidMount() {


    getPrefs('domain').then((value) => {
      this.props.setDomain(value)
    })

    getPrefs('email').then((value) => {
      this.props.setEmail(value)
    })

    getPrefs('platform').then((value) => {
      this.props.setPlatform(value)
    })
  }

  onProceed = () => {
    if (this.props.domain == '') {
      this.setState({ domainError: "Please enter domain" })
      return
    }

    if (this.props.domain.includes('.com') == false) {
      let newDomain = this.props.domain + '.com'
      let newDomain1 = newDomain.replace('..', '.')
      this.props.setDomain(newDomain1)
      return
    }

    this.setState({ domainError: "" })

    if (this.props.email == '') {
      this.setState({ emailError: "Please enter Email" })
      return
    }

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{3})+$/;
    if (reg.test(this.props.email) === false) {
      this.setState({ emailError: "Email is Not Valid" })
      return;
    }

    if (this.props.platform === '') {
      this.setState({ platformError: "Please enter Platform" })
      return;
    }

    this.validateUserAPI()

  }




  validateUserAPI = () => {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      redirect: 'follow'
    };

    var url = `${Constants.AUTH_API_BASE_URL}/test-sso/validate-mobile-auth-user?platform_id=${this.props.platform}&email_address=${this.props.email}&domain=${this.props.domain}`

    debugLog('Request for First Screen Login:')
    console.log(url, requestOptions);
    fetch(url, requestOptions)
      .then(response => response.text())
      .then(result => {
        debugLog('Response:')
        debugLog(result)
        if (result === 'User validated successfully.') {
          setPrefs("domain", this.props.domain)
          setPrefs("email", this.props.email)
          setPrefs("platform", this.props.platform)
          this.props.navigation.navigate('SignInScreen')
        } else {
          //alert('Please enter valid credential')
          alert(result)

        }

      })
      .catch(error => console.log('error', error));
  }

  onLogin = () => {
    this.props.navigation.navigate('SignInScreen')
  }

  render() {

    return (
      <SafeAreaView style={msStyle.container}>
        <View style={msStyle.container}>

          {this.props.isLoginFailed ?


            <View style={{
              flex: 1,
              backgroundColor: this.props.loginFailedMessage.bgColor, justifyContent: 'space-between',
              paddingVertical: 40, alignItems: 'center',
              marginHorizontal: 20, marginVertical: 50, borderRadius: 25, opacity: 0.9
            }} >
              <View>
                <Text style={[msStyle.txtStyle, {
                  color: '#ffffff', fontSize: 30,
                  marginHorizontal: 20, marginBottom: 20, textAlign: 'center'
                }]} >{this.props.loginFailedMessage.title}</Text>

                <Text style={[msStyle.txtStyle, {
                  color: '#ffffff', fontSize: 25, marginHorizontal: 20,
                  textAlign: 'center'
                }]} >{this.props.loginFailedMessage.msg}</Text>
              </View>


              <Image source={this.props.loginFailedMessage.bgColor == '#42ba96' ? require('../../assets/success.png') : require('../../assets/failed.png')} style={{
                height: 150,
                width: 150,
                resizeMode: 'contain',
                tintColor: 'white'
              }} />

              <TouchableOpacity
                style={[msStyle.myshadow2, {
                  // opacity: 0.9,
                  backgroundColor: this.props.loginFailedMessage.bgColor,
                  paddingHorizontal: 30,
                  paddingVertical: 5,
                  borderRadius: 30,
                }]}
                onPress={() => this.onLogin()}
              >
                <Text style={[msStyle.txtStyle, {
                  color: '#ffffff', fontSize: 25, marginHorizontal: 20,
                  textAlign: 'center'
                }]} >{this.props.loginFailedMessage.btnTitle}</Text>
              </TouchableOpacity>


            </View>
            :
            <ScrollView contentContainerStyle={{ paddingBottom: 100 }} >
              <View style={{ marginHorizontal: 30 }}>


                <View style={{ paddingTop: 70 }} >
                  <Image source={require('../../assets/ecomlogo.png')} style={{
                    height: 150,
                    width: '70%',
                    resizeMode: 'contain'
                  }} />

                  <Text style={[msStyle.txtStyle,]}>{"Enter your credential below"}</Text>

                </View>

                <MyInputText
                  placeholder="Domain"
                  onChangeText={(txt) => { this.props.setDomain(txt.replace(/\s+/g, "")) }}
                  style={{ marginTop: 50, }}
                  value={this.props.domain}
                  autoCapitalize='none'
                  keyboardType='email-address'
                />
                <Text style={[msStyle.txtStyle, { color: 'red' }]}>{this.state.domainError}</Text>


                <MyInputText
                  placeholder="Email"
                  onChangeText={(txt) => { this.props.setEmail(txt) }}
                  style={{ marginTop: 15, }}
                  value={this.props.email}
                  autoCapitalize='none'
                  keyboardType='email-address'
                />
                <Text style={[msStyle.txtStyle, { color: 'red' }]}>{this.state.emailError}</Text>

                <MyInputText
                  placeholder="Platform"
                  onChangeText={(txt) => { this.props.setPlatform(txt) }}
                  style={{ marginTop: 25, }}
                  value={this.props.platform}
                  autoCapitalize='none'
                  keyboardType='email-address'

                />
                <Text style={[msStyle.txtStyle, { color: 'red' }]}>{this.state.platformError}</Text>


                <MyButton
                  title='PROCEED'
                  onPress={() => {
                    this.onProceed()

                  }}
                  titleStyle={{ color: Colors.whiteText }}
                  containerStyle={{ marginTop: 100, }}
                />


              </View>
            </ScrollView>
          }
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  MainContainer:
  {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },

  containerStyle: {
    width: '85%',
    alignItems: 'center',
    borderColor: '#ddd',
    borderBottomWidth: 1,
  }
})


const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    domain: common.domain,
    email: common.email,
    platform: common.platform,
    isLoginFailed: jaswant.isLoginFailed,
    loginFailedMessage: jaswant.loginFailedMessage,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    setDomain: (domain) => setDomain(domain),
    setEmail: (email) => setEmail(email),
    setPlatform: (platform) => setPlatform(platform),
    setIsLoginFailed: (isTrue) => setIsLoginFailed(isTrue),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(OneTimeScreen)