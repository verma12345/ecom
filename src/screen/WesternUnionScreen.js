import React, { Component } from "react";
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, TouchableOpacity, ScrollView, ActivityIndicator } from "react-native";
import WebView from "react-native-webview";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { msStyle } from "../common/MyStyle";
import MyLoader from "../components/MyLoader";
import VideoPlayerComponent from "../components/VideoPlayerComponent";
import { hitWestornUnion } from "../redux_store/actions/indexActionsApi";

class WesternUnionScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      message: 'Bank Account\n\n' +
        'Dear BioHealthTechnology Reseller,\n\n' +
        'To make sure you receive your payments quickly and directly into your bank account, we are' +
        'pleased to tell you that we have enlisted Western Union Business Solutions Payee Manager, a' +
        'secure and easy to use reseller portal.\n\n' +
        'This new reseller portal, Payee Manager, will allow you to:' +
        'Securely setup and maintain your reseller profile;\n' +
        'Receive an email notification each time a payment is sent;\n' +
        'Track payments and view historical information;\n' +
        'To change and update contact information, email address and banking information' +
        'quickly and securely.\n\n' +
        'To setup your reseller profile, please click “CREATE ACCOUNT” at the bottom of this page.' +
        'After you have enrolled, you can re- visit the site using this URL to track payments or update' +
        'your information ' +
        'https://payee.globalpay.westernunion.com/PayeeManager/Beneficiary/Login.aspx\n\n' +
        'Please create your reseller profile as soon as possible, as we will not be able to send payments' +
        'to resellers who have not enrolled and created a profile.\n\n',

      message1: '\n\nIf you have any questions, please contact us at info@biohealthtechnology.com\n\n' +
        'Regards\n\n' +
        'BioHealthTechnology Team',

      yes: true,
      no: false,
      isVideoPlayer: false,

    }
  }
  onGoBack = () => {
    this.props.navigation.goBack()
  }

  onPressYes = () => {
    this.setState({
      yes: true,
      no: false
    })
  }

  onPressNo = () => {
    this.setState({
      yes: false,
      no: true
    })
  }


  onCreateAccount = () => {
    this.props.navigation.navigate('WesternUnionCreateAccount')
  };


  onWatchVideo = () => {
    this.setState({ isVideoPlayer: !this.state.isVideoPlayer })
    // https://ecombht.com/images/videos/admin%20functions-WesternUnion.mp4
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container} >

          {/* header */}
          <View style={{ height: 40, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }} >
            <TouchableOpacity
              onPress={() => this.onGoBack()}
              style={{ position: 'absolute', left: 10 }} >
              <Image
                source={require('../../assets/back.png')}
                style={{ height: 25, width: 25, tintColor: '#141212', resizeMode: 'contain' }}
              />
            </TouchableOpacity>
            <Text style={[msStyle.txtStyle, { fontSize: 22, color: '#141212' }]} >{'Western Union'}</Text>
          </View>



          <ScrollView>
            <Text style={[msStyle.txtStyle, { flex: 1, margin: 20 }]} >
              {this.state.message}
              <Text onPress={() => this.onWatchVideo()} style={[msStyle.txtStyle, { flex: 1, margin: 20, color: Colors.themeColor, textDecorationLine: 'underline' }]} >
                {'Watch Instructional Video'}
              </Text>
              {this.state.message1}
            </Text>


            <Text style={[msStyle.txtStyle, { fontSize: 16, color: '#141212', marginHorizontal: 20 }]} >
              {`Please confirm if you have submitted the information?`}
            </Text>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 30, paddingVertical: 10, }} >


              <TouchableOpacity onPress={() => this.onPressYes()} style={{ flexDirection: 'row', alignItems: 'center', }} >
                <View style={{
                  width: 20, height: 20,
                  backgroundColor: 'gray',
                  borderRadius: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 2,
                  zIndex: 2
                }} >
                  <TouchableOpacity onPress={() => this.onPressYes()} style={{ height: 15, width: 15, borderRadius: 30, backgroundColor: this.state.yes ? '#1a7bc2' : 'gray', borderWidth: 2, borderColor: 'white' }} />
                </View>
                <Text style={[msStyle.txtStyle, { fontSize: 16, marginLeft: 10 }]} >{'Yes'}</Text>
              </TouchableOpacity>


              <TouchableOpacity onPress={() => this.onPressNo()} style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }} >
                <View style={{
                  width: 20, height: 20,
                  backgroundColor: 'gray',
                  borderRadius: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 2,
                  zIndex: 2
                }} >
                  <TouchableOpacity onPress={() => this.onPressNo()} style={{ height: 15, width: 15, borderRadius: 30, backgroundColor: this.state.no ? '#1a7bc2' : 'gray', borderWidth: 2, borderColor: 'white' }} />
                </View>
                <Text style={[msStyle.txtStyle, { fontSize: 16, marginLeft: 10 }]} >{'No'}</Text>
              </TouchableOpacity>
            </View>


            <TouchableOpacity
              onPress={() => this.onCreateAccount()}
              style={{
                marginHorizontal: 30, borderRadius: 8, marginTop: 20, marginBottom: 50, paddingVertical: 10,
                backgroundColor: '#1a7bc2', alignItems: 'center', justifyContent: 'center'
              }} >
              <Text style={[msStyle.txtStyle, { color: 'white', fontSize: 18 }]}>{"Create account"}</Text>
            </TouchableOpacity>
          </ScrollView>

          {this.state.isVideoPlayer ?
            <VideoPlayerComponent
              downloadedVideoSize='finished'
              url='https://ecombht.com/images/videos/admin%20functions-WesternUnion.mp4'
              onGoBack={() => this.setState({ isVideoPlayer: !this.state.isVideoPlayer })}
            />
            : null
          }

        </View>
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000a0"
  }
});



const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_loading: common.is_loading,

    accessAndSecretsKey: jaswant.accessAndSecretsKey,
    loginResponse: common.loginResponse,
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    hitWestornUnion: (params) => hitWestornUnion(params),
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(WesternUnionScreen)