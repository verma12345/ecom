import React, { useEffect, useState } from 'react';
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, TouchableOpacity, FlatList, Dimensions, PixelRatio, Platform, ActivityIndicator } from "react-native";
import AWS from 'aws-sdk';
import VideoPlayerComponent from '../components/VideoPlayerComponent';
import { setSelctedVideoUrl } from '../redux_store/actions/indexActionsJaswant';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import base64 from 'react-native-base64'
import { debugLog } from '../common/Constants';
import { setSelectedSponsoredImage_List, setSelectedSponsoredMessage_List, setSelectedSponsoredVideoList, setSponsoredImage_List, setSponsoredMessage_List, setSponsoredVideoList, setUpdateSelectAllSponsoredImage_List, setUpdateSelectAllSponsoredMessage_List, setUpdateSelectAllSponsoredVideoList, setUpdateSponsoredImage_List, setUpdateSponsoredMessage_List, setUpdateSponsoredVideoList, setUpdateSponsoredVideoListURL } from "../redux_store/actions/indexActions";
import SponsoredMessageListPopUp from '../components/SponsoredMessageListPopUp';
import { msStyle } from '../common/MyStyle';
import Colors from '../common/Colors';
import RNFS from 'react-native-fs'
import IsEmptyList from '../components/IsEmptyList';

// SETTINGS
var AWS_KEY = 'AKIAUKGZKWGYWRZ46EPO';
var AWS_SECRET = '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';
var BUCKET = 'ecomnow-images';
// var PREFIX_VIDEO = 'bht/images/reseller/2336/videos';
var PREFIX_VIDEO = 'bht/3/marketing/videos';
var PREFIX_TEXT = 'bht/3/marketing/messages';

var PREFIX_IMAGE = 'bht/3/marketing/images';

const Testing = (props) => {
  const [isVideoPlayer, setVideoPlayer] = useState(false);
  const [videoUrl, setVideoUrl] = useState('');

  const [encodeImagebase64, setencodeImagebase64] = useState('');
  const [message, setMessage] = useState([]);
  const [isMessagePopUp, setMessagePopUp] = useState(false);
  const [videoProgressIndex, setVideoProgressIndex] = useState(null);

  const [totalVideoSize, setTotalVideoSize] = useState(null);
  const [downloadedVideoSize, setDownloadedVideoSize] = useState(null);








  const downloadVideo = () => {
    //var fs = require('fs');
    // var AWS = require('aws-sdk');
    AWS.config.update({ accessKeyId: AWS_KEY, secretAccessKey: AWS_SECRET });

    var s3 = new AWS.S3();

    var params = {
      Bucket: BUCKET,
      Prefix: PREFIX_VIDEO
    }

    s3.listObjects(params, function (err, data) {
      if (err) return debugLog(err);
      let videoList = []

      debugLog(data.Contents)
      debugLog('listObject"""""')
      onSetVideo(data.Contents)
    });
  };





  const onCheckVideo = (index, data) => {
    if (data.path == undefined && data != 'uncheck') {
      alert('Please download video by single tap then try to select!')
      return
    }
    props.setUpdateSponsoredVideoList(index)
  }



  const onPlayVideo = (video_url, index) => {

    if (video_url.path != undefined && video_url.path.includes('.mp4')) {
      props.setSelctedVideoUrl(video_url.path)
      setVideoPlayer(!isVideoPlayer)
      return
    } else {

      debugLog(video_url);
      // return

      setDownloadedVideoSize('Preparing...')
      setVideoProgressIndex(index)

      var s3 = new AWS.S3();
      var key = video_url.Key;
      debugLog('Downloading: ' + key);


      let videoId = video_url.Key.slice(video_url.Key.lastIndexOf('/') + 1)
      debugLog(videoId)

      var fileParams = {
        Bucket: BUCKET,
        Key: key
      }
      setVideoPlayer(!isVideoPlayer)
      props.setSelctedVideoUrl('/storage/emulated/0/Android/data/com.ecomenow/files/')


      s3.getObject(fileParams, async function (err, fileContents) {
        if (err) {
          callback(err);
        } else {
          var contents = await fileContents.Body.toString('base64');

          const LOCAL_PATH_TO_VIDEO = Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/${videoId}` : `${RNFS.ExternalDirectoryPath}/${videoId}`
          const LOCAL_PATH_TO_READ_VIDEO = Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}` : `${RNFS.ExternalDirectoryPath}`

          debugLog('FILE Writing: ')
          debugLog(LOCAL_PATH_TO_VIDEO)
          props.setSelctedVideoUrl(LOCAL_PATH_TO_VIDEO)
          // setVideoPlayer(!isVideoPlayer)
          props.setUpdateSponsoredVideoListURL(LOCAL_PATH_TO_VIDEO, index)
          setDownloadedVideoSize('finished')

          RNFS.writeFile(LOCAL_PATH_TO_VIDEO, contents, 'base64')
            .then(async () => {
              debugLog('FILE WRITTEN: ')
              debugLog(videoProgressIndex)
              let fileList = await RNFS.readDir(`${LOCAL_PATH_TO_READ_VIDEO}`, 'base64');
              // debugLog(fileList)

              // onSetVideo(fileList, index)
            })
            .catch(err => {
              console.log('File Write Error: ', err.message)
            })
        }
      }).on('httpDownloadProgress', function (evt) {
        calculateProgress(evt)
      })

    }
  }


  const calculateProgress = (evt) => {
    var completed = evt.loaded;
    var total = evt.total;
    var progress = Math.ceil(completed / total * 100);
    // console.log(progress);
    // setVideoProgress(progress + "%")

    // return
    console.log('progress:', evt);

    let target = evt.total
    let loaded = evt.loaded

    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (target == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(target) / Math.log(1024)));
    let size = Math.round(target / Math.pow(1024, i), 2) + ' ' + sizes[i];
    setTotalVideoSize(size)
    var j = parseInt(Math.floor(Math.log(loaded) / Math.log(1024)));
    let downloaded = Math.round(loaded / Math.pow(1024, j), 2) + ' ' + sizes[j];
    setDownloadedVideoSize(downloaded)

    if (progress == 100) {
      setDownloadedVideoSize('Saving...')
    }
  }


  const onSetVideo = (videoList) => {
    props.setSponsoredVideoList(videoList)
  }


  const downloadImageFile = () => {
    var async = require('async');
    AWS.config.update({ accessKeyId: AWS_KEY, secretAccessKey: AWS_SECRET });

    var s3 = new AWS.S3();

    var params = {
      Bucket: BUCKET,
      Prefix: PREFIX_IMAGE
    }

    s3.listObjects(params, function (err, data) {
      if (err) return debugLog(err);

      let imageList = []

      async.eachSeries(data.Contents, function (fileObj, callback) {
        var key = fileObj.Key;

        debugLog('Downloading: ' + key);


        var fileParams = {
          Bucket: BUCKET,
          Key: key
        }

        s3.getObject(fileParams, function (err, fileContents) {
          if (err) {
            callback(err);
          } else {
            var contents = fileContents.Body.toString('base64');
            imageList = [...imageList, { uri: 'data:image/jpeg;base64,' + contents }]
            callback();
          }
        });
      }, function (err) {
        if (err) {
          debugLog('Failed: ' + err);
        } else {
          debugLog('Finished');
          // debugLog(imageList)
          props.setSponsoredImage_List(imageList)
        }
      });
    });
  };


  const downloadText = () => {
    var async = require('async');
    AWS.config.update({ accessKeyId: AWS_KEY, secretAccessKey: AWS_SECRET });

    var s3 = new AWS.S3();

    var params = {
      Bucket: BUCKET,
      Prefix: PREFIX_TEXT
    }

    s3.listObjects(params, function (err, data) {
      if (err) return debugLog(err);
      let messageList = []

      async.eachSeries(data.Contents, function (fileObj, callback) {
        var key = fileObj.Key;

        debugLog('Downloading: ' + key)
        var fileParams = {
          Bucket: BUCKET,
          Key: key
        }
        s3.getObject(fileParams, async function (err, fileContents) {
          if (err) {
            callback(err);
          } else {
            debugLog('bbbbbbbbb', fileContents)
            var contents = fileContents.Body.toString('base64');
            // debugLog('encoded', contents)

            // setMessage([contents]);
            let txt = base64.decode(contents);
            messageList = [...messageList, { message: txt }]

            // const LOCAL_PATH_TO_VIDEO = Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/test.txt` : '/storage/emulated/0/EcomeText'

            // var file_path = LOCAL_PATH_TO_VIDEO + '/' + 'test.txt'


            // RNFetchBlob.fs.isDir(path).then((isDir) => {
            //   if (isDir) {
            //     RNFetchBlob.fs.createFile(file_path, txt, 'utf8').then((res)=>{
            //       debugLog('file created')
            //     })
            //   } else {
            //     RNFetchBlob.fs.mkdir(path)
            //   }
            // })
            // RNFS.writeFile(file_path, txt, 'utf8')
            //   .then((success) => {
            //     console.log('FILE WRITTEN!');
            //     debugLog(LOCAL_PATH_TO_VIDEO)
            //   })
            //   .catch((err) => {
            //     console.log(err.message);
            //   });

            // let fileList = await RNFS.readFile(file_path, 'utf8');

            // // debugLog(path)
            // debugLog('file>>>>>>>>')
            // debugLog(fileList)



            callback();
          }
        });
      }, function (err) {
        if (err) {
          debugLog('Failed: ' + err);
        } else {
          debugLog('Finished');
          props.setSponsoredMessage_List(messageList)

        }
      });
    });
  };



  useEffect(() => {
    downloadVideo()
    downloadText()
    downloadImageFile()

    // props.setSponsoredVideoList(props.sponsoredVideoList)
    // props.setSponsoredImage_List(props.sponsoredImageList)
  }, [])






  const renderItemVideo = (dataItem) => {
    // debugLog(dataItem.item.path)
    return (

      <TouchableOpacity
        onLongPress={() => onCheckVideo(dataItem.index, dataItem.item)}
        onPress={() => onPlayVideo(dataItem.item, dataItem.index)}

        style={[msStyle.myshadow, {
          height: PixelRatio.getPixelSizeForLayoutSize(63),
          width: PixelRatio.getPixelSizeForLayoutSize(63),
          borderRadius: 5,
          marginHorizontal: 10,
          marginVertical: 20,
          backgroundColor: '#262626',
          justifyContent: 'center',
          // alignItems: 'center',
          marginLeft: dataItem.index == 0 ? 20 : 0

        }]}

      >

        <ImageBackground
          imageStyle={{ borderRadius: 6 }}
          style={[{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',

          }]}
          source={dataItem.item.path != undefined ? require('../../assets/thumb.png') : require('../../assets/thumb1.png')}
        >
          <Image
            source={require('../../assets/play.png')}
            style={{ height: 35, width: 35, resizeMode: 'contain' }} />

          {
            dataItem.item.selected ?
              <TouchableOpacity
                onPress={() => { onCheckVideo(dataItem.index, 'uncheck') }}
                style={{ borderRadius: 30, elevation: 1, padding: 1, backgroundColor: 'white', position: 'absolute', right: dataItem.index == 1 ? 45 : 10, bottom: 10 }}  >
                <TouchableOpacity
                  onPress={() => { onCheckVideo(dataItem.index, 'uncheck') }}
                  style={{ borderRadius: 30, backgroundColor: 'white' }} >
                  <Image
                    source={require('../../assets/check.png')}
                    style={{ height: 30, width: 30 }}
                  />
                </TouchableOpacity>
              </TouchableOpacity>
              : null
          }

          {
            dataItem.index == 1 ?
              <TouchableOpacity style={{ position: 'absolute', right: 0, borderRadius: 5, backgroundColor: '#3c3c3c', height: '100%', width: 40, justifyContent: 'center', alignItems: 'center' }} >
                <Image
                  source={require('../../assets/drawer/navigate.png')}
                  style={{ width: 20, height: 20, resizeMode: 'contain', }}
                />
              </TouchableOpacity>
              : null
          }
          {
            downloadedVideoSize == 'finished' ?
              null : videoProgressIndex == dataItem.index ?
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                  <ActivityIndicator
                    color='white'
                  />
                  <Text style={[msStyle.txtStyle, { fontSize: 12, color: 'white' }]} >
                    {downloadedVideoSize == 'Saving...' ?
                      downloadedVideoSize :
                      downloadedVideoSize == 'Preparing...' ?
                        downloadedVideoSize :
                        downloadedVideoSize + "/" + totalVideoSize}</Text>
                </View>
                : null
          }
        </ImageBackground>
      </TouchableOpacity>
    )
  }


  const onCheckImage = (index) => {
    props.setUpdateSponsoredImage_List(index)

  }
  const renderItemImage = (dataItem) => {
    return (
      <TouchableOpacity
        onLongPress={() => onCheckImage(dataItem.index)}
        style={[msStyle.myshadow, {
          height: PixelRatio.getPixelSizeForLayoutSize(63),
          width: PixelRatio.getPixelSizeForLayoutSize(63),
          borderRadius: 5,
          marginHorizontal: 10,
          marginTop: 20,
          marginBottom: 30,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: dataItem.index == 0 ? 20 : 0

        }]} >
        <ImageBackground
          source={{ uri: dataItem.item.uri }}
          borderRadius={5}
          style={{ width: '98%', height: "98%", margin: 5, resizeMode: 'contain', flexDirection: 'row', justifyContent: 'flex-end' }} >


          {
            dataItem.item.selected ?
              <TouchableOpacity
                onPress={() => { onCheckImage(dataItem.index) }}
                style={{ borderRadius: 30, elevation: 1, padding: 1, backgroundColor: 'white', position: 'absolute', right: dataItem.index == 1 ? 45 : 10, bottom: 10 }}  >
                <TouchableOpacity
                  onPress={() => { onCheckImage(dataItem.index) }}
                  style={{ borderRadius: 30, backgroundColor: 'white' }} >
                  <Image
                    source={require('../../assets/check.png')}
                    style={{ height: 30, width: 30 }}
                  />
                </TouchableOpacity>
              </TouchableOpacity>
              : null
          }


          {
            dataItem.index == 1 ?
              <View style={{ backgroundColor: '#eaeaea', height: '100%', borderRadius: 4, width: 40, justifyContent: 'center', alignItems: 'center' }} >
                <Image
                  source={require('../../assets/drawer/navigate.png')}
                  style={{ width: 20, height: 20, resizeMode: 'contain', }}
                />
              </View>
              : null
          }

        </ImageBackground>
      </TouchableOpacity>
    )
  }


  const onRecommendation = async () => {

    let selectedVideo = props.setSelectedSponsoredVideoList()
    let selectedImage = props.setSelectedSponsoredImage_List()
    let selectedMessage = props.setSelectedSponsoredMessage_List()

    debugLog(selectedVideo)

    let image = []
    let video = ''
    let message = ''

    selectedImage.map((item, index) => {
      image[index] = item.uri
    })

    selectedVideo.map((item, index) => {
      video = video + "\n\n" + item.path
    })

    selectedMessage.map((item, index) => {
      message = message + "\n\n" + item.message
    })


    if (image.length == 0 && video.length == 0 && message.length == 0) {
      alert('Please select Image, Video or message by long press!')
      return
    }

    if (image.length > 0 && video.length > 0 && message.length > 0) {
      alert('Please select one media type at a time.')
      return
    }

    if (image.length > 0 && video.length > 0 && message.length == 0) {
      alert('Sorry you can not share image and video at a time.')
      return
    }

    if (image.length == 0 && video.length > 0 && message.length > 0) {
      alert('Sorry you can not share Message and video at a time.')
      return
    }

    if (image.length > 0 && video.length == 0 && message.length > 0) {
      alert('Sorry you can not share image and message at a time.')
      return
    }

    if (image.length > 0 && video.length == 0 && message.length == 0) {

      let options = {
        title: 'Share Title',
        message: 'Image Message\n',
        urls: image,
        type: 'image/jpeg',
      };

      debugLog(options)
      Share.open(options)
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          err && console.log(err);
        });
    }


    if (image.length == 0 && video.length > 0 && message.length == 0) {

      let options = {
        title: 'eComNow',
        message: 'Video Message\n',
        url: video,
        type: 'video/mp4',
      };

      debugLog(options)
      Share.open(options)
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          err && console.log(err);
        });
    }


    if (image.length == 0 && video.length == 0 && message.length > 0) {

      let options = {
        title: 'eComNow',
        message: 'Message',
        url: message,
        type: 'plain/text',
      };

      debugLog(options)
      // return
      Share.open(options)
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          err && console.log(err);
        });
    }


    try {
      // const result = await Share.share({
      //   message: 'eComNow\n' + image + "\n\n" + video + "\n\n" + message,
      //   url: image,
      //   title: `eComNow`
      // });




      return

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const onGoBack = () => {
    props.navigation.goBack()
  }

  const onMessage = () => {
    setMessagePopUp(!isMessagePopUp)
  }

  const onOk = () => {
    props.setSelectedSponsoredMessage_List()
    setMessagePopUp(!isMessagePopUp)

  }

  const onSelectMessage = (index) => {

    props.setUpdateSponsoredMessage_List(index)
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={[styles.container,]} >


        <View style={{ height: 40, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }} >
          <TouchableOpacity
            onPress={() => { onGoBack() }}
            style={{ position: 'absolute', left: 10 }} >
            <Image
              source={require('../../assets/back.png')}
              style={{ height: 25, width: 25, tintColor: 'gray', resizeMode: 'contain' }} />
          </TouchableOpacity>
          <Text style={[msStyle.txtStyle, { fontSize: 18 }]}>{'Sponsored Marketing'}</Text>
        </View>



        <View style={{}} >
          <Text style={[msStyle.txtStyle, { fontSize: 18, marginLeft: 20 }]} >{"Videos"}</Text>

          {props.sponsoredVideoList.length == 0 ? <IsEmptyList /> : null}

          <FlatList
            data={props.sponsoredVideoList}
            renderItem={renderItemVideo}
            keyExtractor={(item, index) => 'key' + index}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>

        <View style={{}} >
          <Text style={[msStyle.txtStyle, { fontSize: 18, marginLeft: 20 }]}>{"Images"}</Text>

          {props.sponsoredImageList.length == 0 ? <IsEmptyList /> : null}

          <FlatList
            data={props.sponsoredImageList}
            renderItem={renderItemImage}
            keyExtractor={(item, index) => 'key' + index}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>


        <View style={{ marginHorizontal: 20, }} >
          <Text style={[msStyle.txtStyle, { fontSize: 18 }]}>{"Message"}</Text>
          <TouchableOpacity
            onPress={() => onMessage()}
            style={{ borderWidth: 1, paddingVertical: 10, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, marginVertical: 20, borderRadius: 5 }} >
            <Image
              source={require('../../assets/downArrow.png')}
              style={{ height: 20, width: 20, resizeMode: 'contain' }} />

            <Text style={[msStyle.txtStyle, { fontSize: 14, paddingLeft: 10 }]}>{props.sponsoredSelectedMessageList.length > 0 ?
              props.sponsoredSelectedMessageList.length + " Message selected" : 'Select message'}</Text>

            {
              props.sponsoredMessageList.length == 0 ?
                <ActivityIndicator color={Colors.themeColor} size='small' />
                : null
            }
          </TouchableOpacity>
        </View>


        {/* <TouchableOpacity
          style={{ paddingVertical: 10, flexDirection: 'row', marginLeft: 20, marginBottom: 20 }} >
          <Image
            source={require('../../assets/link.png')}
            style={{ height: 25, width: 25, resizeMode: 'contain' }}
          />
          <Text style={{ fontSize: 16, marginLeft: 10, paddingHorizontal: 20 }} >
            {'Attatch Product'}
          </Text>
        </TouchableOpacity> */}



        <TouchableOpacity
          onPress={() => { onRecommendation() }}
          style={{ marginHorizontal: 30, borderRadius: 8, marginBottom: 50, paddingVertical: 12, backgroundColor: Colors.themeColor, alignItems: 'center', justifyContent: 'center' }} >
          <Text style={[msStyle.txtStyle, { color: 'white', fontSize: 16 }]}>{"RECOMMEND"}</Text>
        </TouchableOpacity>

        {
          isMessagePopUp ?
            <SponsoredMessageListPopUp
              data={props.sponsoredMessageList}
              onSelectMessage={onSelectMessage}
              onOk={() => { onOk() }}
            />
            : null
        }

        {isVideoPlayer ?
          <VideoPlayerComponent
            downloadedVideoSize={downloadedVideoSize}
            totalVideoSize={totalVideoSize}
            url={props.selectedVideoUrl}
            // url={videoUrl}
            onGoBack={() => setVideoPlayer(!isVideoPlayer)}
          />
          : null
        }

      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});


const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_loading: common.is_loading,
    sponsoredVideoList: common.sponsoredVideoList,
    sponsoredImageList: common.sponsoredImageList,
    sponsoredSelectedImageList: common.sponsoredSelectedImageList,
    sponsoredSelectedVideoList: common.sponsoredSelectedVideoList,
    sponsoredMessageList: common.sponsoredMessageList,
    sponsoredSelectedMessageList: common.sponsoredSelectedMessageList,


    selectedVideoUrl: jaswant.selectedVideoUrl,


  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({

    setSelectedSponsoredVideoList: (index) => setSelectedSponsoredVideoList(index),
    setUpdateSelectAllSponsoredVideoList: (index) => setUpdateSelectAllSponsoredVideoList(index),
    setUpdateSponsoredVideoList: (index) => setUpdateSponsoredVideoList(index),
    setSponsoredVideoList: (list) => setSponsoredVideoList(list),
    setUpdateSponsoredVideoListURL: (path, index) => setUpdateSponsoredVideoListURL(path, index),

    setSelectedSponsoredImage_List: (index) => setSelectedSponsoredImage_List(index),
    setUpdateSelectAllSponsoredImage_List: (index) => setUpdateSelectAllSponsoredImage_List(index),
    setUpdateSponsoredImage_List: (index) => setUpdateSponsoredImage_List(index),
    setSponsoredImage_List: (index) => setSponsoredImage_List(index),

    setSelectedSponsoredMessage_List: (index) => setSelectedSponsoredMessage_List(index),
    setUpdateSelectAllSponsoredMessage_List: (index) => setUpdateSelectAllSponsoredMessage_List(index),
    setUpdateSponsoredMessage_List: (index) => setUpdateSponsoredMessage_List(index),
    setSponsoredMessage_List: (index) => setSponsoredMessage_List(index),

    setSelctedVideoUrl: (url) => setSelctedVideoUrl(url),

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Testing)

