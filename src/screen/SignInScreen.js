import React, { Component } from 'react';
import { Button, View, Text, ActivityIndicator, Dimensions, BackHandler, Alert, SafeAreaView, ScrollView, Keyboard, TouchableOpacity, Image } from 'react-native';
import WebView from 'react-native-webview';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import Constants, { debugLog } from '../common/Constants';
import { msStyle } from '../common/MyStyle';
import { getPrefs, setPrefs } from '../common/Prefs';
import { setLoading, setLoginRespons, setRefereshGetProfile } from '../redux_store/actions/indexActions';
import { hitSocialLogin } from '../redux_store/actions/indexActionsApi';
import { setForgotPassMessage, setIsLoginFailed, setLoginFailedMessage } from '../redux_store/actions/indexActionsJaswant';

class SignInScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loading: false,
      isResponse: null,
      isError: null,
      loginResponse: null,
      isHeader: false,
    }
  }



  onValidCredintial = () => {


    if (this.state.loginResponse == null) {
      this.onLoginFailed(
        {
          btnTitle: 'Try again', bgColor: '#fc4103',
          title: 'Login failed!',
          msg: 'Ooops Something went wrong!'
        })
      return
    }



    if (this.state.loginResponse.cbp == null ||
      this.state.loginResponse.domainName == null ||
      this.state.loginResponse.uniqueId == null ||
      this.state.loginResponse.platformId == null ||
      this.state.loginResponse.storeNo == null
    ) {
      this.onLoginFailed(
        {
          btnTitle: 'Try again', bgColor: '#fc0335',
          title: 'Login failed!',
          msg: 'Ooops something went wrong in login response please try again!'
        })
      return
    }


    setPrefs("ACCESS_TOKEN", JSON.stringify(this.state.loginResponse))
    this.props.setLoginRespons(this.state.loginResponse)

    if (this.state.loginResponse.domainName.toLowerCase() != this.props.domain.toLowerCase()) {
      this.showAlert1('Please enter valid platform')
      return
    }


    if (this.state.loginResponse.userEmail != this.props.email) {
      this.showAlert1('Please enter valid email')
      return
    }

    if (this.state.loginResponse.platformId.toLowerCase() != this.props.platform.toLowerCase()) {
      this.showAlert1('Please enter valid platform')
      return
    }

    this.setState({ isResponse: false })
    if (this.props.refereshGetProfile == 1) {
      this.props.setRefereshGetProfile(2)
    }

    this.props.navigation.navigate('ProfileScreen1')
    return
  }

  renderLoadingView() {
    return (
      <View style={{
        borderRadius: 10,
        backgroundColor: "white",
        flex: 1,

      }}>

        <ActivityIndicator
          size={"large"}
          color={Colors.themeColor}
          hidesWhenStopped={true}
        />

      </View>
    );
  }

  showAlert1(msg) {
    Alert.alert(
      'Login Error',
      msg,
      [
        {
          text: 'Go Back',
          onPress: () => {
            this.onOpenOneTimeScreen()
          }
        },
      ]
    );
  }

  onOpenOneTimeScreen = () => {
    this.props.navigation.navigate('OneTimeScreen')
  }

  onLoginFailed = (msg) => {
    this.props.setIsLoginFailed(true)
    this.props.setLoginFailedMessage(msg)
    this.props.navigation.navigate('OneTimeScreen')
  }

  handleWebViewNavigationStateChange = newNavState => {

    debugLog(newNavState)
    debugLog('After Log in')

    this.setState({ isLoading: true })
    const { url } = newNavState;
    if (!url) return;

    if (url.includes('https://login.salesforce')) {
      this.setState({ isHeader: true })
      this.props.setLoginFailedMessage({
        btnTitle: 'Try again', bgColor: '#5bc0de', title: 'Login with Salesforce', msg: 'You canceled Salesforce login. \nPlease try again!'
      })
      return
    }

    if (
      url.includes('https://appleid.apple')) {
      this.setState({ isHeader: true })
      this.props.setLoginFailedMessage({
        btnTitle: 'Try again', bgColor: '#000000', title: 'Login with Apple', msg: 'You canceled Apple login. \nPlease try again!'
      })
      return
    }

    if (
      url.includes('https://accounts.google')) {
      this.setState({ isHeader: true })
      this.props.setLoginFailedMessage({
        btnTitle: 'Try again', bgColor: '#4285f4', title: 'Login with Google', msg: 'You canceled Google login. \nPlease try again!'
      })
      return
    }

    if (url.includes('https://www.facebook')) {
      this.setState({ isHeader: true })
      this.props.setLoginFailedMessage({
        btnTitle: 'Try again', bgColor: '#4267b2', title: 'Login with Facebook', msg: 'You canceled Facebook login. \nPlease try again!'
      })
      return
    }

    
    if (url.includes('https://ecom-sso.auth.us-east-1.amazoncognito.com/error')) {
      this.setState({ isHeader: true })
      this.props.setLoginFailedMessage({
        btnTitle: 'Try again', bgColor: '#4267b2', title: 'Login with Facebook', 
        msg: 'An error was encountered with the required page. \nPlease try again!'
      })
      return
    }

    if (url.includes('forgotPassword')) {
      this.setState({ isHeader: true })
      this.props.setLoginFailedMessage({
        btnTitle: 'Try again', bgColor: '#f55a00', title: 'Forgot Passowrd!', msg: 'Please click on Try again button to Signin or change password!'
      })
      return
    }

    if (url.includes('confirmForgotPassword')) {
      this.props.setForgotPassMessage('confirmForgotPassword')
      this.setState({ isHeader: true })
      this.props.setLoginFailedMessage({
        btnTitle: 'Try again', bgColor: '#f55a00', title: 'Forgot Passowrd!', msg: 'Please click on Try again button to Signin or change password!'
      })
      return
    }

    // if (url.includes('https://ecom-sso.auth.us-east-1.amazoncognito.com/login?') && this.props.forgotPassMessageUrl == 'confirmForgotPassword') {
    //   this.setState({ isHeader: false })
    //   this.props.setLoginFailedMessage({
    //     btnTitle: 'Login', bgColor: '#42ba96', title: 'Forgot Passowrd!',
    //     msg: 'Password Updated successfully!'
    //   })
    //   this.onGoBack()
    //   return
    // }

    if (url.includes('changePassword')) {
      this.setState({ isHeader: true })
      this.props.setLoginFailedMessage({
        btnTitle: 'Try again', bgColor: '#f55a00', title: 'Forgot Passowrd!', msg: 'Please click on Try again button to Signin or change password!'
      })
      return
    }

    // https://ecom-sso.auth.us-east-1.amazoncognito.com/login?

    console.log('urlllll>>>>>', url)
    if (url.includes('/sso/error') || url.includes('test-sso/error/')) {

      this.onLoginFailed({
        error: '',
        btnTitle: 'Try again', bgColor: '#fc4103',
        title: 'Login failed!',
        msg: 'Please contact your Support adminstrator or Submit Contact E-mail as you currently do not have a user Credentials to access the system!'
      })
      return

    }

    if (url.includes("mobile-login-success?authUser=")) {

      const authUserId = url.substr(url.indexOf('=') + 1)

      var apiUrl = `${Constants.AUTH_API_BASE_URL}/test-sso/get-mobile-user?authUser=${authUserId}`
      debugLog(apiUrl)

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
      };

      this.setState({ is_loading: true })
      fetch(apiUrl, requestOptions).then(async response => {
        try {
          let res = await response.json();
          this.setState({ loginResponse: res })
          this.onValidCredintial()

        } catch (error) {
          debugLog(error)
        }
      }).catch((error) => {
        debugLog(error)
      })
    }
  };


  onGoBack = () => {
    this.props.setIsLoginFailed(true)
    this.setState({ isHeader: false })
    this.props.navigation.navigate('OneTimeScreen')
  }

  render() {

    const jsCode = "window.waitForBridge = function(fn) { return (window.postMessage.length === 1) ? fn() : setTimeout(function() { window.waitForBridge(fn) }, 5) }; window.waitForBridge(function() { window.postMessage(document.body.innerText });"

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }} >

          {this.state.isHeader ?
            <View style={{ height: 40, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }} >
              <TouchableOpacity
                onPress={() => this.onGoBack()}
                style={{ position: 'absolute', left: 10 }} >
                <Image
                  source={require('../../assets/back.png')}
                  style={{ height: 25, width: 25, tintColor: '#141212', resizeMode: 'contain' }}
                />
              </TouchableOpacity>
              <Text style={[msStyle.txtStyle, { fontSize: 22, color: '#141212' }]} >{'Sign in'}</Text>
            </View> : null
          }

          <WebView
            incognito={true}
            userAgent="Mozilla/5.0 Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"
            source={{ uri: Constants.AUTH_API_BASE_URL + "/test-sso/login?state=login->true,domain->" + this.props.domain + ",platform->" + this.props.platform + ",source->mobile" }}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            originWhitelist={['*']}
            renderLoading={this.renderLoadingView}
            onNavigationStateChange={this.handleWebViewNavigationStateChange}
            startInLoadingState={true}
            onMessage={event => console.log('Received: ', event.nativeEvent.data)}
            injectedJavaScript={jsCode}
            javaScriptEnabledAndroid={true}
          />

          {
            this.state.is_loading ?
              <View style={{
                position: 'absolute',
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height,
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center'
              }} >
                <ActivityIndicator
                  size={"large"}
                  color={Colors.themeColor}
                />
                <Text style={msStyle.txtStyle} >{"Please wait for a while..."}</Text>
              </View>
              : null
          }
        </View>
      </SafeAreaView>
    )
  }


}



const mapStateToProps = (state) => {
  let common = state.indexReducer;
  let jaswant = state.indexReducerJaswant;

  return {
    is_loading: common.is_loading,
    domain: common.domain,
    email: common.email,
    platform: common.platform,
    is_drawer: common.is_drawer,
    refereshGetProfile: common.refereshGetProfile,
    loginFailedMessage: jaswant.loginFailedMessage,
    forgotPassMessageUrl: jaswant.forgotPassMessageUrl,

  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    hitSocialLogin: (domain) => hitSocialLogin(domain),
    setLoading: (isLoading) => setLoading(isLoading),
    setLoginRespons: (res) => setLoginRespons(res),
    setRefereshGetProfile: (referesh) => setRefereshGetProfile(referesh),
    setIsLoginFailed: (isTrue) => setIsLoginFailed(isTrue),
    setLoginFailedMessage: (msg) => setLoginFailedMessage(msg),
    setForgotPassMessage: (msg) => setForgotPassMessage(msg)

  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen)

