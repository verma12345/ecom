import React, { Component } from 'react';

import { Button, StatusBar, StyleSheet, View } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from './src/screen/SplashScreen';
import OneTimeScreen from './src/screen/OneTimeScreen';
import SignInScreen from './src/screen/SignInScreen';
import RecommendationScreen from './src/screen/RecommendationScreen';
import RecommendationHistoryScreen from './src/screen/RecommendationHistoryScreen.js';
import RecommendationDetailScreen from './src/screen/RecommendationDetailScreen.js';
import store from './src/redux_store/store';
import { connect, Provider } from 'react-redux';
import Colors from './src/common/Colors';
import DrawerContent from './src/screen/DrawerContent';
import { bindActionCreators } from 'redux';
import CertificatesComponent from './src/screen/CertificatesComponent';
import ProfileScreen1 from './src/screen/ProfileScreen1';
import QuillEditorComponent from './src/components/QuillEditorComponent';
import QuillEditorComponent1 from './src/components/QuillEditorComponent1';
import Testing from './src/screen/Testing';
import DashboardScreen from './src/screen/DashboardScreen';
import SponsoredMarketingScreen from './src/screen/SponsoredMarketingScreen';
import WesternUnionScreen from './src/screen/WesternUnionScreen';
import ProductScreen from './src/screen/ProductScreen';
import CustomerReportScreen from './src/screen/CustomerReportScreen';
import MyMarketingScreen from './src/screen/MyMarketingScreen';
import ContactUsScreen from './src/screen/ContactUsScreen';
import HelpScreen from './src/screen/HelpScreen';
import WesternUnionCreateAccount from './src/screen/WesternUnionCreateAccount';
import VideoPlayerComponent from './src/components/VideoPlayerComponent';
import ProductDetailScreen from './src/screen/ProductDetailScreen';
import MessageScreen from './src/screen/MessageScreen';
import MessageHistoryScreen from './src/screen/MessageHistoryScreen';
import MessageDetailScreen from './src/screen/MessageDetailScreen';
import QuillEditorComponent1More from './src/components/QuillEditorComponent1More';
import QuillEditorComponent2More from './src/components/QuillEditorComponent2More ';
import ProductListScreen from './src/screen/ProductListScreen';


const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const Root = () => {
  return (
    <Stack.Navigator
      screenOptions={{ animationEnabled: false, headerShown: false, gestureEnabled: false }} >
      <Stack.Screen name='SplashScreen' component={SplashScreen} screenOptions={{ gestureEnabled: false }} />
      <Stack.Screen name='SignInScreen' component={SignInScreen} screenOptions={{ gestureEnabled: false }} />
      <Stack.Screen name='OneTimeScreen' component={OneTimeScreen} screenOptions={{ gestureEnabled: true }} />

    </Stack.Navigator>
  );
}


class App extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.themeColor}
            barStyle={"light-content"}
          />
          <NavigationContainer>
            <Drawer.Navigator
              drawerContent={(props) => <DrawerContent {...props} />}
              screenOptions={{ animationEnabled: false, headerShown: false }}
            >

              <Drawer.Screen name="Root" component={Root} options={{ gestureEnabled: false }} />

              <Drawer.Screen name='ProfileScreen1' component={ProfileScreen1} />

              <Drawer.Screen name='CertificatesComponent' component={CertificatesComponent} />
              <Drawer.Screen name='QuillEditorComponent' component={QuillEditorComponent} />
              <Drawer.Screen name='QuillEditorComponent1' component={QuillEditorComponent1} />
              <Drawer.Screen name='QuillEditorComponent1More' component={QuillEditorComponent1More} />
              <Drawer.Screen name='QuillEditorComponent2More' component={QuillEditorComponent2More} />


              <Drawer.Screen name='Testing' component={Testing} />

              <Drawer.Screen name='RecommendationScreen' component={RecommendationScreen} />
              <Drawer.Screen name='RecommendationHistoryScreen' component={RecommendationHistoryScreen} />
              <Drawer.Screen name='RecommendationDetailScreen' component={RecommendationDetailScreen} />
              <Stack.Screen name='ProductScreen' component={ProductScreen} />
              <Stack.Screen name='ProductDetailScreen' component={ProductDetailScreen} />
              <Stack.Screen name='MessageScreen' component={MessageScreen} />
              <Stack.Screen name='MessageHistoryScreen' component={MessageHistoryScreen} />
              <Stack.Screen name='MessageDetailScreen' component={MessageDetailScreen} />

              <Drawer.Screen name='DashboardScreen' component={DashboardScreen} />
              <Drawer.Screen name='SponsoredMarketingScreen' component={SponsoredMarketingScreen} />
              <Drawer.Screen name='WesternUnionScreen' component={WesternUnionScreen} />

              <Drawer.Screen name='CustomerReportScreen' component={CustomerReportScreen} />
              <Drawer.Screen name='MyMarketingScreen' component={MyMarketingScreen} />
              <Drawer.Screen name='ContactUsScreen' component={ContactUsScreen} />
              <Drawer.Screen name='HelpScreen' component={HelpScreen} />
              <Drawer.Screen name="WesternUnionCreateAccount" component={WesternUnionCreateAccount} />

              <Drawer.Screen name="VideoPlayerComponent" component={VideoPlayerComponent} />

              <Drawer.Screen name='ProductListScreen' component={ProductListScreen} />

            </Drawer.Navigator>
          </NavigationContainer>
        </View>
      </Provider>
    );
  }
}
// kamalrastogi@123
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default App;

// git commit -m "first commit"
// git remote add origin https://github.com/kamalrastogi/EcomNow.git
// git push -u origin main
